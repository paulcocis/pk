<div class="container fit-on-pc fit-on-tablet fit-on-mobile m-25px-pc-b">
<!-- Row -->
<div class="row ptf-container">

<!-- Page Title -->
<div class="ptf-title">Terms of Use - <span class="txt-pink">pinkEpromise</span></div>
<!-- /Page Title -->

<!-- Pivacy Data Container -->
<div class="panel bg-white-pink">

<!-- Privacy Body -->
<div class="ptf-body">

<!-- Privacy Group -->
<div class="panel-group">

<!-- General Questions -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseOne">1.Acceptance of terms of use and amendments</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseOne" class="panel-collapse collapse in">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; Each time you use or cause access to this website, you agree to be bound by these Terms of Use, as amended from time to time with or without notice to you. 
In addition, if you are using a particular service on this website or accessed via this website, you will be subject to any rules or guidelines applicable to those services,
and they will be incorporated by reference within these Terms of Use. 
<br /><br />
&bull; Please read the site's <a href="privacy-policy.html" class="link-pink" target="_blank">privacy policy</a>, which is incorporated within these Terms of Use by reference.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>             
<!-- /Acceptance of terms of use and amendments -->

<!-- pinkEpromise's service -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwo">2.pinkEpromise's service</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container  -->
<div id="collapseTwo" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; This website and the services provided to you on and via this website are provided on an "AS IS" basis. 
<br /><br />
&bull; You agree that pinkEpromise reserves the right to modify or discontinue provision of this website and its services, and to remove the data you provide, either
 temporarily or permanently, at any time, without notice and without any liability towards you, pinkEpromise will not be held responsible or liable for timeliness,
 removal of information, failure to store information, inaccuracy of information, or improper delivery of information.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>           
<!-- /pinkEpromise's service -->

<!-- Your responsibilities and registration obligations -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThree">3.Your responsibilities and registration obligations</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseThree" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; In order to use this website or certain parts of it, you may be required to register for a user account on this website; in this case, you agree to provide
truthful information when requested. By registering for a user account, you explicitly agree to this site's 
Terms of Use, including any amendments made by pinkEpromise that are published herein.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /Your responsibilities and registration obligations -->

<!-- Privacy policy -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFour">4.Privacy policy</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFour" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; Registration data and other personally identifiable information that the site may collect is subject to the terms of our <a href="privacy-policy.html" class="link-pink" target="_blank">privacy policy</a>.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>                  
<!-- /Privacy policy -->

<!-- Registration and password -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFive">5.Registration and password</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFive" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; You are responsible for maintaining the confidentiality of your password, and you will be responsible for all usage of your user account and/or user name, whether authorized or not authorized by you. 
<br /><br />
&bull; You agree to immediately notify pinkEpromise of any unauthorized use of your user account, user name or password.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>             
<!-- /Registration and password -->


<!-- Refund Policy -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSix">6.Refund Policy</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container  -->
<div id="collapseSix" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
If you are unhappy with your purchase within 14 days of receipt, you can return it for a refund. <br> <br>Please send the item to:
<br>
pinkEpromise<br>
318 S Norwood Ave<br>
Newtown, PA 19840<br>

... and we will process your refund.   <br><br>Be sure to include your name and order confirmation number for a speedy refund.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>              
<!-- /Refund Policy -->

<!-- Your conduct -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSeven">7.Your conduct</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseSeven" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; You agree that all information or data of any kind, whether text, software, code, music or sound, photographs or graphics, video or other materials ("content"),
made available publicly or privately, will be under the sole responsibility of the person providing the said content, or of the person whose user account is used.
pinkEpromise will not be responsible to you in any way for content displayed on this website, nor for any error or omission.
<br /><br />
&bull; By using this website or any service provided, you explicitly agree that:
<br /><br />
1.you will not provide any content or conduct yourself in any way that may be construed as: unlawful; illegal; threatening; harmful; abusive; harassing; stalking;
tortious; defamatory; libelous; vulgar; obscene; offensive; objectionable; pornographic; designed to interfere with or disrupt the operation of this website or any
service provided; infected with a virus or other destructive or deleterious programming routine; giving rise to civil or criminal liability; or in violation of
an applicable local, national or international law;
<br /><br />
2.you will not impersonate or misrepresent your association with any person or entity; you will not forge or otherwise seek to conceal or misrepresent the origin of any content provided by you;
<br /><br />
3.you will not collect or harvest any information about other users;
<br /><br />
4.you will not provide, and you will not use this website to provide, any content or service in any commercial manner, or in any manner that would involve junk mail, spam,
chain letters, pyramid schemes, or any other form of unauthorized advertising or commerce; you will not use this website to promote or operate any service or content without our prior written consent;
<br /><br />
5.you will not provide any content that may give rise to pinkEpromise being held civilly or criminally liable, or that may be considered a violation of any local, national or international
law, including -- but not limited to -- laws relating to copyrights, trademarks, patents, or trade secrets.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>             
<!-- /Your conduct -->

<!-- Submission of content on this website -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseEight">8.Submission of content on this website</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseEight" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; By providing any content to this website
<br /><br />
1.you agree to grant pinkEpromise a worldwide, royalty-free, perpetual, non-exclusive right and license (including any moral rights or other necessary rights.) to use, display,
reproduce, modify, adapt, publish, distribute, perform, promote, archive, translate, and to create derivative works and compilations, in whole or in part. Such license will apply
with respect to any form, media, and technology already known at the time of provision or developed subsequently;
<br /><br />
2.you warrant and represent that you have all legal, moral, and other rights that may be necessary to grant pinkEpromise the license specified in this section;
<br /><br />
3.you acknowledge and agree that pinkEpromise will have the right (but not obligation), at our entire discretion, to refuse to publish, or to remove, or to block access to any
content you provide, at any time and for any reason, with or without notice.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>          
<!-- /Submission of content on this website -->

<!-- Third-party services -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseNine">9.Third-party services</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseNine" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; Goods and services of third parties may be advertised and/or may be made available on or through this website. Representations made regarding products and services
provided by third parties will be governed by the policies and representations made by these third parties. pinkEpromise will not in any manner be liable for or responsible
for any of your dealings or interaction with third parties.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>           
<!-- /Third-party services -->

<!-- Indemnification -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTen">10.Indemnification</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseTen" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; You agree to indemnify and hold harmless pinkEpromise and pinkEpromise's representatives, subsidiaries, affiliates, related parties, officers, directors,
employees, agents, independent contractors, advertisers, partners, and co-branders, from any claim or demand, including reasonable legal fees, that may be filed by
any third party, arising out of your conduct or connection with this website or service, your provision of content, your violation of these Terms of Use, or any
other violation by you of the rights of another person or party.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /Indemnification -->

<!-- DISCLAIMER OF WARRANTIES -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseEleven">11.Disclaimer of Warranties</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseEleven" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; YOU UNDERSTAND AND AGREE THAT YOUR USE OF THIS WEBSITE AND OF ANY SERVICES OR CONTENT PROVIDED (THE "SERVICE") IS AT YOUR OWN RISK. SERVICES AND CONTENT ARE PROVIDED
TO YOU "AS IS", AND pinkEpromise EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EITHER IMPLIED OR EXPRESS, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
<br /><br />
&bull; pinkEpromise MAKES NO WARRANTY, EITHER IMPLIED OR EXPRESS, THAT ANY PART OF THE SERVICE WILL BE UNINTERRUPTED, ERROR-FREE, VIRUS-FREE, TIMELY, SECURE, ACCURATE, 
RELIABLE, OR OF ANY QUALITY, NOR IS IT WARRANTED EITHER IMPLICITLY OR EXPRESSLY THAT ANY CONTENT IS SAFE IN ANY MANNER FOR DOWNLOAD. YOU UNDERSTAND AND AGREE THAT NEITHER
pinkEpromise NOR ANY PARTICIPANT IN THE SERVICE PROVIDES PROFESSIONAL ADVICE OF ANY KIND AND THAT ANY ADVICE OR ANY OTHER INFORMATION OBTAINED VIA THIS WEBSITE MAY BE USED
SOLELY AT YOUR OWN RISK, AND THAT pinkEpromise WILL NOT BE HELD LIABLE IN ANY WAY.
<br /><br />
&bull; Some jurisdictions may not allow disclaimers of implied warranties, and certain statements in the above disclaimer may not apply to you as regarding implied warranties;
the other terms and conditions remain enforceable notwithstanding.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container  -->
</div>        
<!-- /DISCLAIMER OF WARRANTIES -->

<!-- LIMITATION OF LIABILITY -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwelve">12.Limitation of Liability</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseTwelve" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; YOU EXPRESSLY UNDERSTAND AND AGREE THAT THE SITE EDITOR WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES; THIS INCLUDES,
BUT IS NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF pinkEpromise HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES),
RESULTING FROM (I) THE USE OF SERVICES OR THE INABILITY TO USE SERVICES, (II) THE COST OF OBTAINING SUBSTITUTE GOODS AND/OR SERVICES RESULTING FROM ANY TRANSACTION ENTERED INTO
ON THROUGH SERVICES, (III) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR DATA TRANSMISSIONS, (IV) STATEMENTS BY ANY THIRD PARTY OR CONDUCT OF ANY THIRD PARTY USING SERVICES,
OR (V) ANY OTHER MATTER RELATING TO SERVICES.
<br /><br />
&bull; In some jurisdictions, it is not permitted to limit liability and, therefore, such limitations may not apply to you.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>        
<!-- /LIMITATION OF LIABILITY -->

<!-- Reservation of rights -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThirteen">13.Reservation of rights</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseThirteen" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; pinkEpromise reserves all of pinkEpromise's rights, including but not limited to any and all copyrights, trademarks, patents, trade secrets, and any other proprietary
right that pinkEpromise may have in respect of this website, its content, and goods and services that may be provided. 
<br /><br />
&bull; The use of pinkEpromise's rights and property requires pinkEpromise's prior written consent. By making services available to you, pinkEpromise is not providing you
with any implied or express licenses or rights, and you will have no rights to make any commercial use of this website or provided services without pinkEpromise's prior written consent.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>        
<!-- /Reservation of rights -->

<!-- Notification of copyright infringement -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFourteen">14.Notification of copyright infringement</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFourteen" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; If you believe that your property has been used in any way that could be considered a copyright infringement or a violation of your intellectual property rights,
pinkEpromise's copyright agent may be contacted via the <a href="<?php echo  URL::base() ; ?>/contact" class="link-pink" target="_blank">contact us</a> page. 
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>        
<!-- /Notification of copyright infringement -->

<!-- Applicable law -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed txt-16px-pc" data-toggle="collapse" href="#collapseFifteen">15.Applicable law</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container  -->
<div id="collapseFifteen" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; You agree that these Terms of Use and any dispute arising out of your use of this website or products or services provided will be governed by and construed in
accordance with local laws applicable in Illinois, notwithstanding any differences between the said applicable legislation and legislation in force at your location. 
<br /><br />
&bull; By registering for a user account on this website, or by using this website and the services it provides, you accept that jurisdiction is granted to the courts
of Illinois, and that any disputes will be heard by the said courts. 
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>        
<!-- /Applicable law -->

<!-- Miscellaneous information -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed txt-16px-pc" data-toggle="collapse" href="#collapseSixteen">16.Miscellaneous information</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container  -->
<div id="collapseSixteen" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; In the event that any provision of these Terms of Use is deemed to conflict with legislation by a court with jurisdiction over the parties, the said provision will
be interpreted to reflect the original intentions of the parties in accordance with applicable law, and the remainder of these Terms of Use will remain valid and applicable. 
<br /><br />
&bull; The failure of either party to assert any right under these Terms of Use will not be considered to be a waiver of that party's right, and the said right will remain in full force and effect;
<br /><br />
&bull; You agree that any claim or cause in respect of this website or its services must be filed within one (1) year after such claim or cause arose, or the said claim or
cause will be forever barred, without regard to any contrary legislation;
<br /><br />
&bull; pinkEpromise may assign pinkEpromise's rights and obligations under these Terms of Use; in this event, pinkEpromise will be relieved of any further obligation.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>        
<!-- /Miscellaneous information -->

</div> 
<!-- Privacy Group -->

<!-- Document Update -->
<div class="text-center p-10px-b">This document was last updated on January 30, 2013.</div>  
<!-- /Document Update --> 

</div>
<!-- Privacy Body -->
</div>
<!-- /Pivacy Data Container -->
</div>
<!-- /Row -->
</div>
