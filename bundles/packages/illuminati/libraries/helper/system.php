<?php

function serializeClosure($closure)
{
	if ( ! $closure instanceof Closure) return $closure;
	
	return serialize(new \Illuminati\Component\Helper\SerializableClosure($closure));
}


if ( ! function_exists('with'))
{
	/**
	 * Return the given object. Useful for chaining.
	 *
	 * @param  mixed  $object
	 * @return mixed
	 */
	function with($object)
	{
		return $object;
	}
}

