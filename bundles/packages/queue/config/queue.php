<?php

 return array(

	'default' => array(
			'host'       => '127.0.0.1',
			'port'       => '11300',
			'queue'		 => 'default'
 	),


	'test' => array(
			'host'       => '10.0.2.15',
			'port'       => '11301',
			'queue'		 => 'test-queue'
 	)

 );