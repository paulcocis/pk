<?php

 /**
  * ----------------------------------------------------------------------------------------------------------------------------------
  * Start-up file for queue package.
  * ----------------------------------------------------------------------------------------------------------------------------------
  *
  * @author  Paul Cocis <paul@codelab.ro>
  * @version 1.0
  *
  */

 Autoloader::map(array(

 	// The Queue Class
 	'Queue'					 => Bundle::path('queue').'queue.php',
	//'Pheanstalk\\Pheanstalk' => __DIR__ . '/vendor/beanstalk/pheanstalk/classes/Pheanstalk.php',
));

Autoloader::namespaces(array(
    'Queue' => Bundle::path('queue').'library',
));


// Init file for beanstalk library.
$beanstalk_init = dirname(__FILE__).'/vendor/beanstalk/pheanstalk_init.php';

// load the class now.
require_once($beanstalk_init);

function testFunc($id = 0)
{

   if (!$id)
   {
      return FALSE;
   }


}



