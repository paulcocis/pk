<?php

class Queue_Queue_Task {

	public function __construct()
	{
		$this->worker   = new Queue\Worker;
		$this->listener = new Queue\Listener;
	}

	/**
	 * -----------------------------------------------------------------------------------
	 * The queue deamon class.
	 * -----------------------------------------------------------------------------------
	 *
	 * @param  array  $arguments
	 */
	public function listen($arguments = array()) {

		// get formatted options.
		$options = $this->getOptions($arguments);

		// extract them now.
		extract($options);

		// start the listener.
		$this->listener->listen($connection, $queue, $delay, $memory, $timeout);
	}



	public function work($arguments = array()) {

		// get formatted options.
		$options = $this->getOptions($arguments);

		// extract them now.
		extract($options);

		$this->worker->pop($connection, $queue, $delay, $memory, $sleep);
	}



	/**
	 * There is a hack for CLI, to format CLI arguments format like key:value
	 * into array.
	 *
	 * @param  array  $args
	 * @return array
	 */
	function getOptions($args = array())
	{
		$default = array(
			'connection' => 'default',
			'queue'		 => null,
			'memory'	 => '128',
			'delay'		 => '0',
			'timeout'	 => 60,
			'sleep'		 => FALSE
		);

		$options = array();

		if (!empty($args))
		{
			foreach($args as $arg => $option)
			{
				list($key, $value) = explode(':', $option);
				$options[$key] = $value;
			}
		}

		$options = array_merge($default, $options);
		return $options;
	}





}