<?php

class Queue_Test_Task {

	public function __construct()
	{
		$this->worker   = new Queue\Worker;
		$this->listener = new Queue\Listener;
	}

	public function listen($arguments = array()) {



		dd($this->getOptions($arguments));

	}


	/**
	 * There is a hack for CLI, to format CLI arguments format like key:value
	 * into array.
	 *
	 * @param  array  $args
	 * @return array
	 */
	function getOptions($args = array())
	{
		$default = array(
			'connection' => null,
			'queue'		 => null,
			'memory'	 => '128',
			'delay'		 => '0',
			'timeout'	 => 60
		);

		$options = array();

		if (!empty($args))
		{
			foreach($args as $arg => $option)
			{
				list($key, $value) = explode(':', $option);
				$options[$key] = $value;
			}
		}

		$options = array_merge($default, $options);
		return $options;
	}


}