<?php
 class Queue_Home_Controller extends Base_Controller {


   public function action_index()
   {

	  $id = 'my-unique-id';
    $name = 'paul';

      $closure = function($job) use ($id, $name)
      {
          // create a handler.
          $handle = fopen('closure.txt', 'a+');

          // write something.
          fwrite($handle, date('Y-m-d H:i:s').$id.$name."\n");

          // close it now !
          fclose($handle);

          $job->release(10);
      };

      Queue::push($closure);
	}

  public function action_test_class()
  {
      Queue::forge('default')->push('Inbox@send', array('subject' => 'Queue From', 'message' => 'Working from queue'), 'MyQueue');
  }

  public function action_test()
  {

      $inbox = new Inbox();
      $inbox->send(array('subject' => 'Subject Here', 'message' => 'Test Message'));

  }


 }