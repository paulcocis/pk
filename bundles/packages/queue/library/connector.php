<?php

namespace Queue;
use Pheanstalk_Pheanstalk as Pheanstalk;

class Connector implements \Queue\Interfaces\Connector {

	/**
	 * Establish a queue connection.
	 *
	 * @param  array  $config
	 * @return
	 */
	public function connect(array $config)
	{
		// revalidate connection port.
		$config['port'] = $config['port'] ?: '11300';

		// establish the connection.
		$pheanstalk = new Pheanstalk($config['host'], $config['port']);

		return new Queue($pheanstalk, $config['queue']);
	}


}