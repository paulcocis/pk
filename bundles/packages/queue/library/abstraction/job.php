<?php namespace Queue\Abstraction;

abstract class Job {

	/**
	 * The job handler instance.
	 *
	 * @var mixed
	 */
	protected $instance;

	/**
	 * Fire the job.
	 *
	 * @return void
	 */
	abstract public function fire();

	/**
	 * Delete the job from the queue.
	 *
	 * @return void
	 */
	abstract public function delete();

	/**
	 * Release the job back into the queue.
	 *
	 * @param  int   $delay
	 * @return void
	 */
	abstract public function release($delay = 0);

	/**
	 * Get the number of times the job has been attempted.
	 *
	 * @return int
	 */
	abstract public function attempts();

	/**
	 * Resolve and fire the job handler method.
	 *
	 * @param  array  $payload
	 * @return void
	 */
	protected function resolveAndFire(array $payload)
	{
		list($class, $method) = $this->parseJob($payload['job']);

		// Closure support added.
		if ($class == 'IlluminatiQueueClosure')
		{
			if (str_contains($payload['data']['callback'], 'SerializableClosure'))
     		{
        		$closure = with(unserialize($payload['data']['callback']))->getClosure();
        		$closure($this);
      		}
		}

		// Process the class method.
		else
		{
			$this->instance = $this->resolve($class);
			$this->instance->{$method}($this, $payload['data']);
		}
	}

	/**
	 * Resolve the given job handler.
	 *
	 * @param  string  $class
	 * @return mixed
	 */
	protected function resolve($class)
	{
		return new $class;
	}


	/**
	 * Resolve the given closure payload.
	 */
	protected function resolveClosure($payload)
	{
		// unserialize the payload
		$arrayObject = unserialize($payload['data']);

		// lets extract the variables.
		extract($arrayObject['variables']);

		// evaluate the closure.
		eval('$closure = '.$unserialized['code'].';');

		// execute and return closure.
		return $closure($this, $id);
	}


	/**
	 * Parse the job declaration into class and method.
	 *
	 * @param  string  $job
	 * @return array
	 */
	protected function parseJob($job)
	{
		$segments = explode('@', $job);

		return count($segments) > 1 ? $segments : array($segments[0], 'fire');
	}

	/**
	 * Determine if job should be auto-deleted.
	 *
	 * @return bool
	 */
	public function autoDelete()
	{
		return isset($this->instance->delete);
	}

}