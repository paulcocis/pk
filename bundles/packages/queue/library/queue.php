<?php namespace Queue;

use Pheanstalk_Job;
use Pheanstalk_Pheanstalk as Pheanstalk;

class Queue implements \Queue\Interfaces\Queue {

	/**
	 * The Pheanstalk instance.
	 *
	 * @var Pheanstalk
	 */
	protected $driver;

	/**
	 * The name of the default tube.
	 *
	 * @var string
	 */
	protected $default;


	/**
	 * Create a new Beanstalkd queue instance.
	 *
	 * @param  Pheanstalk  $pheanstalk
	 * @param  string  $default
	 * @return void
	 */
	public function __construct(Pheanstalk $driver, $default)
	{
		$this->default = $default;
		$this->driver  = $driver;
	}


	/**
	 * Push a new job onto the queue.
	 *
	 * @param  string  $job
	 * @param  mixed   $data
	 * @param  string  $queue
	 * @return void
	 */
	public function push($job, $data = '', $queue = null)
	{
		$payload = $this->createPayload($job, $data);

		$this->driver->useTube($this->getQueue($queue))->put($payload);
	}


	/**
	 * Push a new job onto the queue after a delay.
	 *
	 * @param  int     $delay
	 * @param  string  $job
	 * @param  mixed   $data
	 * @param  string  $queue
	 * @return void
	 */
	public function later($delay, $job, $data = '', $queue = null)
	{
		$payload = $this->createPayload($job, $data);

		$pheanstalk = $this->driver->useTube($this->getQueue($queue));

		$pheanstalk->put($payload, Pheanstalk::DEFAULT_PRIORITY, $delay);
	}

	/**
	 * Pop the next job off of the queue.
	 *
	 * @param  string  $queue
	 * @return \Illuminate\Queue\Jobs\Job|null
	 */
	public function pop($queue = null)
	{
		$job = $this->driver->watchOnly($this->getQueue($queue))->reserve();

		if ($job instanceof Pheanstalk_Job)
		{
			return new Job($this->driver, $job);
		}
	}

	/**
	 * Get job stats/informations.
	 *
	 * @param  integer $job_id
	 * @return Pheanstalk_Response_ArrayResponse Object
	 */
	public function stats($id = 0, $tube = FALSE)
	{
		if (!$id)
		{
			return $this->driver->stats();
		}

		if ($tube)
		{
			return $this->driver->statsTube($id);
		}


		$stats = $this->driver->statsJob($id);
		return (object) $stats;
	}


	/**
	 * Get the queue or return the default.
	 *
	 * @param  string|null  $queue
	 * @return string
	 */
	protected function getQueue($queue)
	{
		return $queue ?: $this->default;
	}


	/**
	 * Create a payload string from the given job and data.
	 *
	 * @param  string  $job
	 * @param  mixed   $data
	 * @return string
	 */
	protected function createPayload($job, $data = '')
	{
		if ($job instanceof \Closure)
		{
			return json_encode($this->createClosurePayload($job, $data));
		}
		else
		{
			return json_encode(array('job' => $job, 'data' => $data));
		}
	}

	/**
	 * Create a payload string for the given Closure job.
	 *
	 * @param  \Closure  $job
	 * @param  mixed  $data
	 * @return string
	 */
	protected function createClosurePayload($job, $data)
	{
		/*$serializable = new \Illuminati\SerializableClosure($job);
		$closure = $serializable->serialize();
		return array('job' => 'IlluminatiQueueClosure', 'data' => $closure);*/

		$misc = array('job' => 'IlluminatiQueueClosure');

		// serialize the closure now.
		$callback = serializeClosure($job);

		// compacted serialized.
		$compacted = compact('misc', 'callback');

		return array('job' => 'IlluminatiQueueClosure', 'data' => $compacted);
	}

}