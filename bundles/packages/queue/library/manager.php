<?php namespace Queue;

class Manager {


	/**
	 * The array of resolved queue connections.
	 * @var array
	 */
	protected $connections = array();


	/**
	 * Resolve a queue connection instance.
	 *
	 * @param  string $name
	 * @return \Queue\Interfaces\QueueInterface
	 */
	public function connection($name = null)
	{

		if (is_null($name))
		{
			$name = 'default';
		}

		if (!isset($this->connections[$name]))
		{
			$connector = new Connector;
			$this->connections[$name] = $connector->connect($this->getConnection($name));
		}

		return $this->connections[$name];
	}


	/**
	 * Get the queue connection properties.
	 *
	 * @param  string $name
	 * @return array
	 */
	public function getConnection($name = '')
	{
		// get the connection.
		return \Config::get("queue::queue.{$name}", array('host' => null, 'port' => null, 'queue' => null));
	}





}