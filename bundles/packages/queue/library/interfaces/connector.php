<?php namespace Queue\Interfaces;

interface Connector {

	/**
	 * Establish a queue connection.
	 *
	 * @param  array  $config
	 */
	public function connect(array $config);


}