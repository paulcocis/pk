<?php

class Queue {

	/**
	 * Create a new beanstalkd connection.
	 *
	 * @param  string $name Queue connection name
	 * @return \Queue\Queue instance
	 */

	public static function forge($name = null) {

		static $instances = array();

		if (!isset($instances[$name]))
		{
			$manager = new Queue\Manager;
			$instances[$name] = $manager->connection($name);
		}

		return $instances[$name];
	}


	public static function push($job, $queue = '', $connection = 'default')
	{
		$manager = new Queue\Manager;
		// get the queue connection handler.
		$queue = $manager->connection($connection);

		// add new job in queue.
		$queue->push($job, array(), 'testqueue');

		return $queue;
	}

}