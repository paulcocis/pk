<!-- Spacer -->
<div class="p-8px"></div>

<!-- 6.$WEBSITE_FOOTER -->
<div class="footer" align="center">
<!-- Footer Container -->
<div class="footer-fit-on-pc footer-fit-on-tablet footer-fit-on-mobile">

<!-- Card Accept -->
<div class="getaways pull-left">
<div class="txt-white txt-13px-pc txt-12px-sm">We Accept</div>
<div class="div-inline-block"><img src="{{ $theme_data['asset_url'] }}images/icons/master.png" class="getaways-size"></div>
<div class="div-inline-block"><img src="{{ $theme_data['asset_url'] }}images/icons/visa.png" class="getaways-size"></div>
<div class="div-inline-block"><img src="{{ $theme_data['asset_url'] }}images/icons/discovery.png" class="getaways-size"></div>
<div class="clearfix"></div>
<div class="div-inline-block m-2px-t"><img src="{{ $theme_data['asset_url'] }}images/icons/american.png" class="getaways-size"></div>
<div class="div-inline-block m-2px-t"><img src="{{ $theme_data['asset_url'] }}images/icons/paypal.png" class="getaways-size"></div>
</div>

<!-- Blog Story -->
<div class="blog_story pull-left">
<a href="{{ URL::base() }}/about-us" class="link-pinkwhite txt-12px">Our Story<br class="hidden-xs"> Subscribe to our Blog</a>
</div>

<!-- Footer Menu Line 1 -->
<div class="footer-menu_1 pull-left div-inline-block">
<a href="{{ URL::base() }}/about-us" class="link-gray txt-12px">About US</a>
<br>
<a href="{{ URL::base() }}/privacy" class="link-gray txt-12px">Privacy Policy</a>
<br>
<a href="{{ URL::base() }}/terms" class="link-gray txt-12px">Terms of Use</a>
</div>

<!-- Footer Menu Line 2 -->
<div class="footer-menu_2 pull-left div-inline-block">
<a href="{{ URL::base() }}/faq" class="link-gray txt-12px">FAQ</a>
<br>
<a href="{{ URL::base() }}/contact" class="link-gray txt-12px">Contact Us</a>
<br>
<a href="{{ URL::base() }}/affiliate-program" class="link-gray txt-12px">Affiliate Program</a>
</div>

<!-- Footer Social -->
<div class="footer-social pull-left">
<div class="div-inline-block"><a href="https://www.facebook.com/PinkEpromise" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn-footer-social1','','{{ $theme_data['asset_url'] }}images/icons/footer_facebook_o.png',1)"><img src="{{ $theme_data['asset_url'] }}images/icons/footer_facebook.png"  class="footer-social-icons" id="btn-footer-social1"></a></div>
<div class="div-inline-block"><a href="https://twitter.com/pEpdailydeals" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn-footer-social2','','{{ $theme_data['asset_url'] }}images/icons/footer_twitter_o.png',1)"><img src="{{ $theme_data['asset_url'] }}images/icons/footer_twitter.png" class="footer-social-icons" id="btn-footer-social2"></a></div>
<div class="div-inline-block"><a href="http://www.pinterest.com/pinkepromise/" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn-footer-social3','','{{ $theme_data['asset_url'] }}images/icons/footer_p_o.png',1)"><img src="{{ $theme_data['asset_url'] }}images/icons/footer_p.png" class="footer-social-icons" id="btn-footer-social3"></a></div>
<div class="div-inline-block"><a href="http://instagram.com/pinkepromise" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn-footer-social4','','{{ $theme_data['asset_url'] }}images/icons/footer_x_o.png',1)"><img src="{{ $theme_data['asset_url'] }}images/icons/footer_x.png" class="footer-social-icons" id="btn-footer-social4"></a></div>
</div>

</div>
<!-- /Footer Container -->
</div>

<!-- /6.$WEBSITE_FOOTER -->


<!-- Javascripts --> 
<script src="{{ $theme_data['asset_url'] }}js/bootstrap.js"></script> 
<script src="{{ $theme_data['asset_url'] }}js/custom.js"></script> 

<script src="//connect.facebook.net/en_US/all.js#xfbml=1"></script>
<script type="text/javascript" src="{{ $theme_data['asset_url'] }}js/simple-inheritance.min.js"></script>
<script type="text/javascript" src="{{ $theme_data['asset_url'] }}js/code-photoswipe-1.0.11.min.js"></script>


<!-- /Javascripts -->

@yield('js')

</body>
<!-- /$BODY -->
</html>