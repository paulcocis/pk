﻿// Rollover images

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


// Scrollup Right
$(document).ready(function () {
  $(window).scroll(function () {
   if ($(this).scrollTop() > 100) 
    { 
       $('.scrollup_right').fadeIn(); 
    } else { 
       $('.scrollup_right').fadeOut(); 
    }
    });

      $('.scrollup_right').click(function () 
    {
      $("html, body").animate({ scrollTop: 0 }, 600);
       return false;
    });
});


// Scrollup Left
$(document).ready(function () {
  $(window).scroll(function () {
   if ($(this).scrollTop() > 100) 
    { 
       $('.scrollup_left').fadeIn(); 
    } else { 
       $('.scrollup_left').fadeOut(); 
    }
    });

      $('.scrollup_left').click(function () 
    {
      $("html, body").animate({ scrollTop: 0 }, 600);
       return false;
    });
});

// GoBack History
function goBack() {
    window.history.back()
}


// Generates the captcha function    
 function DrawCaptcha()
 {
 var a = Math.ceil(Math.random() * 10)+ '';
 var b = Math.ceil(Math.random() * 10)+ '';       
 var c = Math.ceil(Math.random() * 10)+ '';  
 var d = Math.ceil(Math.random() * 10)+ '';  
 var e = Math.ceil(Math.random() * 10)+ '';  
 var f = Math.ceil(Math.random() * 10)+ '';  
 var g = Math.ceil(Math.random() * 10)+ '';  
 var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g; 
     document.getElementById("txtCaptcha").value = code
 }
// Validate the Entered input aganist the generated security code function   
 function ValidCaptcha(){
 var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
 var str2 = removeSpaces(document.getElementById('txtInput').value);
 if (str1 == str2) return true; return false; }
// Remove the spaces from the entered and generated code
 function removeSpaces(string) { return string.split(' ').join(''); }

// Order Form Commun
function refresh_radio_reset() { 
 $("input:radio").attr("checked", false); 
}

// Order Form Step 1
function show_login_form() { 
if ($("#login_form").is(':hidden')) { 
 $("#login_form").show("fast"); 
 $("#input-form").hide("fast");  
 $("#button-go-back").show("fast"); 
 }
}
function show_register_form() { 
if ($("#register_form").is(':hidden')) { 
 $("#register_form").show("fast"); 
 $("#input-form").hide("fast");  
 $("#button-go-back").show("fast"); 
 }           
}
function back_step1_button() { 
 $("#login_form").hide("fast"); 
 $("#register_form").hide("fast");  
 $("#button-go-back").hide("fast"); 
 $("#input-form").show("fast"); 
 $("input:radio").attr("checked", false); 
}

// Order Form Step 2
function show_shipping_next() { 
if ($("#shipping-next").is(':hidden')) { 
 $("#shipping-next").show("fast"); 
 }
}
function show_add_shipping_form() { 
if ($("#add_shipping").is(':hidden')) {  
 $("#shipping-next").hide("fast"); 
 $("#add_shipping").show("fast"); 
 $("#input-form").hide("fast"); 
 $("#button-go-back").show("fast"); 
 }
}
function back_step2_button() { 
 $("#add_shipping").hide("fast");   
 $("#button-go-back").hide("fast"); 
 $("#input-form").show("fast"); 
 $("input:radio").attr("checked", false); 
}

// Order Form Step 3
function show_paypal_next() { 
if ($("#paypal-next").is(':hidden')) { 
 $("#paypal-next").show("fast"); 
 }
}
function show_card_form() { 
if ($("#card_payment_form").is(':hidden')) {  
 $("#paypal-next").hide("fast");  
 $("#card_payment_form").show("fast"); 
 $("#input-form").hide("fast"); 
 $("#button-go-back").show("fast"); 
 }
}
function back_step3_button() { 
 $("#card_payment_form").hide("fast");   
 $("#button-go-back").hide("fast"); 
 $("#input-form").show("fast"); 
 $("input:radio").attr("checked", false); 
}

// Product Details Facebook Like
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


// Product Details "Buy Now" Change
function buy_product() {
 $("#product-price").hide("fast"); 
 $("#product-details").hide("fast");
 $("#product-add-cart").show("fast");
 $("#product-customize").show("fast");
}

// Facebook Comment Box Resize

(function(d, s, id) { 
	var js, fjs = d.getElementsByTagName(s)[0]; 
		if (d.getElementById(id)) return; 
			js = d.createElement(s); 
				js.id = id; 
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs); 
 }
(document, 'script', 'facebook-jssdk'));

// Norablue Personalization Box's
function nora_select1() {
 $("#disc2").hide("fast");
 $("#disc3").hide("fast");  
}

function nora_select2() {
 $("#disc2").show("fast");
 $("#disc3").hide("fast");  
}

function nora_select3() {
 $("#disc2").show("fast");
 $("#disc3").show("fast"); 
}

