<div id="main-menu" role="navigation">
	<div id="main-menu-inner">
		<div id="menu-content-demo" class="menu-content top">
			<!-- Menu custom content demo
CSS:        styles/pixel-admin-less/demo.less or styles/pixel-admin-scss/_demo.scss
Javascript: html/assets/demo/demo.js
-->
			<div>
				<div class="text-bg">
					<span class="text-slim">Welcome,</span>
					<span class="text-semibold">{{ @Auth::user()->username }}</span></div>
				<img alt="" class="" src="{{ $theme_data['asset_url'] }}demo/avatars/1.jpg">
				<div class="btn-group">
					<a class="btn btn-xs btn-primary btn-outline dark" href="#">
					<i class="fa fa-envelope"></i></a>
					<a class="btn btn-xs btn-primary btn-outline dark" href="#">
					<i class="fa fa-user"></i></a>
					<a class="btn btn-xs btn-primary btn-outline dark" href="#">
					<i class="fa fa-cog"></i></a>
					<a class="btn btn-xs btn-danger btn-outline dark" href="{{ url_to('logout', 'seller_url')}}">
					<i class="fa fa-power-off"></i></a></div>
				<a class="close" href="#">&times;</a> </div>
		</div>
		<ul class="navigation">
			<li class="active"><a href="{{ url_to('home/', 'seller_url')}}">
			<i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
			</li>

			<li class="mm-dropdown"><a href="javascript:void(0);"><i class="menu-icon fa  fa-shopping-cart">
			</i><span class="mm-text">Store </span></a>
			<ul>
				<li><a href="{{ url_to('orders/', 'seller_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">View Orders</span></a> </li>
		
				<li><a href="{{ url_to('deals', 'seller_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Manage 
				Deals </span></a></li>
			</ul>
			</li>
		

			<li class="mm-dropdown"><a href="#"><i class="menu-icon fa fa-wrench">
			</i><span class="mm-text">Administration</span></a>
			<ul>
				<li><a href="{{ url_to('profile', 'seller_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">My Profile</span></a> </li>
		
			</ul>
			</li>
		</ul>
		<!-- / .navigation -->
		<div class="menu-content">
			<a class="btn btn-primary btn-block btn-outline dark" href="{{ url_to('deals/add', 'seller_url')}}">
			Create New Deal</a> </div>
	</div>
	<!-- / #main-menu-inner --></div>
