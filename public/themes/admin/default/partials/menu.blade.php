<?php
 
 $currentLoggedId = Auth::user()->id;

 ?>

<div id="main-menu" role="navigation">
	<div id="main-menu-inner">
		<div id="menu-content-demo" class="menu-content top">
			<!-- Menu custom content demo
CSS:        styles/pixel-admin-less/demo.less or styles/pixel-admin-scss/_demo.scss
Javascript: html/assets/demo/demo.js
-->
			<div>
				<div class="text-bg">
					<span class="text-slim">Welcome,</span>
					<span class="text-semibold">{{ @Auth::user()->username }}</span></div>
				<img alt="" class="" src="{{ $theme_data['asset_url'] }}demo/avatars/1.jpg">
				<div class="btn-group">
					<a class="btn btn-xs btn-primary btn-outline dark" href="#">
					<i class="fa fa-envelope"></i></a>
					<a class="btn btn-xs btn-primary btn-outline dark" href="<?php url_to('setup/admins/edit/'.$currentLoggedId, 'admin_url') ?>">
					<i class="fa fa-user"></i></a>
					<a class="btn btn-xs btn-primary btn-outline dark" href="<?php url_to('setup/admins/edit/'.$currentLoggedId, 'admin_url') ?>">
					<i class="fa fa-cog"></i></a>
					<a class="btn btn-xs btn-danger btn-outline dark" href="{{ url_to('logout', 'admin_url')}}">
					<i class="fa fa-power-off"></i></a></div>
				<a class="close" href="#">&times;</a> </div>
		</div>
		
		<ul class="navigation">
			<li class="active"><a href="{{ url_to('home', 'admin_url')}}">
			<i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
			</li>
			<li class="mm-dropdown"><a href="#"><i class="menu-icon fa fa-table">
			</i><span class="mm-text">Reports</span></a>
			<ul>

<li><a href="{{ url_to('reports/seller/', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Seller Reports</span></a> </li>

				<li><a href="{{ url_to('reports/invoices/', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Seller Invoices</span></a> </li>
				<li><a href="{{ url_to('reports/salesSummaryByTransactions', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Sales Summary By Transactions</span></a> </li>
				<li><a href="{{ url_to('billing/transactions', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">View all transactions</span></a> </li>
			</ul>
			</li>
			
			<li class="mm-dropdown"><a href="javascript:void(0);"><i class="menu-icon fa  fa-shopping-cart">
			</i><span class="mm-text">Store </span></a>
			<ul>

	<li><a href="{{ url_to('customers/', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Customers</span></a> </li>
				<li><a href="{{ url_to('orders/view?pk=1', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">PinkeStore 
				Orders</span></a> </li>
				<li><a href="{{ url_to('orders/view', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">PEP 
				Orders</span></a> </li>
				<li><a href="{{ url_to('promotions', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Coupon 
				Administration</span></a> </li>
				<li><a href="{{ url_to('deals', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Manage 
				Deals </span></a></li>

	<li><a href="{{ url_to('deals/categories', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Manage 
				Categories </span></a></li>

				<li><a href="{{ url_to('deals/?status=unscheduled', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Review 
				Unscheduled Deals </span></a></li>
			</ul>
			</li>
				<li ><a href="{{ url_to('orders/manage', 'admin_url')}}"><i class="menu-icon fa  fa-shopping-cart">
			</i><span class="mm-text">Order Manager </span></a>
			</li>
			<li class="mm-dropdown"><a href="#"><i class="menu-icon fa fa-money">
			</i><span class="mm-text">Billing</span></a>
			<ul>
				<li><a href="{{ url_to('billing/transactions', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Transactions
				</span></a></li>
		
			</ul>
			</li>

			<li class="mm-dropdown"><a href="#"><i class="menu-icon fa fa-th">
			</i><span class="mm-text">Utilities</span></a>
			<ul>
				<li style="display:none;"><a href="#" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Traffic 
				Monitor </span></a></li>
				<li><a href="#" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Blog 
				Administration <label class="label label-success">6</label>
				</span></a></li>
				<li style="display:none;"><a href="#" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Inbox
				<label class="label label-success">4</label> </span></a></li>
			</ul>
			</li>
			<li class="mm-dropdown"><a href="#"><i class="menu-icon fa  fa-users">
			</i><span class="mm-text">Seller Managment</span></a>
			<ul>
				<li><a href="{{ url_to('seller/home?status=pending', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Pending 
				Sellers <label class="label label-warning">{{ count(Seller::forge()->retrieve('pending')) }}</label> </span></a>
				</li>
				<li><a href="{{ url_to('seller/home', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">View 
				Seller List</span></a> </li>
			
			</ul>
			</li>
			<li class="mm-dropdown"><a href="#"><i class="menu-icon fa fa-wrench">
			</i><span class="mm-text">Administration</span></a>
			<ul>
				<li><a href="{{ url_to('setup', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">General 
				Settings</span></a> </li>
				<li><a href="{{ url_to('setup/store', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Store 
				Settings</span></a> </li>
				<li><a href="{{ url_to('setup/paymentgateways', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Payment 
				Gateways</span></a> </li>
				<li><a href="{{ url_to('setup/admins', 'admin_url')}}" tabindex="-1">
				<i class="menu-icon fa fa-th-list"></i><span class="mm-text">Manage 
				Administrators</span></a> </li>
			</ul>
			</li>
		</ul>
		<!-- / .navigation -->
		<div class="menu-content">
			<a class="btn btn-primary btn-block btn-outline blue" href="{{ url_to('deals/add', 'admin_url')}}">
			Create New Deal</a> </div>
	</div>
	<!-- / #main-menu-inner --></div>
