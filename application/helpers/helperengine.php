<?php

class HelperEngine {

	/**
	 * Loaded helpers.
	 * @var array
	 */
	public static $helpers = array();

	/**
	 * Add new helper
	 * @param string $helper [description]
	 */
	public static function add($helper = '')
	{
		if (!static::exists($helper))
		{
			static::$helpers[$helper] = $helper;
			return TRUE;
		}

		return FALSE;
	}

	public static function instance($helper)
	{
		if (static::exists($helper))
		{
			$class = 'Helper_'.ucfirst($helper);
			return new $class;
		}

		return FALSE;
	}

	public static function get()
	{
		return static::$helpers;
	}

	/**
	 * Remove a helper.
	 * @param  string $helper [description]
	 * @return array
	 */
	public static function remove($helper)
	{
		if (static::exists($helper))
		{
			unset(static::$helpers[$helper]);
			usort(static::$helpers);
		}

		return static::$helpers;
	}

	/**
	 * Sync helpers with theme
	 * @param  Theme  $theme
	 */
	public function sync(&$theme)
	{

		foreach(HelperEngine::get() as $helper => $name)
		{
			$instance = HelperEngine::instance($helper);

			$name = ucfirst($name);
			$theme->set($name, $instance);
		}
	}


	protected static function exists($helper = '')
	{
		return (bool) isset(static::$helpers[$helper]);
	}


}