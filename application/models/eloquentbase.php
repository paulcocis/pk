<?php

class EloquentBase extends Eloquent
{

	protected $prefix = '';

	/**
	 * Construct
	 *
	 * @param array   $attributes
	 * @param boolean $exists
	 */
	public function __construct($attributes = array(), $exists = false)
	{
		parent::__construct($attributes, $exists);

		// Set the prefix
		$prefix = \Config::get('verify::verify.prefix');

		$this->prefix = $prefix
			? $prefix.'_'
			: '';
	}

	/**
	 * initiate new instance.
	 * @return class object
	 */
	public static function instance()
	{
		return new static;
	}

	/**
	 * Get the table name
	 *
	 * @return string
	 */
	public function table()
	{
		$table = parent::table();

		$table = $this->prefix
			? $this->prefix.$table
			: $table;

		return $table;
	}

	/**
	 * Format post data and remove params that has missing in 
	 * specified table.
	 * @param  [type] $data  [description]
	 * @param  [type] $table [description]
	 * @return [type]        [description]
	 */
	public static function formatPostData($data, $table = NULL)
	{
		return table_columns($table, $data);
	}


	/**
	 * Similar with format data, but also with posibility
	 * with filling it.
	 * @param array   $data [description]
	 * @param boolean $fill [description]
	 */
	public function set_data($data = array(), $fill = FALSE)
	{
		// format table columns
		$data = table_columns($this->table(), $data);

		if ($fill)
		{
			// set and return the class object.
			$this->fill($data);
		}
		else
		{
			foreach($data as $field => $value)
			{
				$this->{$field} = $value;
			}
		}

		return $this;
	}

	/**
	 * Get the data.
	 * @param  array   $data [description]
	 * @param  boolean $fill [description]
	 * @return [type]        [description]
	 */
	public function get_data($data = array(), $fill = FALSE)
	{
		// format table columns
		$data = table_columns($this->table(), $data);
		return $data;
	}


	/**
	 * Generate form select from enum field.
	 * @param  string $field [description]
	 * @return [type]        [description]
	 */
	public function enum_dropdown($field = '')
	{
		if (!empty($field))
		{
			// create html select .
			$select = "<select name=".$field.">\n";

			$list = enum_mysql($this->table, $field);

			foreach($list as $option)
			{
				$select .= "<option value=".$option.">".$option."</option>\n";
			}

			$select .= "<select>\n";

			return $select;
		}

		return FALSE;
	}

	/**
	 * Return field enum values in array format.
	 * @param  string $field [description]
	 * @return mixed
	 */
	public function enum_field($field = '')
	{
		if (!empty($field))
		{
			$list = enum_mysql($this->table(), $field);
			return $list;
		}

		return FALSE;
	}

	public function created()
	{
		return date('Y-m-d', strtotime($this->created_at));
	}


	public function updated()
	{
		return date('Y-m-d', strtotime($this->updated_at));
	}


}