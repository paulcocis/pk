<?php

class Billing extends EloquentBase {

 	public static $table = 'user_billing_settings';

    public function user()
    {
        return $this->belongs_to('User');
    }

    public function commission_rate($master = FALSE)
    {	
    	$rate = $this->comission_rate;

    	if (empty($rate))
    	{
    		$rate = \App\Setup\Settings::forge()->rate();
    	}

        if ($master)
        {
            return \App\Setup\Settings::forge()->rate();
        }

    	// get the commision rate.
    	$rate = (100 - $rate);

    	return $rate;
    }

}