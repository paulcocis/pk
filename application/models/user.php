<?php

class User extends EloquentBase
{

	/**
	 * Accessible
	 *
	 * @var array
	 */
	public static $accessible = array(
		'username', 
		'password', 
		'salt', 
		'email', 
		'role_id', 
		'verified', 
		'deleted', 
		'disabled', 
		'id', 
		'encoded_password', 
		'user_ref_id',
		'signup_ip',
		'referer_id', 
		'');

	/**
	 * To check cache
	 *
	 * @var object
	 */
	public static $to_check_cache;

	/**
	 * Role
	 *
	 * @return object
	 */
	public function roles()
	{
		return $this->has_many_and_belongs_to('Role', $this->prefix.'role_user');
	}

	public function avatar()
	{
		return new UserAvatar($this->id);
	}

	/**
	 * Contacts table relationship.
	 * @return
	 */
	public function contacts()
	{
		return $this->has_many('Contacts');
	}

	public function billing()
	{
		return $this->has_many('Billing');
	}

	/**
	 * Get user primary contact.
	 *
	 * @return object array
	 */
    public function primary()
    {
    	return Contacts::where('primary', '=', 1)->where('user_id','=', $this->id)->first();
    }

    public function order()
    {
    	return $this->has_many("\App\Order", 'customer_id');
    }

    /**
     * Retrieve the user billing details.
     */ 
    public function bill()
    {
    	return Billing::where('user_id','=', $this->id)->first();
    }

    /**
     * Check if use has billing settings.
     */ 
    public function hasBill()
    {
    	$bill = $this->bill();

    	$check = is_null($bill) ? FALSE : TRUE;
    	return $check;
    }

    public function manager()
    {
    	$manager = FALSE;

    	if ($this->is('staff') AND !$this->is('admin'))
    	{
    		$manager = TRUE;
    	}

    	return TRUE;
    }

    public function last_login()
    {
    	if ($this->last_login_at == '0000-00-00 00:00:00')
    	{
    		return FALSE;
    	}

    	return $this->last_login_at;
    }

    public function ban($status = 0)
    {
    	$this->set_data(array('disabled' => $status))->save();
    	return $this;
    }

    public function unban()
    {
    	return $this->ban(0);
    }

    public function approve($SendConfirmationEmail = TRUE)
    {
    	// mark as active.
    	$this->status = 'active';

    	// update the user.
    	$this->save();

    	// send confirmation email if required.
    	if ($SendConfirmationEmail)
    	{
    		\App\Message\Message::forge('seller')->approved($this->id);
    	}

    }

  
    public function createDefaultBillingProfile()
    {
    	$default = array(
			'contact_id' => @$this->primary()->id,
			'user_id'    => $this->id,
    	);

    	$this->billing()->save(array($default));
    }

    public function updateBilling($data)
    {
    	$bill = $this->bill();

    	$instance = $this->billing()->find($bill->id);

    	$instance->set_data($data)->save();
    }

    public function delete()
    {
    	// delete billing profile.
    	$this->billing()->delete();

    	// delete created contacts.
    	$this->contacts()->delete();

    	// delete roles.
    	$this->roles()->delete();

    	// delete account now.
    	return parent::delete();
    }

    public function deleteByRef($id)
    {
    	$result = $this->find($id);

    	if (!is_null($result))
    	{
    		$result->delete();
    	}
    }

	/**
	 * Salts and saves the password
	 *
	 * @param string $password
	 */
	public function set_password($password)
	{
		$salt = md5(\Str::random(64) . time());
		$hashed = \Hash::make($salt . $password);

		$this->set_attribute('password', $hashed);
		$this->set_attribute('salt', $salt);
		$this->set_attribute('decoded_password', Crypter::encrypt($password));
	}

	public function verify()
	{
		$this->verified = 1;
		$this->save();
	}

	/**
	 * Change a user password.
	 *
	 * @param  string $new_password
	 * @return $this
	 */
	public function changePassword($new_password = '')
	{
		// set new password.
		$this->password = $new_password;

		// save new password.
		$this->save();

		return $this;
	}


	/**
	 * Get the real password.
	 *
	 * @return string
	 */
	public function get_real_password()
	{
		// return decoded.
		return Crypter::decrypt($this->decoded_password);
	}

	public function get_encrypted_password()
	{
		return $this->password;
	}

	/**
	 * Verify if user is active.
	 *
	 * @return boolean
	 */
	public function isActive()
	{
		$active = FALSE;

		if ($this->status == 'active')
		{
			$active = TRUE;
		}

		return $active;
	}

	/**
	 * Verify if user is pending
	 *
	 * @return boolean
	 */
	public function isPending()
	{
		$pending = FALSE;

		if ($this->status == 'pending')
		{
			$pending = TRUE;
		}

		return $pending;
	}


	public static function validate($data)
	{
		$rules = array(
			'username' => 'required|unique:users|max:50',
			'email'	   => 'required|email|unique:users',
			'password' => 'required'
		);

		$validation = Validator::make($data, $rules);

		if ($validation->fails())
		{
			return $validation->errors;
		}

		return TRUE;
	}

	/**
	 * Can the User do something
	 *
	 * @param  array|string $permissions Single permission or an array or permissions
	 * @return boolean
	 */
	public function can($permissions)
	{
		$permissions = !is_array($permissions)
			? array($permissions)
			: $permissions;

		$to_check = $this->get_to_check();

		// Are we a super admin?
		foreach ($to_check->roles as $role)
		{
			if ($role->name === \Config::get('verify.super_admin'))
			{
				return TRUE;
			}
		}

		$valid = FALSE;
		foreach ($to_check->roles as $role)
		{
			foreach ($role->permissions as $permission)
			{
				if (in_array($permission->name, $permissions))
				{
					$valid = TRUE;
					break 2;
				}
			}
		}

		return $valid;
	}

	/**
	 * Is the User a Role
	 *
	 * @param  array|string  $roles A single role or an array of roles
	 * @return boolean
	 */
	public function is($roles)
	{
		$roles = !is_array($roles)
			? array($roles)
			: $roles;

		$to_check = $this->get_to_check();

		$valid = FALSE;
		foreach ($to_check->roles as $role)
		{
			// Is the role in array, or is user Super Admin
			if (in_array($role->name, $roles) || $role->name == \Config::get('verify.super_admin'))
			{
				$valid = TRUE;
				break;
			}
		}

		return $valid;
	}

	/**
	 * Is the User a certain Level
	 *
	 * @param  integer $level
	 * @param  string $modifier [description]
	 * @return boolean
	 */
	public function level($level, $modifier = '>=')
	{
		$to_check = $this->get_to_check();

		$max = -1;
		$min = 100;
		$levels = array();

		foreach ($to_check->roles as $role)
		{
			$max = $role->level > $max
				? $role->level
				: $max;

			$min = $role->level < $min
				? $role->level
				: $min;

			$levels[] = $role->level;
		}

		switch ($modifier)
		{
			case '=':
				return in_array($level, $levels);
				break;

			case '>=':
				return $max >= $level;
				break;

			case '>':
				return $max > $level;
				break;

			case '<=':
				return $min <= $level;
				break;

			case '<':
				return $min < $level;
				break;

			default:
				return false;
				break;
		}
	}

	/**
	 * Get to check
	 *
	 * @return object
	 */
	private function get_to_check()
	{
		$class = get_class();

		if(empty($this->to_check_cache))
		{
			$to_check = new $class;

			$to_check = $class::with(array('roles', 'roles.permissions'))
				->where('id', '=', $this->get_attribute('id'))
				->first();

			$this->to_check_cache = $to_check;
		}
		else
		{
			$to_check = $this->to_check_cache;
		}

		return $to_check;
	}

    public function get_role_names() {

    	$role_names = array();

        foreach($this->roles as $role)
        {
            $role_names[] = $role->name;
        }

        return $role_names;
    }


    public function has_role($key)
    {
        foreach($this->roles as $role)
        {
            if($role->name == $key)
            {
                return true;
            }
        }

        return false;
    }

    public function has_role_id($id)
    {
        foreach($this->roles as $role)
        {
            if($role->id == $id)
            {
                return true;
            }
        }

        return false;
    }

    public function has_any_role($keys)
    {
        if( ! is_array($keys))
        {
            $keys = func_get_args();
        }

        foreach($this->roles as $role)
        {
            if(in_array($role->name, $keys))
            {
                return true;
            }
        }

        return false;
    }


	public function send_password_recovery_email()
	{
		$data = $this;

		\Message::send(function($message) use($data)
		{
			$input = \Input::get();

		    $message->to($data->email);
		    $message->from('system@pinkepromise.com', 'Your account password');

		    $message->subject('PINKE PROMISE - Your account password');

		    $message->body->account = $data;
		    $message->body('view: emails.password_recovery');

		    // You can add View data by simply setting the value
		    // to the message.

		    $message->html(true);
		});	

	}

}
