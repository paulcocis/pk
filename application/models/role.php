<?php

class Role extends EloquentBase
{

	/**
	 * Accessible
	 *
	 * @var array
	 */
	public static $accessible = array('name', 'description', 'level', 'display_name');

	/**
	 * Users
	 *
	 * @return object
	 */
	public function users()
	{
		return $this->has_many_and_belongs_to('User', $this->prefix.'role_user');
	}

	/**
	 * Permissions
	 *
	 * @return object
	 */
	public function permissions()
	{
		return $this->has_many_and_belongs_to('Permission', $this->prefix.'permission_role');
	}


	/**
	 * Return the admin role unique id.
	 */
	public static function getAdminRoleId()
	{
		$record = Role::where('name', '=', 'admin')->first();
		return $record->id;
	}

	/**
	 * Return the default role unique id.
	 */
	public static function getUserRoleId()
	{
		$record = Role::where('name', '=', 'user')->first();
		return $record->id;
	}

	/**
	 * Return the staff role unique id.
	 */
	public static function getStaffRoleId()
	{
		$record = Role::where('name', '=', 'staff')->first();
		return $record->id;
	}	

	/**
	 * Return the customer role unique id.
	 */
	public static function getCustomerRoleId()
	{
		$record = Role::where('name', '=', 'customer')->first();
		return $record->id;
	}		

	/**
	 * Get a role id by name.
	 *
	 * @param  array  $names
	 * @return mixed
	 */
	public static function get_id_by_name($names = array())
	{
		$roles = Role::where_in('name', $names)->get();

		// create ids array.
		$ids = array();

		if (!is_null(($roles)))
		{
			foreach($roles as $role)
			{
				$ids[$role->id] = $role->id;
			}

			return $ids;
		}

		return FALSE;
	}

	/**
	 * Get default role id.
	 *
	 * @return int $id
	 */
	public static function default_role_id()
	{
		$role = Role::where('name', '=', 'user')->first();
		return $role->id;
	}

}