<?php

class Contacts extends EloquentBase {

    public function user()
    {
        return $this->has_many('User');
    }


	public function set_data($data = array(), $fill = FALSE)
	{
		// format table columns
		$data = table_columns($this->table(), $data);

		if (isset($data['instant_chat_networks']))
		{
			$data['instant_chat_networks'] = json_encode($data['instant_chat_networks']);
		}


		if ($fill)
		{
			// set and return the class object.
			$this->fill($data);
		}
		else
		{
			foreach($data as $field => $value)
			{
				$this->{$field} = $value;
			}
		}

		return $this;
	}


}