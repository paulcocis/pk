<?php

 class UserAvatar {

 	protected $user = NULL;

 	public function __construct($user_id = 0)
 	{	
 		// find the user.
 		$this->set($user_id);
 	}

 	public function set($id)
 	{
 		$this->user = User::find($id);
 	}


 	public function find($id)
 	{
 		$UserAvatar = new UserAvatar($id);
 		return $UserAvatar->get();
 	}

 	protected function avatar_path()
 	{	
 		// full path.
 		$path = path('public').'users/'.$this->user->id.'/avatar/';

 		// generate it recursive.
 		if (!file_exists($path))
 		{
 			mkdir($path, 0777, TRUE);
 		}

 		return $path;
 	}

 	public function get($width = 150, $height = 150)
 	{	
 	
 		if (!$this->name())
 		{
 			$url = \URL::base().'/users/no-avatar.jpg';
 			return $url;
 		}


 		// get avatar path.
 		$avatar = $this->avatar_path().$this->name();

 		$thumb_name = 'thumb_'.$width.'X'.$height.'_'.$this->name();

 		// get thumb location.
 		$thumb_location = $this->avatar_path().$thumb_name;


		// thumb need to be generated.
		if (!file_exists($thumb_location))
		{
		    // Save a thumbnail
   			$success = \Resizer::open( $avatar )
        		->resize( $width , $height , 'crop' )
        		->save( $thumb_location, 90 );	
		}

		return $this->url($thumb_name);
 	}


 	public function url($thumb_name = '')
 	{
 		$url = \URL::base().'/users/'.$this->user->id.'/avatar/'.$thumb_name;
 		return $url;

 	}

 	public function name()
 	{
 		return @$this->user->avatar_name;
 	}


 	public function upload() 
 	{

		$inputFileName = 'file';

		$needUpload = FALSE;

		if (\Input::file("{$inputFileName}.name"))
		{
			$needUpload = TRUE;
		}

		$input = \Input::get();

		if ($needUpload)
		{
			$name = \Input::file("{$inputFileName}.name");

			// upload the file.
			\Input::upload($inputFileName, $this->avatar_path(), $name);

			$this->user->set_data(array(
				'avatar_name'     => $name,
				'avatar_location' => $this->avatar_path()
			))->save();
		}
 	}



 }