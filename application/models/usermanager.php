<?php

 class UserManager {

 	protected $userObject = NULL;


 	protected $rules = array(
			
		'email'    => 'required|email|unique:users,email,[?]',
		'username' => 'required|user_usernamecheck'

 	);

 	protected $messages = array(
		'user_emailcheck'    => 'Email address already exists in our database.',
		'user_usernamecheck' => 'Username already exists in our database.'

 	);


 	function __construct()
 	{
 		$this->userObject = new User;
 	}

 	public function create($data = array())
 	{
 		// initiate validation.
 		$validation = Validator::make($data, $this->rules, $this->messages);

 		if ($validation->fails())
 		{
 			return array(
 				'status' => 0,
 				'errors' => $validation->errors->all()
 			);
 		}

 		$billingData = NULL;

 		if (isset($data['contact']))
 		{
 			$contactData = $data['contact'];
 			unset($data['contact']);
 		}
 		else
 		{
 			// save the contacts data.
 			$contactData = $data['contacts'];

 			// remote it from main array.
 			unset($data['contacts']);
 		}

 		if (isset($data['bill']))
 		{
 			$billingData = $data['bill'];
 			unset($data['bill']);
 		}


 		$result = $this->user()->set_data($data);
 		$result->save();


 		// data roles.
		if (isset($data['roles']))
		{
			$roles = $data['roles'];
			unset($data['roles']);
		}
		else
		{
 			$roles = (array) Role::getAdminRoleId();	
		}

		// end data roles.

 		$result->roles()->sync($roles);

		// assign primary value to contact.
		$contactData['primary'] = 1;

		// create the primary contact.
		$result->contacts()->save(array($contactData));


		if (!$billingData)
		{
			$result->createDefaultBillingProfile();
		}
		else
		{
			$result->createDefaultBillingProfile();
			$result->updateBilling($billingData);
		}

 		return array(
 			'status' => 1,
 			'id'	 => $result->id

 		);
 	}


 	public function modify($id = 0, $data = array(), $validate = TRUE)
 	{

 		$mRules = $this->rules;
 		unset($mRules['username']);

 		$rules = str_replace('[?]', $id, $mRules);

 		// initiate validation.
 		$validation = Validator::make($data, $rules, $this->messages);

 		if ($validate)
 		{
 			if ($validation->fails())
 			{
	 			return array(
					'status'   => 0,
					'modified' => 0,
					'errors'   => $validation->errors->all()
	 			);
 			}
 		}
		
		$roles = NULL;
		$contact = NULL;

		if (isset($data['contact']))
		{
			$contact = $data['contact'];
			unset($data['contact']);
		}

		if (isset($data['roles']))
		{
			$roles = $data['roles'];
			unset($data['roles']);
		}

 		$result = $this->user()->find($id);

 		if (!empty($data))
 		{
 			$result->set_data($data);

 			$result->save();		
 		}
 
		// manage roles 		
 		if ($roles)
 		{
 			$result->roles()->sync($roles);
 		}

 		if ($contact)
 		{
 			$primaryContact = $result->primary();

 			if (!$primaryContact)
 			{
 				// assign primary value to contact.
				$contact['primary'] = 1;

				// create the primary contact.
				$result->contacts()->save(array($contact));
 			}
 			else
 			{
 				$result->primary()->set_data($contact)->save();
 			}
 		}
 			
 		return array(
			'status'   => 1,
			'modified' => 1,
			'user_id'  => $result->id,
			'profile'  => $result
		);
 	}


 	public function modifyBilling($id = 0, $data = array())
 	{

 		$user = $this->user()->find($id);

 		if (!$user)
 		{
 			return FALSE;
 		}

 		if (!$user->hasBill())
 		{
 			$user->createDefaultBillingProfile();
 		}

 		// get primary bill profile.
 		$userBilling = $user->bill();

 		// set data to update.
 		$userBilling->set_data($data)->save();

 		return TRUE;
 	}


 	public function user()
 	{
 		return $this->userObject;
 	}

 	public static function forge()
 	{
 		return $instance = new static;
 	}



 }


 Validator::register('user_emailcheck', function($attribute, $value, $parameters)
{
	$value = strtolower(trim($value));

	// check for email.
	$found = UserManager::forge()->user()->where('email', '=', $value)->first();

	if (!$found)
	{
		return TRUE;
	}


	return FALSE;


});

  Validator::register('user_usernamecheck', function($attribute, $value, $parameters)
{
	$value = strtolower(trim($value));

	// check for email.
	$found = UserManager::forge()->user()->where('username', '=', $value)->first();

	if (!$found)
	{
		return TRUE;
	}

	return FALSE;

	
});