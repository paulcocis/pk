<?php

class Permission extends EloquentBase
{

	/**
	 * Accessible
	 *
	 * @var array
	 */
	public static $accessible = array('name', 'description');

	/**
	 * Roles
	 *
	 * @return object
	 */
	public function roles()
	{
		return $this->has_many_and_belongs_to('Role', $this->prefix.'permission_role');
	}

}