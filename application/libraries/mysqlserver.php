<?php

class MysqlServer {

	/**
	 * Check if mySQL server is alive.
	 * @return boolean
	 */
	public function alive()
	{
		// get mySQL connection.
		$connection = Config::get('database.connections.mysql');

		$alive = @mysql_connect($connection['host'].':'.$connection['port'], $connection['username'], $connection['password']);
		
		if (!$alive)
		{
			return FALSE;
		}

		return TRUE;
	}

}