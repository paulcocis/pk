<?php

class ODate {

	public function getRange($months = array())
	{
		// get months
		$months = (array) $months;

		if (empty($months))
		{
			$months = ODate::pastMonths();
		}

		$start = strtotime('2013-01-01');
		$now   = time();

		// get first monday.
		$monday = Odate::firstMonday('01', '2013');

		$i = true;

		while ($i)
		{
			$range  = strtotime('+6 days', $monday);
			$data[] = array('start' => date('Y-M-d',$monday), 'end' => date('Y-M-d',$range));

			// set next monday.
			$monday = strtotime('+7 days', $monday);

			if ($monday > $now)
			{
				$i = FALSE;
			}
		}

		return $data;
	}

	protected function firstMonday($month,$year)
	{
		$num = date('w',mktime(0,0,0,$month,1,$year));

		if($num>1)
			$date = date('Y-M-d H:i:s',mktime(0,0,0,$month,1,$year)+(86400*(8-$num)));
		else
			$date = date('Y-M-d H:i:s',mktime(0,0,0,$month,1,$year));

		return strtotime($date);
	}

	/**
	 * Generate month week range.
	 *
	 * @param  array  $months
	 * @return array
	 */
	public function weekDropdown($months = array(), $desc = TRUE)
	{
		$months = (array) $months;

		if (empty($months))
		{
			$months = ODate::pastMonths();
		}

		foreach($months as $month)
		{
			$firt_day = null;
			$end_day  = null;
			$weeks    = null;
			// get the month weeks.
			$weeks = ODate::monthWeeks($month);

			for ($i = 1; $i <= $weeks; $i++)
			{
				if ($i == 1)
				{
					$firt_day = $i;
					$end_day  = 7;
				}
				else
				{
					$firt_day = $end_day + 1;
					$end_day  = $firt_day + 7;
				}

				if ($end_day > ODate::monthDays($month))
				{
					$end_day = ODate::monthDays($month);
				}

				$date_start = date('Y').'-'.$month.'-'.$firt_day;
				$date_end   = date('Y').'-'.$month.'-'.$end_day;

				$timestamp_start = strtotime($date_start);
				$date_start_nice = date('Y-M-d', $timestamp_start);

				$timestamp_end = strtotime($date_end);
				$date_end_nice = date('Y-M-d', $timestamp_end);

				$range = $date_start.'|'.$date_end;
				$range_nice = $date_start_nice.' - '.$date_end_nice;

				$list[$month][$i] = array('start' => $firt_day, 'end' => $end_day, 'month' => $month, 'year' => date('Y'), 'range' => $range, 'range_nice' => $range_nice);
			}
		}

		// descendent sorting
		if ($desc)
		{
			return array_reverse($list, true);
		}

		return $list;
	}

	/**
	 * Get the month weeks.
	 *
	 * @param  string $month
	 * @param  mixed $year
	 * @return numeric
	 */
	public function monthWeeks($month = '1', $year = null)
	{
		if (is_null($year))
		{
			$year = date('Y');
		}

		// get weeks number.
		$weekNumber = cal_days_in_month(CAL_GREGORIAN, $month, $year);

		$total = $weekNumber/7;
		return round($total, 0);
	}

	/**
	 * Get month days.
	 * @param  string $month
	 * @param  mixed  $year
	 *
	 * @return string
	 */
	public function monthDays($month = '1', $year = null)
	{
		if (is_null($year))
		{
			$year = date('Y');
		}

		// get weeks number.
		$number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		return $number;
	}
	/**
	 * Get the months until current date.
	 *
	 * @return array Month List.
	 */
	public function pastMonths()
	{
		$yearMonths = 12;
		$currentMonth = date('m');

		// first month.
		if ($currentMonth == '1')
		{
			return array($currentMonth);
		}

		$months = array();

		$i = 1;
		while ($i <= $currentMonth)
		{
			array_push($months, $i);
			$i++;
		}

		return $months;
	}

	public function lastWeek()
	{
		// get last week monday.
		$lastWeekMonday = strtotime("-2 monday midnight");

		// get last day of week.
		$endLastWeek = strtotime("+6 days", $lastWeekMonday);

		return date('Y-m-d', $lastWeekMonday).'|'.date('Y-m-d', $endLastWeek);		
	}

	public function currentWeek()
	{
		// get current week monday.
		$currentWeekMonday = strtotime("-1 monday midnight");

		// get last day of week.
		$endWeek = strtotime("+6 days", $currentWeekMonday);
	
		return date('Y-m-d', $currentWeekMonday).'|'.date('Y-m-d', $endWeek);
	}
}

/*
function week()
{
	$d = new DateTime();
	$weekday = $d->format('w');
	$diff = 7 + ($weekday == 0 ? 6 : $weekday - 1); // Monday=0, Sunday=6
	$d->modify("-$diff day");
	echo $d->format('Y-m-d') . ' - ';
	$d->modify('+6 day');
	echo $d->format('Y-m-d');
}*/