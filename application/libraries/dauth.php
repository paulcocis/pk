<?php

/**
 * Verification Library
 *
 * @author Todd Francis
 * @version 3.0.1
 */
class Dauth extends Laravel\Auth\Drivers\Driver
{

	/**
	 * __construct
	 */
	public function __construct()
	{
		parent::__construct();

		// Populate the user variable
		$this->user();
	}


	public static function instance()
	{
		return new Dauth;
	}


	/**
	 * Get the current user of the application.
	 *
	 * If the user is a guest, null should be returned.
	 *
	 * @param  int         $id
	 * @return mixed|null
	 */
	public function retrieve($id)
	{
		if (filter_var($id, FILTER_VALIDATE_INT) !== false)
		{
			return $this->model()->find($id);
		}
	}

	/**
	 * Attempt to log a user into the application.
	 *
	 * @param  array  $arguments
	 * @return void
	 */
	public function attempt($arguments = array())
	{
		$valid = false;

		// Get the username fields
		$usernames = Config::get('verify.username');
		$usernames = (!is_array($usernames))
			? array($usernames)
			: $usernames;


		foreach ($usernames as $identify_by)
		{

			$user = $this->model()
				->where($identify_by, '=', array_get($arguments, 'username'))
				->first();

			if (!is_null($user))
			{
				// Is user password is valid?
                if(!Hash::check($user->salt . array_get($arguments, 'password'), $user->password))
                {
                    throw new UserPasswordIncorrectException('User password is incorrect');
                }

                if (isset($arguments['roles']) AND !empty($arguments['roles']))
                {
                	$roles = (array) $arguments['roles'];

                	if (!$user->is($roles))
                	{
                		throw new \UserRoleException('You are not allowed to this section.');
                	}
                }

				// Valid user, but are they verified?
				if (!$user->verified)
				{
					throw new UserUnverifiedException('User is unverified');
				}

				// Is the user disabled?
				if ($user->disabled)
				{
					throw new UserDisabledException('User is disabled');
				}

				// Is the user deleted?
				if ($user->deleted)
				{
					throw new UserDeletedException('User is deleted');
				}

				$valid = true;
				break;
			}
		}

		if ($valid)
		{
			// auth now.
			$login = $this->login($user->get_key(), array_get($arguments, 'remember'));

			// update last login.
			$this->updateLastLogin($user->get_key());

			return $login;
		}

		else
		{
			throw new UserNotFoundException('User can not be found');
		}
	}


	protected function updateLastLogin($user_id = 0)
	{
		$user = User::find($user_id);

		if (!is_null($user))
		{
			$user->last_login_at = date('Y-m-d H:i:s');
			$user->last_login_ip = Request::ip();

			$user->save();

			return TRUE;
		}

		return FALSE;
	}



	/**
	 * Get a fresh model instance.
	 *
	 * @return Eloquent
	 */
	protected function model()
	{
		$model = Config::get('verify.user_model');

		return new $model;
	}

	/**
	 * Is the User a Role
	 *
	 * @param  array|string  $roles A single role or an array of roles
	 * @param  object|integer|null  $user  Leave null for current logged in user, or pass a User ID/User object
	 * @return boolean
	 */
	public function is($roles, $user = NULL)
	{
		$user = $this->get_user($user);

		if (!$user)
		{
			return false;
		}

		return $user->is($roles);
	}

	/**
	 * Can the User do something
	 *
	 * @param  array|string $permissions Single permission or an array or permissions
	 * @param  object|integer|null  $user  Leave null for current logged in user, or pass a User ID/User object
	 * @return boolean
	 */
	public function can($permissions, $user = NULL)
	{
		$user = $this->get_user($user);

		if (!$user)
		{
			return FALSE;
		}

		return $user->can($permissions);
	}

	/**
	 * Is the User a certain Level
	 *
	 * @param  integer $level
	 * @param  string $modifier [description]
	 * @param  object|integer|null  $user  Leave null for current logged in user, or pass a User ID/User object
	 * @return boolean
	 */
	public function level($level, $modifier = '>=', $user = NULL)
	{
		$user = $this->get_user($user);

		if (!$user)
		{
			return FALSE;
		}

		return $user->level($level, $modifier);
	}

	/**
	 * Get a user
	 *
	 * @param  object|integer|null  $user  Leave null for current logged in user, or pass a User ID/User object
	 * @return object|null
	 */
	private function get_user($user = NULL)
	{
		if (!is_null($user))
		{
			// Are we passed a user ID?
			if (is_numeric($user))
			{
				$user = $this->retrieve($user);
			}
			// If $user isn't an object, we don't want it
			else if (!is_object($user))
			{
				$user = NULL;
			}
		}
		else
		{
			// Use currently logged in user
			$user = $this->user;
		}

		return $user;
	}


	public function is_account_locked($user = 0)
	{
		if (!is_object($user))
		{
			$user = User::find($user);
		}

		if ($user->status == 'locked')
		{
			return true;
		}

		return false;
	}


	/**
	 * Determine if the user is logged in.
	 *
	 * @return boolean
	 */
	public function is_logged($roles = array())
	{
		$logged_in = $this->check();

		if (!$logged_in)
		{
			return FALSE;
		}

		// check against roles
		if (!empty($roles))
		{
			foreach(Auth::user()->get_role_names() as $role_name)
			{
				if (in_array($role_name, $roles))
				{
					return TRUE;
				}
			}

			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Determine if the user is logged in.
	 *
	 * @return boolean
	 */
	public function logged_in($roles = array())
	{
		return $this->is_logged($roles);
	}

	/**
	 * Returns the current user id if loged in or (bool) FALSE.
	 *
	 * @return mixed
	 */
	public function id()
	{
		if ($this->logged_in())
		{
			return Auth::user()->id;
		}

		return FALSE;
	}

	/**
	 * Shadow a user account.
	 *
	 * @param  integer $id
	 * @param  string  $field
	 * @return boolean
	 */
	public function shadow($id = 0, $field = 'user_shadow_id')
	{
		if ($this->logged_in())
		{
			Session::put($field, $this->id());
			$this->login($id);

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Returns the shadow id.
	 *
	 * @return mixed
	 */
	public function getShadowId()
	{
		return Session::get('user_shadow_id', FALSE);
	}

	/**
	 * Validate roles on authentification wizard.
	 *
	 * @param  object $user
	 * @param  array  $roles
	 * @return boolean
	 */
	protected function valid_role($user, $roles = array())
	{

		if (!is_object($user))
		{
			return FALSE;
		}

		foreach($user->roles as $role)
		{
			if (in_array($role->name, $roles))
			{
				return TRUE;
			}
		}

		return FALSE;
	}


}

class UserNotFoundException extends Exception {};
class UserUnverifiedException extends Exception {};
class UserDisabledException extends Exception {};
class UserDeletedException extends Exception {};
class UserRoleException extends Exception {};
class UserPasswordIncorrectException extends Exception {};
