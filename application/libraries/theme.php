<?php

class Theme {


	/**
	* The theme path inside root path.
	* @var string
	*/
	protected $theme_path      = '';

	/**
	* The theme root path
	* @var string
	*/
	protected $theme_root_path = '';

	/**
	* The current theme name.
	* @var string
	*/
	protected $theme_name      = '';

	/**
	 * Theme data.
	 * @var array
	 */
	protected $theme_data      = array();

	/**
	* Array with all specified partials.
	* @var array
	*/
	protected $partials        = array();

	/**
	* The theme partials path.
	* @var string
	*/
	protected $partials_path   = 'partials/';


	/**
	* The current layout name.
	* @var string
	*/
	protected $layout          = 'default';

	/**
	* Theme layouts path.
	* @var string
	*/
	protected $layout_path     = 'layouts/';

	/**
	* Custom javascript code to inject.
	* @var array
	*/
	protected $custom_js_code  = array();

	/**
	* List of all assets.
	* @var array
	*/
	protected $assets          = array();

	/**
	 * The assets path.
	 * @var string
	 */
	protected $assets_path     = 'assets/';


	final public function __construct($theme_name = '', $config = array())
	{

		// set the theme name is pass.
		$this->setTheme($theme_name);

		// get the config.
		$config = $this->getConfig($config);

		// set the config if not passed.
		if (!empty($config))
		{
			foreach($config as $key => $value)
			{
				$this->{$key} = $value;
			}
		}

	}

	public static function forge($theme = '', $config = array())
	{
		static $themeInstance = NULL;

		// create the instance if null.
		if ($themeInstance === NULL)
		{
			$themeInstance = new static($theme, $config);
		}

		return $themeInstance;
	}


	public function getConfig($config = array())
	{
		// merge them
		$config = array_merge(\Config::get('theme'), $config);
		return $config;
	}


	/**
	 * Set the current theme name.
	 *
	 * @param string $name [description]
	 * @return \Illuminati\Component\Theme
	 *
	 */
	public function setTheme($name = '')
	{
		$this->theme_name = $name;
		return $this;
	}

	public function setLayout($layout = '')
	{
		$this->layout = $layout;
		return $this;
	}

	public function set($key, $value = '')
	{

		if (is_array($key))
		{
			foreach($key as $k => $v)
			{
				$this->theme_data[$k] = $v;
			}
		}
		else
		{
			$this->theme_data[$key] = $value;
		}

		return $this;
	}

	/**
	 * Set the theme root path.
	 *
	 * @param string $path
	 * @return \Illuminati\Component\Theme
	 */
	public function setThemeRootPath($path = '')
	{
		$this->theme_root_path = $path;
		return $this;
	}

	/**
	 * Get the theme root path.
	 * @return string
	 */
	public function getThemeRootPath()
	{
		return $this->theme_root_path;
	}

	/**
	 * Set the theme path.
	 * @param string $path
	 */
	public function setThemePath($path = '')
	{
		$this->theme_path = $this->getThemeRootPath().$path;
		return $this;
	}

	/**
	 * Get the theme path.
	 * @return string
	 */
	public function getThemePath()
	{
		return $this->getThemeRootPath().$this->theme_path.$this->theme_name.DS;
	}

	/**
	 * Get the theme assets path.
	 * @return string
	 */
	public function getAssetPath()
	{
		return $this->getThemePath().$this->assets_path;
	}

	/**
	 * Get theme partials path.
	 * @return string
	 */
	public function getPartialsPath()
	{
		return $this->getThemePath().$this->partials_path;
	}

	/**
	 * Get theme layout path.
	 * @return string
	 */
	public function getLayoutPath()
	{
		return $this->getThemePath().$this->layout_path;
	}

	/**
	 * Get the assets url
	 * @return string
	 */
	public function getAssetUrl()
	{
		$url = URL::base();
		return $url.'/'.$this->theme_path.$this->theme_name.'/'.$this->assets_path;
	}


	/**
	 * Add new theme partial.
	 *
	 * @param  string $partial
	 * @param  array  $data
	 * @return object $this
	 */
	public function addPartial($partial = '', $data = array())
	{
		$this->partials[$partial]   = $data;
		$this->theme_data[$partial] = $data;

		return $this;
	}

	/**
	 * Add custom javascript.
	 *
	 * @param script $script
	 * @param string $unique_key
	 */
	public function addCustomJs($script, $unique_key = '')
	{
		$this->custom_js_code[$unique_key] = $script;
	}

	/**
	 * Add new asset.
	 *
	 * @param  string $filename
	 * @param  string $location
	 * @return object $this
	 */
	public function addAsset($filename, $group = 'general',  $force_css_in_js = FALSE)
	{
		// get the asset location.
		$location = $this->getAssetUrl();

		// add group if not exists.
		if (!in_array($group, $this->assets))
		{
			$this->assets[] = $group;
		}

        if(ends_with($filename, ".js"))
        {
            $asset_file_path = $location . "js/" . $filename;
        }

        if(ends_with($filename,".css"))
        {
        	// a simple trick for css files that are located in js folder.
        	$css_folder_name = ($force_css_in_js === TRUE) ? 'js/' : 'css/';
            $asset_file_path = $location.$css_folder_name.$filename;
        }

        Asset::container($group)->add($filename, $asset_file_path);
        return $this;

	}

    public function composer($views, $composer) {

        $views = (array) $views;

		$theme_name    = $this->theme_name;
		$theme_path    = $this->getThemePath();
		$partials_path = $this->getPartialsPath();

        foreach ($views as $view)
        {
            if (file_exists($tpath = $partials_path.$view. EXT))
            {
                $view = "path: " . $tpath;
            }
            elseif (file_exists($tpath = $partials_path.$view. BLADE_EXT))
            {
                $view = "path: " . $tpath;
            }

       		View::composer($view, $composer);
        }
    }

	public function fetch($page, $data = null)
	{
		try
		{
			$result = $this->_fetch($page, $data);
			return $result;
		} catch(ErrorException  $e)
		{
			echo $e->getMessage();
		}
	}

	public function display($page, $data = null)
	{
		echo $this->fetch($page, $data);
	}

	protected function _fetch($page, $data = null)
	{
		if (is_array($data))
		{
			foreach($data as $key => $value)
			{
				$this->theme_data[$key] = $value;
			}
		}

		// get the theme path.
		$theme_path = $this->getThemePath();

		// get layout path.
		$layout_path = $this->getLayoutPath();

		// get layout absolute path.
		$layout_absolute_path = $layout_path . $this->layout;

		if (file_exists($path = $layout_absolute_path.EXT))
		{
			$view = View::make("path: " . $path);
		}
		else
		{
			$path = $layout_absolute_path.BLADE_EXT;
			$view = View::make("path: " . $path);
		}

		// init function here.
		$this->initThemeFunc();

		$theme = array();

		foreach($this->assets as $group)
		{
			$theme['asset_'.$group]['scripts'] = Asset::container($group)->scripts();
			$theme['asset_'.$group]['styles']  = Asset::container($group)->styles();
		}

		$theme['theme_name'] = $this->theme_name;
		$theme['custom_js']  = $this->parseCustomJs();
		$theme['asset_url']  = $this->getAssetUrl();


		if ($view)
		{
			// share theme.
			View::share('theme_data', $theme);
			View::share('images_url', $this->getAssetUrl().'images/');

			// get them data.
			$theme_data = $this->theme_data;

			// load partials.
			$view = $this->loadPartials($view);

			// theme content.
			$view->nest('theme_content', $page, $theme_data);

			if (!empty($theme_data))
			{
				foreach($theme_data as $key => $value)
				{
					$view->$key = $value;
				}
			}

			return $view;
		}

		return FALSE;
	}

     private function parseCustomJs()
     {
         if(is_array($this->custom_js_code))
         {
             $script = implode("\n",$this->custom_js_code);

             $script = "<script type='text/javascript'>
                        $script
                        </script>";
             return $script;
         }

         return FALSE;
     }


	/**
	 * Render a partial.
	 *
	 * @param  string $partial
	 * @param  array  $data
	 * @return mixed
	 */
	public function renderPartial($partial, $data = array())
	{
		$partials   = $this->partials;

		// get theme path.
		$theme_path = $this->getThemePath();

		// get partials path.
		$partials_path = $this->getPartialsPath();

		// partials names array.
		$theme_partials = array();

		if (!empty($partials))
		{
			$theme_partials = array_keys($partials);
		}

		if (in_array($partial, $theme_partials))
		{
			// get partial data.
			$partial_data = $partials[$partial];

			if (!empty($partial_data))
			{
				$data = array_merge($partial_data, $data);
			}

			if (file_exists($path = $partials_path.$partial.EXT))
			{
				return  View::make("path: " . $path, $data);
			}
			else
			{
				$path = $partials_path.$partial.BLADE_EXT;
				return View::make("path: " . $path, $data);
			}
		}
		else
		{
			if (\View::exists($partial))
			{
				return render($partial, $data);
			}
			else
			{
				if (file_exists($path = $partials_path.$partial.EXT))
				{
					return  View::make("path: " . $path, $data);
				}
				else
				{
					$path = $partials_path.$partial.BLADE_EXT;
					return View::make("path: " . $path, $data);
				}
			}

		}

		return FALSE;
	}

	protected function loadPartials(&$viewObject)
	{
		if (!empty($this->partials))
		{
			// load partials.
			$partials  = $this->partials;

			// load theme path.
			$partialsPath = $this->getPartialsPath();

			foreach($partials as $partial => $partial_data)
			{
				// get them data.
				$theme_data = $this->theme_data;

				// get all data.
				$data = array_merge($theme_data, $partial_data);

				// get the partial name.
				$partial_name = 'partial_'.$partial;

				$partial_path = $partialsPath . $partial.'.php';

				if (!file_exists($partial_path))
				{
					$partial_path = $partialsPath . $partial . BLADE_EXT;
				}

				if (!empty($data))
				{
					$viewObject->nest($partial_name, "path: ".$partial_path, $data);
				}
				else
				{
					$viewObject->nest($partial_name, "path: ".$partial_path);
				}
			}
		}

		return $viewObject;
	}

	protected function initThemeFunc($path = null)
	{
		// get theme path.
		$path = $path ?: $this->getThemePath();

		// get the theme name.
		$theme_name     = strtolower($this->theme_name);

		// theme function.
		$theme_function = 'theme_'.$theme_name;


		// absolute script path.
		$script_path = $path.'theme_function.php';

		if (file_exists($script_path))
		{
			require_once($script_path);

			if (function_exists($theme_function))
			{
				call_user_func($theme_function, $this);
			}
		}
	}

}


// Theme Helpers
//
function render_partial($partial, $data = array())
{
    $theme = IoC::resolve('Theme');
    return $theme->renderPartial($partial, $data);
}
