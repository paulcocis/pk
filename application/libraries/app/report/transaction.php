<?php namespace App\Report;

use App\Billing\Transaction as BillingTransaction;

class Transaction {

	public function salesSummaryByPeriod($start = '', $end = '')
	{
		// create model instance.
		$query = new BillingTransaction;

		$result = $query->where(\DB::raw('DATE(created_at)'), '>=', $start)
			  ->where(\DB::raw('DATE(created_at)'), '<=', $end)
			  ->where('status', '=', 'approved')
			  ->get();
		
		$total            = '0.00';
		$total_Shipping   = '0.00';
		$total_tax        = '0.00';
		$total_commission = '0.00';

		foreach($result as $row)
		{
			// get the order informations.
			$order = $row->order();


			if ($order)
			{
				foreach ($order->products()->get() as $ProductRow)
			    {
					$total          += $ProductRow->simple_cost();
					$total_Shipping += $ProductRow->shipping_amount;
					$total_tax      += $ProductRow->tax;

					// lets get the commision now.


					if ($ProductRow->deal() AND $ProductRow->deal()->user())
					{
						$commission = $ProductRow->deal()->user()->bill()->comission_rate;
						$total_commission += $commission;
					}
			   }


			}

		}

		$gross      = number_format($total, 2);
		$net        = number_format($gross - $total_Shipping, 2);
		$tax        = number_format($total_tax,2);
		$commission = number_format($total_commission, 2);

		return array(
			'gross'      => $gross,
			'net'        => $net,
			'tax'        => $tax,
			'shipping'   => number_format($total_Shipping,2),
			'commission' => $commission
		);
	}


}