<?php namespace App\Setup;

class Settings extends \App\App {

	public static $table = 'settings';

	public static $timestamps = true;

	public static function forge()
	{
		return new static;
	}

	public function modify($data = array())
	{
		if ($this->count())
		{
			$row = $this->first();
			$row->set_data($data)->save();
		}
		else
		{
			$this->set_data($data);
			$this->save();
		}

		return TRUE;
	}

	public function retrieve()
	{
		if (!$this->count())
		{	
			$this->save();
		}

		return $this->first();
	}

	public function rate()
	{
		return $this->retrieve()->commission_rate;
	}


}

