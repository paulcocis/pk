<?php namespace App\Setup;

class Store extends \App\App {

	public static $table = 'settings_store';

	public static $timestamps = true;


	public function modify($data = array())
	{
		if ($this->count())
		{
			$row = $this->first();
			$row->set_data($data)->save();
		}
		else
		{
			$this->set_data($data);
			$this->save();
		}

		return TRUE;
	}

	public function retrieve()
	{
		if (!$this->count())
		{	
			$this->save();
		}

		return $this->first();
	}


}

