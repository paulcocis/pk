<?php namespace App\Setup;

class PaymentGateway extends \App\App {

	public static $table = 'settings_gateways';

	public static $timestamps = true;


	protected $validationRules = array(
		'name'         => 'required|gw_name',
		'display_name' => 'required'

	);

	protected $validationMessages = array(
		'gw_name' => 'This gateway name already exists in database, please choose another.'

	);


	public function add($data = array())
	{
		$validator = \Validator::make($data, $this->validationRules, $this->validationMessages);

		// ops.
		if ($validator->fails())
		{
			return $validator->errors->all();
		}

		// insert data.
		$this->set_data($data)->save();
		return TRUE;
	}

}


\Validator::register('gw_name', function($attribute, $value, $parameters)
{
	$value = trim(strtolower($value));

	$found = PaymentGateway::where('name', '=', $value)->count();

	if (!$found)
	{
		return TRUE;
	}

	return FALSE;
	
});