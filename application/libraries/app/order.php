<?php namespace App;


 use App\Order\Product as Product;

class Order extends \App\App {

	public static $table = 'orders';

	public static $timestamps = true;


	public function delete()
	{	
		// delete product options.
		foreach($this->products()->get() as $product)
		{
			$product->deleteOptions();
		}

		// delete the order products.
		$this->products()->delete();

		// delete the order.
		parent::delete();
	}

	/**
	 * Retrieve customer informations.
	 */
	public function user()
	{
		return \User::find($this->customer_id);
	}

	public function sellerInvoice()
	{
		return \App\SellerInvoice::where('order_id', '=', $this->id);
	}

	public function regenerateSellerInvoices()
	{
		return \App\SellerInvoice::instance()->regenerate($this->id);
	}


	public function getAvailablePromotions()
	{
		$cart_id = $this->cart_id;

		$promotions = NULL;

		if ($cart_id AND !empty($cart_id))
		{
			$check = \Cart\Promotion::where('cart_id', '=', $cart_id)->get();

			if ($check)
			{
				foreach($check as $promo)
				{
					$promotions[] = \App\Promotion::find($promo->promotion_id);
				}
			}
		}

		return $promotions;
	}



	/**
	 * Retrieve order comment class instance.
	 */
	public function comment()
	{
		return new \App\Order\Comment;
	}

	public function status()
	{
		$status = $this->status;

		switch($status)
		{	
			// case paid.
			case 'paid': $status = 'Order Payment Complete'; break;

			// case unpaid
			case 'unpaid': $status = 'Waiting For Order Payment'; break;
			
			// case refunded
			case 'refunded': $status = 'Order Refunded'; break;
		}

		return trim($status);
	}


	/**
	 * Retrieve order comments.
	 */
	public function comments()
	{
		return $obj = \App\Order\Comment::where('order_id', '=', $this->id);
	}

	/**
	 * Get shipping user informations.
	 */
	public function shipping_user()
	{
		return \Contacts::find($this->shipping_contact_id);
	}

	public function transactions()
	{
		return \App\Billing\Transaction::where('order_id', '=', $this->id);
	}

	/**
	 * Get first order payment transaction.
	 */
	public function firstPaymentTransaction()
	{
		return \App\Billing\Transaction::where('order_id', '=', $this->id)->where('status','=','approved')->first();
	}

	/**
	 * Get refund transactions.
	 */
	public function getRefundTransactions()
	{
		return \App\Billing\Transaction::where('order_id', '=', $this->id)->where('status','=','refunded')->get();
	}

	/**
	 * Get refund transactions amount.
	 */
	public function getRefundTransactionsAmount()
	{
		$amount = '0.00';

		$list = \App\Billing\Transaction::where('order_id', '=', $this->id)->where('status','=','refunded')->get();

		if ($list)
		{
			foreach($list as $row)
			{
				$amount += $row->amount_out;
			}
		}

		return number_format($amount,2);
	}

	/**
	 * Get all refunded amount.
	 */
	public function refundAmount()
	{	
		return $this->getRefundTransactionsAmount();
	}

	public function maxOrderRefundAmount()
	{
		return $this->totalCost() - $this->refundAmount();
	}

	public function removeRefundedTransactions()
	{
		return $this->transactions()->where('status', 'refunded')->delete();
	}


	public function isRefundAllowed()
	{
		if ($this->maxOrderRefundAmount() > 0)
		{
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Generate an order refund.
	 */
	public function create_refund($data = array())
	{

		$totalOrderAmount = $this->totalCost();

		$totalOrderRefund = $this->refundAmount() + $data['amount'];

		// ops we dont want to refund much that maximum order.
		if ($totalOrderRefund > $totalOrderAmount)
		{	
			$maxRefund = $this->maxOrderRefundAmount();

			throw new \Exception('Refund amount cannot be higher that order amount. Maximum amount that you can refund now is '.$maxRefund);			
		}

		if (!isset($data['custom_invoice_notice']))
		{
			$data['custom_invoice_notice'] = $data['refund_reason'];
		}

		// New refund transaction data.
		$newRefundRecord = array(
			
			'order_id'      => $this->id,
			'amount_out'    => $data['amount'],
			'gateway_id'    => $this->gateway_id,
			'status'        => 'refunded',
			'user_id'       => $this->customer_id,
			'refund_reason' => $data['refund_reason'],
			'custom_invoice_notice' => $data['custom_invoice_notice']

		);

		// create transaction instance.
		$transaction = new \App\Billing\Transaction;

		// create new transaction.
		$transaction->set_data($newRefundRecord)->save();

		// update order details.
		$this->status         = 'refunded';
		$this->last_refund_at = date('Y-m-d H:i:s');

		// save updates.
		$this->save();

		// regenerate.
		$this->regenerateSellerInvoices();

		// return new transaction details.
		return $transaction;
	}


	public function products()
	{
		return \App\Order\Product::where('order_id', '=', $this->id);
	}

	public function gateway()
	{
		return \App\Setup\PaymentGateway::find($this->gateway_id);
	}

	public function schedule($status = 'shipped' , $date = FALSE, $message = NULL, $sendEmail = TRUE)
	{
		if (!$date)
		{
			$date = date('Y-m-d');
		}

		$this->shipping_status       = $status;
		$this->scheduled_shipping_at = $date;
		$this->shipping_message      = $message;

		// save changes.
		$this->save();

		// Attempt to delivery shipping mail confirmation.
		if ($status == 'shipped' AND $sendEmail)
		{
			\App\Message\Message::forge('order')->shippingScheduled($this->id);
		}
	}

	public function cancelShipping()
	{
		$this->shipping_status       = 'unshipped';
		$this->scheduled_shipping_at = NULL;
		$this->shipping_message      = NULL;

		$this->save();
	}

	public function changeGateway($new_gateway_id = 0)
	{
		$this->gateway_id = $new_gateway_id;
		return $this->save();
	}

	public function pk_earning()
	{
		$cost = $this->totalCost(TRUE);
		
		$allSellerInvoices = \App\SellerInvoice::where('order_id', '=', $this->id)->get();

		$sellerAmount = 0;

		foreach($allSellerInvoices as $invoice)
		{
			$sellerAmount += $invoice->amount;
		}

		// calculate difference.
		$earning = $cost - $sellerAmount;
		return number_format($earning,2);

	}

	public function finance()
	{	
		// create order finances array.
		$OrderFinances = array();

		$OrderFinances = array(
			'orderAmount'    => $this->totalCost(false),
			'taxAmount'   	 => $this->totalTaxAmount(),
			'shippingAmount' => $this->totalShippingAmount(),
		);

		$totalOrderAmount = $this->totalCost(true);

		// get the rate.
		$rate = \App\Setup\Settings::forge()->rate();

		// get the pinke promise earnings.
		$earn = $this->pk_earning();

		$OrderFinances['totalOrderAmount'] = $totalOrderAmount;
		$OrderFinances['pk_earn'] = $earn;

		return (object) $OrderFinances;
	}


	public function totalCost($apply_tax_and_shipping = TRUE)
	{	
		$totalCost = '0.00';

		if ($this->products()->count())
		{
			foreach($this->products()->get() as $p)
			{
				$totalCost += $p->simple_cost();
			}
		}

		if ($apply_tax_and_shipping)
		{
			$totalCost = $totalCost + ($this->totalTaxAmount() + $this->totalShippingAmount());
		}


		return number_format($totalCost,2);
	}

	public function shipped()
	{
		return ($this->shipping_status === 'shipped');
	}

	public function totalTaxAmount()
	{	
		$totalCost = '0.00';

		if ($this->products()->count())
		{
			$totalCost = $this->products()->sum('tax');
		}

		return number_format($totalCost,2);
	}

	public function totalShippingAmount()
	{
		$totalCost = '0.00';

		if ($this->products()->count())
		{
			$totalCost = $this->products()->sum('shipping_amount');
		}

		return number_format($totalCost,2);
	}

	public function totalDiscountAmount()
	{
		$totalCost = '0.00';

		if ($this->products()->count())
		{
			$totalCost = $this->products()->sum('discount_amount');
		}

		return number_format($totalCost,2);
	}	

	public function retrieve($pk = FALSE)
	{
		$Object = new Order;

		if ($pk)
		{
			$orders = NULL;

			$p = Product::where('pk_item', '=', 1)->group_by('order_id')->get();

			if ($p)
			{
				foreach($p as $row)
				{
					$orders[] = Order::find($row->order_id);
				}
			}

			return $orders;
		}
		else
		{
			$orders = NULL;

			$p = Product::where('pk_item', '=', 0)->group_by('order_id')->get();

			if ($p)
			{
				foreach($p as $row)
				{
					$orders[] = Order::find($row->order_id);
				}
			}			
		}

		return $orders;
	}

	public function available($pk = FALSE)
	{
		$Object = new Order;

		if ($pk)
		{
			$Object = $Object->where('pk_order', '=', 1);
		}
		else
		{
			$Object = $Object->Where('pk_order', '!=', 1);
		}

		return $Object->count();
	}


	public function sendPaymentConfirmationEmail()
	{
		if ($this->products()->count())
		{
			// get products.
			$products = $this->products()->get();

			$physicalProducts     = array();
			$downloadableProducts = array();

			// lets group now by deal type.
			foreach($products as $product)
			{
				if ($product->is_downloadable())
				{
					$downloadableProducts[] = $product;
				}
				else
				{
					$physicalProducts[] = $product;
				}
			}
		}

		$order = $this;

		\Message::send(function($message) use($order, $physicalProducts, $downloadableProducts)
		{
			$input = \Input::get();

		    $message->to($order->user()->email);
		    $message->from('system@pinkepromise.com', 'Order Confirmation');

		    $message->subject('PINKE PROMISE - Order Payment Confirmation');

		    $message->body->order = $order;
			$message->body->physicalProducts     = $physicalProducts;
			$message->body->downloadableProducts = $downloadableProducts;
		    $message->body('view: emails.order_payment_confirmation');

		    // You can add View data by simply setting the value
		    // to the message.

		    $message->html(true);
		});			


	}





}

