<?php namespace App;

class Admin extends \App\App {

	protected $admin_role_id = 1;


	/**
	 * @method  retrieve()
	 *
	 */

	public function retrieve()
	{	
		$list = $this->_retrieve()->get();
		return $list[0]->users;
	}

	/**
	 * @method  fetch()
	 *
	 */

	public function fetch()
	{
		return $this->_retrieve();
	}

	/**
	 * @method  _retrieve()
	 *
	 */

	private function _retrieve()
	{
		return \Role::with(array('users' => function($query)
				{
					// nothing here.
				}))->where('id', '=', $this->admin_role_id);
	}
	


}

