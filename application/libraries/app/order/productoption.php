<?php namespace App\Order;

class ProductOption extends \App\App {

	public static $table = 'orders_products_options';

	public static $timestamps = false;

	/**
	 * Get option name.
	 */
	
	public function name()
	{
		$r = \DealOptions::where('id', '=', $this->option_id)->first();
		return @$r->name;
	}

	/**
	 * Get option value name.
	 */
	public function option_value_name()
	{
		$r = \DealOptionValue::where('id', '=', $this->option_list_id)->first();
		return @$r->name;
	}	


}

