<?php namespace App\Order;

class Comment extends \App\App {

	public static $table      = 'orders_comments';
	
	public static $timestamps = true;

	public function author()
	{
		return \User::find($this->author_id);
	}

	public function product()
	{
		return Product::find($this->product_id);
	}


	public function add($data = array())
	{
		if (!isset($data['author_id']))
		{
			$data['author_id'] = \Auth::user()->id;
		}

		$Comment = new Comment;

		// add new comment
		$Comment->set_data($data)->save();

		return $Comment;
	}

	public function datediff()
	{
		$date1 = new \DateTime();
		$date2 = new \DateTime($this->created_at);

		// calculate interval
		$interval = $date1->diff($date2);

		return $interval->d." days ".$interval->h." hours ".$interval->i." mins"; 

	}



}

