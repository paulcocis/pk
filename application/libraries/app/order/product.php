<?php namespace App\Order;

class Product extends \App\App {

	public static $table = 'orders_products';

	public static $timestamps = true;


	public function deal()
	{
		return \App\Deal::find($this->parent_product_id);
	}

	public function seller()
	{
		return \User::find($this->seller_id);
	}

	public function is_downloadable()
	{
		$downloadable = FALSE;

		if (!$this->deal())
		{
			return FALSE;
		}


		if ($this->deal()->deal_type == 'downloadable')
		{
			$downloadable = TRUE;
		}

		return $downloadable;
	}



	/**
	 * Delete product and his options.
	 */
	public function delete()
	{
		ProductOption::where('order_product_id', '=', $this->id)->delete();

		// delete product.
		parent::delete();
	}

	/**
	 * Delete Product options.
	 */
	public function deleteOptions()
	{
		ProductOption::where('order_product_id', '=', $this->id)->delete();
	}

	/**
	 * Get product total cost.
	 */
	public function cost()
	{	
		// get cost.
		$cost = ($this->amount * $this->quantity);

		$cost = $cost + ($this->tax + $this->shipping_amount);

		$cost = $cost - $this->discount_amount;

		return number_format($cost, 2);
	}

	public function simple_cost()
	{
		$cost = ($this->amount * $this->quantity);
		$cost = $cost - $this->discount_amount;

		return number_format($cost, 2);
	}

	/**
	 * Get product options.
	 */
	public function options()
	{
		return ProductOption::where('order_product_id', '=', $this->id);
	}

}

