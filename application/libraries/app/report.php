<?php namespace App;

class Report
{

	public static function instance()
	{
		static $instance = NULL;

		if (!$instance)
		{
			$instance = new static;
		}

		return $instance;
	}

	/**
	 * Get current month reports.
	 */
	public function currentMonthOrders()
	{
		// create instance
		$order = new Order;

		// count
		$total = $order->where(\DB::raw('MONTH(created_at)'), '=', date('m'))->where(\DB::raw('YEAR(created_at)'), '=', date('Y'))->count();

		return $total;
	}

	public function succesfulTransactions()
	{
		$transaction = new \App\Billing\Transaction;

		$total = $transaction->where(\DB::raw('MONTH(created_at)'), '=', date('m'))
							 ->where(\DB::raw('YEAR(created_at)'), '=', date('Y'))

							 ->where('status', '=', 'approved')->count();

		return $total;
	}

	public function currentMonthTopSeller($view = 'user')
	{
		$product = new \App\Order\Product;

		$result = $product->where(\DB::raw('MONTH(created_at)'), '=', date('m'))->where(\DB::raw('YEAR(created_at)'), '=', date('Y'))
				->group_by('parent_product_id', 'DESC')
				->first();

		$total = 0;
		$user  = 'NONE';
		if ($result)
		{
			$rows = $product->where(\DB::raw('MONTH(created_at)'), '=', date('m'))->where(\DB::raw('YEAR(created_at)'), '=', date('Y'))
						   ->where('parent_product_id', '=', $result->parent_product_id)->get();


			foreach($rows as $row)
			{
				$total += $row->cost();
			}	

			if ($result->deal())
			{
				// get the username.
				$user = $result->deal()->user()->username;				
			}


			if ($view == 'user')
			{
				return $user;
			}

			return $total;
		}
	}


	public function lastMonthTopSeller($view = 'user')
	{
		$product = new \App\Order\Product;

		$lastMonth = date('m', strtotime('-1 month'));

		$result = $product->where(\DB::raw('MONTH(created_at)'), '=', $lastMonth)->where(\DB::raw('YEAR(created_at)'), '=', date('Y'))
				->group_by('parent_product_id', 'DESC')
				->first();

		$total = 0;
		$user = NULL;
		if ($result)
		{
			$rows = $product->where(\DB::raw('MONTH(created_at)'), '=', $lastMonth)->where(\DB::raw('YEAR(created_at)'), '=', date('Y'))
						   ->where('parent_product_id', '=', $result->parent_product_id)->get();


			foreach($rows as $row)
			{
				$total += $row->cost();
			}	

			if ($result->deal())
			{
				// get the username.
				$user = $result->deal()->user()->username;			
			}


			if ($view == 'user')
			{
				return $user;
			}

			return $total;
		}
	}


}

