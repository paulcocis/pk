<?php namespace App;

class Seller extends \App\App {

	protected $seller_role_id = 2;


	/**
	 * @method  retrieve()
	 *
	 */

	public function retrieve($status = NULL)
	{	
		$list = $this->_retrieve($status)->get();
		return $list[0]->users;
	}

	/**
	 * @method  fetch()
	 *
	 */

	public function fetch()
	{
		return $this->_retrieve();
	}

	/**
	 * @method  _retrieve()
	 *
	 */

	private function _retrieve($status)
	{
		return \Role::with(array('users' => function($query) use ($status)
				{
					if ($status)
					{
						$query->where('status', '=', $status);
					}

					// nothing here.
				}))->where('id', '=', $this->seller_role_id);
	}


	public static function DataCollection($user = 0)
	{
		$user = \User::find($user);

		// initiate the data collection class.
		$DataCollection = new \App\Seller\DataCollection($user);

		return $DataCollection;
	}


}

