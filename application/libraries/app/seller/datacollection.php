<?php namespace App\Seller;

// aliases.
use App\Order as Order;
use App\Order\Product as OrderProduct;

class DataCollection extends \App\App {

	/**
	 * User model instance
	 */
	protected $seller = NULL;

	final public function __construct(\User $seller)
	{
		$this->seller = $seller;
	}

	/**
	 * Returns the seller profile.
	 */
	public function profile()
	{
		return $this->seller;
	}

	/**
	 * Get seller deals.
	 */
	public function deals()
	{
		return \App\Deal::where('seller_id', '=', $this->seller->id);
	}

	public function activeDeals()
	{
		return $this->deals()->where('status', '=', 'approved');
	}

	public function awaitingForApproval()
	{
		return $this->deals()->where('status', '!=', 'approved');
	}

	public function invoices()
	{
		return \App\SellerInvoice::forge()->where('seller_id', '=', $this->profile()->id);
	}


	public function sellerOrderInvoice($order)
	{
		return $this->invoices()->where('order_id', '=', $order)->first();
	}

	/**
	 * Get total earnings.
	 */
	public function earned()
	{		
		// create \App\SellerInvoice instance.
		$SellerInvoice = new \App\SellerInvoice;

		// count.
		$TotalEarned = $SellerInvoice->where('seller_id', '=', $this->seller->id)->sum('amount');

		// return formated number.
		return number_format($TotalEarned, 2);
	}

	public function paid()
	{
		// create \App\SellerInvoice instance.
		$SellerInvoice = new \App\SellerInvoice;

		// count.
		$TotalEarned = $SellerInvoice->where('seller_id', '=', $this->seller->id)->where('status', '=', 'Paid')->sum('amount');

		// return formated number.
		return number_format($TotalEarned, 2);		
	}

	public function unpaid()
	{
		// create \App\SellerInvoice instance.
		$SellerInvoice = new \App\SellerInvoice;

		// count.
		$TotalEarned = $SellerInvoice->where('seller_id', '=', $this->seller->id)->where('status', '=', 'OrderPaymentComplete')->sum('amount');

		// return formated number.
		return number_format($TotalEarned, 2);		
	}


	public function perOrderEarning($order)
	{
		// create \App\SellerInvoice instance.
		$SellerInvoice = new \App\SellerInvoice;

		// count.
		$TotalEarned = $SellerInvoice->where('seller_id', '=', $this->seller->id)
									 ->where('order_id', '=', $order)
									 ->sum('amount');

		// return formated number.
		return number_format($TotalEarned, 2);		
	}


	/**
	 * Get per order sales reports.
	 */
	public function perOrderSales($interval = NULL)
	{
		// get the copy of seller id.
		$seller_id = $this->profile()->id;

		// custom interval check
		$applyDateInterval = FALSE;

		// custom interval 
		if ($interval)
		{
			list($startDate, $endDate) = explode(array('|', ':'), $interval);
			$applyDateInterval = TRUE;
		}

		// create \App\Order\Product instance.
		$OrderProduct = new OrderProduct;

		$query = $OrderProduct->where('seller_id', '=', $seller_id);

		if ($applyDateInterval)
		{
			$query = $query->where(\DB::raw('DATE(created_at)'), '>=', $startDate)
						   ->where(\DB::raw('DATE(created_at)'), '<=', $startDate);				
		}


		// lets group now by order.
		$query = $query->group_by('order_id', 'DESC');

		// no data available.
		if (!$records = $query->get())
		{
			return NULL;
		}	

		$data = NULL;

		foreach($records as $record)
		{
			$record->SellerDataCollection = $this;

			// set the record object.
			$record->order = Order::find($record->order_id);

			$data[] = $record;
		}

		return $data;
	}


}

