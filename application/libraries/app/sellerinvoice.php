<?php namespace App;

 use App\Order as Order;
 use App\Order\Product as Product;
 use App\SellerInvoice\Item as InvoiceItem;

class SellerInvoice extends \App\App {

	public static $table      = 'seller_invoices';
	
	public static $timestamps = true;

	public function __construct( $attributes = array(), $exists=false )
	{
	      parent::__construct($attributes, $exists);
	}
	
	/**
	 * get invoice items.
	 */
	public function items()
	{
		return InvoiceItem::where('invoice_id', '=', $this->id)->get();
	}

	/**
	 * Retrieve seller informations.
	 */
	public function seller()
	{
		return \User::find($this->seller_id);
	}

	public function order()
	{
		return Order::find($this->order_id);
	}


	/**
	 * get amount total.
	 */
	public function amount()
	{	
		return number_format($this->amount, 2);
	}

	public function status()
	{
		$status = NULL;

		switch($this->status)
		{
			case 'WaitingOrderPayment': $status  = 'Waiting for order payment'; break;
			case 'OrderPaymentComplete': $status = 'Order payment complete'; break;
			case 'Paid': $status                 = 'Seller Paid'; break;
		}

		return $status;
	}

	public function isPaid()
	{
		return ($this->status === 'Paid');
	}

	public function setPaidStatus()
	{
		$this->status = 'Paid';
		$this->save();
	}

	public function setUnpaidStatus()
	{
		$this->status = 'OrderPaymentComplete';
		$this->save();
	}


	public function generate($order_id = 0)
	{
		// get order details.
		$order = Order::find($order_id);

		if (!$order)
		{
			return FALSE;
		}

		// check if exists.
		 SellerInvoice::where('order_id', '=', $order_id)->delete();


		$sellers = Product::where('order_id', '=', $order->id)->group_by('seller_id')->get();


		foreach($sellers as $seller)
		{
			$this->_generate($seller);
		}

		return TRUE;
	}

	/**
	 * Regenerate order seller invoices.
	 */
	public function regenerate($order_id = 0)
	{
		if ($this->order_id)
		{
			$order_id = $this->order_id;
		}

		return $this->generate($order_id);
	}


	private function _generate($seller)
	{
		$products = Product::where('order_id', '=', $seller->order_id)->where('seller_id', '=', $seller->seller_id)->get();

		if (!$products)
		{
			return FALSE;
		}

		// get order refund amount.
		$orderRefundAmount = Order::find($seller->order_id)->getRefundTransactionsAmount();

		$totalAmount     = '0.00';
		$amount          = '0.00';
		$shipping_amount = '0.00';

		foreach($products as $product)
		{	

			if (!$product->seller())
			{
				break;
			}

			$rate   = $product->seller()->bill()->commission_rate(TRUE);
			
			$math   = ($product->deal()->sale_price * $rate) / 100;
			$amount += $product->deal()->sale_price - $math;

			$shipping_amount += $product->shipping_amount;
		}

		$totalAmount = $amount + $shipping_amount;
		$totalAmount = $totalAmount - $orderRefundAmount;

		$sellerInvoice = new SellerInvoice;

		$sellerInvoice->set_data(array(
			'order_id'  => $seller->order_id,
			'seller_id' => $seller->seller_id,
			'amount'    => $totalAmount
		));

		$sellerInvoice->save();
	}


	

}

