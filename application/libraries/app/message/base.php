<?php namespace App\Message;

class Base {

	protected $mail = NULL;
	protected $data = NULL;

	protected $from      = 'PinkePromise';
	protected $fromEmail = 'no-reply@pinkepromise.com';


	/**
	 * Class constructor;
	 */
	public function __construct($data = NULL)
	{
		$this->mail = \Message::instance();
		$this->data = $data;
	}

	/**
	 * Get mail instance.
	 */
	public function mail()
	{
		return $this->mail;
	}

	public function setFrom($from)
	{
		$this->from = $from;
		return $this;
	}

	public function setFromEmail($fromEmail)
	{
		$this->fromEmail = $fromEmail;
		return $this;
	}

}