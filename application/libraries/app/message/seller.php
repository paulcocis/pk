<?php namespace App\Message;

use View;
use User;


class Seller extends Base {

	/**
	 * Seller Sign-Up Information
	 */

	public function signup($seller)
	{	
		// allow to pass order id instead of instance.
		if (!$seller instanceof User)
		{
			$seller = User::find($seller);
		}

		$result = View::make('emails.seller_signup')
					   ->with('Seller', $seller);
	
		$mail = $this->mail();

		// make sure that html is added.
		$mail->html(true);

		// set address to
		$mail->to($seller->email)
			 ->from($this->fromEmail, $this->from)
			 ->subject('PinkePromise - New Seller Sign Up')
			 ->body($result)
			 ->send();
	}

	/**
	 * Send approval notification to seller.
	 */
	public function approved($seller)
	{
		// allow to pass order id instead of instance.
		if (!$seller instanceof User)
		{
			$seller = User::find($seller);
		}

		$result = View::make('emails.seller_confirmed')
					   ->with('Seller', $seller);
	

		$mail = $this->mail();

		// make sure that html is added.
		$mail->html(true);

		// set address to
		$mail->to($seller->email)
			 ->from($this->fromEmail, $this->from)
			 ->subject('PinkePromise - Your seller account has been approved.')
			 ->body($result)
			 ->send();	
	}


}