<?php namespace App\Message;

class Message {


	public static function forge($class, $data = NULL)
	{	
		$class = ucfirst($class);
		
		// get full class.
		$class = "\App\Message\\".$class;
		
		// initiate object.
		return new $class($data);
	}


}
