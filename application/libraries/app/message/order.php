<?php namespace App\Message;

use View;
use App\Order as OrderManager;


class Order extends Base {

	/**
	 * Order confirmation.
	 */

	public function confirmation($order)
	{	
		// allow to pass order id instead of instance.
		if (!$order instanceof OrderManager)
		{
			$order = OrderManager::find($order);
		}

		// retrieve customer informations.
		$Customer     = $order->user();
		
		// retrieve shipping user.
		$ShippingUser = $order->shipping_user();

		// retrieve order products.
		$Products = $order->products()->get();

		$result = View::make('emails.order_confirmation')
					   ->with('Order', $order)
					   ->with('Customer', $Customer)
					   ->with('ShippingUser', $ShippingUser)
					   ->with('Products', $Products);

		$mail = $this->mail();

		// make sure that html is added.
		$mail->html(true);

		// set address to
		$mail->to($Customer->email)
			 ->from($this->fromEmail, $this->from)
			 ->subject('Thank you for shopping at pinkEpromise.com! Order number: '.$order->id)
			 ->body($result)
			 ->send();
	}

	/**
	 * Order payment confirmation.
	 */
	public function paymentConfirmation($order)
	{	
		// allow to pass order id instead of instance.
		if (!$order instanceof OrderManager)
		{
			$order = OrderManager::find($order);
		}

		// retrieve customer informations.
		$Customer     = $order->user();
		
		// retrieve shipping user.
		$ShippingUser = $order->shipping_user();

		// retrieve order products.
		$Products = $order->products()->get();

		$DownloadableProducts = NULL;

		foreach($Products as $Product)
		{
			if ($Product->is_downloadable())
			{
				$DownloadableProducts[] = $Product;
			}
		}

		$result = View::make('emails.order_paymentconfirmation')
					   ->with('Order', $order)
					   ->with('Customer', $Customer)
					   ->with('ShippingUser', $ShippingUser)
					   ->with('Products', $Products)
					   ->with('DownloadableProducts', $DownloadableProducts);

		$mail = $this->mail();

		// make sure that html is added.
		$mail->html(true);

		// set address to
		$mail->to($Customer->email)
			 ->from($this->fromEmail, $this->from)
			 ->subject('pinkEpromise.com - Your payment for order '.$order->id.' has been confirmed!')
			 ->body($result)
			 ->send();
	}

	public function shippingScheduled($order)
	{
		// allow to pass order id instead of instance.
		if (!$order instanceof OrderManager)
		{
			$order = OrderManager::find($order);
		}

		// retrieve customer informations.
		$Customer = $order->user();

		// retrieve shipping user.
		$ShippingUser = $order->shipping_user();

		$result = View::make('emails.order_shippingscheduled')
					   ->with('Order', $order)
					   ->with('Customer', $Customer)
					   ->with('ShippingUser', $ShippingUser);

		$mail = $this->mail();

		// make sure that html is added.
		$mail->html(true);

		// set address to
		$mail->to($Customer->email)
			 ->from($this->fromEmail, $this->from)
			 ->subject('pinkEpromise.com - Your order has been shipped!')
			 ->body($result)
			 ->send();


	}

}