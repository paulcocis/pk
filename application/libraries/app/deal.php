<?php namespace App;

class Deal extends \App\App {

	public static $table      = 'deals';
	
	public static $timestamps = true;

	public function user()
	{
		return \User::find($this->seller_id);
	}

	public function images()
	{
		$o = new \App\Deal\Image;
		return $o->where('deal_id', '=', $this->id)->get();
	}

	public function primaryImage()
	{
		$o = new \App\Deal\Image;
		return $o->where('deal_id', '=', $this->id)->where('primary','=',1)->first();
	}

	public function image()
	{
		return $this->has_many('\\App\Deal\Image');
	}

	public function options()
	{
		return $this->has_many('\\App\Deal\Option');
	}

	public function optionlist()
	{
		return $this->has_many('\\App\Deal\OptionList');
	}

	public function orders()
	{
		return \App\Order\Product::where('parent_product_id', '=', $this->id)->group_by('order_id')->get();
	}

	public function get_discount_price()
	{

		$discount_price = $this->calculate_discount($this->original_price, $this->sale_price);

		return number_format($discount_price, 2);
	}

	public function downloadable_link($item_id = 0)
	{	
		// ENCODING DEAL ID
		$secretId = ($this->id * 100) * 9999;

		// ENCODING ITEM ID.
		$secretItemId = ($item_id * 1000);

		$link = \URL::base().'/download-file/'.$secretId.'-'.$secretItemId;

		return $link;
	}

	public function download($s = 0)
	{
		if (!$s) return FALSE;

		// get codes.
		list($secretId, $itemId) = explode('-', $s);

		// get the real id.
		$realId = ($secretId / 100) / 9999;

		// get real item id.
		$realItemId = ($itemId / 1000);

		// attempt to retrieve deal informations.
		$deal = Deal::find($realId);

		if ($deal)
		{
			// create product instance.
			$product = \App\Order\Product::find($realItemId);

			if ($deal->deal_max_downloads < $product->dl_times)
			{
				return -1;
			}

			// increment the dl times.
			$product->set_data(array('dl_times' => $product->dl_times + 1))->save();
		
			// get the location.
			$deal_location = $this->files_location().$deal->deal_downloadable_file;

			return array(
				$deal_location,
				$deal->deal_downloadble_filename
			);

			// execute the download.
		}
	}


	public function setAvailableQuantity($quantity = NULL)
	{
		if ($quantity)
		{	
			// get current deal stock.
			$currentQuantity = $this->quantity;

			if ($currentQuantity)
			{	
				// get the new stock quantity.
				$qty = $currentQuantity - $quantity;			
			}
			else
			{
				$qty = 0;
			}

			// get deal.
			$deal = Deal::find($this->id);
			$deal->set_data(array('quantity' => $qty))->save();

			return $deal;
		}

		return FALSE;
	}



	/**
	 * 
	 */
	public function delete()
	{
		$this->image()->delete();

		$this->options()->delete();

		$this->optionlist()->delete();

		return parent::delete();
	}

	/**
	 * 
	 */
	public function startDate()
	{
		return date('Y-m-d', strtotime($this->deal_start_date));
	}

	/**
	 * 
	 */
	public function endDate()
	{
		return date('Y-m-d', strtotime($this->deal_end_date));
	}


	/**
	 * get today deals.
	 */
	public function getTodayDeals()
	{
		$deal = new Deal;

		$r = $deal->where(\DB::raw('DATE(created_at)'), '=', date('Y-m-d'));

		if (!$r->count())
		{
			return FALSE;
		}

		return $r->get();
	}


	public function getPrimaryImage()
	{
		$image = new \App\Deal\Image;
		$result = $image->where('deal_id', '=', $this->id)->where('primary', '=', 1)->first();

		return $result;		
	}


	/**
	 * Get deal primary image location.
	 */
	public function getDealPrimaryImageLocation()
	{

		$path = NULL;

		$image = new \App\Deal\Image;
		$result = $image->where('deal_id', '=', $this->id)->where('primary', '=', 1)->first();

		if (!$result)
		{
			return $path;
		}

		$path = $result->image_url();
		return $path;
	}

	/**
	 * Retrieve.
	 */
	public function retrieve($apply_end_date = TRUE, $pkStore = FALSE, $feautured = FALSE)
	{

		$deal = new Deal();

		$result = $deal->order_by('id', 'DESC');

		if ($pkStore)
		{
			$result = $result->where('pkstore_item','=',1)->or_where('pk_move', '=', 1)->where(\DB::raw('DATE(pk_end_date)'), '>=', date('Y-m-d'))->where(\DB::raw('DATE(deal_end_date)'), '<', date('Y-m-d'));
		}

		if ($feautured)
		{
			$result = $result->where('feautured', '=', 1);
		}

		else
		{
			if ($apply_end_date)
			{
				$result = $result->where(\DB::raw('DATE(deal_end_date)'), '>=', date('Y-m-d'));
			}	
		}

		$result = $result->where('nora_bleu', '=', 0);
		$result = $result->where('blank_you', '=', 0);
		$result = $result->where('pkstore_item', '=', 0);

		$result = $result->where('status', '=','approved');
		return $result->get();
	}



	public function retrieveFeautured($apply_end_date, $pkStore)
	{


		$deal = new Deal();

		$result = $deal->order_by('id', 'DESC');

		if ($pkStore)
		{
			$result = $result->where('pk_move', '=', 1)->where(\DB::raw('DATE(pk_end_date)'), '>=', date('Y-m-d'))->where(\DB::raw('DATE(deal_end_date)'), '<', date('Y-m-d'));
		}

		else
		{
			if ($apply_end_date)
			{
				$result = $result->where(\DB::raw('DATE(deal_end_date)'), '>=', date('Y-m-d'));
			}	
		}

		$result = $result->where('feautured', '=', 1);
		$result = $result->where('status', '=','approved');

		return $result->get();
	}



	/**
	 * Retrieve.
	 */
	public function nora($category = NULL)
	{
		$deal = new Deal();

		$result = $deal->order_by('id', 'DESC');

		$result = $result->where(\DB::raw('DATE(deal_end_date)'), '>=', date('Y-m-d'))->where('status', '=','approved')->where('nora_bleu', '=', 1);

		if ($category)
		{
			$result = $result->where('category_id', '=', $category);
		}

		return $result->get();
	}



	/**
	 * Retrieve.
	 */
	public function blankyou($category = NULL)
	{
		$deal = new Deal();

		$result = $deal->order_by('id', 'DESC');

		// with expiration date.
		//$result = $result->where(\DB::raw('DATE(deal_end_date)'), '>=', date('Y-m-d'))->where('status', '=','approved')->where('blank_you', '=', 1);
		$result = $result->where('status', '=','approved')->where('blank_you', '=', 1);

		if ($category)
		{
			$result = $result->where('category_id', '=', $category);
		}

		return $result->get();
	}



	/**
	 * 
	 */
	public function getTimeLeft()
	{	
		// get deal end date.
		$dealEndDate = $this->deal_end_date;

		$d = new \DateTime($dealEndDate);

		// get diff interval.
		$interval = $d->diff(new \DateTime());

		$prettyTimeLeft = NULL;

		$and = NULL;

		if ($interval->d >= 1)
		{
			$prettyTimeLeft = $interval->d.' days ';
			$and = 'and ';
		}

		if ($interval->h >= 1)
		{
			$prettyTimeLeft .= $and.' '.$interval->h.' hours';
		}

		return $prettyTimeLeft;
	}

	/**
	 * 
	 */
	public function getPkTimeLeft()
	{	
		// get deal end date.
		$dealEndDate = $this->pk_end_date;

		$d = new \DateTime($dealEndDate);

		// get diff interval.
		$interval = $d->diff(new \DateTime());

		$prettyTimeLeft = NULL;

		$and = NULL;

		if ($interval->d >= 1)
		{
			$prettyTimeLeft = $interval->d.' days ';
			$and = 'and ';
		}

		if ($interval->h >= 1)
		{
			$prettyTimeLeft .= $and.' '.$interval->h.' hours';
		}

		return $prettyTimeLeft;
	}	

	/**
	 * 
	 */
	public function add($data = array())
	{
		$inputFileName = 'deal_downloadable_file';


		$needUpload = FALSE;

		if (\Input::file("{$inputFileName}.name"))
		{
			$needUpload = TRUE;
		}

		$data['deal_start_date'] = date('Y-m-d', strtotime($data['deal_start_date']));
		$data['deal_end_date']   = date('Y-m-d', strtotime($data['deal_end_date']));

		$deal = new Deal;

		if (!isset($data['discount_percent_off']))
		{
			$data['discount_percent_off'] = 0;
		}

		if (empty($data['discount_price']))
		{
			$min = $data['original_price'] - $data['sale_price'];

			$discountPrice = ($min / $data['original_price']) * 100;

			$discountPrice = number_format($discountPrice, 2);

			$data['discount_price'] = $discountPrice;
		}


		// create new deal.
		$deal->set_data($data)->save();

		if ($needUpload)
		{
			// attempt to uload FileName
			$uploadedFileName = $this->upload_file($deal->id);

			$deal->deal_downloadable_file = $uploadedFileName;
			$deal->save();
		}

		return $deal;
	}


	public function calculate_discount($original = '0.00', $sale = '0.00')
	{

		$min = $original - $sale;

		$discountPrice = ($min / $original) * 100;

		$discountPrice = number_format($discountPrice, 2);

		return $discountPrice;
	}


	/**
	 * 
	 */
	public function modify($id = 0, $data = array())
	{
		$deal = $this->find($id);

		if (!$deal)
		{
			return FALSE;
		}

		$inputFileName = 'deal_downloadable_file';

		$needUpload = FALSE;

		if (\Input::file("{$inputFileName}.name"))
		{
			$needUpload = TRUE;
		}

		// check before perform update.
		if (isset($data['original_price']))
		{
			$data['discount_price'] = $this->calculate_discount($data['original_price'], $data['sale_price']);
		}

		$deal->set_data($data)->save();

		if ($needUpload)
		{
			// attempt to uload FileName
			$uploadedFileName = $this->upload_file($deal->id);

			$deal->deal_downloadable_file = $uploadedFileName;
			$deal->save();
		}


		return TRUE;
	}

	/**
	 * 
	 */
	protected function upload_file($deal_id = 0)
	{
		// no deal passed.
		// deal id is added on file name.
		if (!$deal_id)
		{
			return FALSE;
		}

		$inputName = 'deal_downloadable_file';

		// upload the file.
		$originalUploadFileName = \Input::file("{$inputName}.name");

		// format the name.
		$uploadFileName = $deal_id.'_'.$originalUploadFileName;


		// upload the file.
		\Input::upload($inputName, $this->files_location(), $uploadFileName);

		return $uploadFileName;
	}


	public function quantity()
	{
		if ($this->quantity > 1)
		{
			return $this->quantity;
		}

		return FALSE;
	}


	/**
	 * 
	 */
	public function files_location()
	{
		return path('deal_downloadable_path');
	}


	public function quantityDropdown($select_name = 'quantity')
	{
		if ($this->quantity < 1)
		{
			return FALSE;
		}

		$html = "<select name='".$select_name."'>";

		for ($i = 1; $i <= $this->quantity; $i++)
		{
			$amount = $i * $this->sale_price;
			$html .= "<option value=".$i."> ".$i." - $".number_format($amount,2)." </option>";
		}

		$html .= "</select>";
		return $html;

	}



}

