<?php namespace App;

class Promotion extends \App\App {

	public static $table      = 'promotions';
	
	public static $timestamps = true;


	public function add($data = array())
	{

		// single use.
		isset($data['single_use']) OR $data['single_use'] = 0;

		// stackable.
		isset($data['stackable']) OR $data['stackable'] = 0;

		$promotion = new Promotion;

		// insert to database.
		$promotion->set_data($data)->save();

		return $promotion;
	}


	public function applyAmountDiscount($amount = NULL)
	{

		$promotionAmount = $this->discount;
		$promotionType   = $this->type;

		if ($promotionType == 'fixed')
		{
			$resultAmount   = ($amount -> $promotionAmount);
			$discountAmount = $promotionAmount;
		}

		if ($promotionType == 'percentage')
		{
			$_discount = ($amount * $promotionAmount) / 100;
			// get discount amount.
			$discountAmount = number_format($_discount, 2);
			
			$resultAmount   = ($amount - $discountAmount);
		}

		return $_discount;

	}


	public function valid($deal, $promotion)
	{
		$deal = Deal::find($deal);

		if (!$deal)
		{
			return FALSE;
		}

		$promotion = Promotion::find($promotion);

		$promotionEndDate = date('Y-m-d', strtotime($promotion->end_date));

		$now = date('Y-m-d');

		if ($now > $promotionEndDate)
		{			
			return FALSE;
		}

		if ($promotion->deal_id != $deal->id)
		{
			return FALSE;
		}

		return TRUE;

	}

	public function stackable()
	{
		return (bool) $this->stackable;
	}


	public function usedTimes()
	{
		return \Cart\Promotion::where('promotion_id', '=', $this->id)->count();
	}




}

