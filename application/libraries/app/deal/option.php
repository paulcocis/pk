<?php namespace App\Deal;

class Option extends \App\App {

	public static $table      = 'deals_options';
	
	public static $timestamps = true;

	public function options()
	{
		return OptionList::where('option_id', '=', $this->id);
	}

 	public function optionlist()
    {
        return $this->has_many('\\App\Deal\OptionList', 'option_id');
    }

    public function delete()
    {
    	// remove
    	$this->optionlist()->delete();

    	// remove data now.
    	return parent::delete();
    }


	public function add()
	{	

		$newOption = new Option;

		$newOptionData = array(
			'name'    => \Input::get('name', NULL),
			'deal_id' => \Input::get('deal_id', 0)
		);

		// create the option.
		$newOption->set_data($newOptionData)->save();

		$oName = \Input::get('optionName' , NULL);
		$oLimit = \Input::get('optionLimit');

		if ($oName)
		{
			foreach($oName as $key => $name)
			{
				if (!empty($name))
				{
					$limit = $oLimit[$key];

					$oData = array(
						'deal_id'   => $newOption->deal_id,
						'option_id' => $newOption->id,
						'name'      => $name,
						'limit'     => $limit
					);

					OptionList::forge()->set_data($oData)->save();	
				}
			}
		}

	}



}

