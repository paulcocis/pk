<?php namespace App\Deal;

class OptionList extends \App\App {

	public static $table      = 'deals_optionlist';
	
	public static $timestamps = true;

	public function add($data = array())
	{
		$option = new OptionList;
		$option->set_data($data)->save();
	}

}

