<?php namespace App\Deal;

class Image extends \App\App {

	public static $table = 'deals_images';
	public static $key   = 'id';
	
	public static $timestamps = true;



	public function delete()
	{	
		// remove the image from path.
		@unlink($this->image_full_location());

		parent::delete();
	}

	public function image_full_location()
	{
		$location = $this->image_path.'/'.$this->image_name;
		return $location;
	}

	public function image_url()
	{

		$this->generate_thumb();

		$url = \URL::base();
		$image_url = $url.'/deals/images/'.$this->deal_id.'/'.$this->image_name;
		return $image_url;
	}

	public function thumb_url($width = 400, $height = 400)
	{

		if (!$this->image_name OR empty($this->image_name))
		{
			return 'none';
		}


		$this->generate_thumb($width, $height);

		$url = \URL::base();

		$image_url = $url.'/deals/images/'.$this->deal_id.'/thumb_'.$this->image_name;

		if ($width != 400)
		{
			$image_url = $url.'/deals/images/'.$this->deal_id.'/thumb_'.$width.'X'.$height.'_'.$this->image_name;
		}
		
		return $image_url;
	}	

	
	public function upload($primary = 0)
	{
		$inputFileName = 'dataimage';

		$needUpload = FALSE;

		if (\Input::file("{$inputFileName}.name"))
		{
			$needUpload = TRUE;
		}

		$input = \Input::get();

		if ($needUpload)
		{

			if ($primary)
			{
				$this->removePrimaryImage($input['deal_id']);
			}

			$name = \Input::file("{$inputFileName}.name");

			// upload the file.
			\Input::upload($inputFileName, $this->retrieve_image_path($input['deal_id']), $name);	

			$new_image_data = array(
				'deal_id' => $input['deal_id'],
				'image_name' => $name,
				'image_path' => $this->retrieve_image_path($input['deal_id']),
				'primary'    => $primary
			);

			$image = new Image;
			$image->set_data($new_image_data)->save();

			return $image;
		}

		return FALSE;
	}

	public function primary()
	{
		Image::where('deal_id', '=', $this->deal_id)->update(array('primary' => 0));

		$this->primary = 1;
		$this->Save();
	}

	public function setPrimaryImage($image_id = 0, $deal_id = 0)
	{
		Image::where('deal_id', '=', $deal_id)->update(array('primary' => 0));

		$image = Image::find($image_id);

		$image->fill(array('primary' => 1))->save();
	}

	public function removePrimaryImage($deal_id)
	{
		return Image::where('deal_id', '=', $deal_id)->update(array('primary' => 0));
	}


	public function retrieve_image_path($deal_id = 0)
	{	
		// get deal images location.
		$location = path('deal_images').$deal_id;

		if (!file_exists($location))
		{
			mkdir($location, 0777);
		}

		return $location;
	}


	public function generate_thumb($width = 400, $height = 400)
	{	
		if (!$this->id)
		{
			return FALSE;
		}

		$location = $this->image_full_location();

		$supported_image = array(
		    'gif',
		    'jpg',
		    'jpeg',
		    'png'
		);		


		$ext = strtolower(pathinfo($location, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
		
		if (!in_array($ext, $supported_image)) 
		{
		    return FALSE;
		} 


		if (!file_exists($location))
		{
			return FALSE;
		}

		if ($width != 400)
		{
			$thumb_name = 'thumb_'.$width.'X'.$height.'_'.$this->image_name;
		}
		else
		{
			$thumb_name = 'thumb_'.$this->image_name;
		}


		$thumb_location = $this->image_path.'/'.$thumb_name;


		// thumb need to be generated.
		if (!file_exists($thumb_location))
		{

		    // Save a thumbnail
   			$success = \Resizer::open( $location )
        		->resize( $width , $height , 'crop' )
        		->save( $thumb_location, 90 );	
		}

		return TRUE;
	}



}

