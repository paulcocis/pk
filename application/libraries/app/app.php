<?php namespace App;

abstract class App extends \EloquentBase {

	/**
	 * Get the user model.
	 * @return model user.
	 */
	protected function user()
	{
		return $user = new \User;
	}

	public static function forge()
	{
		return new static;
	}

}