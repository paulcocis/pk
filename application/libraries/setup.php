<?php

 use App\Setup\Settings as AppSettings;

 class Setup {



 	/**
 	 * Class constructor.
 	 */
 	function __construct() {}

 	public static function instance()
 	{	
 		// define instance
 		static $instance = NULL;

 		if (!$instance)
 		{
 			$instance = new static;
 		}

 		return $instance;
 	}


 	public function init()
 	{	
 		// initiate the email configuration.
 		$this->setEmailConfiguration();
 	}


 	/**
 	 * Set the email configuration.
 	 */
 	protected function setEmailConfiguration()
 	{
 		// get website configuration.
 		$configuration = AppSettings::first();

		Config::set('messages::config.transports.smtp.host', $configuration->smtp_hostname);
		Config::set('messages::config.transports.smtp.port', $configuration->smtp_port);
		Config::set('messages::config.transports.smtp.username', $configuration->smtp_username);
		Config::set('messages::config.transports.smtp.password', $configuration->smtp_password);
		Config::set('messages::config.transports.smtp.encryption', null); 		

		return TRUE;
 	}



 }