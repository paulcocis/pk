<?php namespace Cart;

class ItemOption extends \EloquentBase {


	/**
	 * Main shopping cart model table.
	 *
	 */
	public static $table      = 'cart_item_options';
	
	public static $timestamps = true;


	public function name()
	{
		$r = \DealOptions::where('id', '=', $this->option_id)->first();
		return $r->name;
	}

	public function option_value_name()
	{
		$r = \DealOptionValue::where('id', '=', $this->option_value_id)->first();
		return $r->name;
	}	

	public function add($data = array())
	{	
		$optionValue = \DealOptionValue::find($data['option_value_id']);

		if ($optionValue)
		{
			$data['option_id'] = $optionValue->option_id;

			$new = new ItemOption;
			$new->set_data($data)->save();

			return $new;
		}

		return FALSE;
	}



}