<?php namespace Cart;

class Base extends \EloquentBase {


	/**
	 * Main shopping cart model table.
	 *
	 */
	public static $table      = 'cart';
	
	public static $timestamps = true;


	/**
	 *  Create shopping cart session instance.
	 */
	public function forge()
	{
		// get the session id.
		$session_id = \Session::$instance->session['id'];

		$check = Base::instance()->where('cart_id', '=', $session_id)->where('completed', '!=', 1);
	
		if (!$check->count())
		{
			$newCartSession = new Base;

			// add values.
			$newCartSession->cart_id    = $session_id;
			$newCartSession->ip_address = \Request::ip();

			// save the cart.
			$newCartSession->save();

			return $newCartSession;
		}

		return $check->first();
	}

	/**
	 * Destroy the shopping cart instance.
	 */
	public function destroy()
	{
		$b = Base::forge();
		$b->set_data(array('completed' => 1))->save();

		// regenerate session.
		\Session::regenerate();

		return TRUE;
	}

	/**
	 * Modify the credit card.
	 */
	public function modify($data = array())
	{	
		// initiate base cart.
	    $modify = Base::forge();

	    // update cart.
	    $modify->set_data($data)->save();

	    return $modify;
	}
}