<?php namespace Cart;

use \App\Order as AppOrder;
use \App\Setup\PaymentGateway as Gateway;
use \App\Billing\Transaction as Transaction;

class Payment  {

	/**
	 * Process paypal standard payments.
	 */
	public function paypal_standard($order = 0)
	{
		$order = AppOrder::find($order);

		if (!$order)
		{
			throw new \Exception('We cannot process your paypal payment request.');		
		}

		// retrieve paypal informations.
		$paypal = Gateway::where('name', '=', 'paypal')->first();

		// Generate paypal html form.
		$Paypal_Html_Form = '

			<h1> Please wait while redirecting to paypal ... </h1>

			<form action="'.$paypal->api_url.'" method="POST" name="paypalCheckout" id="pp">				
				<input type ="hidden" name="cmd"        value="_xclick">
				<input type ="hidden" name="notify_url" value="'.$paypal->api_postback_url.'">
				<input type ="hidden" name="business"   value="'.$paypal->api_username.'">
				<input type ="hidden" name="item_name"  value="Payment for order #'.$order->id.'">
				<input type ="hidden" name="custom"     value="'.$order->id.'">
				<input type ="hidden" name="amount"     value="'.$order->totalCost().'">
				<input type ="hidden" name="email"      value="'.$order->user()->email.'">	
		    </form>

			<script type="text/javascript">
					document.getElementById("pp").submit();
			</script>
		    ';	

        return $Paypal_Html_Form;
	}

	public function paypal_standard_ipn($input = array())
	{
		$options = array(
			'sandbox' => FALSE
		);

		$input = \Input::get();

		// get paypal db data.
		$paypal = Gateway::where('name', '=', 'paypal')->first();

		// get the IPN Response.
		$response = \Gateway\Paypal::ipn($input);

		if ($response)
		{
			// get the order.
			$order = AppOrder::find($input['custom']);

			// transaction has been made.
			$order->status     = 'paid';
			$order->gateway_id = $paypal->id;

			// update order.
			$order->save();

			// generate order transaction.
			$this->generateTransaction($order, array('reference_transaction_id' => $input['txn_id'], 'gw_postback_logs' => json_encode($input)));	

			// Send payment fonfirmation.
			App\Message\Message::forge('order')->paymentConfirmation($order);

			// set the stock control.
			foreach($order->products()->get() as $product)
			{
				// set the available quantity.
				$product->deal()->setAvailableQuantity($product->quantity);
			}

			// completed payment.
			return array('success' => 1, 'order' => $order->id, 'message' => 'SUCCESS');
		}

	}

	/**
	 * Direct paypal credit card payments.
	 */
	public function paypal_express($order = 0, $cc_info = array())
	{
		$order = AppOrder::find($order);

		if (!$order)
		{
			throw new \Exception('We cannot process your paypal payment request.');		
		}

		// retrieve paypal informations.
		$paypal = Gateway::where('name', '=', 'paypal-express')->first();

		// initiate the paypal payment flow.
		$PaypalFlow = new \Gateway\PaypalFlow('pinkEpromise', 'Paypal', 'pinkEpromise', 'Marytrent2014', 'single');

		// extract the credit card info.
		
		$exp_date = $cc_info['month'].$cc_info['year'];
		$cvv      = $cc_info['cvv'];
		$number   = $cc_info['number'];

		// get credit card full name.
		$cc_full_name = $order->user()->primary()->firstname.' '.$order->user()->primary()->lastname;

		// other name has been passed.
		if (isset($cc_info['name']))
		{
			$cc_full_name = $cc_info['name'];
		}

		$PaypalFlow->setEnvironment('test');                           
		$PaypalFlow->setTransactionType('S');                          
		$PaypalFlow->setPaymentMethod('C');                            
		$PaypalFlow->setPaymentCurrency('USD');                        

		$PaypalFlow->setAmount($order->totalCost(), FALSE);
		$PaypalFlow->setCCNumber($number);
		$PaypalFlow->setCVV($cvv);
		$PaypalFlow->setExpiration($exp_date);
		$PaypalFlow->setCreditCardName($cc_full_name);

		// verify if transaction has been processed.
		if ($PaypalFlow->processTransaction())
		{
			$response = $PaypalFlow->getResponse();

			// transaction has been made.
			$order->status     = 'paid';
			$order->gateway_id = $paypal->id;

			// update order.
			$order->save();

			// generate order transaction.
			$this->generateTransaction($order, array('reference_transaction_id' => $response['PPREF'], 'gw_postback_logs' => json_encode($response)));	

			// completed payment.
			return array('success' => 1, 'order' => $order->id, 'message' => 'SUCCESS');
		}

		// get the response.
		$response = $PaypalFlow->getResponse();

		// return failed transaction.
		return array('success' => 0, 'order' => $order->id, 'message' => $response['RESPMSG']);
	}


	/**
	 * Generate payment transaction.
	 */

	protected function generateTransaction($order, $data = array())
	{
		$transaction = new Transaction;

		$new_data = array_merge(array(
			'gateway_id' => $order->gateway_id,
			'user_id'	 => $order->user()->id,
			'order_id'   => $order->id,
			'amount'	 => $order->totalCost(),
			'amount_in'  => $order->totalCost(),
			'amount_out' => '0.00',
			'gw_fee'	 => '0.00',
			'status'     => 'approved'), $data);


		$transaction->set_data($new_data)->save();

		return $transaction;
	}





}