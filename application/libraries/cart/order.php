<?php namespace Cart;

class Order extends Base {


	/**
	 * Get the customer login info.
	 */
	public function customer()
	{
		return \Auth::user();
	}

	/**
	 * Get checkout settings session.
	 */
	public function checkout_session()
	{
		return \Session::get('checkout', FALSE);
	}

	/**
	 * Process cart.
	 */
	public function process($cart_unique_id = 0)
	{
 		// invalit cart unique id passed.
	 	if (!Cart::find($cart_unique_id))
	    {
	    	throw new \Exception("Unknown shopping cart error. Please start shop again.", 1);	
	    }
	    
	    // get items.
	    $items = Cart::instance()->items();

	    // no items found.
	    if (!$items)
	    {
	    	throw new \Exception("Error Processing Request", 1);
	    }

		$store_items    = array();
		$pk_store_items = array();

	    foreach($items as $item)
	    {
	    	$store_items[] = $item;
	    }

	
	    $order = $this->process_order($store_items);

	    // send email confirmation.		
		\App\Message\Message::forge('order')->confirmation($order);

	    // -----------------------------------------------

	    // Destroy the cart and regenerate.
   		Cart::instance()->destroy();

   		return $order;
	}

	/**
	 * Process normal deal orders.
	 */
	protected function process_order($items = array())
	{
		// create the order.
		$order = $this->_create(array('cart_id' => Cart::forge()->id));	

		foreach($items as $item)
		{
			$product = new \App\Order\Product;

			$new_product = array(
				'order_id'          => $order->id,
				'seller_id'			=> $item->deal()->seller_id,
				'parent_product_id' => $item->item_id,
				'name'              => $item->name,
				'tax'               => $item->tax,
				'discount_amount'   => $item->discount_price,
				'original_amount'   => $item->original_price,
				'shipping_amount'   => $item->shipping_price,
				'amount'            => $item->sale_price,
				'quantity'          => $item->quantity,
				'pk_item'           => $item->pk_item,
				'comments'	        => $item->comments
			);

			$product->set_data($new_product)->save();
			
			$products[] = $product;

			$this->save_item_options($item, $product);
		}

		// attempt to generate seller invoices.
		$generatePayout = new \App\SellerInvoice;

		// generate payout as well.
		$generatePayout->generate($order->id);

		return $order;
	}

	/**
	 * Same the item options.
	 */
	protected function save_item_options($cartItem, $product)
	{
		$itemOption = ItemOption::where('item_id', '=', $cartItem->id);

		if ($itemOption->count())
		{
			foreach($itemOption->get() as $i)
			{
				$orderProductOption = new \App\Order\ProductOption;

				$new_item_option = array(
					'order_product_id' => $product->id,
					'option_id'        => $i->option_id,
					'option_list_id'   => $i->option_value_id
				);

				$orderProductOption->set_data($new_item_option)->save();
			}

			return TRUE;
		}

		return FALSE;
	}


	/**
	 * Create new order.
	 * @return \App\Order class instance.
	 */
	protected function _create($data = array())
	{	
		// create new orders instance.
		$order = new \App\Order;

		$data = array_merge($data, array(
			'customer_id'         => $this->customer()->id,
			'gateway_id'          => 4,
			'status'              => 'unpaid',
			'shipping_status'     => 'unshipped',
			'shipping_contact_id' => \Session::get('checkout.shipping_contact_id', 0)
		));

		// create the order.
		$order->set_data($data)->save();

		return $order;
	}

	/**
	 * sendConfirmationEmail($order);
	 */
	public function sendConfirmationEmail($order = 0)	
	{
		return $this->_sendConfirmationEmail($order);
	}

	protected function _sendConfirmationEmail($order)
	{
		// attempt to find the order.
		$order = \App\Order::find($order);

		if (!$order)
		{
			return FALSE;
		}

		\Message::send(function($message) use($order)
		{
			$input = \Input::get();

		    $message->to($order->user()->email);
		    $message->from('system@pinkepromise.com', 'New Order Placed');

		    $message->subject('PINKE PROMISE - New Order');

		    $message->body->order = $order;
		    $message->body('view: emails.new_order');

		    // You can add View data by simply setting the value
		    // to the message.

		    $message->html(true);
		});	
	}



}