<?php namespace Cart;

class Promotion extends \EloquentBase {


	/**
	 * Main shopping cart model table.
	 *
	 */
	public static $table      = 'cart_promotions';
	
	public static $timestamps = false;


	public function add($data = array())
	{
		// create promotion instance.
		$promotion = new Promotion;

		// add promotion.
		$promotion->set_data($data)->save();

		return $promotion;
	}

	public function promotion_exists($id = 0)
	{
		$exists = Promotion::where('promotion_id', '=', $id)->where('cart_id','=',Cart::forge()->id)->count();
		return (bool) $exists;
	}

	
}