<?php namespace Cart;

use \App\Promotion as AppPromotion;

class Item extends \EloquentBase {


	/**
	 * Main shopping cart model table.
	 *
	 */
	public static $table      = 'cart_items';
	
	public static $timestamps = true;


	public function deal()
	{
		return \Deal::find($this->item_id);
	}


	/**
	 * Get all items from shopping cart.
	 */
	public function fetch()
	{
		$item = new Item;

		// fetch items.
		$data = $item->where('cart_id', '=', Cart::forge()->id)->get();

		return $data;
	}

	public function countItems()
	{
		$item = new Item;

		// fetch items.
		$total = $item->where('cart_id', '=', Cart::forge()->id)->count();

		return $total;
	}


	public function item_exists($item_id = 0)
	{
		$found = Item::where('cart_id', '=', Cart::forge()->id)->where('item_id', '=', $item_id)->count();

		if ($found)
		{
			return TRUE;
		}

		return FALSE;
	}


	/**
	 * Add new item in shopping cart.
	 */
	public function add($item_id = 0, $options = array(), $settings = array(), $cc = NULL)
	{
		// attempt to retrieve item informations.
		$item = \Deal::find($item_id);

		if (!$item)
		{
			throw new \Exception('This product was not found in database.');		
		}

		$tax = '0.00';
		$shipping_cost = $item->first_item_shipping_cost;

		if ($this->item_exists($item_id))
		{
			$shipping_cost = $item->aditional_item_shipping_cost;

			// get the item.
			$updatedItem = Item::where('cart_id', '=', Cart::forge()->id)->where('item_id', '=', $item_id)->first();

			// get the quantity
			$newQty = (isset($settings['quantity'])) ? $settings['quantity'] : 1;

			$updatedItem->quantity = $updatedItem->quantity + $newQty;
			$updatedItem->shipping_price = $updatedItem->shipping_price + $shipping_cost;

			$updatedItem->save();
			return TRUE;

		}

		if ($item->tax)
		{
			$tax = $item->comission_rate_override;
		}

		// check for quantity.
		if (!isset($settings['quantity']) OR empty($settings['quantity']))
		{
			$settings['quantity'] = 1;
		}

		$discount_price = '0.00';

		$leadge         = NULL;

		// attempt to retrieve if promo code is present.
		$coupon = AppPromotion::find($cc);

		if ($coupon)
		{
			if ($coupon->valid($item->id, $coupon->id))
			{
				$couponAdd = TRUE;
				
				// ops coupon 
				if (!$coupon->stackable() AND Promotion::promotion_exists($coupon->id))
				{
					$couponAdd = FALSE;
				}				

				if ($couponAdd)
				{
					$discount_price = $coupon->applyAmountDiscount($item->sale_price);	

					$newPromotionData = array(
						'cart_id'      => Cart::forge()->id,
						'deal_id'      => $item->id,
						'promotion_id' => $coupon->id
					);

					// add the promotion for tracking.
					$leadge = Promotion::add($newPromotionData);					
				}
			}
		}

		// ---- end coupon settings.

		$new_item_data = array(
			'cart_id'        => Cart::forge()->id,
			'item_id'        => $item->id,
			'name'           => $item->name,
			'sale_price'     => $item->sale_price,
			'original_price' => $item->original_price,
			'shipping_price' => $shipping_cost,
			'discount_price' => $discount_price,
			'tax'			 => $tax
		);

		// apply extra settings if needed.
		$new_item_data = array_merge($new_item_data, $settings);

		$newItem = new Item;

		// add new item in cart.
		$newItem->set_data($new_item_data)->save();

		// this is for promotion code.
		if ($leadge)
		{
			$leadge->item_id = $newItem->id;
			$leadge->save();
		}


		// inserted options.
		$insertedOptions = NULL;

		if (!empty($options))
		{
			foreach($options as $option)
			{
				$newOptionData = array(
					'cart_id'         => Cart::forge()->id,
					'item_id'         => $newItem->id,
					'option_value_id' => $option
				);

				$insertedOptions[] = ItemOption::instance()->add($newOptionData);
			}
		}

		return $newItem;
	}

	/**
	 * Update an shopping cart item.
	 */
	public function change($item_id = 0, $new_data = array(), $options = array())
	{
		// attempt to retrieve data.
		$item = Item::find($item_id);

		if (!$item)
		{
			return FALSE;
		}

		if (!empty($new_data))
		{
			// save the new data.
			$item->set_data($new_data)->save();	
		}

		if (!empty($options))
		{	

			// attempt to remove old options.
			ItemOption::where('item_id', '=', $item->id)->delete();

			foreach($options as $option)
			{
				$newOptionData = array(
					'cart_id'         => Cart::forge()->id,
					'item_id'         => $item->id,
					'option_value_id' => $option
				);

				$insertedOptions[] = ItemOption::instance()->add($newOptionData);
			}
		}
	}

	/**
	 * Delete a shopping cart item and his options.
	 */
	public function delete()
	{
		ItemOption::where('item_id', '=', $this->id)->delete();

		Promotion::where('item_id', '=', $this->id)->delete();

		parent::delete();
	}


	/**
	 * Calculate an item total.
	 */
	public function total()
	{
		// get total
		$total = ($this->sale_price * $this->quantity) + $this->tax;
		$total = $total - $this->discount_price;
		return number_format($total, 2);
	}

	/**
	 *  Get all cart item options.
	 */
	public function options()
	{
		return ItemOption::where('item_id', '=', $this->id)->get();
	}	



}