<?php namespace Cart;

class Cart extends Base {


	public function items()
	{
		return Item::instance()->fetch();
	}

	public function countItems()
	{
		return Item::instance()->countItems();
	}


	public function item($item_id = 0)
	{
		return Item::find($item_id);
	}

	/**
	 * Add new item in shopping cart.
	 * ----------------------------------------------------------------------------------------------
	 */

	public function add($item_id = 0, $options = array(), $settings = array(), $pid)
	{
		return Item::instance()->add($item_id, $options, $settings, $pid);
	}

	/**
	 * Modify an existing item.
	 * ----------------------------------------------------------------------------------------------
	 */

	public function change($item_id = 0, $new_data = array(), $options = array())
	{
		return Item::instance()->change($item_id, $new_data, $options);
	}

	/**
	 * Delete an item from shopping cart.
	 * ----------------------------------------------------------------------------------------------
	 */

	public function delete($item_id)
	{
		$item = Item::find($item_id);

		if ($item)
		{
			$item->delete();
		}
	}



	/**
	 * Cart total
	 */

	public function cart_total()
	{
		$total = '0.00';

		$items = $this->items();

		if (!$items)
		{
			return '0.00';
		}

		$total = $this->cart_subtotal() + $this->cart_shipping_total();

		foreach($items as $item)
		{
			$total += $item->tax;
		}

		return number_format($total, 2);
	}

	/**
	 * Cart subtotal.
	 */
	public function cart_subtotal()
	{
		$items = $this->items();

		if (!$items)
		{
			return '0.00';
		}

		$total = '0.00';

		foreach($items as $item)
		{
			$total += $item->total();
		}

		return number_format($total, 2);
	}

	/**
	 * Cart shipping total.
	 */
	public function cart_shipping_total()
	{
		$total = '0.00';

		// fetch items.
		$items = $this->items();

		if (!$items)
		{
			return $total;
		}

		foreach($items as $item)
		{
			$total += $item->shipping_price;
		}

		return number_format($total, 2);
	}

	/**
	 * Cart shipping total.
	 */
	public function cart_discount_total()
	{
		$total = '0.00';

		// fetch items.
		$items = $this->items();

		if (!$items)
		{
			return $total;
		}

		foreach($items as $item)
		{
			$total += $item->discount_price;
		}

		return number_format($total, 2);
	}

	public function applyCoupon($coupon = '')
	{
		// attempt to find the promotion.
		$promotion = \App\Promotion::where('code', '=', $coupon);

		// no valid coupon.
		if (!$promotion->count())
		{
			throw new \Exception('Invalid coupon code.');			
		}

		// ops no items found.
		if (!$this->countItems())
		{
			throw new \Exception('No items found in your shopping cart.');			
		}

		// get promotion id.
		$PromotionData = $promotion->first();

		// get items
		$Items = $this->items();

		$appliedFor = 0;

		foreach($Items as $item)
		{	
			// validate promotion for deal.
			if ($PromotionData->valid($item->deal()->id, $PromotionData->id))
			{

				$couponAdd = TRUE;
				
				// ops coupon 
				if (!$PromotionData->stackable() AND Promotion::promotion_exists($PromotionData->id))
				{
					$couponAdd = FALSE;
				}

				if ($couponAdd)
				{
					$discount_price = $PromotionData->applyAmountDiscount($item->sale_price);	

					$newPromotionData = array(
						'cart_id'      => Cart::forge()->id,
						'deal_id'      => $item->id,
						'promotion_id' => $PromotionData->id
					);

					// add the promotion for tracking.
					$leadge = Promotion::add($newPromotionData);

					$item->discount_price = $discount_price;
					$item->save();		

					// count items that coupon was applied.
					$appliedFor++;

				}
			}
		}
	}

}