<?php namespace Cart;

class Checkout {

	protected $shipping_address = FALSE;


	/**
	 * Class constructor
	 */
	public function __construct()
	{
		if (!\Session::has('checkout'))
		{
			\Session::put('checkout', array());
		}
	}

	/**
	 * Initiate class instance.
	 */

	public static function instance()
	{
		static $instance = NULL;

		if (!$instance instanceof Checkout)
		{
			$instance = new Checkout;
		}

		return $instance;
	}

	/**
	 * Add data to checkout session.
	 */
	public function add($data = array())
	{
		$session = \Session::get('checkout');

		// add data to session.
		$data = array_merge($session, $data);

		\Session::put('checkout', $data);
	}

	/**
	 * Verify if checkout is allowed.
	 * @return boolean
	 */
	public function is_checkout_allowed()
	{	
		if (!Cart::instance()->items())
		{
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Process checkout step 1.
	 * @return array
	 */
	public function step1($data = array())
	{
		extract($data);

		if ($action == 'sign-in')
		{
			// try to auth.
			$result = $this->checkout_signin($data);
		}

		if ($action == 'sign-up')
		{

		   // make sure that you are not logged in.
			\Auth::logout();

			// initiate the signup process.
			$result = $this->checkout_signup($data);			
		}

		return $result;
	}


	/**
	 * Process checkout step 2.
	 * @return array
	 */
	public function step2($data = array())
	{
		// get the user.
		$user = \User::find(\Auth::id());

		if (!$user)
		{
			return FALSE;
		}


		if ($data['shipping'] == 'existing')
		{
			Checkout::instance()->add(array('shipping_contact_id' => @$user->primary()->id));
		}

		if ($data['shipping'] == 'new')
		{
			$c = new \Contacts;
				
			// make sure that everything its ok
			$contact_data = table_columns($c->table(), $data);

			// add new contact
			$result = $user->contacts()->insert($contact_data);

			// set the session.
			Checkout::instance()->add(array('shipping_contact_id' => $result->id));
		}

		return TRUE;
	}

	/**
	 * Process the checkout sign in action.
	 */
	protected function checkout_signin($data = array())
	{	
		$auth  = TRUE;
		$error = NULL;
		$e     = NULL;

		// get the credentials.
		$credentials = array('username' => $data['username'], 'password' => $data['password']);

		try
		{
			$result = \Auth::attempt($credentials);
		}
		
		catch (\Exception $e)
		{
			$error = (array) $e->getMessage(); 			
		}

		// verify.
		if ($e instanceof \Exception)
		{
			$auth = FALSE;
		}

		return array('status' => $auth, 'error' => $error);
	}

	/**
	 * Process the checkout sign up action.
	 */
	protected function checkout_signup($data = array())
	{
		$userManager = new \UserManager;

		// set the roles.
		$data['roles'] = array(3);

		// attepmt to create the admin.
		$result = $userManager->create($data);

		if ($result['status'])
		{	
			// get user
			$user = \User::find($result['id']);

			$user->set_data(array('verified' => 1))->save();

			$credentials = array('username' => $user->email, 'password' => $user->real_password);
			$done        = \Auth::attempt($credentials);

			return array('status' => 1, 'error' => NULL);
		}
		else
		{
			return array('status' => 0, 'error' =>  $result['errors']);
		}
	}


}