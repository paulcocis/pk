<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="#">Home</a></li>
			<li class="active"><a href="#">Dashboard</a></li>
		</ul>
		<div class="page-header">
		
		<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-dashboard page-header-icon"></i>&nbsp;&nbsp;Dashboard</h1>

				<div class="col-xs-12 col-sm-8">
					<div class="row">
						<hr class="visible-xs no-grid-gutter-h">
						<!-- "Create project" button, width=auto on desktops -->
						<div class="pull-right col-xs-12 col-sm-auto"><a href="{{ $app['seller_url'] }}deals/add" class="btn btn-primary btn-labeled" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span>Create Deal</a></div>

						<!-- Margin -->
						<div class="visible-xs clearfix form-group-margin"></div>

					</div>
				</div>
			</div>

<div class="clearfix"></div>
<br>
<hr>
		
		
@section('scripts')
<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Latest 5 orders for your account.');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>
@endsection



<div class="table-primary">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
<thead>
<tr>
<th>#</th>
<th>Date</th>
<th>Amount</th>
<th>Items</th>
<th>Customer</th>
<th>Payment Status</th>
<th>Shipping Status</th>
<th>Actions</th>
</tr>
</thead>
<tbody class="t">


  @if (!$orders)

  @else

  @foreach($orders as $o)

	<tr>
		<td>{{ $o->id }}</td>
		<td><a href="#">{{ $o->created_at }}</a></td>
		<td>${{ $o->totalCost() }}</td>
		<td class="center"> {{ $o->products()->count() }}</td>
		<td class="center"> 

		@if ($o->user())
		{{ @$o->user()->primary()->firstname }}	{{ @$o->user()->primary()->lastname }} ({{ @$o->user()->email }})			 						
		@endif

		</td>
		<td class="center"> {{ $o->status }}</td>
		<td class="center">{{ $o->shipping_status }}</td>
		<td>

		<div class="btn-group btn-group-sm">
		<button type="button" class="btn btn-default" onclick="window.location='{{ $app['seller_url'] }}orders/view/{{ $o->id }}'"><i class="fa fa-edit"></i> Edit </button>

		</div>
		</td>
	</tr>
 @endforeach
 @endif




</tbody>
</table>
		
				<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->

		<!-- Page wide horizontal line -->

	</div>