<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">

<ul id="uidemo-tabs-default-demo" class="nav nav-tabs">

<li class="active">
<a href="#user-settings"  data-toggle="tab"><i class="fa fa-check"></i> My Seller Profile</a>

</li> <!-- / .dropdown -->
</ul>

<div class="tab-content tab-content-bordered no-padding">
<!-- / .tab-pane -->
<!-- / .tab-pane -->


<div class="tab-pane fade active in" id="user-settings" style="background-color:#fff;" >




<form action="{{ $app['seller_url'] }}deals/add" enctype="multipart/form-data" method="POST">
<input type='hidden' name='action' value='add'>
<input type='hidden' name='seller_id' value='{{ Auth::user()->id }}'>
<div style="padding:20px;">

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Email Address: </label>
<div class="col-sm-2">
<input type="text" class="form-control" id="inputEmail2" name='name' value='{{ Auth::user()->email }}'>
</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Sign-up date: </label>
<div class="col-sm-6">
{{ Auth::user()->created_at }}
</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Commision Rate: </label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='name' value='{{ @Auth::user()->bill()->comission_rate }}'>
</div>
</div> <!-- / .form-group -->



</div>


</div>




</div> <!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>

@section('scripts')
<script type="text/javascript">
init.push(function () {

// date options.
var options2 = {orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'}
$('#bs-datepicker-range').datepicker(options2);
$('#bs-datepicker-inline').datepicker();


	$('#bs-datepicker-component').datepicker({
format: "yyyy-mm-dd"
});

// Multiselect
$("#jquery-select2-multiple").select2({placeholder: "assign roles"});
})

window.PixelAdmin.start(init);
</script>


@endsection