<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">

<ul id="uidemo-tabs-default-demo" class="nav nav-tabs">

<li class="active">
<a href="#user-settings"  data-toggle="tab"><i class="fa fa-check"></i> Create New Deal</a>

</li> <!-- / .dropdown -->
</ul>

<div class="tab-content tab-content-bordered no-padding">
<!-- / .tab-pane -->
<!-- / .tab-pane -->


<div class="tab-pane fade active in" id="user-settings" >


@if ($added)
		
	<div class='alert alert-success'>

	   Deal has been succesfully added. <br>
	   To manage your added deal click <a href="{{ $app['seller_url'] }}deals/edit/{{ $added->id }}">here.</a>

	</div>


@endif


<form action="{{ $app['seller_url'] }}deals/add" enctype="multipart/form-data" method="POST">
<input type='hidden' name='action' value='add'>
<input type='hidden' name='seller_id' value='{{ Auth::user()->id }}'>
<input type='hidden' name='status' value='onhold'>

<div style="padding:20px;">

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Deal Title:</label>
<div class="col-sm-6">
<input type="text" class="form-control" id="inputEmail2" placeholder="Enter your deal title" name='name'>
</div>
</div> <!-- / .form-group -->








<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Deal Description:</label>
<div class="col-sm-6">
<textarea class="form-control" rows="4" name='description'></textarea>
</div>
</div> <!-- / .form-group -->

<hr>

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Product Type:</label>
<div class="col-sm-3">
<select id="jquery-select2-multiple" class="form-control" name='deal_type'>
@foreach (Deal::forge()->enum_field('deal_type') as $deal_type)
<option value="{{ $deal_type }}" label="{{ $deal_type }}">{{ $deal_type }}</option>
@endforeach								

</select>

</div>
</div> <!-- / .form-group -->


<div id="downloadableOptions" style="display:">


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Select an file:</label>
<div class="col-sm-6">
<input type="file" name="deal_downloadable_file" id="downloadable"  />

</div>
</div> <!-- / .form-group -->

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Downloading filename:</label>
<div class="col-sm-3">
<input type="text" class="form-control" id="inputEmail2" placeholder="enter the filename when downloading" name='deal_downloadble_filename'>
</div>
</div> <!-- / .form-group -->

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Maximum downloads number:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='deal_max_downloads'>
</div>
</div> <!-- / .form-group -->



<!-- / .form-group -->



<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Deal Interval:</label>
<div class="col-sm-4">
<div class="input-daterange input-group" id="bs-datepicker-range">
<input type="text" class="input-sm form-control" name="deal_start_date" placeholder="Start date">
<span class="input-group-addon">to</span>
<input type="text" class="input-sm form-control" name="deal_end_date" placeholder="End date">
</div>
</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Tax Status :</label>
<div class="col-sm-2">
<select id="jquery-select2-multiple" class="form-control" name='tax'>
<option value="1" label="Taxable">Taxable</option>
<option value="0" label="Not Taxable">Not Taxable</option>


</select>

</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Commission Rate Override:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='comission_rate_override'>

</div>
</div> <!-- / .form-group -->

<h3>Pricing </h3>
<hr>
<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Original Price:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='original_price'>

</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Sale Price:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='sale_price'>

</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Discount Amount:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='discount_price'>
</div>
<div class="col-sm-1" style="padding-top:4px;">
<label class="checkbox-inline">
<input type="checkbox" id="inlineCheckbox1" name='discount_percent_off' value="1" class="px"> <span class="lbl"> Percent Off?</span>
</label>
</div>

</div> <!-- / .form-group -->


<h3>Quantity And Shipping </h3>
<hr>
<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Quantity:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='quantity'>

</div>
</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Ships within :</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='ship_within'>

</div>


</div> <!-- / .form-group -->


<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">First Item Shipping Cost:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='first_item_shipping_cost'>

</div>
</div> <!-- / .form-group -->

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Aditional Item Shipping Cost:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='aditional_item_shipping_cost'>

</div>
</div> <!-- / .form-group -->

<!-- / .form-group -->

<hr>

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Maximum orders / customer:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='maximum_order_per_customer'>

</div>
</div> <!-- / .form-group -->
<hr>

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Move to PinkeStore after deal ends?:</label>
<div class="col-sm-1">
<input type="checkbox" id="inlineCheckbox1" value="1" name='pk_move'>


</div>
</div> <!-- / .form-group -->

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">PinkeStore Price:</label>
<div class="col-sm-1">
<input type="text" class="form-control" id="inputEmail2" name='pk_price'>

</div>
</div> <!-- / .form-group -->

<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">PinkeStore End Date:</label>
<div class="col-sm-1">
<div class="input-group date" id="bs-datepicker-component">
							<input type="text" class="form-control" name='pk_end_date'><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
</div>
</div> <!-- / .form-group -->




<div class="form-group">
<label for="inputEmail2" class="col-sm-2 control-label">Require Personalization?:</label>
<div class="col-sm-1">
<input type="checkbox" id="inlineCheckbox1"   value="1" name='require_personalization'>


</div>
</div>


<div class="form-group" style="margin-top:20px;">
<div class="col-sm-2">
<label for="inputEmail2" class="col-sm-2 control-label">&nbsp;</label>
</div>
<div class="col-sm-1">


<button class='btn btn-success'>Add Deal</button>

</div>
</div>



<!-- / .form-group -->
<br>

</div>


</div>




</div> <!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>

@section('scripts')
<script type="text/javascript">
init.push(function () {

// date options.
var options2 = {orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'}
$('#bs-datepicker-range').datepicker(options2);
$('#bs-datepicker-inline').datepicker();


	$('#bs-datepicker-component').datepicker({
format: "yyyy-mm-dd"
});

// Multiselect
$("#jquery-select2-multiple").select2({placeholder: "assign roles"});
})

window.PixelAdmin.start(init);
</script>


@endsection