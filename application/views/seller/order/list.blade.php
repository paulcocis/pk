<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="{{ $app['seller_url'] }}home/">Home</a></li>
<li class="active"><a href="{{ $app['seller_url'] }}orders/view/">Order List</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================



No label form
-->





<!-- /7. $NO_LABEL_FORM -->

@section('scripts')
<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Orders');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>
@endsection



<div class="table-primary">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
<thead>
<tr>
<th>#</th>
<th>Date</th>
<th>Amount</th>
<th>Your Earning</th>
<th>Items</th>
<th>Customer</th>
<th>Payment Status</th>
<th>Shipping Status</th>
<th>Actions</th>
</tr>
</thead>
<tbody class="t">


  @if (!$orders)

  @else

  @foreach($orders as $o)

	<tr>
		<td>{{ $o->id }}</td>
		<td><a href="#">{{ $o->created_at }}</a></td>
		<td>${{ $o->totalCost() }}</td>
		<td>
@if ($o->sellerInvoice()->where('seller_id', '=', Auth::user()->id)->count())
<div class="label label-success"> ${{ $o->sellerInvoice()->where('seller_id', '=', Auth::user()->id)->sum('amount') }}</div>
@endif			

		</td>

		<td class="center"> {{ $o->products()->count() }}</td>
		<td class="center"> 

		@if ($o->user())
		{{ @$o->user()->primary()->firstname }}	{{ @$o->user()->primary()->lastname }} ({{ @$o->user()->email }})			 						
		@endif

		</td>
		<td class="center"> {{ $o->status }}</td>
		<td class="center">{{ $o->shipping_status }}</td>
		<td>

		<div class="btn-group btn-group-sm">
		<button type="button" class="btn btn-default" onclick="window.location='{{ $app['seller_url'] }}orders/view/{{ $o->id }}'"><i class="fa fa-edit"></i> Edit </button>

		</div>
		</td>
	</tr>
 @endforeach
 @endif




</tbody>
</table>

<br><br>


@section('scripts')
<script>
init.push(function () {
$('.ui-wizard-example').pixelWizard({
onChange: function () {
console.log('Current step: ' + this.currentStep());
},
onFinish: function () {
// Disable changing step. To enable changing step just call this.unfreeze()
this.freeze();
console.log('Wizard is freezed');
console.log('Finished!');
}
});

$('.wizard-next-step-btn').click(function () {
$(this).parents('.ui-wizard-example').pixelWizard('nextStep');
});

$('.wizard-prev-step-btn').click(function () {
$(this).parents('.ui-wizard-example').pixelWizard('prevStep');
});

$('.wizard-go-to-step-btn').click(function () {
$(this).parents('.ui-wizard-example').pixelWizard('setCurrentStep', 2);
});
});
</script>


<script>
init.push(function () {
var options2 = {
orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'
}
$('#bs-datepicker-range').datepicker(options2);

$('#bs-datepicker-inline').datepicker();
});
</script>


@endsection


<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>