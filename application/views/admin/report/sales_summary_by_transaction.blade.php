

<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="{{ $app['admin_url'] }}home/">Reports</a></li>
<li class="active"><a href="{{ $app['admin_url'] }}reports/invoices/">Sales Summary by transactions </a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================




<!-- /7. $NO_LABEL_FORM -->

@section('scripts')
<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Seller Invoices');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>

<script>
$(document).ready(function () {


$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();

    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});




});
</script>

<script type="text/javascript">
init.push(function () {

// date options.
var options2 = {orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'}
$('#bs-datepicker-range').datepicker(options2);
$('#bs-datepicker-inline').datepicker();


	$('#bs-datepicker-component').datepicker({
format: "yyyy-mm-dd"
});

// Multiselect
$("#jquery-select2-multiple").select2({placeholder: "assign roles"});
})

window.PixelAdmin.start(init);
</script>


@endsection





<?php
 
   $report = new App\Report\Transaction;

	$start = Input::get('start', date('Y-m-d', strtotime('-1 month')));
	$end   = Input::get('end', date('Y-m-d'));

	$start = date('Y-m-d', strtotime($start));
	$end   = date('Y-m-d', strtotime($end));

   $salesSummaryByPeriod = $report->salesSummaryByPeriod($start, $end);

?>


<!-- Primary table -->
								<div class="table-primary">
									<div class="table-header">
										<div class="table-caption">
											Sales Summary by Period  ( {{ $start}} - {{ $end }} )
										</div>
									</div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Gross</th>
												<th>Net</th>
												<th>Total Shipping</th>
												<th>Total Tax</th>
												<th>Total Commission</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>${{ $salesSummaryByPeriod['gross'] }}</td>
												<td>${{ $salesSummaryByPeriod['net'] }}</td>
												<td>${{ $salesSummaryByPeriod['shipping'] }}</td>
												<td>${{ $salesSummaryByPeriod['tax'] }}</td>
												<td>${{ $salesSummaryByPeriod['commission'] }}</td>
											</tr>
								
										</tbody>
									</table>
									<div class="table-footer">

<form name='SelectInterval' method="GET" action="{{ $app['admin_url'] }}reports/salesSummaryByTransactions" enctype="multipart/form-data" >
 
<div class="form-group">
<label for="inputEmail2" class="col-sm-1 control-label" style="padding-top:5px;">Apply Interval:</label>
<div class="col-sm-4">
<div class="input-daterange input-group" id="bs-datepicker-range">
<input type="text" class="input-sm form-control" name="start" placeholder="Start date">
<span class="input-group-addon">to</span>
<input type="text" class="input-sm form-control" name="end" placeholder="End date">
</div>
</div>
</div> <!-- / .form-group -->

<div class="form-group" style="margin-top:20px;">
<div class="col-sm-1">
<label for="inputEmail2" class="col-sm-2 control-label">&nbsp;</label>
</div>
<div class="col-sm-1">


<button class='btn btn-success'>Apply Interval</button>

</div>
</div>

</form>


									</div>
								</div>
								<!-- / Primary table -->