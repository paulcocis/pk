<style>
.datepicker { z-index:1051 !important; }



.items
{
 margin-bottom:20px;
 float: left;
 padding-top:10px;
 padding-bottom:10px;
border-top:1px solid #e1e1e1;
width: 98%;


}

.items .image {
 float:left;
 width: 109px;
 padding:5px;
 height: 110px;
 border-radius:5px;
 border:1px solid #e1e1e1;

}

.items .desc {
float:left;
padding-left:10px;
}


.ttable {
 padding:0px;
 margin: 0px;
 border: 0px;
 background-color: #fff;
 font-size:14px;

}

.ttable td
{
	padding: 10px;
 background-color: #fff;
 font-size:14px;	
}

</style>


<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">X</button>
								<h4 class="modal-title">Order Details</h4>
							</div>
							<div class="modal-body" style="width:600px;">

 <div style="font-size:14px;">

@if ($order->shipped())
<div class="alert alert-info">

 Order has been marked as shipped and scheduled date is: <?php echo date('Y-m-d', strtotime($order->scheduled_shipping_at)); ?>

</div>
@else
<div class="alert alert-warning">

 <b>Order has not been yet shipped. </b>

</div>

@endif


<div class="alert alert-info">


<form action="{{ $app['admin_url'] }}orders/manage/schedule-shipping/{{ $order->id }}" id="form" style="margin-top:0px;">
<input type="hidden" name='action' value='schedule'>
<table cellpadding="0" cellspacing="0" class="ttable" style="margin-top:10px;" >

<tr>
<td>New status:</td>
<td>
	<select style="padding:4px;" name='status'>
	@foreach (Order::forge()->enum_field('shipping_status') as $status)
		<option value="{{ $status }}">{{ $status }}</option>
	@endforeach
	</select>

</td>
</tr>
<tr>
<td valign="top">Shipping mail message:</td>
<td><textarea name='shipping_message' style="width:100%; height:100px;">{{ $order->shipping_message }}</textarea></td>
</tr>

<tr>
<td >Shipping Date:</td>
<td><input type="text" class="form-control sDate" id="orderDate" name='date' value="{{ $order->scheduled_shipping_at }}">	
</td>
</tr>


<tr>
<td> </td>
<td><input type="submit" class="btn btn-small" value="Update Shipping Info">
</td>
</tr>

</table>
</form>
</div>
<br>

</div>


<script>

//callback handler for form submit
$("#form").submit(function(e)
{
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            if (data == 'OK')
            {
            	location.reload();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
    e.unbind(); //unbind. to stop multiple form submit.
});

</script>