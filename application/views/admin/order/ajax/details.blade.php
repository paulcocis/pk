<style>

.items
{
 margin-bottom:20px;
 float: left;
 padding-top:10px;
 padding-bottom:10px;
border-top:1px solid #e1e1e1;
width: 98%;


}

.items .image {
 float:left;
 width: 109px;
 padding:5px;
 height: 110px;
 border-radius:5px;
 border:1px solid #e1e1e1;

}

.items .desc {
float:left;
padding-left:10px;
}


table {
 padding:0px;
 margin: 0px;
 border: 0px;

}

table td
{
	padding: 10px;
}

</style>


<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">X</button>
								<h4 class="modal-title">Order Details</h4>
							</div>
							<div class="modal-body" style="width:600px;">

 <div style="font-size:14px;">

<table cellpadding="0" cellspacing="0">
<tr>
<td>Cost</td>
<td>${{ $order->totalCost(FALSE) }}</td>
</tr>
<tr>
<td>Shipping Cost</td>
<td>${{ $order->totalShippingAmount() }}</td>
</tr>
<tr>
<td>PinkePromise Earn</td>
<td>${{ $order->pk_earning() }}</td>
</tr>

<tr>
<td>Status</td>
<td>{{ $order->status }}</td>
</tr>

<tr>
<td>Shipping Status</td>
<td>{{ $order->shipping_status }}</td>
</tr>

</table>




<br><br>
 Order has been placed at {{ $order->created_at }} by:
 <div style="font-size:13px;">

<b>Customer</b><br>
 {{ @$order->user()->primary()->firstname }} {{ @$order->user()->primary()->lastname }}<br>
 {{ $order->user()->email }}<br>
 <b>Shipping</b><br>
 {{ $order->shipping_user()->firstname }} {{ $order->shipping_user()->lastname }}<br>
 {{ $order->shipping_user()->address }} <br>
{{ $order->shipping_user()->city }}, {{ $order->shipping_user()->state }}, {{ $order->shipping_user()->zipcode }} <br>
 {{ $order->shipping_user()->country }} 
 </div> 

</div>

<br><br>								
 @foreach($order->products()->get() as $row)

<div class="items">

   
<div class="image"> 
 @if ($row->deal() AND $row->deal()->getPrimaryImage())
 <img src="{{ $row->deal()->getPrimaryImage()->thumb_url(100,100) }}" style="border-radius:4px;">
 @endif
 </div>

<div class="desc"> 
   <span style="font-size:16px;">{{ $row->name }}</span> <br>
   <span style="font-size:12px;color:#333;">Owned by: {{ $row->deal()->user()->email }}</span> <br>

    <span style="font-size:12px;color:#333;">Cost: ${{ $row->simple_cost() }}</span> <br>
    <span style="font-size:12px;color:#333;">Shipping cost: ${{ $row->shipping_amount }}</span> <br>
   <span style="font-size:12px;color:#333;">Quantity: {{ $row->quantity }}</span> <br>

   </div>


</div>

 @endforeach







							</div>