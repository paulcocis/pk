<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================



No label form
-->

<style>
.list-group-item { border-color:#f1f1f1;}
</style>

<style>
body.modal-open .datepicker {
    z-index: 1200 !important;
}
</style>

<!-- /7. $NO_LABEL_FORM -->

@section('scripts')
<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Deals');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});

$(document).ready(function() {

$('#manageShipping').on('show.bs.modal', function (e) {
     $('#schedule_shipping_at').datepicker({
         format: 'yyyy-mm-dd',
         changeMonth: true,
         changeYear: true,
     });
})


});

$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();
    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});

</script>

@endsection

@if (Session::has('ActionMessage'))
<div class="alert alert-success">
{{ Session::get('ActionMessage') }}
</div>
@endif


				

<div id='left-pp' style="float:left; width:60%;">
		<ul id="uidemo-tabs-default-demo" class="nav nav-tabs">
							<li class="active">
								<a href="#generalOrderInfo" data-toggle="tab">General Order Informations</a>
							</li>
							<li class="">
								<a href="#orderTransactions" data-toggle="tab">Transactions <span class="badge badge-primary">{{ $Order->transactions()->count() }}</span></a>
							</li>
				
		<li class="">
								<a href="#promotionUsage" data-toggle="tab">Promotions Usage <span class="badge badge-primary"></span></a>
							</li>
				

						</ul>



						<div class="tab-content" style="padding:0px;">
							<div class="tab-pane fade active in" id="generalOrderInfo">

				<div class="panel colourable">
					<div class="panel-heading">
						<span class="panel-title">General Order Information</span>
						<div class="panel-heading-controls">
							<button class="btn btn-xs btn-warning btn-outline" onclick="window.location='{{ $app['admin_url'] }}orders/view/do/regenerateSellerTransactions?order={{ $Order->id }}'"><span class="fa fa-refresh"></span>&nbsp;&nbsp;Regenerate Seller Invoices</button>
							<div class="btn-group btn-group-xs">
								<button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-cog"></span>&nbsp;<span class="fa fa-caret-down"></span></button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#">Send Confirmation Email</a></li>
									<li class="divider"></li>
									<li><a href="#">Delete Order</a></li>
								</ul> <!-- / .dropdown-menu -->
							</div> <!-- / .btn-group -->
						</div> <!-- / .panel-heading-controls -->
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

<ul class="list-group">
										<li class="list-group-item">
											<span style='float:right;'>{{ $Order->created_at }}</span>
											Order Date
										</li>

							<li class="list-group-item"  style="height:170px;">
											<span style='float:right;'>
											<b>{{ $Order->user()->primary()->firstname }} {{ $Order->user()->primary()->lastname }}</b> (<a href="{{ $app['admin_url'] }}customers/info/{{ $Order->user()->id }}" target="_blank">view profile</a>)<br>
											<small>
											{{ $Order->user()->email }}
											<br>
											customer since 18, Jun, 2014
											<br><br>
											<b>Order Will Be Shipped To:</b><br>
											{{ $Order->shipping_user()->firstname }} {{ $Order->shipping_user()->lastname }}<br>
											{{ $Order->shipping_user()->address }}<br>
											{{ $Order->shipping_user()->city }}, {{ $Order->shipping_user()->state }}, {{ $Order->shipping_user()->zipcode }}


											</small>
											</span>
											Customer


										</li>

										<li class="list-group-item">
											<span style='float:right;'>{{ $Order->gateway()->display_name }}  (<a href='#' data-toggle="modal" data-target="#changeGateway">change</a>)</span>
											Tender
										</li>

										<li class="list-group-item">
											<span style='float:right;'>{{ $Order->status() }} (<a href='#' data-toggle="modal" data-target="#changeOrderStatus">change</a>)</span>
											Status
										</li>

										<li class="list-group-item">
											<span style='float:right;'>{{ $Order->shipping_status }} (<a href='#' data-toggle="modal" data-target="#manageShipping">manage</a>)</span>
											Shipping Status
										</li>										

										<li class="list-group-item">
											<span style='float:right;'>${{ $Order->totalCost(false) }}</span>
											Initial cost
										</li>
	<li class="list-group-item" style="border-top:0px;">
											<span style='float:right;'>${{ $Order->totalTaxAmount() }}</span>
											Taxes total
										</li>

		<li class="list-group-item" style="border-top:0px;">
											<span style='float:right;'>${{ $Order->totalShippingAmount() }}</span>
											Total Shipping Cost
										</li>
		<li class="list-group-item" style="border-top:0px;"> 
											<span style='float:right;'>${{ $Order->totalDiscountAmount() }}</span>
											Total Generated Discount
										</li>


		<li class="list-group-item" style="border-top:1px solid #f1f1f1; background-color:#E6F5DF;">
											<span style='float:right;'>${{ $Order->totalCost() }}</span>
											Total Order Amount
									</li>	


		<li class="list-group-item" style="border-top:1px solid #f1f1f1;">
											<span style='float:right;'>${{ $Order->sellerinvoice()->sum('amount') }}</span>
											Seller Net
									</li>									
		<li class="list-group-item" style="border-top:1px solid #f1f1f1;">
											<span style='float:right;'>${{ $Order->pk_earning() }}</span>
											PinkePromise Net
									</li>								
									</ul>


					</div>
				</div> <!-- / .panel -->
</div>
							<div class="tab-pane fade" id="orderTransactions">
								

				<div class="panel colourable">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<span class="panel-title">Available transactions</span>
						<div class="panel-heading-controls">
							<span class="panel-heading-text">&nbsp;&nbsp;<span style="color: #ccc">|</span>&nbsp;&nbsp;</span>
							<button class="btn btn-xs"  data-toggle="modal" data-target="#createRefundTransaction">Create Refund Transaction</button>
						</div>
					</div> <!-- / .panel-heading -->

					<!-- Table -->
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Tender</th>
								<th>Tender ID</th>
								<th>Status</th>
								<th>Amount</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($Order->transactions()->get() as $transaction)
							<tr>
								<td>{{ $transaction->id }}</td>
								<td>{{ $transaction->gateway()->display_name }}</td>
								<td>{{ $transaction->parentTransactionId() }}</td>
								<td>{{ $transaction->status }}</td>
								<td>${{ $transaction->amount() }}</td>
								<td>
								<a href='#' onclick="$('#d-{{ $transaction->id }}').toggle();"> More Details</a> |
								<a href="{{ $app['admin_url'] }}orders/view/do/deleteTransaction?order={{ $Order->id }}&transactionId={{ $transaction->id }}">Delete</a></td>
							</tr>

							<tr id="d-{{ $transaction->id }}" style="display:none;">
							<td colspan="6">
								<div style="padding:10px">

								@if ($transaction->status == 'refunded')
								Reason: <b>{{ $transaction->getRefundReason() }} </b><br>
								@endif

								
								<div class="alert alert-info" style="margin-top:5px;">
								@if (!empty($transaction->custom_invoice_notice))
								{{ $transaction->custom_invoice_notice }} <br>	
								@else
								No additional commnets for that transaction.

								@endif
								<hr>
								Change comment:<br>
								<form action="{{ $app['admin_url'] }}orders/view/do/changeTransactionComment" method="GET">

								<input type="hidden" name='transactionId' value="{{ $transaction->id }}">
								<input type="hidden" name='order' value="{{ $Order->id }}">

								<textarea name="comment" style="margin-top:10px; width:100%; height:100px;"></textarea>
								<button type="submit">Save transaction comment</button>
								</form>
								</div>							
								



								</div>

							</td>
							</tr>

							@endforeach
				
						</tbody>
					</table>
				</div>




							</div>



							<div class="tab-pane fade" id="promotionUsage">
								

				<div class="panel colourable">
					<!-- Default panel contents -->
					
					<div style="padding:10px"> 

					@if ($Order->getAvailablePromotions())
					<h4> Following promotions was used:</h4><hr>
					@foreach ($Order->getAvailablePromotions() as $o)

						<b>{{ $o->code }}</b> &nbsp;&nbsp;<button type="button" style="display:none;" class="btn btn-default btn-sm" data-toggle="modal" href='#editPromotional' data-remote-target="#editPromotional .modal-content"  data-load-remote='{{ $app["admin_url"] }}promotions/edit/{{ $o->id }}'><i class="fa fa-edit"></i> View/Edit </button><br>

					@endforeach

					@endif

					</div>


				</div>




							</div>





</div>



</div>

<div id='right-pp' style="float:right; width:39%;">
	<div class="col-sm-4 col-md-12" style="float:left; width:100%;margin:0px;margin-top:35px;">
						<div class="stat-panel">
							<!-- Danger background, vertically centered text -->
							<div class="stat-cell bg-success valign-middle">
								<!-- Stat panel bg icon -->
								<i class="fa fa-trophy bg-icon"></i>
								<!-- Extra large text -->
								<span class="text-xlg"><span class="text-lg text-slim">$</span><strong>{{ $Order->pk_earning() }}</strong></span><br>
								<!-- Big text -->
								<span class="text-bg">PinkePromise Earnings</span><br>
								<!-- Small text -->
							</div>
						</div> <!-- /.stat-panel -->
					</div>
				<div class="panel colourable"  style="float:left; width:100%;">
					<div class="panel-heading">
						<span class="panel-title">Order Products</span>
					
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

<ul class="list-group">
	

@foreach ($Order->products()->get() as $product)
	
	@if (!$product->is_downloadable())


	<li class="list-group-item" style="float:left; width:100%; height:180px;">
	<div style="float:left; width:120px;height:120px;border:1px solid #f1f1f1;padding:10px;">
		<img src='{{ $product->deal()->getPrimaryImage()->thumb_url(100,100) }}' width="100" height="100">
	</div>
	<div style="float:left;margin-left:20px; width:70%;">
		<span style='font-size:16px;'>
		{{ $product->name }} </span>
		<br>
		<small>
		Quantity: {{ $product->quantity }}<br>
		 Taxes (if applicable): ${{ $product->tax }} <br>
		 Shipping cost: ${{ $product->shipping_amount }}<br>
		 Discount amount: ${{ $product->discount_amount }} <br>
		 Total cost: ${{ $product->cost() }}<br>
		</small>
	</div>

	<div style="float:left;width:99%;margin-top:10px;padding-top:6px;border-top:1px solid #f1f1f1;">
		<a href='#'>Delete Product </a> | <a href='#'>View Options </a>
	</div>
	</li>


    @else

	<li class="list-group-item" style="float:left; width:100%; height:180px;">
	<div style="float:left; width:120px;height:120px;border:1px solid #f1f1f1;padding:10px;">
		<img src='{{ $images_url }}digitalitem.png' width="100" height="100">
	</div>
	<div style="float:left;margin-left:20px; width:70%;">
		<span style='font-size:16px;'>
			{{ $product->name }}  </span>
		<br>
			<small>
		Quantity: {{ $product->quantity }}<br>
		 Taxes (if applicable): ${{ $product->tax }} <br>
		 Shipping cost: ${{ $product->shipping_amount }}<br>
		 Discount amount: ${{ $product->discount_amount }} <br>
		 Total cost: ${{ $product->cost() }}<br>
		</small>
	</div>

	<div style="float:left;width:99%;margin-top:10px;padding-top:6px;border-top:1px solid #f1f1f1;">
		<a href='#'>
		Delete Product | Test Download </a>
	</div>
	</li>

	@endif

	@endforeach


</ul>

					</div>
				</div> <!-- / .panel -->


<div class="panel widget-threads" style="float:left; width:100%;">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-comments-o"></i>Seller Invoices (<a href="{{ $app['admin_url'] }}reports/invoices?orderId={{ $Order->id }}">manage</a>)</span>
					</div> <!-- / .panel-heading -->
					<div class="panel-body">
						
						@if ($Order->sellerInvoice()->count() == 0)

						@else

						@foreach($Order->sellerInvoice()->get() as $sellerInvoice)
						<div class="thread">
							<div class="thread-body" style="margin-left:0px;">
								<span class="thread-time">{{ $sellerInvoice->created_at }}</span>
								<a href="#" class="thread-title">Invoice number #{{ $sellerInvoice->id }}</a>
								<div class="thread-info">for seller <a href="#" title="">{{ $sellerInvoice->seller()->bill()->business_name }}</a> in <a href="#" title="">amount of ${{ $sellerInvoice->amount() }}</a>
								<br>
								</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->
						@endforeach

						@endif

			
					</div> <!-- / .panel-body -->
				</div> 



	<div class="panel widget-comments" style="float:left; width:100%;">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-comment-o"></i>Comments</span>
<div class="panel-heading-controls">
							<button class="btn btn-xs btn-warning btn-outline" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span>&nbsp;&nbsp;Add Comment</button>
							
						</div> <!-- / .panel-heading-controls -->


					</div> <!-- / .panel-heading -->
					<div class="panel-body">
					@if (!$Order->comments()->count())
						<div class="alert alert-warning">
						  No comments available for this order.
						</div>
					@else

					@foreach($Order->comments()->get() as $comment)
						<div class="comment">
							<img src="{{ $comment->author()->avatar()->get() }}" alt="" class="comment-avatar">
							<div class="comment-body">
								<div class="comment-by">
									Author <a href="#" title="">{{ $comment->author()->primary()->firstname }} {{ $comment->author()->primary()->lastname }}</a> 
									@if ($comment->product())
									commented for product <a href="#" title="">{{ $comment->product()->name }}</a>
									@endif
								</div>
								<div class="comment-text">
									{{ $comment->description }}
								</div>
								<div class="comment-actions">
									<a href="#"><i class="fa fa-pencil"></i>Edit</a>
									<a href="{{ $app['admin_url'] }}orders/view/do/deleteOrderComment?commentId={{ $comment->id }}&order={{ $Order->id }}"><i class="fa fa-times"></i>Remove</a>
									<span class="pull-right">{{ $comment->datediff() }}</span>
								</div>
							</div> <!-- / .comment-body -->
						</div> <!-- / .comment -->
						@endforeach

					@endif
			

					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->

</div>





<!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>


@section('modals')
		<!-- Modal -->
				<div id="myModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Add New Order Comment</h4>
							</div>
							<div class="modal-body">
							

	<form action="{{ $app['admin_url'] }}orders/view/do/addOrderComment" id='AddCommentForm' method='POST' class="panel form-horizontal" style="border:0px; background-color:#f1f1f1;">
	<input type='hidden' name='order' value='{{ $Order->id }}'>
	<input type='hidden' name='author_id' value='{{ Auth::user()->id }}'>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
	<select class="form-control input-sm" style="margin-bottom: 0;" name='product_id'>
	<option value="0">All products</option>
							@foreach ($Order->products()->get() as $product)

							<option value="{{ $product->id }}">{{ $product->name }}</option>
							@endforeach
						</select>
							</div>
				
						</div><!-- row -->
						<br>
						<textarea class="form-control" rows="5" name='description' placeholder="Message"></textarea>
					</div>
					
				</form>




							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick='$("#AddCommentForm").submit();' >Add Comment</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->

<!-- /5. $DEFAULT_MODAL -->

		<!-- Modal -->
				<div id="changeGateway" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Change Payment gateway</h4>
							</div>
							<div class="modal-body">
							


	<form action="{{ $app['admin_url'] }}orders/view/do/changeOrderGateway" id='gatewayForm' method='POST' class="panel form-horizontal" style="border:0px; background-color:#f1f1f1;">
	<input type='hidden' name='order' value='{{ $Order->id }}'>
					<div class="panel-body" style="border:0px;background-color:#f1f1f1;">
						<div class="row">
							<div class="col-md-12">

	Select the new tender (gateway): 							
	<select class="form-control input-md" style="margin-bottom: 0;margin-top:3px;" name='gateway_id'>
							@foreach (App\Setup\PaymentGateway::get() as $gw)

							<option value="{{ $gw->id }}">{{ $gw->display_name }}</option>

							@endforeach
						</select>
							</div>
				
						</div><!-- row -->
						<br>
						<div class="alert alert-info">
						 This action will change the current tender ({{ $Order->gateway()->display_name }}) with selected one.
						</div>
					</div>
				</form>




							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick='$("#gatewayForm").submit();'>Save Changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->


		<!-- Modal -->
				<div id="changeOrderStatus" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Manage Order Status</h4>
							</div>
							<div class="modal-body">
							

	<form action="{{ $app['admin_url'] }}orders/view/do/ChangeOrderStatus" id='StatusForm' method='POST' class="panel form-horizontal" style="border:0px; background-color:#f1f1f1;">
	<input type='hidden' name='order' value='{{ $Order->id }}'>

					<div class="panel-body" style="border:0px;background-color:#f1f1f1;">
						<div class="row">
							<div class="col-md-12">

	Select the new order status: 							
	<select class="form-control input-md" style="margin-bottom: 0;margin-top:3px;" name='status'>
							@foreach (Order::forge()->enum_field('status') as $status)

							<option value="{{ $status }}">{{ $status }}</option>

							@endforeach
						</select>
							</div>
				
						</div><!-- row -->
						<br>
					
					</div>
				</form>




							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick='$("#StatusForm").submit();'>Save Changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->


		<!-- Modal -->
				<div id="manageShipping" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Manage Shipping</h4>
							</div>
							<div class="modal-body">
							
							@if ($Order->shipped())	
								<div class="alert alert-warning">
								<b>Warning</b> <br>
								Your order has been marked as shipped, and was scheduled for date {{ $Order->scheduled_shipping_at }}.<br>
								If you want to cancel the process please click on button below. <br>
								<br>
								<button class="btn btn-small btn-info" onclick="window.location='{{ $app['admin_url'] }}orders/view/do/cancelOrderShipping?order={{ $Order->id }}'">Cancel shipping process </button>


								</div>

							@else




	<form action="{{ $app['admin_url'] }}orders/view/do/scheduleOrderShipping" id='ShippingForm' method='POST' class="panel form-horizontal" style="border:0px; background-color:#f1f1f1;">
	<input type='hidden' name='order' value='{{ $Order->id }}'>
					<div class="panel-body" style="border:0px;">

				<div class="alert alert-info">
					Schedule shipping for order <b>#{{ $Order->id }}</b><br>
					You can add a custom message when deliver the shipping delivery email to customer.
				</div>


						<div class="row">					
							<div class="col-md-12">
<input type="text" class="form-control" name='schedule_shipping_at' id="schedule_shipping_at" placeholder='Select the shipping date ...'>
							</div>
					
						</div><!-- row -->
						<br>
						<textarea class="form-control" rows="5"name='shipping_message' placeholder="Add a custom message when sending shipping email to customer."></textarea>
					</div>
			
				</form>



							@endif

							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick='$("#ShippingForm").submit();'>Save Changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->




		<!-- Modal -->
				<div id="createRefundTransaction" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Create New Refund Transaction</h4>
							</div>
							<div class="modal-body">
							
							@if (!$Order->isRefundAllowed())	
								<div class="alert alert-warning">
								<b>Warning</b> <br>
								You cannot create anoter refund transaction for this order.<br>
								You have reached the maximum refund amount that is allowed.

								<br>
								<button class="btn btn-small btn-info" onclick="window.location='{{ $app['admin_url'] }}orders/view/do/removeAllRefundTransactions?order={{ $Order->id }}'">Remove all refund transactions</button>


								</div>

							@else




	<form action="{{ $app['admin_url'] }}orders/view/do/createRefundTransaction" id='refundTransactionForm' method='POST' class="panel form-horizontal" style="border:0px; background-color:#f1f1f1;">
	<input type='hidden' name='order' value='{{ $Order->id }}'>
					<div class="panel-body" style="border:0px;">

				<div class="alert alert-info">
					Maximum refund amount allowed at this moment is ${{ $Order->maxOrderRefundAmount() }}.
				</div>


						<div class="row">					
							<div class="col-md-12">
<input type="text" class="form-control" name='amount'  placeholder='Enter refund amount ...' style="width:40%;">
							</div>
						<div class="col-md-12" style="margin-top:10px;">
			<select name="refund_reason" style="width:100%;padding:5px;">
    @foreach(App\Billing\Transaction::forge()->enum_field('refund_reason') as $reason)
    <option>{{ $reason }}</option>
    @endforeach

</select>
										</div>

						</div><!-- row -->
						<br>
						<textarea class="form-control" rows="5"name='custom_invoice_notice' placeholder="Add a custom notice to this transaction."></textarea>
					</div>
			
				</form>



							@endif

							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick='$("#refundTransactionForm").submit();'>Save Changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- /.modal -->
				<!-- / Modal -->



<div id="editPromotional" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>
				</div>


@endsection


