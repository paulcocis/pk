<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================



No label form
-->




<!-- /7. $NO_LABEL_FORM -->

<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Deals');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>


<button class="btn btn-sm btn-default" onclick="window.location='{{ $app['admin_url'] }}orders/view/order/{{ $o->id }}?action=resendConfirmationEmail'">Resend Order Email </button> <button class="btn btn-sm btn-danger" onclick="window.location='{{ $app['admin_url'] }}orders/view/order/{{ $o->id }}?action=delete'">Delete</button> 

<div class="clearfix"></div>
<hr>
<ul id="uidemo-tabs-default-demo" class="nav nav-tabs">
<li class="active">
<a href="#uidemo-tabs-default-demo-home" data-toggle="tab">Order Details</a>
</li>
<li class="">
<a href="#products" data-toggle="tab">Products <span class="badge badge-primary">{{ $o->products()->count() }}</span></a>
</li>

	<li class="dropdown">
								<a href="#" class="dropdown-toggle" style="display:none;" data-toggle="dropdown">Comments &nbsp;&nbsp;<i class="fa fa-caret-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#comments" data-toggle="tab">View Comments</a></li>
									<li><a href="#uidemo-tabs-default-demo-dropdown2" data-toggle="tab">Add new comment</a></li>
								</ul>
							</li> <!-- / .dropdown -->



</ul>

<style>
.itable{


}

.itable td
{

padding-top:10px;
font-size:13px;
font-family:Calibri;
font-weight:normal;
color:#333;
}

.itable td  a
{
color: #5CABE4!important;
}


</style>

<div class="tab-content tab-content-bordered no-padding">


<div class="tab-pane fade in no-padding" id="comments">
<div class="panel widget-support-tickets" style="margin-bottom:0px;">

<div class="panel-body">

<!-- / .ticket -->


<div class="ticket">
<a href="#" title="" class="ticket-title">

<button class="btn btn-xs btn-success" onclick="$('#c1').toggle();">View Comment</button>&nbsp;<span> By: </span> Sales Manager on 2014/04/15 15:20 &nbsp; &nbsp;&nbsp; </a>
<span class="ticket-info" id="c1" style="display:none;" >
<hr>
<div style="width:40%;margin-top:10px;font-size:13px;">
  Thank you for your order. Your product will be shipped today.

<div class="clearfix"></div>


 
</div>



<div class="clearfix"></div>


</span>



</div>


<div class="ticket">
<a href="#" title="" class="ticket-title"><button class="btn btn-xs btn-success" onclick="$('#c2').toggle();">View Comment</button>&nbsp; <span>By: </span> John Doe on 2014/04/15 14:20 &nbsp; &nbsp;&nbsp;</a>
<span class="ticket-info" id="c2" style="display:none;" >
<hr>
<div style="width:40%;margin-top:10px;font-size:13px;">
 Please let me know when product will be shipped.


</div>


<div class="clearfix"></div>


</span>



</div>













</div> <!-- / .panel-body -->
</div> <!-- / .panel -->
</div>





<div class="tab-pane fade in no-padding" id="products">
<div class="panel widget-support-tickets" style="margin-bottom:0px;">

<div class="panel-body">

<!-- / .ticket -->


@if ($o->products()->count())



@foreach($o->products()->get() as $item)

<div class="ticket">
<a href="#" title="" class="ticket-title"><button class="btn btn-xs btn-success" onclick="$('#p{{ $item->id }}').toggle();">More details</button>&nbsp; Deal: <span>{{ $item->name }}&nbsp; &nbsp;&nbsp; </span></a>
<span class="ticket-info" id="p{{ $item->id }}" style="display:none;" >
<hr>
<div style="width:40%;margin-top:10px;font-size:13px;">

<div class="form-group" style="">
<div class="col-sm-4" >Tax Amount:</div>
<div class="col-sm-4">${{ $item->tax }}</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Shipping cost:</div>
<div class="col-sm-4">${{ $item->shipping_amount }}</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Discount amount:</div>
<div class="col-sm-7">${{ $item->discount_amount }} </div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Original price:</div>
<div class="col-sm-4">${{ $item->original_amount }}</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Total cost:</div>
<div class="col-sm-4">${{ $item->cost() }}</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Quantity:</div>
<div class="col-sm-4"><span class="badge badge badge-info">{{ $item->quantity }}</span> <span style="font-size:11px"> - (112 available in stock)</span></div>
</div>
<!-- / .form-group -->

<hr>
<h4>Product Options</h4>
<hr>


@if ($item->options()->count())

@foreach($item->options()->get() as $option)


<div class="form-group">
<div class="col-sm-6" >{{ $option->name() }}:</div>
<div class="col-sm-4">{{ $option->option_value_name() }}</div>
</div>
<!-- / .form-group -->

@endforeach


@else

<div class="alert alert-warning">
 This product has no options.
</div>


@endif

@if ($item->comments)
<h3> Custom personalization </h3>

<div class="alert alert-info">

{{ $item->comments }}

</div>

@endif




</div>

<div class="col-sm-10">
<hr>


<button class="btn btn-xs btn-success" data-toggle="modal" data-target="#sellerReport">Edit Product</button>
<button class="btn btn-xs btn-danger">Delete Product</button> 			


</div>

<div class="clearfix"></div>


</span>



</div>
@endforeach
@else

<div class='alert alert-warning'>No products found for this order. </div>


@endif





</div> <!-- / .panel-body -->
</div> <!-- / .panel -->
</div>





<div class="tab-pane fade active in no-padding" id="uidemo-tabs-default-demo-home">


<div class="panel widget-support-tickets" style="margin-bottom:0px;">

<div class="panel-body">

<!-- / .ticket -->


<div class="ticket">
<a href="#" title="" class="ticket-title"><button class="btn btn-xs btn-success" data-toggle="modal" data-target="#editOrder">Edit</button>&nbsp; Order: <span>{{ $o->id }}&nbsp; (<strong>{{ strtoupper($o->status) }}</strong>) </span></a>
<span class="ticket-info" id="p1" style="display:;" >
<hr>
<div style="width:40%;margin-top:10px;font-size:13px;">

<div class="form-group" style="">
<div class="col-sm-4" >Order Date:</div>
<div class="col-sm-4">{{ $o->created_at }}</div>
</div>



<div class="form-group" style="">
<div class="col-sm-4" >Tax Amount:</div>
<div class="col-sm-4">$ {{ $o->totalTaxAmount() }} @if ($o->totalTaxAmount() == '0.00') - Not taxable. @endif</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Shipping cost:</div>
<div class="col-sm-4">$ {{ $o->totalShippingAmount() }}</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Discount amount:</div>
<div class="col-sm-7">$ {{ $o->totalDiscountAmount() }}</div>
</div>
<!-- / .form-group -->


<div class="form-group">
<div class="col-sm-4" >Total cost:</div>
<div class="col-sm-4">$ {{ $o->totalCost() }}</div>
</div>
<!-- / .form-group -->


<div class="form-group">
<div class="col-sm-4" >Payment Method:</div>
<div class="col-sm-4">{{ $o->gateway()->display_name }}</div>
</div>
<!-- / .form-group -->


<div class="form-group">
<div class="col-sm-4" >Order Status:</div>
<div class="col-sm-4">{{ $o->status }}</div>
</div>
<!-- / .form-group -->

<div class="form-group">
<div class="col-sm-4" >Shipping Status:</div>
<div class="col-sm-6">{{ $o->shipping_status }}</div>
</div>
<!-- / .form-group -->


<div class="form-group">
<div class="col-sm-4" >Items:</div>
<div class="col-sm-4"><span class="badge badge badge-primary">{{ $o->products()->count() }}</span> <span style="font-size:11px"></span></div>
</div>
<!-- / .form-group -->

<hr>
@if ($o->user())

<h4>{{ @$o->user()->primary()->firstname }} {{ @$o->user()->primary()->lastname }}	-	<button class="btn btn-xs btn-success" data-toggle="modal" data-target="#sellerReport">Edit</button>
</h4>
<hr>


<div class="form-group">
<div class="col-sm-6" style="font-size:14px;" >


{{ @$o->shipping_user()->firstname }} {{ @$o->shipping_user()->lastname }}<br>
{{ $o->user()->email }} <br>
{{ @$o->shipping_user()->phone }} <br>
{{ @$o->shipping_user()->address }} , {{ @$o->shipping_user()->city }} <br>
{{ @$o->shipping_user()->state }}  {{ @$o->shipping_user()->zipcode }} <br>
{{ @$o->shipping_user()->country }} </div>

@endif

<div class="col-sm-4"></div>
</div>
<!-- / .form-group -->







</div>

<div class="col-sm-10">
<hr>


<button class="btn btn-xs btn-success" data-toggle="modal" data-target="#editOrder">Edit Order</button>
<button class="btn btn-xs btn-success" data-toggle="modal" data-target="#sellerReport">Schedule Shipping</button>

<button class="btn btn-xs btn-danger">Delete Order</button> 			


</div>

<div class="clearfix"></div>


</span>
</div>

</div> <!-- / .panel-body -->
</div>




<!-- / .panel -->
</div> <!-- / .tab-pane -->
<div class="tab-pane fade" id="uidemo-tabs-default-demo-profile">
<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
</div> <!-- / .tab-pane -->
<div class="tab-pane fade" id="uidemo-tabs-default-demo-dropdown1">
<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
</div> <!-- / .tab-pane -->
<div class="tab-pane fade" id="uidemo-tabs-default-demo-dropdown2">
<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
</div> <!-- / .tab-pane -->
</div> <!-- / .tab-content -->


<!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>