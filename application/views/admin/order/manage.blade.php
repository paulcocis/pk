
<!-- /7. $NO_LABEL_FORM -->
<style>
.datepicker { z-index:1051 !important; }


</style>

<style>


table {
  border-collapse: collapse;
  border: 1px solid #666666;
  font: normal 11px verdana, arial, helvetica, sans-serif;
  color: #363636;
  background: #f6f6f6;
  text-align:left;
  width: 100%;
  }
caption {
  text-align: left;
  font: bold 16px arial, helvetica, sans-serif;
  background: transparent;
  padding:6px 4px 8px 0px;
  color: #444;
  text-transform: uppercase;
}
thead, tfoot {
background-color: #ccc;
text-align:left;
height:30px;
}
thead th, tfoot th {
padding:10px;
}
table a {
color: #333333;
text-decoration:none;
}
table a:hover {
text-decoration:underline;
}
tr.odd {
background-color: #f1f1f1;
border-bottom:1px solid #e1e1e1;
}
tbody th, tbody td {
padding:15px;
font-size:12px;
background-color: #f1f1f1;

}


.pagination { 
  width:602px;
	margin:5px auto;
	padding:20px;
	margin-top:15px;
	text-align:center;
	color:#aaa;
	border:1px solid #ddd; 
	border-radius:5px;
	background:#fcfcfc;
	background-image:-webkit-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-moz-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-o-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-ms-linear-gradient(top, #fcfcfc, #eaeaea);
}
.pagination a {
	padding:10px;
	text-decoration:none; 
	color:#667F99;
}
.pagination li.active a {
	font-weight:bold;
}
.pagination li:not(.disabled) a:hover {
	border:1px solid #ddd; 
	border-radius:5px;
	color:#000;
	padding:10px 9px;
	background:#666;
	background-image:-webkit-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-moz-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-o-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-ms-linear-gradient(top, #fcfcfc, #eaeaea);
}

.pagination li
{
	display: inline;
	list-style-type: none;
}

.pagination ul
{
	margin: 0px 0px;
	padding: 0 0px;
}

.pagination  li.disabled a:hover {
       pointer-events: none;
       cursor: default;
}

.pagination  li.disabled a {
       color: #B6B8BB;
}



#primary_nav_wrap
{
	margin-top:15px
}

#primary_nav_wrap ul
{
	list-style:none;
	position:relative;
	float:left;
	margin:0;
	padding:0
}

#primary_nav_wrap ul a
{
	display:block;
	color:#333;
	text-decoration:none;
	font-weight:700;
	font-size:12px;
	line-height:32px;
	padding:0 15px;
	font-family:"HelveticaNeue","Helvetica Neue",Helvetica,Arial,sans-serif
}

#primary_nav_wrap ul li
{
	position:relative;
	float:left;
	margin:0;
	padding:0
}

#primary_nav_wrap ul li.current-menu-item
{
	background:#ddd
}

#primary_nav_wrap ul li:hover
{
	background:#f6f6f6
}

#primary_nav_wrap ul ul
{
	display:none;
	position:absolute;
	top:100%;
	left:0;
	background:#fff;
	padding:0
}

#primary_nav_wrap ul ul li
{
	float:none;
	width:200px
}

#primary_nav_wrap ul ul a
{
	line-height:120%;
	padding:10px 15px
}

#primary_nav_wrap ul ul ul
{
	top:0;
	left:100%
}

#primary_nav_wrap ul li:hover > ul
{
	display:block
}
</style>


<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="{{ $app['admin_url'] }}home/">Home</a></li>
<li class="active"><a href="{{ $app['admin_url'] }}orders/view/">Order List</a></li>
</ul>
<div class="page-header"  style="min-height:600px;">

<!-- 7. $NO_LABEL_FORM =============================================================================





No label form
-->
@if (Session::has('ActionMessage'))
<div class="alert alert-success">
 {{ Session::get('ActionMessage') }}
</div>
@endif



<form method="GET" action='{{ $app["admin_url"] }}orders/manage'>


<div class="form-group">

<div class="col-sm-2">
<input type='text' name='UniqueOrderId' style="padding:5px;" placeholder='Enter an order number'>
</div>
</div> <!-- / .form-group -->

<div class="form-group">
<div class="col-sm-3">
Select an interval:
<div class="input-daterange input-group" id="bs-datepicker-range" style="margin-top:5px;">
<input type="text" class="input-sm form-control" name="start" placeholder="Start date">
<span class="input-group-addon">to</span>
<input type="text" class="input-sm form-control" name="end" placeholder="End date">
</div>
</div>

<div class="col-sm-2" style="margin-right:5px;padding-right:5px; width:180px;">
Select order status:
<br>
<select name='status[]' style="padding:5px; width:150px; height:110px;" multiple="">
@foreach (Order::forge()->enum_field('status') as $status)
<option value="{{ $status }}">{{ $status }}</option>
@endforeach
</select>

</div>

<div class="col-sm-2">
Select Shipping status:
<br>
<select name="shipping_status[]" style="padding:5px; width:200px; height:60px;" multiple="">
@foreach (Order::forge()->enum_field('shipping_status') as $shipping_status)
<option value="{{ $shipping_status }}">{{ $shipping_status }}</option>
@endforeach
</select>

</div>

</div> <!-- / .form-group -->


<div class="form-group">

<div class="col-sm-2">
<input type="button" value="Reset" onclick="window.location='{{ $app['admin_url'] }}orders/manage'">

<input type="submit" value="Search with selected criteria">
</div>
</div> <!-- / .form-group -->
</form>

<hr>

 <table summary="Submitted table designs">
            <caption>Order Manager</caption>
            <thead>
               <tr>
                  <th scope="col">Order ID</th>
                  <th scope="col">Customer</th>
                  <th scope="col">Customer Email</th>
                  <th scope="col">Payment Transaction ID</th>
                  <th scope="col">Order Date</th>
                  <th scope="col">Order Amount</th>
				  <th scope="col">Status</th>
				  <th scope="col">Refund Amount</th>
				  <th scope="col">Tender</th>
				  <th scope="col">Actions:</th>


               </tr>
            </thead>
            <tfoot>
               <tr>
                  <th scope="row">Total</th>
                  <td colspan="9">{{ $list->count }} orders.</td>
               </tr>
            </tfoot>
            <tbody>
            @foreach($list->results as $order)
               <tr class='odd'>
                  <th scope="row" id="r100"><a href="javascript:void(0);">{{ $order->id }}</a>
                  
                  @if ($order->shipped())
                  <img src="{{ $images_url }}ship1.png"  class='ship' style="height:40px;width:40px; cursor:pointer;" data-toggle="popover" data-placement="right" data-content="Order has been marked as shipped and set for delivery at {{ date('Y-m-d',strtotime($order->scheduled_shipping_at)) }} ." data-title="Order has been shipped." data-original-title="" title=""> 
                  @endif
                  <img src="{{ $images_url }}bank.png" style="cursor:pointer;height:30px; " onclick="$('#order-{{ $order->id }}').toggle('slow');">
                    <a href="{{ $app['admin_url'] }}orders/manage/do/regenerateSellerInvoice/{{ $order->id }}"><img src="{{ $images_url }}note.png" title="Regenerate seller invoices." style="cursor:pointer;height:30px;"></a>
                   <a href="{{ $app['admin_url'] }}orders/manage/do/sendConfirmationEmail/{{ $order->id }}"><img src="{{ $images_url }}d.png" style="cursor:pointer; height:30px;"></a>

                  </th>
                  <td>{{ $order->user()->primary()->firstname }} {{ $order->user()->primary()->lastname }}</td>
                  <td>{{ $order->user()->email }}</td>
                  <td>
                  	@if ($order->firstPaymentTransaction())
                  		<b>{{ $order->firstPaymentTransaction()->reference_transaction_id }}</b>

                  	@else
                  	<span style='color:red'>No payment found.</span>
                  	@endif



                  </td>
                  <td>{{ $order->created_at }}</td>
                  <td>${{ $order->totalCost() }}</td>
                  <td>{{ $order->status }}</td>
                  <td>${{ $order->refundAmount() }}</td>
                  <td>{{ $order->gateway()->display_name }}</td>
                  <td>
                  <a href='{{ $app["admin_url"] }}orders/view/order/{{ $order->id }}'>Edit Order</a>

		<!-- Default -->
						<div class="btn-group btn-group-small" style="display:none;">
							<button type="button" class="btn btn-small btn-default">Manage</button>
							<button type="button" class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
							<ul class="dropdown-menu">
								<li><a  data-toggle="modal" href='#ss' data-remote-target="#ss .modal-content"  data-load-remote='{{ $app["admin_url"] }}orders/manage/details/{{ $order->id }}'>Details</a></li>
								<li><a data-toggle="modal"  href='#ss' data-remote-target="#ss .modal-content"  data-load-remote='{{ $app["admin_url"] }}orders/manage/schedule-shipping/{{ $order->id }}'>Schedule Shipping</a></li>
								<li><a data-toggle="modal"  href='#ss' data-remote-target="#ss .modal-content"  data-load-remote='{{ $app["admin_url"] }}orders/manage/comments/{{ $order->id }}'>Comments</a></li>
								<li><a data-toggle="modal"  href='#ss' data-remote-target="#ss .modal-content"  data-load-remote='{{ $app["admin_url"] }}orders/manage/refund/{{ $order->id }}'>Refund Managment</a></li>
								<li class="divider"></li>
								<li><a href="{{ $app['admin_url'] }}orders/manage/delete/{{ $order->id }}">Definitively delete</a></li>
							</ul>
						</div>&nbsp;&nbsp;

                  </td>
               </tr>

               <?php

               // get order finances.
               	$finance = $order->finance();
               ?>

               <tr id='order-{{ $order->id }}' style="display:none;">
 					<td colspan="10" style="border-bottom:2px solid #222; background-color:#fff;padding:25px;">
 						<div style="float:left;font-size:13px; width:250px;">
 						<div style="float:left;border-bottom:1px solid #111; width:250px;padding-bottom:6px; font-weight:bold;">
 						Order Finance Summary:
 						</div>
 						<br><br>

 						<div  style="float:left;border-bottom:1px solid #e1e1e1;padding-bottom:5px;">
 						<div style="float:left; width:150px;">Order Amount</div>
 						<div style="float:left; width:80px;">${{ $finance->orderAmount }}</div>
 						</div>

			<div  style="float:left;border-bottom:1px solid #e1e1e1;padding-bottom:5px;padding-top:5px;">
 						<div style="float:left; width:150px;">Order Taxes</div>
 						<div style="float:left; width:80px;">${{ $finance->taxAmount }}</div>
 						</div>


			<div  style="float:left;border-bottom:1px solid #e1e1e1;padding-bottom:5px;padding-top:5px;">
 						<div style="float:left; width:150px;">Shipping Costs</div>
 						<div style="float:left; width:80px;">${{ $finance->shippingAmount }}</div>
 						</div>

	<div  style="float:left;border-bottom:1px solid #e1e1e1;padding-bottom:5px;padding-top:5px;">
 						<div style="float:left; width:150px;">PinkePromise Earn</div>
 						<div style="float:left; width:80px;">${{ $finance->pk_earn }}</div>
 						</div>


			<div  style="float:left;border-top:1px solid #111;padding-bottom:5px;padding-top:5px;margin-top:10px;">
 						<div style="float:left; width:150px;">Total Order Cost<br><span style='font-size:11px'>shipping and taxes included.</span></div>
 						<div style="float:left; width:80px;">${{ $finance->totalOrderAmount }}</div>
 						</div>

 						</div>

 						<div style="float:left;margin-left:10px;border-left:1px solid #333;padding-left:10px; min-height:200px;">

	<div style="float:left;border-bottom:1px solid #111; width:300px;padding-bottom:6px; font-weight:bold;">
 						Seller Payouts
 						</div>
 						<br><br>	@foreach(App\SellerInvoice::where('order_id', '=', $order->id)->get() as $payout) 						
	<div  style="float:left;border-bottom:1px solid #e1e1e1;padding-bottom:5px;padding-top:5px;">
 						<div style="float:left; width:230px;">Seller: {{ $payout->seller()->email }}</div>
 						<div style="float:left; width:80px;"><b>${{ $payout->amount }}</b></div>
 						</div>

 						<div class="clear:both"></div>
 	@endforeach





 						</div>




 					</td>
               </tr>


               @endforeach
          
            </tbody>
         </table>

         <?php
         	$data = Input::get();
         	
         	unset($data['page']);


	        echo $list->appends($data)->links();
          ?>



@section('scripts')
<script type="text/javascript">
init.push(function () {

// date options.
var options2 = {orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'}
$('#bs-datepicker-range').datepicker(options2);
$('#bs-datepicker-inline').datepicker();


	$('#bs-datepicker-component').datepicker({
format: "yyyy-mm-dd"
});

// Multiselect
$("#jquery-select2-multiple").select2({placeholder: "assign roles"});
})

window.PixelAdmin.start(init);

$(document).ready(function() {
$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();

    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});





});


					init.push(function () {
						$('.ship').popover();

						
					});

$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
</script>
@endsection


<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>

@section('modals')
<!-- 6. $MODALS_BLURRED_BACKGROUND =================================================================

				Modal with blurred background

				To enable blurred background effect just add class .modal-blur to the .modal
-->
				<!-- Template -->
				<div id="ss" class="modal fade modal-blur" tabindex="-1" role="dialog" style="display: none;">
					<div class="modal-dialog modal-sm">
						<div class="modal-content" style="width:600px;">
							
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div> <!-- / .modal -->
				<!-- / Template -->
	
<!-- /6. $MODALS_BLURRED_BACKGROUND -->
@endsection