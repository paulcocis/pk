<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="#">Home</a></li>
			<li class="active"><a href="#">Dashboard</a></li>
		</ul>
		<div class="page-header">
			
			<button class="btn btn-primary" onclick="window.location='{{ url_to('deals/add', 'admin_url')}}'">Create New Deal</button> 
			<br><br>

@section('scripts')

<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Deals');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>

@endsection
	
		
<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
								<thead>
									<tr>
										<th>Deal Title</th>
										<th>Seller</th>
										<th>Taxable</th>
										<th>Created on</th>
										<th>Availability</th>
										<th>Product Type</th>
										<th>Orders</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody class="t">
								
								@if (!Deal::count())	


								@else
									
									@foreach(Deal::get() as $deal)

									<tr>
										<td>{{ $deal->name }}</td>


										<td>
										@if ($deal->user())
										<a href="#">{{ $deal->user()->username }}</a>
										@endif

										</td>
										<td>

										@if ($deal->tax)
										<span class="label label-success">YES</span>
										@else
										<span class="label label-warning">NO</span>
										@endif

										</td>
										<td class="center"> {{ $deal->created() }}</td>
										<td class="center">  <strong>{{ $deal->startDate() }}</strong> to  
										<strong>{{ $deal->endDate() }}</strong>
										<br><p>(<strong>{{ $System->dateDiff($deal->startDate(), $deal->endDate()) }} </strong>days left)</p>
										
										</td>
										<td class="center"> {{ ucfirst($deal->deal_type) }} Item</td>
										<td class="center"><a href="#">{{ count ($deal->orders()) }}</a></td>
										<td>
										
										<div class="btn-group btn-group-sm">
							<button type="button" class="btn btn-primary" onclick="window.location='{{ $app['admin_url'] }}deals/edit/{{ $deal->id }}'"><i class="fa fa-edit"></i> Edit </button>
							<button type="button" class="btn btn-danger" onclick="window.location='{{ $app['admin_url'] }}deals/index/?delete={{ $deal->id }}'"><i class="fa fa-times"></i> Delete</button>
							
						</div>

										
										
										</td>
									</tr>

						@endforeach
						@endif
	
									
									
		
		</tbody>
							</table>
		
		<br><br>
		

		@section('scripts')

				<script>
					init.push(function () {
						$('.ui-wizard-example').pixelWizard({
							onChange: function () {
								console.log('Current step: ' + this.currentStep());
							},
							onFinish: function () {
								// Disable changing step. To enable changing step just call this.unfreeze()
								this.freeze();
								console.log('Wizard is freezed');
								console.log('Finished!');
							}
						});

						$('.wizard-next-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('nextStep');
						});

						$('.wizard-prev-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('prevStep');
						});

						$('.wizard-go-to-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('setCurrentStep', 2);
						});
					});
				</script>

	@endsection	
		
				<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->

		<!-- Page wide horizontal line -->

	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div>