<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">


<a class="btn btn-small" href="{{ $app['admin_url'] }}deals/edit/{{ $deal->id }}"> Edit Deal</a>
<a class="btn btn-small" target="_blank" href="{{ URL::base() }}/home/details/{{ $deal->id }}"> Go to Deal Page</a>

<br><br>

<ul id="uidemo-tabs-default-demo" class="nav nav-tabs">

<li class="active">
<a href="#user-settings"  data-toggle="tab"><i class="fa fa-check"></i> Setup your created deal</a>

</li> <!-- / .dropdown -->
</ul>

<div class="tab-content tab-content-bordered no-padding">
<!-- / .tab-pane -->
<!-- / .tab-pane -->


<div class="tab-pane fade active in" id="user-settings" >



<div style="padding:20px">

<h3>Primary Image Section</h3>
<form action="{{ $app['admin_url'] }}deals/post-setup/{{ $deal->id }}" enctype="multipart/form-data" method="POST" id="primaryImage">
<input type='hidden' name='action' value='setDealPrimaryImage'>
<input type='hidden' name='deal_id' value='{{ $deal->id }}'>

<div class="form-group" style="background-color:#fff;padding:15px;border:2px solid #eee;">
<label for="inputEmail2" class="col-sm-2 control-label">Select deal primary image:</label>
<div class="col-sm-6">
<input type="file" name="dataimage" id="pp">
</div>

<div class="col-sm-6">
@if ($deal->getPrimaryImage())
<br>
	Your current primary image is:<br>
	<img src='{{ $deal->getPrimaryImage()->thumb_url(200,100) }}'>
@endif
</div>

</div> <!-- / .form-group -->
</form>

<h3>Manage deal images</h3>
<form action="{{ $app['admin_url'] }}deals/post-setup/{{ $deal->id }}" enctype="multipart/form-data" method="POST" id="addMoreImages">
<input type='hidden' name='action' value='addMoreImages'>
<input type='hidden' name='deal_id' value='{{ $deal->id }}'>

<div class="form-group" style="background-color:#fff;padding:15px;border:2px solid #eee;">


<label for="inputEmail2" class="col-sm-2 control-label">Add more images:</label>
<div class="col-sm-9">
<input type="file" name="dataimage" id="addMoreImagesFile">

</div>

@if ($deal->image()->where('primary', '=', 0)->get())
<br>
<div class="alert alert-info" style="margin-top:20px;font-size:16px;">
Added images: <b>{{ $deal->image()->where('primary', '=', 0)->count() }}</b>
<br> To get more image actions click on image.	
<br><br>


<div id='image-actions'>

</div>

</div>


<?php $i = 0; ?>
@foreach ($deal->image()->where('primary', '=', 0)->get() as $image)
@if ($i == 4)
 <br> <br><?php $i = 0; ?>
@endif


<a href='javascript:void(0);' id='showImageAction' onclick="addImageActions({{ $image->id }})"><img src='{{ $image->thumb_url(200,100) }}'></a>


<?php $i++; ?>
@endforeach

@endif

</div> <!-- / .form-group -->
</form>

<h3>Manage Deal Options</h3>

<form action="{{ $app['admin_url'] }}deals/post-setup/{{ $deal->id }}" enctype="multipart/form-data" method="POST" id="addNewOption">
<input type='hidden' name='action' value='addNewOption'>
<input type='hidden' name='deal_id' value='{{ $deal->id }}'>

<div class="form-group" style="background-color:#fff;padding:15px;border:2px solid #eee;">
<label for="inputEmail2" class="col-sm-1 control-label">Add New :</label>
<div class="col-sm-6">
<input type="text" name="name" >
<input type="button" value='Create' id='createOption'>



</div>
<br>

 @if ($deal->options()->count())
<div class="alert alert-info" style="margin-top:20px;">
Added options: <b>{{ $deal->options()->count() }}</b>
<br>
<b>You can add option values by clicking on 'Edit' button.</b>
</div>

<table style="border:1px solid #e1e1e1;" cellpadding="0" cellspacing='0'>
@foreach ($deal->options()->get() as $option)
<tr>

<td width="200" style="padding:15px;padding-bottom:2px;"><b> {{ $option->name }}</b></td>
<td style="padding:15px;padding-bottom:2px;"><a class="btn btn-sm btn-edit"  data-toggle="modal"  data-target="#tInfo" href='{{ $app["admin_url"] }}deals/editoption/{{ $option->id }}'>edit</a></td>
<td style="padding:15px;padding-bottom:2px;"><a class="btn btn-sm btn-edit" href="{{ $app['admin_url'] }}deals/post-setup/{{ $deal->id }}?action=deleteOption&optionId={{ $option->id }}">delete</a></td>
</tr>	


@endforeach
<tr>
<td colspan="3" style="height:10px;"></td>
</tr>
</table>

@else
<div class="alert alert-warning" style="margin-top:20px;">

 <b>You have no options created!</b>
 <br>
 To create new option add a name in text file specified up and push 'create button'.
 <br>
 You can create as many options you want for a deal.


</div>



@endif




</div> <!-- / .form-group -->
 
	

</form>



</div>


<!-- / .form-group -->
<br>

</div>


</div>




</div> <!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>

@section('scripts')

<script>
$(document).ready(function()
{
// select the file input (using a id would be faster)
$('#pp').change(function() { 
    // select the form and submit
    $('#primaryImage').submit(); 
});


// select the file input (using a id would be faster)
$('#addMoreImagesFile').change(function() { 
    // select the form and submit
    $('#addMoreImages').submit(); 
});


// select the file input (using a id would be faster)
$('#createOption').click(function() { 
    // select the form and submit
    $('#addNewOption').submit(); 
});





});


function addImageActions(id)
{

	var html = '<hr style="color:#fff;border-color:#fff;">';
	html = html+'<a class="btn btn-small" href="{{ $app["admin_url"] }}deals/post-setup/{{ $deal->id }}?action=deleteImage&imageId='+id+'"> Delete </a>';
	html = html+'<a class="btn btn-small" href="{{ $app["admin_url"] }}deals/post-setup/{{ $deal->id }}?action=makeImagePrimary&imageId='+id+'"> Make Primary Image </a>';

	$('#image-actions').html(html);

}

</script>

@endsection

@section('modals')
<div id="tInfo" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>

</div>
@endsection