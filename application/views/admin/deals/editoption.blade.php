<script>

   function deleteOptionValue(id, deal_id)
   {
		 $.ajax({
		        url: '{{ $app["admin_url"] }}deals/editoption/'+deal_id,
		        type: "POST",
		        data: { action: 'delete', optionValueId: id, deal_id: deal_id },
		        success: function(data){
		            $("#data-update").html(data);
		        }
		    })
   }


   function updateOptionName(id, deal_id)
   {
		 $.ajax({
		        url: '{{ $app["admin_url"] }}deals/editoption/'+deal_id,
		        type: "POST",
		        data: { action: 'updateOptionName', option_id: id, deal_id: deal_id, new_name: $('#name').val() },
		        success: function(data){
		           $('#notificationUpdate').show();
		        }
		    })
   }


</script>
					

<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Close</button>
								<h4 class="modal-title" id="myModalLabel">Edit Deal Option</h4>
							</div>
						<div class="modal-body">
			
					<div class="alert alert-success" id='notificationUpdate' style="display:none;margin-top:10px;">Options has been succesfully updated.</div>

					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Option set Title:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="name" value="{{ $o->name }}">

							</div>
							<div class="col-sm-2">
							<a href="javascript:void(0);" class="btn btn-success" onclick='updateOptionName({{ $o->id }}, {{ $o->id }});'>Update</a>
							</div>

						</div> <!-- / .form-group -->
						
						<div id='data-update'>
							<form name='udateOptions'>
						@if ($o->options()->count())
						@foreach($o->options()->get() as $row)
					
						<div class="form-group" >
							<label for="inputPassword" class="col-sm-3 control-label">Option:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control input-sm" id="inputPassword" value="{{ $row->name }}">
							</div>
							
							<label for="inputPassword" class="col-sm-2 control-label">Limit:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control input-sm" id="inputPassword" value="{{ $row->limit }}">
							</div>
								<div class="col-sm-2">
								<a href="javascript:void(0);" class="btn btn-danger" onclick='deleteOptionValue("{{ $row->id }}", "{{ $o->id }}");'>delete</a>
							</div>
							
						</div>
						
						@endforeach
					
							<div class='cleafix'></div>
						@endif
						</form>
						</div>
						<hr>
						<a href="#" style="font-weight:normal;" class="btn btn-xs btn-success" onclick="$('.cloneInput').toggle();">Add New Option</a> 

						<hr>
									
						<div class="cloneInput" style="display:none;">

						<form class="panel form-horizontal" method='POST' action='{{ $app["admin_url"] }}deals/editoption/{{ $o->id }}' id='addEEE'>
								<input type='hidden' name='deal_id' value='{{ $o->deal_id }}'>
								<input type='hidden' name='object' value='options'>
								<input type='hidden' name='option_id' value='{{ $o->id }}'>
								<input type='hidden' name='action' value='add'>

						<div class="form-group" style="padding-top:10px;" >
							<label for="inputPassword" class="col-sm-3 control-label">Option:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control input-sm" id="gwegwegew" name='name' placeholder="">
							</div>
							
							<label for="inputPassword" class="col-sm-2 control-label">Limit:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control input-sm" id="gegwegwegwe" name='limit' placeholder="">
							</div>

 <input class="btn btn-success" id="rggergrgergerggrg" type="button" value="Save" 
                            onclick="ajaxForm('#addEEE', function(data) { $('#data-update').html(data); })">
						</div> 
						</form>
						</div>
						<div class="clone"></div>						
						
					
			
					
					</div>
				</form>
				


							
							</div> <!-- / .modal-body -->

