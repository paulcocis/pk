<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="#">Home</a></li>
			<li class="active"><a href="#">Dashboard</a></li>
		</ul>
		<div class="page-header">
			
			<br><br>

@section('scripts')

<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Deal Categories');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>

@endsection

<form action="{{ $app['admin_url'] }}deals/categories/" enctype="multipart/form-data" method="POST" id='updateDealSettings'>

<input type='hidden' name='action' value='add'>


<div>
<div style="padding:20px;border:2px solid #e1e1e1; background-color:#fff;">
	
			<div class="form-group">
	<label for="inputEmail2" class="col-sm-1 control-label">Type:</label>
	<div class="col-sm-3">
	<select name='type'>
	<option value="norableu">NoraBleu</option>
	<option value="blankyou">BlankYou</option>
	</select>
	</div>
</div> <!-- / .form-group -->


		<div class="form-group">
	<label for="inputEmail2" class="col-sm-1 control-label">Category Name:</label>
	<div class="col-sm-3">
		<input type="text" class="form-control" id="inputEmail2" value="" name='name'>
	</div>
</div> <!-- / .form-group -->

		<div class="form-group">
	<label for="inputEmail2" class="col-sm-1 control-label"></label>
	<div class="col-sm-3">
		 <input class="btn btn-sm btn-success" id="inputPassword" type="submit" value="Add Category" >
	</div>
</div> 



</div>

</div>
</form>

	
		
<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
								<thead>
									<tr>
										<th>Category Name</th>
										<th>Type</th>
										<th>Created on</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody class="t">
								
								@if (!DealCategory::count())	


								@else
									
									@foreach(DealCategory::get() as $category)

									<tr>
										<td>{{ $category->name }}</td>
										<td>{{ $category->type }}</td>

										<td>{{ $category->created_at }}</td>
										<td>
										
										<div class="btn-group btn-group-sm">
							<button type="button" class="btn btn-primary" onclick="window.location='{{ $app['admin_url'] }}deals/categories/{{ $category->id }}'"><i class="fa fa-edit"></i> Edit </button>
							<button type="button" class="btn btn-danger" onclick="window.location='{{ $app['admin_url'] }}deals/categories/?action=delete&id={{ $category->id }}'"><i class="fa fa-times"></i> Delete</button>
							
						</div>

										
										
										</td>
									</tr>

						@endforeach
						@endif
	
									
									
		
		</tbody>
							</table>
		
		<br><br>
		

		@section('scripts')

				<script>
					init.push(function () {
						$('.ui-wizard-example').pixelWizard({
							onChange: function () {
								console.log('Current step: ' + this.currentStep());
							},
							onFinish: function () {
								// Disable changing step. To enable changing step just call this.unfreeze()
								this.freeze();
								console.log('Wizard is freezed');
								console.log('Finished!');
							}
						});

						$('.wizard-next-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('nextStep');
						});

						$('.wizard-prev-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('prevStep');
						});

						$('.wizard-go-to-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('setCurrentStep', 2);
						});
					});
				</script>

	@endsection	
		
				<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->

		<!-- Page wide horizontal line -->

	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div>