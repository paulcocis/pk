<style>
.pagination { 
	margin:5px auto;
	padding:10px;
	text-align:center;
	color:#aaa;

}
.pagination a {
	padding:10px;
	text-decoration:none; 
	color:#667F99;
}
.pagination li.active a {
	font-weight:bold;
}
.pagination li:not(.disabled) a:hover {
	border:1px solid #ddd; 
	border-radius:5px;
	color:#000;
	padding:10px 9px;
	background:#666;
	background-image:-webkit-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-moz-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-o-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-ms-linear-gradient(top, #fcfcfc, #eaeaea);
}

.pagination li
{
	display: inline;
	list-style-type: none;
}

.pagination ul
{
	margin: 0px 0px;
	padding: 0 0px;
}

.pagination  li.disabled a:hover {
       pointer-events: none;
       cursor: default;
}

.pagination  li.disabled a {
       color: #B6B8BB;
}


</style>

<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="{{ $app['admin_url'] }}home/">Reports</a></li>
<li class="active"><a href="{{ $app['admin_url'] }}reports/invoices/">Seller Invoices</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================




<!-- /7. $NO_LABEL_FORM -->

@section('scripts')
<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Seller Invoices');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>

<script>
$(document).ready(function () {


$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();

    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});




});
</script>
@endsection

@if (Session::has('ActionMessage'))
<div class="alert alert-success">
 
  {{ Session::get('ActionMessage') }}

</div>
@endif





  @if (!SellerInvoice::count())

  @else







	<!-- Primary table -->
	<div class="table-primary">
		<div class="table-header">
			<div class="table-caption">
				Seller Invoices
			</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Order:</th>
					<th>Seller:</th>
					<th>Amount:</th>
					<th>Generated At:</th>
					<th>Status:</th>
					<th>Actions:</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($records->results as $row)
				<tr>
					<td>{{ $row->id }}</td>
					<td>{{ $row->order_id }}</td>
					<td>{{  $row->seller()->email }}</td>
					<td>{{ $row->amount() }}</td>
					<td>{{ $row->created_at }}</td>
					<td>{{ $row->status() }}</td>
					<td><a href='#' onclick="$('#r-{{ $row->id }}').toggle();">Details</a></td>
				</tr>
			<tr id='r-{{ $row->id }}' style="display:none;">
				<td colspan="7" style="background-color:#e1e1e1;padding:20px;">
					
				<div class="panel colourable" style="width:50%;">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<span class="panel-title">Detailed Informations</span>
					</div>

					<!-- Table -->
					<table class="table">
				
						<tbody>
				<tr>
								<td>Order Number</td>
								<td>{{ $row->order_id }}</td>
							</tr>
							<tr>
								<td>Seller</td>
								<td><b>{{ $row->seller()->bill()->business_name }}</b> </td>
							</tr>
				<tr>
								<td>Seller Email Address</td>
								<td>{{ $row->seller()->email }}</td>
							</tr>							
							<tr>
								<td>Seller Paypal</td>
								<td>{{ $row->seller()->bill()->paypal_email }} </td>
							</tr>			

						<tr>
								<td>Total Payment</td>
								<td><b>${{ $row->amount() }}</b> </td>
							</tr>

						<tr>
								<td>Current status</td>
								<td><b>{{ $row->status() }}</b> </td>
							</tr>


						</tbody>
					</table>
				</div>
				<button class="btn btn-danger" onclick="window.location='{{ $app['admin_url'] }}reports/invoices?do=delete&invoice={{ $row->id }}'">Delete</button>

				@if (!$row->isPaid())
				<button class="btn btn-success" onclick="window.location='{{ $app['admin_url'] }}reports/invoices?do=markAsPaid&invoice={{ $row->id }}'">Mark as paid</button>
				@else
				<button class="btn btn-info" onclick="window.location='{{ $app['admin_url'] }}reports/invoices?do=markAsUnpaid&invoice={{ $row->id }}'" >Mark as unpaid</button>
				@endif

				<br>
	<br>

				</td>
			</tr>

			@endforeach
	<tr>
				<td colspan="7"></td>
			</tr>

			</tbody>
		</table>
		<div class="table-footer">
			{{ $records->links() }}
		</div>
	</div>
	<!-- / Primary table -->
 @endif


<br><br>





<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>

@section('modals')
<div id="tInfo" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>

</div>

@endsection