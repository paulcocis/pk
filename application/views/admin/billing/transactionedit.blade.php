		
<style>
.datepicker { z-index:1051 !important; }


</style>




				<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title">Edit Transaction -  
							<!-- Small -->
						<div class="btn-group btn-group-sm">
							<button type="button" class="btn btn-danger">More Actions</button>
							<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
							<ul class="dropdown-menu">
								<li><a href="#">Remove </a></li>
								<li class="divider"></li>
								<li><a href="#">Email Customer</a></li>
							</ul>
						</div>&nbsp;&nbsp;
							
							</h4> 	

							</div>
							<div class="modal-body">
							
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Created at:</label>
							<div class="col-sm-2">
<input type="text" class="form-control sDate" id="orderDate" value="{{ $o->created() }}">						
							</div>
						</div> <!-- / .form-group -->
							
							
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Amount:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputEmail2" value="{{ $o->amount }}">
							</div>
						</div> <!-- / .form-group -->
						
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Fees:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputEmail2" value="{{ $o->gw_fee }}">
							</div>
						</div> <!-- / .form-group -->
						
						

		<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Refference Gateway ID:</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="inputEmail2" value="{{ $o->reference_transaction_id }}" >
							</div>
						</div> <!-- / .form-group -->
							
					

	
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Gateway:</label>
							<div class="col-sm-6">
<select id="jquery-select2-multiple" class="form-control"  placeholder="">

@foreach (App\Setup\PaymentGateway::get() as $gw)

<option value="{{ $gw->id }}" @if ($gw->id == $o->gateway_id) selected="selected" @endif>{{ $gw->display_name }}</option>

@endforeach

</select>
							</div>
						</div> <!-- / .form-group -->
						
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Status:</label>
							<div class="col-sm-6">
<select id="jquery-select2-multiple" class="form-control"  placeholder="">

@foreach ($o->enum_field('status') as $status)
<option value="{{ $status }}" @if ($status == $o->status) selected="selected" @endif>{{ $status }}</option>
@endforeach

						

</select>
							</div>
						</div> <!-- / .form-group -->
	

			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Customer:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputEmail2" disabled="disabled"  value="{{ $o->user()->primary()->firstname }} {{ $o->user()->primary()->lastname }} ( {{ $o->user()->email }})">
							</div>
						</div> <!-- / .form-group -->

			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Order:</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="inputEmail2" disabled="disabled"  value="{{ $o->order_id }}">
							</div>
						</div> <!-- / .form-group -->
					
	
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Description:</label>
							<div class="col-sm-6">
								<textarea class="form-control" rows="4">{{ $o->description }}</textarea>
							</div>
						</div> <!-- / .form-group -->
			<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label"></label>
							<div class="col-sm-6">
							<button class="btn btn-sm btn-success">Save Changes</button>
							</div>
						</div> <!-- / .form-group -->

						
							
							
							</div>



