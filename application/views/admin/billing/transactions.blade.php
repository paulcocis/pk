<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================



No label form
-->
<script>
init.push(function () {
var options2 = {
orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'
}
$('#bs-datepicker-range').datepicker(options2);

$('#bs-datepicker-inline').datepicker();
});
</script>



<form action="" class="panel form-horizontal">
<div class="panel-heading">
<span class="panel-title" onclick="$('#adv-search').toggle('slow');" style="cursor:pointer;">Advanced 
	Transaction Search (<span style="font-size:11px; color:#666; margin:0px;">click to view search options</span>)</span>
</div>
<div class="panel-body" id="adv-search" style="display:none;">
<div class="row">
<div class="col-md-2">
<input type="text" name="name" placeholder="Enter transaction number ..." class="form-control form-group-margin">
</div>
<div class="col-md-2">
<select id="jquery-select2-multiple" class="form-control"  placeholder="">
<option value="a">Select a transaction status ...</option>

@foreach(Transaction::forge()->enum_field('status') as $status)
	<option value="{{ $status }}">{{ $status }}</option>
@endforeach

</select>
</div>
</div><!-- row -->
<h6>Search Interval</h6>
<hr>
<div class="row">
<div class="col-md-4">
<div class="input-daterange input-group" id="bs-datepicker-range">
<input type="text" class="input-sm form-control" name="start" placeholder="Start date">
<span class="input-group-addon">to</span>
<input type="text" class="input-sm form-control" name="end" placeholder="End date">
</div>
</div>
<div class="col-md-2">
<button class="btn btn-md  btn-success">Search</button>
</div>
<div class="col-md-2">
</div>
</div>


</div>

</form>
<!-- /7. $NO_LABEL_FORM -->

<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Transactions');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>


 @if ($transactionStatus)
<div class="alert alert-success"  id='tdelete'>
							<button type="button" class="close" data-hide="alert">×</button>
							<strong>Well done!</strong> Transaction has been removed.
						</div>

 @endif



<div class="table-primary">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
<thead>
<tr>
<th>#</th>
<th>Date</th>
<th>Amount</th>
<th>Gateway</th>
<th>Order</th>
<th>Customer</th>
<th>Status</th>
<th>Actions</th>
</tr>
</thead>
<tbody class="t">

 @if (!Transaction::count())

 @else


	 @foreach(Transaction::get() as $row)
			<tr>
			<td>{{ $row->id }}</td>
			<td><a href="#">{{ $row->created(); }}</a></td>
			<td>${{ $row->amount }}</td>
			<td class="center"> {{ @$row->gateway()->display_name }}</td>
			<td class="center"> <a href="#">{{ $row->order_id }}</a></td>
			<td class="center"> <a href="#">

			@if ($row->user())

			{{ @$row->user()->primary()->firstname }} {{ @$row->user()->primary()->lastname }}</a> </td>

			@else

			 {{ $row->id }}

			@endif

			<td class="center">{{ $row->status }}</td>
			<td>

				<div class="btn-group btn-group-sm">
					<button type="button" class="btn btn-default"   data-toggle="modal" href='#tInfo' data-remote-target="#tInfo .modal-content"  data-load-remote='{{ $app["admin_url"] }}billing/transactions/view/{{ $row->id }}'><i class="fa fa-edit"></i> Edit </button>
					<button type="button" class="btn btn-default" onclick='window.location="{{ $app["admin_url"] }}billing/transactions/delete/{{ $row->id }}"'><i class="fa fa-times"></i> Delete</button>
				</div>

			</td>
		</tr>

	 @endforeach
 @endif

</tbody>
</table>

<br><br>



<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>



<style>
.datepicker { z-index:1051 !important; }


</style>



@section('scripts')
<script>
$(function() {
// Multiselect
$("#jquery-select2-multiple").select2({
placeholder: "assign roles"
});

});


</script>



<script>
$(document).ready(function () {


$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();

    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});




});
</script>


@endsection




@section('modals')
<div id="tInfo" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>

</div>

@endsection