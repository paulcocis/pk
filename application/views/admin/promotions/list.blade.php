<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="{{ $app['admin_url'] }}home/">Home</a></li>
<li class="active"><a href="{{ $app['admin_url'] }}orders/view/">Order List</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================



No label form
-->





<!-- /7. $NO_LABEL_FORM -->

@section('scripts')
<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Promotions');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});



$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();
    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});

</script>
@endsection


<button class="btn btn-success" data-toggle="modal" href='#addNewPromotion' data-remote-target="#addNewPromotion .modal-content"  data-load-remote='{{ $app["admin_url"] }}promotions/add'>Add New Promotion</button> 
<br><br>

<div class="table-primary">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Discount Type</th>
<th>Discount Value</th>
<th>Start Date</th>
<th>End Date</th>
<th>Status</th>
<th>Used Times</th>
<th>Actions</th>
</tr>
</thead>
<tbody class="t">


  @if (!Promotion::count())

  @else

  @foreach(Promotion::get() as $o)

	<tr>
		<td>{{ $o->id }}</td>
		<td><a href="#">{{ $o->title }} ( {{ $o->code }} )</a></td>
		<td>{{ $o->type }}</td>
		<td class="center"> {{ $o->discount }}</td>
		<td class="center"> 

		{{ $o->start_date }}

		</td>
		<td class="center"> {{ $o->end_date }}</td>
		<td class="center">{{ $o->status }}</td>
		<td class="center">{{ $o->usedTimes() }}</td>
		<td>

		<div class="btn-group btn-group-sm">
		<button type="button" class="btn btn-default" data-toggle="modal" href='#editPromotional' data-remote-target="#editPromotional .modal-content"  data-load-remote='{{ $app["admin_url"] }}promotions/edit/{{ $o->id }}'><i class="fa fa-edit"></i> Edit </button>
		<button type="button" class="btn btn-default" onclick="window.location='{{ $app['admin_url'] }}promotions/index/?delete=true&id={{ $o->id }}'"><i class="fa fa-times"></i> Delete</button>

		</div>
		</td>
	</tr>
 @endforeach
 @endif




</tbody>
</table>

<br><br>



<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>


<style>
.datepicker { z-index:1051 !important; }


</style>



@section('modals')
<div id="addNewPromotion" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>

</div>
<div id="editPromotional" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>
				</div>

@endsection