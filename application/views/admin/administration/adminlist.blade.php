<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="#">Home</a></li>
			<li class="active"><a href="#">Dashboard</a></li>
		</ul>
		<div class="page-header">
			
			<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create New Admin</button> 
			<br><br>
		
<table class="table table-bordered">
							<thead>
								<tr style="background-color:#eee;">
									<th>#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Username</th>
									<th>Created</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>								
								@foreach (App\Admin::forge()->retrieve() as $admin)
								<tr>
									<td>{{ $admin->id }}</td>
									<td>{{ @$admin->primary()->firstname }} {{ @$admin->primary()->lastname }}</td>
									<td>{{ $admin->email }}</td>
									<td>{{ $admin->username }}</td>
									<td>{{ $admin->created_at }}</td>
									<td><a href="{{ $app['admin_url'] }}setup/admins/edit/{{ $admin->id }}">manage</a> 
									 @if (Auth::user()->id != $admin->id)
									| <a href="{{ $app['admin_url'] }}setup/admins/index/?delete={{ $admin->id }}">delete</a>
									@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
		
		<br><br>
			
				<div class="panel colourable">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<span class="panel-title">Available system roles</span>
					</div>
				

					<!-- List group -->
					<ul class="list-group">

					@foreach (Role::get() as $role)
						
						<li class="list-group-item">{{ $role->name }}</li>

					@endforeach	
					
					</ul>	<div class="panel-body">
						<a href="#">Create new role
					</a>
					</div>
				</div>

				<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->

		<!-- Page wide horizontal line -->

	</div>


	@section('modals')
	<div id="myModal" class="modal fade active in" tabindex="-1" role="dialog" style="display: ;">
					<div class="modal-dialog">
						<div class="modal-content" style="width:700px;">
							
							<div class="modal-body">

<div class="alert alert-info" style='display:none' id='addNewAdminAlert'>
							<button type="button" class="close" data-hide="alert">×</button>
							<div id='result-msg'></div>
						</div>


								<form class="panel form-horizontal" action='{{ $app["admin_url"] }}setup/admins/create' id='addNewAdmin'>
					<div class="panel-heading">
						<span class="panel-title">Add New Admin</span>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">First Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputEmail2" placeholder="" name='contacts[firstname]'>
							</div>
						</div> <!-- / .form-group -->

	<div class="form-group">
							<label for="inputEmail2" class="col-sm-3 control-label">Last Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputEmail2" placeholder="" name='contacts[lastname]'>
							</div>
						</div> <!-- / .form-group -->

						<div class="form-group">
							<label for="inputPassword" class="col-sm-3 control-label">Email:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='email'>
							</div>
						</div> 
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-3 control-label">Username:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='username'>
							</div>
						</div> 
				
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-3 control-label">Password:</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="inputPassword" placeholder="" name='password'>
							</div>
						</div> 
									
					</div>
				</form>
				


							
							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick="ajaxForm('#addNewAdmin', function(data) {  $('#addNewAdminAlert').show(); $('#result-msg').html(data); })">Create Admin</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div>

				@endsection