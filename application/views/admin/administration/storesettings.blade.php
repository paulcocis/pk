<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="{{ $app["admin_url"] }}setup/">Setup</a></li>
			<li class="active"><a href="{{ $app["admin_url"] }}setup/store">Store Settings</a></li>
		</ul>
		<div class="page-header">
			
				<ul id="uidemo-tabs-default-demo" class="nav nav-tabs">
							
							<li class="active">
								<a href="#user-settings"  data-toggle="tab"><i class="fa fa-user"></i> General Settings </a>
							
							</li> <!-- / .dropdown -->
						</ul>

						<div class="tab-content tab-content-bordered no-padding">
							 <!-- / .tab-pane -->
							 <!-- / .tab-pane -->
						

<div class="tab-pane fade active in" id="user-settings" >

<div class="alert alert-success" style='display:none' id='storeSettingsAlert'>
							<button type="button" class="close" data-hide="alert">×</button>
							<strong>Well done!</strong> Informations has been succesfully updated.
						</div>

<form class="panel form-horizontal no-padding" action='{{ $app["admin_url"] }}setup/store/' id='storeSettings'>
					<div class="auto-style1">
						<img src="{{ $assets }}images/option_a.png"> Store Settings</div>
					<div class="panel-body">
											<div class="form-group">
							<label class="col-sm-2 control-label">Allow new orders?:</label>
							<div class="col-sm-10">

<div id="switchers-colors-square" class="form-group-margin">

<input type="checkbox" data-class="switcher-success"  name='allow_new_order' value='1' @if ($row->allow_new_order) checked="checked" @endif>			
</div>

				</div>
 <!-- / .col-sm-10 -->
						</div>
						
							<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">Allow notes on checkout?:</label>
							<div class="col-sm-10">

<div id="switchers-colors-square" class="form-group-margin">

<input type="checkbox" data-class="switcher-success" name='allow_checkout_notes' value='1' @if ($row->allow_checkout_notes) checked="checked" @endif>			
</div>

				</div>
						</div> <!-- / .form-group -->
						
		<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">Allow guest checkout?:</label>
							<div class="col-sm-10">

<div id="switchers-colors-square" class="form-group-margin">

<input type="checkbox" data-class="switcher-success" name='allow_guest_checkout' value='1' @if ($row->allow_guest_checkout) checked="checked" @endif>		<p class="help-block">If enabled visitor will be redirected directly to payment gateway.<br> PinkePromise account will be created based on gateway post-back informations.</p>
	
</div>

				</div>
						</div> <!-- / .form-group -->
						
		<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">Allow coupon usage?:</label>
							<div class="col-sm-10">

<div id="switchers-colors-square" class="form-group-margin">

<input type="checkbox" data-class="switcher-success" name='allow_coupon_usage' value='1' @if ($row->allow_coupon_usage) checked="checked" @endif>		<p class="help-block">Enable this feature if you want to use the coupons on your shopping cart.</p>
	
</div>

				</div>
						</div> <!-- / .form-group -->
						
						
						
<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">Terms Of Service:</label>
							<div class="col-sm-10">

<div id="switchers-colors-square" class="form-group-margin">

<input type="checkbox" data-class="switcher-success" name='enable_terms' value='1' @if ($row->enable_terms) checked="checked" @endif>		<p class="help-block">If enabled on checkout customer will need to check the terms of service checkbox. <br> Below you can set your terms of service text.</p>
	
</div>

				</div>
						</div>						
				


<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">Terms Of Service Text:</label>
							<div class="col-sm-10">

						<textarea class="form-control" id="summernote-example" rows="10" name='terms_text'>{{ $row->terms_text }}</textarea>

</div>

				</div>
						
						
	
<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">New Order Email Notification:</label>
							<div class="col-sm-10">

<div id="jq-expanding-input-callbacks">
							<textarea rows="1" class="form-control" name='email_notification_list'>{{ $row->email_notification_list }}</textarea>
							<p class="help-block">Enter email addresses separated by ','</p>
							
						</div>
</div>

				</div>

					
						
						
						

		

						
								<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
								<input type="button" class="btn btn-sm btn-success" id="inputPassword" value="Save Changes" onclick="ajaxForm('#storeSettings', function(data) {  $('#storeSettingsAlert').show(); })">
							</div>
						</div>


					</div>
				</form>
							</div>




						</div> <!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->

		<!-- Page wide horizontal line -->

	</div>



@section('scripts')
<script type="text/javascript">
	init.push(function () {

	$('#switchers-colors-default > input').switcher();
						$('#switchers-colors-square > input').switcher({ theme: 'square' });
						$('#switchers-colors-modern > input').switcher({ theme: 'modern' });




	if (! $('html').hasClass('ie8')) {
					$('#summernote-example').summernote({
						height: 200,
						//tabsize: 2,
						codemirror: {
							theme: 'monokai'
						}
					});
				}
				$('#summernote-boxed').switcher({
					on_state_content: '<span class="fa fa-check" style="font-size:11px;"></span>',
					off_state_content: '<span class="fa fa-times" style="font-size:11px;"></span>'
				});
				$('#summernote-boxed').on($('html').hasClass('ie8') ? "propertychange" : "change", function () {
					var $panel = $(this).parents('.panel');
					if ($(this).is(':checked')) {
						$panel.find('.panel-body').addClass('no-padding');
						$panel.find('.panel-body > *').addClass('no-border');
					} else {
						$panel.find('.panel-body').removeClass('no-padding');
						$panel.find('.panel-body > *').removeClass('no-border');
					}
				});
				
				
				$("#jq-expanding-input-callbacks").expandingInput({
							target: 'textarea',
							hidden_content: '> div',
							placeholder: 'Enter email addresses',
							onBeforeExpand: function () {
								console.log('onBeforeExpand callback');
							},
							onAfterExpand: function () {
								console.log('onAfterExpand callback');
								this.$target.attr('rows', 4);
							}
						});






	})
	window.PixelAdmin.start(init);
	

	
</script>

@endsection