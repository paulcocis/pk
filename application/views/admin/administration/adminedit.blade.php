@section('body-type')page-profile@endsection

<div id="content-wrapper">
<!-- 5. $PROFILE ===================================================================================

		Profile
-->
		<div class="profile-full-name">
			<span class="text-semibold">Admin Account</span>'s profile
		</div>
	 	<div class="profile-row">
			<div class="left-col">
				<div class="profile-block">
					<div class="panel profile-photo">
						<a href='#'  data-toggle="modal" data-target="#myModal2"><img src="{{ UserAvatar::find($user->id) }}" alt=""></a>
					</div><br>
					<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;Remove</a>
					<a href="#" class="btn btn-sm"><i class="fa fa-comment"></i></a>
				</div>
				
				<div class="panel panel-transparent">
					<div class="panel-heading">
						<span class="panel-title">Profile Informations</span>
					</div>
					<div class="panel-body">
						Created On:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="label label-info">{{ $user->created_at }}</span> <br>
						<small>Last login on {{ $user->last_login_at }}</small>					
					</div>
				</div>

				<div class="panel panel-transparent profile-skills">
					<div class="panel-heading">
						<span class="panel-title">Roles</span>
					</div>
					<div class="panel-body">
					@foreach ($user->roles as $role)
						<span class="label label-primary">{{ $role->display_name }}</span>
					@endforeach
						
					</div>
				</div>

				<div class="panel panel-transparent">
					<div class="panel-heading">
						<span class="panel-title">Social</span>
					</div>
					<div class="list-group">
						<a href="#" class="list-group-item"><i class="profile-list-icon fa fa-facebook-square" style="color: #1a7ab9"></i> FB Profile</a>
						<a href="#" class="list-group-item"><i class="profile-list-icon fa fa-envelope" style="color: #888"></i>{{ $user->email }}</a>
					</div>
				</div>

			</div>
			<div class="right-col">

				<hr class="profile-content-hr no-grid-gutter-h">
				
				<div class="profile-content">

					<ul id="profile-tabs" class="nav nav-tabs">
						<li class="active">
							<a href="#profile-tabs-board" data-toggle="tab">Dashboard</a>
						</li>
	<li class="">
							<a href="#profile-tabs-board" data-toggle="modal" data-target="#myModal1">Other Account Settings</a>
						</li>
						
										
					</ul>

					<div class="tab-content tab-content-bordered panel-padding">
						<div class="widget-article-comments tab-pane panel no-padding no-border fade in active" id="profile-tabs-board">

<div class="alert alert-info" style='display:none' id='editAdminAlert'>
							<button type="button" class="close" data-hide="alert">×</button>
							<div id='result-msg'></div>
						</div>


						<form action='{{ $app["admin_url"] }}setup/admins/edit/{{ $user->id }}' id='editAdmin'>
					
					<div>
						<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">First Name:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputEmail2" name='contact[firstname]' placeholder="" value='{{ @$user->primary()->firstname }}'>
							</div>
						</div> <!-- / .form-group -->

						<div class="form-group">
							<label for="inputEmail2" class="col-sm-2 control-label">Last Name:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputEmail2" name='contact[lastname]' placeholder="" value='{{ @$user->primary()->lastname }}'>
							</div>
						</div> <!-- / .form-group -->

						

						<div class="form-group" style=" padding:0px;">
							<label for="inputEmail2" class="col-sm-2 control-label">Username:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputEmail2" placeholder="" name='username' value="{{ $user->username }}" disabled="TRUE" >
							</div>
						</div> <!-- / .form-group -->
						
						
						<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Email:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='email' value="{{ $user->email }}" >
								<p class="help-block">Example block-level help text here.</p>
							</div>
						</div> 
					
						<!-- / .form-group -->
						<div class="form-group">
							<label for="asdasdas" class="col-sm-2 control-label">Roles:</label>
							<div class="col-sm-10">


								<select multiple="multiple" id="jquery-select2-multiple" class="form-control" name='roles[]'>
								

								@foreach (Role::get() as $r)


									<option value="{{ $r->id }}" @if ($user->has_role_id($r->id)) selected="selected" @endif>{{ $r->name }}</option>
								@endforeach
												
						
						</select>
						
							</div>
						</div> <!-- / .form-group -->
						<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Signature Email:</label>
							<div class="col-sm-10">
								<textarea rows="5" class="form-control"></textarea>
								<p class="help-block">Will be added when sending emails or creating helpdesk tickets.</p>
							</div>
						</div> 
						
						
	<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
								<input onclick="ajaxForm('#editAdmin', function(data) {  $('#editADminAlert').show(); $('#result-msg').html(data); })" type="button" class="btn btn-lg btn-success" value="Save Changes">							</div>
						</div> 
						
						

					</div>
				</form>

						</div> <!-- / .tab-pane -->
						 <!-- / .tab-pane -->
						 <!-- / .tab-pane -->
						 <!-- / .tab-pane -->
					</div> <!-- / .tab-content -->
				</div>
			</div>
		</div>
		

	</div>


	@section('scripts')

<script type="text/javascript">
	init.push(function () {
		
		
		$('#profile-tabs').tabdrop();

		$("#leave-comment-form").expandingInput({
			target: 'textarea',
			hidden_content: '> div',
			placeholder: 'Write message',
			onAfterExpand: function () {
				$('#leave-comment-form textarea').attr('rows', '3').autosize();
			}
		});
		
		// end comment
		
	$('#switcher-example-2').switcher({
		theme: 'square',
		on_state_content: '<span class="fa fa-check"></span>',
		off_state_content: '<span class="fa fa-times"></span>'
	});


   // Multiselect
	$("#jquery-select2-multiple").select2({
		placeholder: "assign roles"
	});
			
});
	
	
	
	
	window.PixelAdmin.start(init);
</script>


@endsection

	@section('modals')

	<div id="myModal1" class="modal fade active in" tabindex="-1" role="dialog" style="display: ;">
					<div class="modal-dialog">
						<div class="modal-content">
							
							<div class="modal-body">
							
							
											
		<!-- 11. $THREADS ==================================================================================

				Threads
-->
				<div class="panel widget-threads" style="	 min-height:100px;">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-comments-o"></i>Advanced Account Options</span>
					</div> <!-- / .panel-heading -->
					<div class="panel-body">
						
						<div class="thread" >
							<img src="{{ $assets }}images/option_a.png" alt="" class="thread-avatar">
							<div class="thread-body">
								<span class="thread-time">	<select class="form-control">
							<option value="">Active</option>
							<option value="">Pending</option>							
						</select>
</span>
								<a href="#" class="thread-title">Account status</a>
								<div class="thread-info">change the account current status.</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->


					<div class="thread" >
							<img src="{{ $assets }}images/option_a.png" alt="" class="thread-avatar">
							<div class="thread-body">
								<span class="thread-time">	<input type="text" class="form-control" id="inputPassword" placeholder="">
</span>
								<a href="#" class="thread-title">New Password</a>
								<div class="thread-info">set account new password</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->



	
					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
<!-- /11. $THREADS -->

				


							
							</div> <!-- / .modal-body -->
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div>



<div id="myModal2" class="modal fade active in" tabindex="-1" role="dialog" style="display: ;">
					<div class="modal-dialog">
						


<form action='{{ $app["admin_url"] }}seller/home/setAvatar/{{ $user->id }}' enctype="multipart/form-data" method="post" id='ttt'>


						<div class="modal-content">
							
							<div class="modal-body">
							
							
											
		<!-- 11. $THREADS ==================================================================================

				Threads
-->
				<div class="panel widget-threads" style="	 min-height:100px;">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-comments-o"></i>Edit Your Profile Avatar</span>
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

					<div class="alert alert-success" id='advancedSellerSettingsAlert' style="display:none;">Data has been modified</div>
						
						<div class="thread" >
							<img src="{{ $assets }}images/option_a.png" alt="" class="thread-avatar">
							<div class="thread-body">
								<span class="thread-time">								<input type="file" name="file" size="40" value='Select an image for avatar.'>

</span>
								<a href="#" class="thread-title">Select avatar</a>
								<div class="thread-info">Select the image that you want to be <br>your avatar.</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->




					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
<!-- /11. $THREADS -->

							</div> <!-- / .modal-body -->


							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick="$('#ttt').submit();">Save changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
					</form>
				</div>
@endsection