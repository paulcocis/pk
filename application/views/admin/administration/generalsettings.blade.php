<div id="content-wrapper">
    <ul class="breadcrumb breadcrumb-page">
        <div class="breadcrumb-label text-light-gray">You are here:
        </div>
    <li><a href="{{ $app["admin_url"] }}setup/">Setup</a></li>
    <li class="active"><a href='{{ $app["admin_url"] }}setup/'>General Settings</a></li>
</ul>
<div class="page-header">
    <ul class="nav nav-tabs" id="uidemo-tabs-default-demo">
        <li class="active">
            <a data-toggle="tab" href="#uidemo-tabs-default-demo-home">General
                <span class="label label-success">G</span></a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#uidemo-tabs-default-demo-profile">Mail
                Settings
                <span class="badge badge-primary">M</span></a>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="tab" href="#user-settings">
                <i class="fa fa-user"></i>
                &nbsp;User & Signup Settings
            </a>
        </li>
        <!-- / .dropdown -->
    </ul>
    <div class="tab-content tab-content-bordered">
        <div class="tab-pane fade active in" id="uidemo-tabs-default-demo-home">
           
<div class="alert alert-success" style='display:none' id='generalSettingsAlert'>
							<button type="button" class="close" data-hide="alert">×</button>
							<strong>Well done!</strong> Informations has been succesfully updated.
						</div>

            <form class="panel form-horizontal" method='POST' action='{{ $app["admin_url"] }}setup/' id='generalSettings'>
                <div class="auto-style1">
                    General Settings</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Website
                            name:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="website_name" name='website_name'  type="text" value="{{ $row->website_name }}">
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Company
                            name:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="company_name" name='company_name' value="{{ $row->company_name }}" type="text">
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Company contact
                            email:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="inputEmail2" name='company_contact_email' value="{{ $row->company_contact_email }}"type="text">
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Company
                            Profie:<br>
                            <small style="font-weight:normal;">provide some description about
                                company profile.</small>
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="inputEmail2" name='company_profile' placeholder="Email" rows="10">{{ $row->company_profile }}</textarea>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Company
                            Location:<br>
                            <small style="font-weight:normal;font-size:10px;">street address, city,
                                state, country, zipcode, etc.</small>
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="inputEmail2" name='company_location' placeholder="Email" rows="4">{{ $row->company_location }}</textarea>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Default System
                            Timezone:</label>
                        <div class="col-sm-10">
                            <select class="form-control input-sm" name="default_timezone" style="margin-bottom: 0;">
                                @foreach ($System->timezoneList() as $key => $value)
                                <option value="{{ $key }}" @if ($row->default_timezone == $key) selected="selected" @endif>{{ $value }}
                                </option>
                                @endforeach 
                            </select>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputPassword"></label>
                        <div class="col-sm-9">
                            <input class="btn btn-lg btn-success" id="inputPassword" type="button" value="Save Changes" 
                            onclick="ajaxForm('#generalSettings', function(data) {  $('#generalSettingsAlert').show(); })">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- / .tab-pane -->
        <div class="tab-pane fade in" id="uidemo-tabs-default-demo-profile">

<div class="alert alert-success" style='display:none' id='smtpSettingsAlert'>
                            <button type="button" class="close" data-hide="alert">×</button>
                            <strong>Well done!</strong> Informations has been succesfully updated.
                        </div>


            <form class="panel form-horizontal"  method='POST' action='{{ $app["admin_url"] }}setup/' id='smtpSettings'>
                <div class="auto-style1">
                    <img src="{{ $assets }}images/option_a.png">
                    STMP and General Email Settings</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">SMTP
                            Hostname:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="smtp_hostname" name='smtp_hostname' placeholder="Hostname" type="text" value='{{ $row->smtp_hostname }}'>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">SMTP
                            Port:</label>
                        <div class="col-sm-2">
                            <input class="form-control" id="smtp_port" name='smtp_port' placeholder="" type="text" value='{{ $row->smtp_port }}'>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">SMTP
                            Username:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="smtp_username" name='smtp_username' placeholder="User/Email Address" type="text" value='{{ $row->smtp_username }}'>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">SMTP
                            Password:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="inputEmail2" name='smtp_password' placeholder="SMTP account password" type="text" value='{{ $row->smtp_password }}'>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Secure</label>
                        <div class="col-sm-10">

                                @foreach($app['smtp_secure_types'] as $type)
                                <div class="radio">
                                
                                    <label>
                                        <input  class="px" id="op[]" name="smtp_secure" type="radio" value="{{ $type }}" @if ($type == $row->smtp_secure) checked="checked" @endif />
                                        <span class="lbl">{{ $type }}</span>
                                    </label>

                                </div>
                                @endforeach
                               


                    
                            <!-- / .radio -->
                            <!-- / .radio -->
                        </div>
                        <!-- / .col-sm-10 -->
                    </div>
                    <hr>
                    <h1>
                        Other Settings</h1>
                    <hr>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">From
                            Email:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="smtp_from_email" name='smtp_from_email' placeholder="example: noreply@earnads.com" type="text" value='{{ $row->smtp_from_email }}'>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">From
                            Name:</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="smtp_from_name" name='smtp_from_name' placeholder="example: My Company Name" type="text" value='{{ $row->smtp_from_name }}'>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Default Email
                            signature:<br>
                            <small style="font-weight:normal;">this signature will be included in
                                all emails delivered by earnads system.</small>
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="inputEmail2" name='smtp_default_signature' placeholder="Email" rows="10">{{ $row->smtp_default_signature }}</textarea>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputPassword"></label>
                        <div class="col-sm-9">
                            <input class="btn btn-lg btn-success" id="inputPassword" type="button" value="Save Changes"  onclick="ajaxForm('#smtpSettings', function(data) {  $('#smtpSettingsAlert').show(); })">
                        </div>
                    </div>
                    <!-- / .form-group -->
                </div>
            </form>
        </div>
        <!-- / .tab-pane -->
        <div class="tab-pane fade in" id="user-settings">

<div class="alert alert-success" style='display:none' id='signupSettingsAlert'>
                            <button type="button" class="close" data-hide="alert">×</button>
                            <strong>Well done!</strong> Informations has been succesfully updated.
                        </div>


            <form class="panel form-horizontal"  method='POST' action='{{ $app["admin_url"] }}setup/home/signup' id='signupSettings'>
                <div class="auto-style1">
                    <img src="{{ $assets }}images/option_a.png">
                    User And Signup Settings</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Allow new user signup?:</label>
                        <div class="col-sm-10">
                            <div class="radio-inline">
                                <label>
                                    <input  class="px" id="sgEn1" name="signup_enabled" type="radio" value="1" @if ($rowSignup->signup_enabled == 1) checked='checked' @endif>
                                    <span class="lbl">Yes</span>
                                </label>
                            </div>
                            <!-- / .radio -->
                            <div class="radio-inline">
                                <label>
                                    <input class="px" id="sgEn0" name="signup_enabled" type="radio" value="0" @if ($rowSignup->signup_enabled != 1) checked='checked' @endif>
                                    <span class="lbl">No</span>
                                </label>
                            </div>
                            <!-- / .radio -->
                            <!-- / .radio -->
                        </div>
                        <!-- / .col-sm-10 -->
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Allow users
                            updating their profile?:</label>
                        <div class="col-sm-10">
                            <div class="form-group-margin" id="switchers-colors-square">
                                <input  data-class="switcher-success" name='user_allow_update_profile' type="checkbox"  value='1' @if ($rowSignup->user_allow_update_profile == 1) checked='checked' @endif>
                            </div>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail2">Allow users
                            updating their email?:</label>
                        <div class="col-sm-10">
                            <div class="form-group-margin" id="switchers-colors-square">
                                <input  data-class="switcher-success" name='user_allow_update_email' type="checkbox" value='1' @if ($rowSignup->user_allow_update_email == 1) checked='checked' @endif>
                            </div>
                        </div>
                    </div>
                    <!-- / .form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputPassword"></label>
                        <div class="col-sm-9">
                            <input class="btn btn-sm btn-success" id="inputPassword" type="button" value="Save Changes" onclick="ajaxForm('#signupSettings', function(data) {  $('#signupSettingsAlert').show(); })">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- / .tab-content -->
</div>
<!-- / .page-header -->
<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->
<!-- Page wide horizontal line -->
<!-- Page wide horizontal line -->
</div>