<div id="content-wrapper">
        <ul class="breadcrumb breadcrumb-page">
            <div class="breadcrumb-label text-light-gray">You are here: </div>
            <li><a href="{{ $app["admin_url"] }}setup/">Setup</a></li>
            <li class="active"><a href="{{ $app["admin_url"] }}setup/paymentgateways">Payment Gateways</a></li>
        </ul>
        <div class="page-header">
            
            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add New Gateway</button> 
            <br><br>
        
<table class="table table-bordered">
                            <thead>
                                <tr style="background-color:#eee;">
                                    <th  class="hidden-xs">#</th>
                                    <th  class="hidden-xs">Name:</th>
                                    <th>Display Name:</th>
                                    <th  class="hidden-xs" class="center">Orders:</th>
                                    <th  class="hidden-xs" class="center">Transactions:</th>
                                    <th>Status:</th>
                                    <th  class="hidden-xs">Created:</th>
                                    <th>Actions:</th>
                                </tr>
                            </thead>
                            <tbody>
                                

                                @if (App\Setup\PaymentGateway::count() == 0)
                                <tr>
                                <td colspan="8">No gateways found</td
                                </tr>

                                @else


                                @foreach (App\Setup\PaymentGateway::get() as $record)
                                <tr>
                                    <td  class="hidden-xs">{{ $record->id }}</td>
                                    <td  class="hidden-xs"><i class="fa fa-edit"></i> {{ $record->name }}</td>
                                    <td >{{ $record->display_name }}</td>
                                    <td  class="hidden-xs" class="center"><a href="#">2545</a></td>
                                    <td  class="hidden-xs" class="center"><a href="#">2545</a></td>
                                    <td>


                                    <label class="label @if ($record->status == 'active')label-success @else label-warning @endif">{{ $record->status }}</label></td>
                                    <td  class="hidden-xs">{{ $record->created_at }}</td>
                                    <td><a href="{{ $app['admin_url'] }}setup/paymentgateways/edit/{{ $record->id }}">manage</a></td>
                                </tr>
                                @endforeach


                                @endif
                         

                            </tbody>
                        </table>
        
        <br><br>
            
                <!-- Light info -->
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="panel-title">What are payment gateways?</span>
                        <div class="panel-heading-controls">
                            <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
                        </div>
                    </div>
                    <div class="panel-body">
                        The payment gateways are the payment options for consumers.<br> On checkout customers will be able to choose from one of added payment gateways as preferred payment option.
                        <br>
                        <hr>
                        From their administration panel they can set an default payment for future orders.
 
                    </div>
                </div> <!-- / .panel -->

                <!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

        <!-- Page wide horizontal line -->

        <!-- Page wide horizontal line -->

    </div>




                <!-- Modal -->
                <div id="myModal" class="modal fade active in" tabindex="-1" role="dialog" style="display: ;">
                    <div class="modal-dialog">
                        <div class="modal-content" style="width:700px;">
                            
                            <div class="modal-body">

<div class="alert alert-info" style='display:none' id='createGatewayAlert'>
                            <button type="button" class="close" data-hide="alert">×</button>
                            <div id='result-msg'></div>
                        </div>



                                <form class="panel form-horizontal" action='{{ $app["admin_url"] }}setup/paymentgateways/' id='createGateway' >
                                <input type='hidden' name='action' value='create'>
                    <div class="panel-heading">
                        <span class="panel-title">Add New Payment Gateway</span>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="inputEmail2" class="col-sm-3 control-label">Name:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputEmail2" name='name' placeholder="enter gateway name ...">
                            </div>
                        </div> <!-- / .form-group -->
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">Display Name:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name='display_name' id="inputPassword" placeholder="">
                                <p class="help-block">this is will be displayed on shopping cart checkout form.</p>
                            </div>
                        </div> 
                        
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">Support Email:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name='support_email' id="inputPassword" placeholder="">
                            </div>
                        </div> 
                    <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">Web URL:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name='url' id="inputPassword" placeholder="">
                            </div>
                        </div> 
            
                        
                        
            <!-- / .form-group -->
                        <div class="form-group">
                            <label for="asdasdas" class="col-sm-3 control-label">Status:</label>
                            <div class="col-sm-4">
                                <select id="jquery-select2-multiple" class="form-control" name='status'>
                            @foreach (App\Setup\PaymentGateway::forge()->enum_field('status') as $key)
                                <option value="{{ $key }}">{{ ucfirst($key) }}</option>

                            @endforeach
                        
                        </select>
                        
                            </div>
                        </div> <!-- / .form-group -->                       
                        
                        
                        <hr>
                        <h3 onclick="$('#apiForm').toggle('slow');" style="cursor:pointer;">API Informations <img src="{{ $assets }}images/toggle.gif"> </h3>
                        <hr>
                        
                        
                        <div id="apiForm" style="display:none;">
                        
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">API URL:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_url'>
                                <p class="help-block">Example block-level help text here.</p>
                            </div>
                        </div>

            <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">Connection Port:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_port'>
                                <p class="help-block">Example block-level help text here.</p>
                            </div>
                        </div>
                        
                        
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">API Username:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_username'>
                                <p class="help-block">leave empty if no needed.</p>
                            </div>
                        </div>
                        
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">API Password:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_password'>
                                <p class="help-block">leave empty if no needed.</p>
                            </div>
                        </div>
                    
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">API Key:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_key'>
                                <p class="help-block">leave empty if no needed.</p>
                            </div>
                        </div>

    <div class="form-group">
                            <label for="inputPassword" class="col-sm-3 control-label">Custom Post Fields:</label>
                            <div class="col-sm-8">
                                <textarea rows="4" class="form-control" name='api_custom_fields'></textarea>
                                <p class="help-block">If you need to post more data for authentification to api or to grant access please enter extra post fields in format (key = value) one per line.</p>
                            </div>
                        </div>
</div>

                        
            
                    
                    </div>
                </form>
                


                            
                            </div> <!-- / .modal-body -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary"

onclick="ajaxForm('#createGateway', function(data) {  $('#createGatewayAlert').show(); $('#result-msg').html(data); })"
                                >Create Gateway</button>
                            </div>
                        </div> <!-- / .modal-content -->
                    </div> <!-- / .modal-dialog -->
                </div> <!-- /.modal -->
                <!-- / Modal -->


<script>
$(function() {
// Multiselect
                        $("#jquery-select2-multiple").select2({
                            placeholder: "assign roles"
                        });

});

</script>
