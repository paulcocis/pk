
@section('body-type')page-profile@endsection


<div id="content-wrapper">
<!-- 5. $PROFILE ===================================================================================

        Profile
-->
        <div class="profile-full-name">
            <span class="text-semibold">{{ $row->display_name }}</span> payment gateway administration.
        </div>
        <div class="profile-row">
            <div class="left-col">
                <div class="profile-block">
                    <br>    <br>    <br>    <br>    

                        <!-- Small -->
                        <div class="btn-group btn-group-lg" style="float:left;">
                            <button type="button" class="btn @if ($row->status != 'active') btn-warning @else btn-success @endif">Gateway: {{ $row->status }}</button>
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-down"></i></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">View transactions</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ $app['admin_url'] }}setup/paymentgateways/edit/{{ $row->id }}/?action=delete">Delete Gateway</a></li>
                            </ul>
                        </div>&nbsp;&nbsp;
                
                </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        
                                        <div class="panel panel-transparent">
                                    <div class="panel-body">
                        Last modification on {{ $row->updated_at }}
                                            </div>
                </div>



                                        

                    <div class="panel panel-info panel-dark widget-profile">
                    <div class="panel-heading">
                        <div class="widget-profile-bg-icon"><i class="fa fa-bar-chart-o"></i></div>
                        <div class="widget-profile-header">
                            <span>Summary Report</span><br>

                        </div>
                    </div> <!-- / .panel-heading -->
                    <div class="list-group">
                        <a href="#" class="list-group-item"><i class="fa fa-list-alt list-group-icon"></i>Orders<span class="badge badge-info">14</span></a>
                        <a href="#" class="list-group-item"><i class="fa fa-money list-group-icon"></i>Transactions<span class="badge badge-warning">7</span></a>
                        <a href="#" class="list-group-item"><i class="fa fa-bell-o list-group-icon"></i>Logs<span class="badge badge-danger">11</span></a>
                        <a href="#" class="list-group-item"></a>
                    </div>
                </div> <!-- / .panel -->

                
                

                

            </div>
            <div class="right-col">

                <hr class="profile-content-hr no-grid-gutter-h">
                
                <div class="profile-content">

                    <ul id="profile-tabs" class="nav nav-tabs">
                        <li class="active">
                            <a href="#profile-tabs-board" data-toggle="tab">General Settings</a>
                        </li>
                        <li>
                            <a href="#products"  data-toggle="tab" >API Settings</a>
                        </li>
                    
                    </ul>
                    
                    

                    <div class="tab-content tab-content-bordered panel-padding">
                    <div class="widget-article-comments tab-pane panel no-padding no-border fade" id="products">
                    
                    <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                             This section settings allows you to interract with gateway application interface.<br>
                             Informations that are requested on this form are generally provided by the gateway provider.
                        </div>

                    <div id="apiForm" style="display:;">
                    
<div class="alert alert-info" style='display:none' id='editApiAlert'>
                            <button type="button" class="close" data-hide="alert">×</button>
                            <div id='result-msg'></div>
                        </div>


                        <form action='{{ $app["admin_url"] }}setup/paymentgateways/edit/{{ $row->id }}' id='editGatewayApi'>
<input type='hidden' name='gwid' value='{{ $row->id }}'>
        <div class="form-group" style="padding-top:6px;padding-bottom:1px; background-color:#eee;border-bottom:1px solid #e1e1e1;">
                            <label for="inputPassword" class="col-sm-2 control-label" style="padding-top:8px;">Test Mode:</label>
                            <div class="col-sm-4">
                                                    <div id="switchers-colors-default" class="form-group-margin" style="margin-top:8px;">

                                        <input type="checkbox" data-class="switcher-primary" checked="checked">&nbsp;&nbsp;
                                            </div>
                        </div>
                        
                        </div>
                    
                    
                    
                        
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">API URL:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_url' value='{{ $row->api_url }}'>
                                <p class="help-block">The gateway provider API url address.</p>
                            </div>
                        </div>

             <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">API Post-back URL:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_postback_url' value='{{ $row->api_postback_url }}'>
                                <p class="help-block">The gateway provider API url address.</p>
                            </div>
                        </div>                        

            <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Connection Port:</label>
                            <div class="col-sm-1">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_port' value='{{ $row->api_port }}'>
                            </div>
                        </div>
                        
                        
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">API Username:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_username' value='{{ $row->api_username }}'>
                            </div>
                        </div>
                        
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">API Password:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_password' value='{{ $row->api_password }}'>
                            </div>
                        </div>
                    
        <div class="form-group" >
                            <label for="inputPassword" class="col-sm-2 control-label" style="padding-top:5px;">API Key:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='api_key' value='{{ $row->api_key }}'>
                            </div>
                        </div>
                        
                        <hr>

    <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Custom Post Fields:</label>
                            <div class="col-sm-8">
                                <textarea rows="4" class="form-control" name='api_custom_postfields'> {{ $row->api_custom_postfields }}</textarea>
                                <p class="help-block">If you need to post more data for authentification to api or to grant access please enter extra post fields in format (key = value) one per line.</p>
                            </div>
                        </div>
                        
                        
    <!-- / .form-group -->
                        <div class="form-group">
                            <label for="asdasdas" class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                <input type="button" class="btn btm-sm btn-success" value="Save Changes" onclick="ajaxForm('#editGatewayApi', function(data) {  $('#editApiAlert').show(); $('#result-msg').html(data); })">

                            </div>
                        </div> <!-- / .form-group -->                           
                        
          </form>              
                        
</div>


                    </div>
                    
                        <div class="widget-article-comments tab-pane panel no-padding no-border fade in active" id="profile-tabs-board">

<div class="alert alert-info" style='display:none' id='editGatewayAlert'>
                            <button type="button" class="close" data-hide="alert">×</button>
                            <div id='result-msg1'></div>
                        </div>


                        <form action='{{ $app["admin_url"] }}setup/paymentgateways/edit/{{ $row->id }}' id='editGatewaySettings'>

                        <input type='hidden' name='gwid' value='{{ $row->id }}'>
                     
                    <div>
                        <div class="form-group">
                            <label for="inputEmail2" class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="inputEmail2" placeholder="enter gateway name ..." name='name' value='{{ $row->name }}'>
                            </div>
                        </div> <!-- / .form-group -->
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Display Name:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="inputPassword" placeholder="" name='display_name' value='{{ $row->display_name }}'>
                                <p class="help-block">this is will be displayed on shopping cart checkout form.</p>
                            </div>
                        </div> 
                        
        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Support Email:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="inputPassword" name='support_email' placeholder="" value='{{ $row->support_email }}'> 
                            </div>
                        </div> 
                    <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Web URL:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="inputPassword" name='url' placeholder="" value='{{ $row->url }}'>
                            </div>
                        </div> 
            
                        
                        
            <!-- / .form-group -->
                        <div class="form-group">
                            <label for="asdasdas" class="col-sm-2 control-label">Status:</label>
                            <div class="col-sm-4">
                                <select id="jquery-select2-multiple" class="form-control" name='status'>
                            @foreach (App\Setup\PaymentGateway::forge()->enum_field('status') as $key)
                                <option value="{{ $key }}"
                                    @if ($key == $row->status)
                                        selected='selected'
                                    @endif


                                > {{ ucfirst($key) }}</option>

                            @endforeach
                        
                        </select>
                        
                            </div>
                        </div> <!-- / .form-group -->       
                        
                            <hr>
                        <h3 onclick="$('#apiForm').toggle('slow');" style="cursor:pointer;">More Options <img src="{{ $assets }}images/toggle.gif"> </h3>
                        <hr>
                        

                        
    <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Offline Payment Description:</label>
                            <div class="col-sm-6">
                                <textarea rows="6" class="form-control" name='offline_payment_description'> {{ $row->offline_payment_description }}</textarea>
                                <div class="alert alert-info" style="margin-top:7px;">
                             If this is an offline payment like <strong>bank transfer</strong> you can provide payment details like bank account, etc. Text will be added on new order confirmation email.<br>
                             
                        </div>

                            </div>
                        </div> 
<hr>
<div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">Gateway Commission:</label>
                            <div class="col-sm-1">
                                <input type="text" class="form-control inline" id="inputPassword" placeholder="" name='commision' value='{{ $row->commision }}'>
                                </div>
                                <div class="col-sm-2">
                                                                
<select  class="form-control" name='comission_type'>
                                <option value="percentage">Percentage</option>
                                <option value="fixed">Fixed</option>                                
                        
                        </select>

                                </div>
                        </div> 



<hr>



        <!-- / .form-group -->
                        <div class="form-group">
                            <label for="asdasdas" class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                <input type="button" class="btn btm-sm btn-success" value="Save Changes" onclick="ajaxForm('#editGatewaySettings', function(data) {  $('#editGatewayAlert').show(); $('#result-msg1').html(data); })">

                            </div>
                        </div> <!-- / .form-group -->       



                    </div>
                </form>

                        </div> <!-- / .tab-pane -->
                         <!-- / .tab-pane -->
                         <!-- / .tab-pane -->
                         <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                </div>
            </div>
        </div>
  
    </div> <!-- / #content-wrapper -->


@section('scripts')
    <script type="text/javascript">
    init.push(function () {
        $('#profile-tabs').tabdrop();

        $("#leave-comment-form").expandingInput({
            target: 'textarea',
            hidden_content: '> div',
            placeholder: 'Write message',
            onAfterExpand: function () {
                $('#leave-comment-form textarea').attr('rows', '3').autosize();
            }
        });
    })
    window.PixelAdmin.start(init);
</script>
<script>
$(function() {

// Colors
                        $('#switchers-colors-default > input').switcher();
                        $('#switchers-colors-square > input').switcher({ theme: 'square' });
                        $('#switchers-colors-modern > input').switcher({ theme: 'modern' });



    $('#switcher-example-2').switcher({
                            theme: 'square',
                            on_state_content: '<span class="fa fa-check"></span>',
                            off_state_content: '<span class="fa fa-times"></span>'
                        });


// Multiselect
                        $("#jquery-select2-multiple").select2({
                            placeholder: "assign roles"
                        });

});

</script>


@endsection