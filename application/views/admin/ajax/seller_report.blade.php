
		<div class="panel colourable" style="width:55%;">
					<div class="panel-heading">
						<span class="panel-title">Seller: <u>{{ $seller->profile()->bill()->business_name }} ({{ $seller->profile()->email }})</u></span>
						<ul class="nav nav-tabs nav-tabs-xs">
							<li class="active">
								<a href="#uidemo-tabs-default-demo-home" data-toggle="tab">General View</a>
							</li>
							<li>
								<a href="#uidemo-tabs-default-demo-profile" data-toggle="tab">Financial detailed report</a>
							</li>
						
						</ul> <!-- / .nav -->
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

			<div class="tab-content">
							<div class="tab-pane fade active in" id="uidemo-tabs-default-demo-home">

<ul class="list-group">
										<li class="list-group-item">
											<span style='float:right;'>{{ $seller->profile()->created_at }}</span>
											Account Creation Date
										</li>

							<li class="list-group-item" >
											<span style='float:right;'>
											@if (!$seller->profile()->last_login())
												Never
											@else
												{{ $seller->profile()->last_login() }}
											@endif

											</span>
											Last login at
										</li>

										<li class="list-group-item">
											<span class="badge badge-info">{{ $seller->deals()->count() }}</span>
											Total Created Deals
										</li>
										<li class="list-group-item">
											<span class="badge badge-success">{{ $seller->activeDeals()->count() }}</span>
											Active Deals
										</li>
										<li class="list-group-item">
											<span class="badge badge-warning">{{ $seller->awaitingForApproval()->count() }}</span>
											Awaiting for approval deals
										</li>
										<li class="list-group-item" style="background-color:#f1f1f1;">
											<span style='float:right;font-weight:bold;'>${{ $seller->earned() }}</span>
											Current earnings	
										</li>
									</ul>




							</div> <!-- / .tab-pane -->
							<div class="tab-pane fade" id="uidemo-tabs-default-demo-profile">


								<!-- Light table -->
								<div class="table-light">
									<div class="table-header">
										<div class="table-caption">
											Per Order Sales
										</div>
									</div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Order #</th>
												<th>Selled Items</th>
												<th>Seller Earnings</th>
												<th>Seller Paid</th>
											</tr>
										</thead>
										<tbody>
										@if ($seller->perOrderSales())
										@foreach ($seller->perOrderSales() as $record)
											<tr>
												<td>{{ $record->order_id }}</td>
												<td>{{ $record->order->products()->count() }}</td>
												<td>${{ $record->SellerDataCollection->perOrderEarning($record->order_id) }}</td>
												<td>
												@if ($record->SellerDataCollection->sellerOrderInvoice($record->order_id)->isPaid())
													
													<i class="fa fa-check-square"></i>
												@else
													<i class="fa fa-minus-circle"></i>

												@endif
												</td>
											</tr>
										@endforeach

										@else
											<tr>
											<td colspan="4">No data found</td>
											</tr>


										@endif
									
										
										</tbody>
									</table>
									<div class="table-footer">
										For this seller has been paid ${{ $seller->paid() }} and ${{ $seller->unpaid() }} waiting to be paid.
									</div>
								</div>
								<!-- / Light table -->


			<div class="table-light">
									<div class="table-header">
										<div class="table-caption">
											Generated sales invoices.
										</div>
									</div>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Invoice #</th>
												<th>Status</th>
												<th>Amount</th>
												<th>Order</th>
												<th>Date</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($seller->invoices()->get() as $invoice)
											<tr>
												<td>{{ $invoice->id }}</td>
												<td>{{ $invoice->status() }}</td>
												<td>${{ $invoice->amount() }}</td>
												<td>{{ $invoice->order_id }}</td>
												<td>{{ $invoice->created_at }}</td>
											</tr>
											@endforeach
										
										</tbody>
									</table>
								
								</div>

							</div> <!-- / .tab-pane -->
						
						
						</div> <!-- / .tab-content -->



					</div>
				</div> <!-- / .panel -->
