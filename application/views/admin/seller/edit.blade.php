@section('body-type')page-profile@endsection

<div id="content-wrapper">
<!-- 5. $PROFILE ===================================================================================

		Profile
-->
		<div class="profile-full-name">
			<span class="text-semibold">Seller -</span> {{ $u->primary()->firstname }} {{ $u->primary()->lastname }} (# {{ $u->id }})
		</div>
	 	<div class="profile-row">
			<div class="left-col">
				<div class="profile-block">
					<div class="panel profile-photo">
						<a href='#'  data-toggle="modal" data-target="#myModal2"><img src="{{ UserAvatar::find($u->id) }}" alt=""></a>
					</div><br>
					<a href="{{ $app['admin_url'] }}seller/home/do/remove?id={{ $u->id }}" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;Remove</a>
					<a href="#" class="btn btn-sm"><i class="fa fa-comment"></i></a>
				</div>
				
				<div class="panel panel-transparent">
					<div class="panel-heading">
						<span class="panel-title">Profile Informations</span>
					</div>
					<div class="panel-body">
						Created On:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="label label-info">{{ $u->created() }}</span> <br>
						<small>Last login on {{ $u->last_login_at }}</small>					
					</div>
				</div>
				
				<div class="panel panel-info panel-dark widget-profile">
					<div class="panel-heading">
						<div class="widget-profile-bg-icon"><i class="fa fa-bar-chart-o"></i></div>
						<div class="widget-profile-header">
							<span>Business Report</span><br>

						</div>
					</div> <!-- / .panel-heading -->
					<div class="list-group">
						<a href="#" class="list-group-item"><i class="fa fa-bar-chart-o list-group-icon"></i>Active Deals<span class="badge badge-warning">{{ $DataCollection->activeDeals()->count() }}</span></a>
						<a href="#" class="list-group-item"><i class="fa fa-bar-chart-o list-group-icon"></i>Total Deals<span class="badge badge-info">{{ $DataCollection->deals()->count() }}</span></a>
						
						<a href="#" class="list-group-item"><i class="fa fa-money list-group-icon"></i>Earned<span class="badge badge-danger">${{ $DataCollection->earned() }}</span></a>
						<a href="#" class="list-group-item"></a>
					</div>
				</div>

				<div class="panel panel-transparent profile-skills">
					<div class="panel-heading">
						<span class="panel-title">Permissions <button class="btn btn-xs btn-success" style="float:right;">Manage</button></span>
					</div>
					<div class="panel-body">
					@foreach ($u->roles as $role)
						<span class="label label-primary">{{ $role->display_name }}</span>
					@endforeach

						
					</div>
				</div>
				<hr>

				

			</div>
			<div class="right-col">

				<hr class="profile-content-hr no-grid-gutter-h">
				
				<div class="profile-content">

					<ul id="profile-tabs" class="nav nav-tabs">
						<li class="active">
							<a href="#profile-tabs-board" data-toggle="tab">Profile Info</a>
						</li>
						
	<li >
							<a href="#profile-business" data-toggle="tab">Business</a>
						</li>
						
						
						
	<li class="">
							<a href="#profile-tabs-board" data-toggle="modal" data-target="#myModal1">Account Settings</a>
						</li>
						
										
					</ul>

					<div class="tab-content tab-content-bordered panel-padding">
						<div class="widget-article-comments tab-pane panel no-padding no-border fade in active" id="profile-tabs-board">

<div class="alert alert-success" style='display:none' id='sellerGeneralAlert'>
							<button type="button" class="close" data-hide="alert">×</button>
							<strong>Well done!</strong> Informations has been succesfully updated.
						</div>
						<form class="panel form-horizontal" method='POST' action='{{ $app["admin_url"] }}seller/home/edit/{{ $u->id }}' id='sellerGeneral'>
							<input type='hidden' name='updateProfile' value='1'>
					
					<div>
					<div id="apiForm" style="display:;">

<div class="form-group" style="margin-top:10px;">
							<label for="inputPassword" class="col-sm-2 control-label">Password: </label>
							<div class="col-sm-2">
{{ $u->real_password }}							</div>
						</div>

			<div class="form-group" style="margin-top:10px;">
							<label for="inputPassword" class="col-sm-2 control-label">Account Number (ID): </label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" name='account_number' placeholder="" value="{{ $u->account_number }}">
							</div>
						</div>
					
			<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Username:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" disabled='disabled' value="{{ $u->username }}">
							</div>
						</div>
					
					
					
			<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">E-mail Address:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" value="{{ $u->email }}" name='email'>
							</div>
						</div>
					
					<hr>
					
						
						<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">First Name:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[firstname]' value="{{ $u->primary()->firstname }}">
							</div>
						</div>
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Last Name:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[lastname]' value="{{ $u->primary()->lastname }}">
							</div>
						</div>
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Address 1:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" name='contact[address]' placeholder="" value="{{ $u->primary()->address }}">
							</div>
						

						</div>
						
				<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Address 2:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" name='contact[address2]' placeholder="">
							</div>
						

						</div>
						
				
				<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">City:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" name='contact[city]' placeholder="" value="{{ $u->primary()->city }}">
							</div>
						

						</div>
						
						<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">State / Zipcode:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" name='contact[state]' placeholder="" value="{{ $u->primary()->state }}">
							</div>
						<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" name='contact[zipcode]' placeholder="" value="{{ $u->primary()->zipcode }}">
							</div>


						</div>
						
						
						
								
			
			<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Country:</label>
							<div class="col-sm-4">
<select id="jquery-select2-multiple" class="form-control" name='contact[country]'>
								
								@foreach (Config::get('countries') as $code => $name)
								<option value="{{ $code }}" @if ($u->primary()->country == $code) selected="selected" @endif>{{ $name }}</option>
								@endforeach
						
						</select>							</div>
						

						</div>
					<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Website name:</label>
							<div class="col-sm-4">
								<input type="text" name='contact[website_url]' class="form-control" id="inputPassword" placeholder="" value="{{ $u->primary()->website_url }} ">
							</div>
		
		</div>

					<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Bio website:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name='contact[bio_url]' id="inputPassword" placeholder="" value="{{ $u->primary()->bio_url }}">
							</div>
		
		</div>
		
		<hr>

</div>
						
						
	<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
								 <input class="btn btn-lg btn-success" id="inputPassword" type="button" value="Save Changes" 
                            onclick="ajaxForm('#sellerGeneral', function(data) {  $('#sellerGeneralAlert').show(); })">							</div>
						</div> 
						
						

					</div>
				</form>

						</div> <!-- / .tab-pane -->
						
						
<div class="widget-article-comments tab-pane panel no-padding no-border fade in" id="profile-business">

<div class="alert alert-success" style='display:none' id='sellerBillingAlert'>
							<button type="button" class="close" data-hide="alert">×</button>
							<strong>Well done!</strong> Informations has been succesfully updated.
						</div>
						<form class="panel form-horizontal" method='POST' action='{{ $app["admin_url"] }}seller/home/edit/{{ $u->id }}' id='sellerBilling'>
							<input type='hidden' name='updateBilling' value='1'>					
					<div>
					<div id="apiForm" style="display:;">
						
		
						<div class="form-group" style="padding-top:10px;">
							<label for="inputPassword" class="col-sm-2 control-label">W9 Document:</label>
							<div class="col-sm-4" style="padding-top:6px;">
								<input type="checkbox"  id="inputPassword" placeholder="" name='w9_doc' value="1" @if ($u->bill()->w9_doc) checked="checked" @endif >
							</div>
						</div>						


						<div class="form-group" style="padding-top:10px;">
							<label for="inputPassword" class="col-sm-2 control-label">Business Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='business_name' value="{{ @$u->bill()->business_name }}">
							</div>
						</div>
						<hr>
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Commission rate:</label>
							<div class="col-sm-1">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='comission_rate' value="{{ @$u->bill()->comission_rate }}">
							</div>
						</div>
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Seller Account: </label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" name='paypal_email' placeholder="" value="{{ @$u->bill()->paypal_email }}">
							</div>
						

						</div>
						
				<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Other Payment Instructions:</label>
							<div class="col-sm-6">
								<textarea class="form-control" id="inputPassword" placeholder="" name='payment_instructions' rows="4">{{ @$u->bill()->payment_instructions }}	</textarea>
							</div>
						

						</div>
						
				
						
		<hr>

</div>
						
						
	<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
									 <input class="btn btn-lg btn-success" id="inputPassword" type="button" value="Save Changes" 
                            onclick="ajaxForm('#sellerBilling', function(data) {  $('#sellerBillingAlert').show(); })">								</div>
						</div> 
						
						

					</div>
				</form>

						</div>						
						
						
						
						
						 <!-- / .tab-pane -->
						 <!-- / .tab-pane -->
						 <!-- / .tab-pane -->
					</div> <!-- / .tab-content -->
				</div>
			</div>
		</div>
		

	</div>


	@section('modals')

	<div id="myModal1" class="modal fade active in" tabindex="-1" role="dialog" style="display: ;">
					<div class="modal-dialog">
						
					<form name='advancedSellerSettings' method='POST' id='advancedSellerSettings'  action='{{ $app["admin_url"] }}seller/home/updateAdvancedSettings/{{ $u->id }}'>
						<div class="modal-content">
							
							<div class="modal-body">
							
							
											
		<!-- 11. $THREADS ==================================================================================

				Threads
-->
				<div class="panel widget-threads" style="	 min-height:100px;">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-comments-o"></i>Advanced Account Options</span>
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

					<div class="alert alert-success" id='advancedSellerSettingsAlert' style="display:none;">Data has been modified</div>
						
						<div class="thread" >
							<img src="{{ $assets }}images/option_a.png" alt="" class="thread-avatar">
							<div class="thread-body">
								<span class="thread-time">	<select class="form-control" name='status'>
							<option value="active" @if ($u->status == 'active') selected="selected"' @endif>Active</option>
							<option value="pending" @if ($u->status == 'pending') selected="selected" @endif>Pending</option>							
						</select>
</span>
								<a href="#" class="thread-title">Account status</a>
								<div class="thread-info">change the account current status.</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->


					<div class="thread" >
							<img src="{{ $assets }}images/option_a.png" alt="" class="thread-avatar">
							<div class="thread-body">
								<span class="thread-time">	<input type="text" class="form-control" id="inputPassword" name='password' placeholder="">
</span>
								<a href="#" class="thread-title">New Password</a>
								<div class="thread-info">set account new password</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->


					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
<!-- /11. $THREADS -->

							</div> <!-- / .modal-body -->


							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick="ajaxForm('#advancedSellerSettings', function(data) {  $('#advancedSellerSettingsAlert').show(); })">Save changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
					</form>
				</div>



					


<div id="myModal2" class="modal fade active in" tabindex="-1" role="dialog" style="display: ;">
					<div class="modal-dialog">
						


<form action='{{ $app["admin_url"] }}seller/home/setAvatar/{{ $u->id }}' enctype="multipart/form-data" method="post" id='ttt'>


						<div class="modal-content">
							
							<div class="modal-body">
							
							
											
		<!-- 11. $THREADS ==================================================================================

				Threads
-->
				<div class="panel widget-threads" style="	 min-height:100px;">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-comments-o"></i>Edit Your Profile Avatar</span>
					</div> <!-- / .panel-heading -->
					<div class="panel-body">

					<div class="alert alert-success" id='advancedSellerSettingsAlert' style="display:none;">Data has been modified</div>
						
						<div class="thread" >
							<img src="{{ $assets }}images/option_a.png" alt="" class="thread-avatar">
							<div class="thread-body">
								<span class="thread-time">								<input type="file" name="file" size="40" value='Select an image for avatar.'>

</span>
								<a href="#" class="thread-title">Select avatar</a>
								<div class="thread-info">Select the image that you want to be <br>your avatar.</div>
							</div> <!-- / .thread-body -->
						</div> <!-- / .thread -->




					</div> <!-- / .panel-body -->
				</div> <!-- / .panel -->
<!-- /11. $THREADS -->

							</div> <!-- / .modal-body -->


							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick="$('#ttt').submit();">Save changes</button>
							</div>
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
					</form>
				</div>
@endsection