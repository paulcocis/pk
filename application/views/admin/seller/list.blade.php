<div id="content-wrapper">
		<ul class="breadcrumb breadcrumb-page">
			<div class="breadcrumb-label text-light-gray">You are here: </div>
			<li><a href="#">Home</a></li>
			<li class="active"><a href="#">Dashboard</a></li>
		</ul>
		<div class="page-header">
			
			<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create New Seller</button> 
			<br><br>
		
		@section ('scripts')
		<script>
					init.push(function () {
						$('#jq-datatables-example').dataTable();
						$('#jq-datatables-example_wrapper .table-caption').text('Manage Seller List');
						$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});


$("#exportData").click(function(event){
  event.preventDefault();

  var searchIDs = [];

  $("#eetable input:checkbox:checked").map(function(){
    searchIDs.push($(this).val());
  });

  console.log(searchIDs);

	$.ajax({
		type: "POST",
		url: "{{ $app['admin_url'] }}seller/home/do/export",
		data: {'ids': searchIDs},
		success: function(data)
		{
		}

	});


});


		</script>
		@endsection


@if (Session::has('ActionMessage'))
 <div class="alert alert-success">
 		{{ Session::get('ActionMessage', 'No data on action messages.') }}
 </div>
@endif

		
		
<div class="table-default" id="eetable">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
								<thead>
									<tr>
										<th></th>
										<th>Business</th>
										<th>Website</th>
										<th>Contact Name</th>
										<th>Comission</th>
										<th>E-mail</th>
										<th>Paypal Account</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody class="t">
									

									@if ( count(Seller::forge()->retrieve(Input::get('status', NULL))) == 0 )
									


									@else
									

									@foreach(Seller::forge()->retrieve(Input::get('status', NULL)) as $row) 


									<tr>
										<td> <input type="checkbox" value='{{ $row->id }}'></td>
										<td>{{ @$row->bill()->business_name }}</td>
										<td>{{ @$row->primary()->website_url }} </td>
										<td>{{ @$row->primary()->firstname }} {{ @$row->primary()->lastname }}</td>
										<td class="center"> {{ @$row->bill()->comission_rate }}</td>
										<td class="center"> {{ $row->email }} </td>
										<td class="center"> {{ @$row->bill()->paypal_email }}</td>
										<td><a href="{{ $app['admin_url'] }}seller/home/edit/{{ $row->id }}">Edit Seller</a>

										@if ($row->status == 'pending')
										| <a href="{{ $app['admin_url'] }}seller/home/do/approve/{{ $row->id }}">Approve</a>
										@endif


										| <a href="{{ $app['admin_url'] }}seller/home/do/remove?id={{ $row->id }}">Delete</a>

										</td>
									</tr>
									@endforeach

									@endif


		</tbody>
							</table>
		
		<br><br>
		With selected: 
		<button class="btn btn-success btn-sm" id="exportData">Export</button>&nbsp;<button class="btn btn-success btn-sm">Delete</button>

		<br><br>
		
				<script>
					init.push(function () {
						$('.ui-wizard-example').pixelWizard({
							onChange: function () {
								console.log('Current step: ' + this.currentStep());
							},
							onFinish: function () {
								// Disable changing step. To enable changing step just call this.unfreeze()
								//this.freeze();
								console.log('Wizard is freezed');
								console.log('Finished!');

								//alert($('#myModal').('input').serialize());

								 InputData = $('#myModal :input').serialize();

								send(InputData);

							



							}
						});

						$('.wizard-next-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('nextStep');
						});

						$('.wizard-prev-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('prevStep');
						});

						$('.wizard-go-to-step-btn').click(function () {
							$(this).parents('.ui-wizard-example').pixelWizard('setCurrentStep', 2);
						});
					});

				
				function send(data)
				{
					$.ajax({
		   			type: "POST",
					url: "{{ $app['admin_url'] }}seller/home/index",
					data: data,
					success: function(data)
					{
						if (data == 'OK')
						{
							window.location = "{{ $app['admin_url'] }}seller/home/index";
						}
						else
						{
							$('#cError').html(data).show();

						}
					}


					});
				}

				</script>

		
		
				<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

		<!-- Page wide horizontal line -->

		<!-- Page wide horizontal line -->

	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div>


@section('modals')
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
<input type='hidden' name='addNewSeller' value='TRUE'>
					<div class="modal-dialog">
						<div class="modal-content" style="width:700px;">
							<div class="alert alert-warning" style="display:none" id='cError'>
							
							</div>

							<div class="wizard ui-wizard-example">

							<div class="wizard-wrapper">
								<ul class="wizard-steps">
									<li data-target="#wizard-example-step1" >
										<span class="wizard-step-number">1</span>
										<span class="wizard-step-caption">
											Step 1
											<span class="wizard-step-description">General Account Informations</span>
										</span>
									</li
									><li data-target="#wizard-example-step2"> <!-- ! Remove space between elements by dropping close angle -->
										<span class="wizard-step-number">2</span>
										<span class="wizard-step-caption">
											Step 2
											<span class="wizard-step-description">Business Setup</span>
										</span>
									</li
									><li data-target="#wizard-example-step3"> <!-- ! Remove space between elements by dropping close angle -->
										<span class="wizard-step-number">3</span>
										<span class="wizard-step-caption">
											Step 3
											<span class="wizard-step-description">Other Account Settings</span>
										</span>
									</li
									>
								</ul> <!-- / .wizard-steps -->
							</div> <!-- / .wizard-wrapper -->
							<div class="wizard-content panel">
								<div class="wizard-pane" id="wizard-example-step1">
								
								<div id="apiForm" style="display:;">
						
						<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">First Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[firstname]'>
							</div>
						</div>
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Last Name:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[lastname]'>
							</div>
						</div>
						
		<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Address 1:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[address]'>
							</div>
						

						</div>
						
				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Address 2:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[address2]'>
							</div>
						

						</div>
						
				
				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">City:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[city]'>
							</div>
						

						</div>
						
						<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">State / Zipcode:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[state]'>
							</div>
						<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[zipcode]'>
							</div>


						</div>
						
						
						
								
			
			<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Country:</label>
							<div class="col-sm-6">
<select id="jquery-select2-multiple" class="form-control" name='contact[country]'>
							@foreach (Config::get('countries') as $code => $name)
								<option value="{{ $code }}">{{ $name }}</option>
								@endforeach
						
						</select>							</div>
						

						</div>
					<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Website name:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[website_url]'>
							</div>
		
		</div>

					<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Bio website:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[bio_url]'>
							</div>
		
		</div>
		
		<hr>
					<button class="btn btn-primary wizard-next-step-btn">Continue to step 2</button>

</div>
								
								
																	</div> <!-- / .wizard-pane -->
								<div class="wizard-pane" id="wizard-example-step2" style="display: none;">
									<div id="apiForm" style="display:;">
									
	
									
						<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Business Name:</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='bill[business_name]' >
							</div>
						</div>
						<hr>
		<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Commission rate:</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='bill[comission_rate]'>
							</div>
						

						</div>
						
				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Paypal Account (email):</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='bill[paypal_email]'>
							</div>
						

						</div>
						
				
				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Other Payment Instructions:</label>
							<div class="col-sm-6">
								<textarea  class="form-control" id="inputPassword" placeholder="" name='bill[payment_instructions]'></textarea>
							</div>
						

						</div>
					
		
		<hr>

</div>

									<button class="btn wizard-prev-step-btn">Previous</button>
									<button class="btn btn-primary wizard-next-step-btn">Next Step</button>
								</div> <!-- / .wizard-pane -->
								<div class="wizard-pane" id="wizard-example-step3" style="display: none;">
									
									
									<div id="apiForm" style="display:;">
									
	
				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Account Number (ID) :</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='account_number' >
							</div>
						</div>

				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Username :</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='username' >
							</div>
						</div>

	
			<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Account Password :</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='password' >
							</div>
						</div>
	
		<hr>
									
						<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">E-mail Address :</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='email' >
							</div>
						</div>
						
					<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Phone Number :</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="inputPassword" placeholder="" name='contact[phone]' >
							</div>
						</div>
						
												
				
				<div class="form-group">
							<label for="inputPassword" class="col-sm-4 control-label">Account Notes:</label>
							<div class="col-sm-6">
								<textarea  class="form-control" id="inputPassword" placeholder="" rows="5"></textarea>
							</div>
						

						</div>
					
		
		<hr>

</div>
																		
									<button class="btn wizard-prev-step-btn">Prev</button>
									<button class="btn btn-success wizard-next-step-btn">Create Account</button>
								</div> <!-- / .wizard-pane -->
								 <!-- / .wizard-pane -->
							</div> <!-- / .wizard-content -->
						</div> <!-- / .wizard -->
						</div> <!-- / .modal-content -->
					</div> <!-- / .modal-dialog -->
				</div>
@endsection