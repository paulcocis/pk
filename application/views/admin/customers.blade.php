<div id="content-wrapper">
<ul class="breadcrumb breadcrumb-page">
<div class="breadcrumb-label text-light-gray">You are here: </div>
<li><a href="#">Home</a></li>
<li class="active"><a href="#">Dashboard</a></li>
</ul>
<div class="page-header">

<!-- 7. $NO_LABEL_FORM =============================================================================



No label form
-->

<style>
.pagination { 
  width:602px;
	margin:5px auto;
	padding:0px;
	text-align:center;
	color:#aaa;
	border:1px solid #ddd; 
	border-radius:5px;
	background:#fcfcfc;
	background-image:-webkit-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-moz-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-o-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-ms-linear-gradient(top, #fcfcfc, #eaeaea);
}
.pagination a {
	padding:7px;
	text-decoration:none; 
	color:#667F99;
}
.pagination li.active a {
	font-weight:bold;
}
.pagination li:not(.disabled) a:hover {
	border:1px solid #ddd; 
	border-radius:5px;
	color:#000;
	padding:10px 6px;
	background:#666;
	background-image:-webkit-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-moz-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-o-linear-gradient(top, #fcfcfc, #eaeaea);
	background-image:-ms-linear-gradient(top, #fcfcfc, #eaeaea);
}

.pagination li
{
	display: inline;
	list-style-type: none;
}

.pagination ul
{
	margin: 0px 0px;
	padding: 0 0px;
}

.pagination  li.disabled a:hover {
       pointer-events: none;
       cursor: default;
}

.pagination  li.disabled a {
       color: #B6B8BB;
}
</style>


<!-- /7. $NO_LABEL_FORM -->

<script>
init.push(function () {
$('#jq-datatables-example').dataTable();
$('#jq-datatables-example_wrapper .table-caption').text('Manage Customers');
$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
});
</script>




<div class="table-primary">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
<thead>
<tr>
<th>#</th>
<th>Customer Name</th>
<th>Email</th>
<th>Country</th>
<th>Customer since:</th>

<th>Actions</th>
</tr>
</thead>
<tbody class="t">

 @if (!Transaction::count())

 @else


	 @foreach($customers->results as $row)
			<tr>
			<td>{{ $row->id }}</td>
			<td>{{ @$row->primary()->firstname }} {{ @$row->primary()->lastname }}</a></td>
			<td>{{ $row->email }}</td>
			<td>{{ @$row->primary()->country }}</td>
			<td>{{ $row->created_at }}</td>

			<td>

			<a href="{{ $app['admin_url'] }}customers/info/{{ $row->id }}">More Info </a>
				

			</td>
		</tr>

	 @endforeach
 @endif

</tbody>
</table>
{{ $customers->links() }}

<br><br>



<!-- / .tab-content -->
</div> <!-- / .page-header -->


<!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

<!-- Page wide horizontal line -->

<!-- Page wide horizontal line -->

</div> <!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div>



<style>
.datepicker { z-index:1051 !important; }


</style>



@section('scripts')
<script>
$(function() {
// Multiselect
$("#jquery-select2-multiple").select2({
placeholder: "assign roles"
});

});


</script>



<script>
$(document).ready(function () {


$('[data-load-remote]').on('click',function(e) {
    e.preventDefault();

    var $this = $(this);
    var remote = $this.data('load-remote');
    if(remote) {
        $($this.data('remote-target')).load(remote, function() {
			$('.sDate').datepicker({
    			format:'yyyy-mm-dd',
    			startDate: '-10y'
			})
        });
    }
});




});
</script>


@endsection




@section('modals')
<div id="tInfo" class="modal fade"  role="dialog" >
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
						
						
						
					</div><!-- / .modal-dialog -->
				</div>

</div>

@endsection