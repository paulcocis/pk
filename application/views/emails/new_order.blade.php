
  Hey {{ $order->user()->primary()->firstname }} {{ $order->user()->primary()->lastname }},
  <br>
  Thank you for placing an order with us.<br>
  Below you can view general informations about your order: <br>

  <br>
  ================================================ <br>
  Order ID: {{ $order->id }} <br>
  Order Date: {{ $order->created_at }} <br>
  Order Cost: ${{ $order->totalCost(TRUE) }} (Items: {{ $order->products()->count() }})  <br>
  ================================================ <br>

  <br>
  @if ($order->status != 'paid') 
   <b> Payment for this order has not been confirmed at this moment. </b><br>
   If you want to pay with direct credit card use the link below: <br>
   <a href='{{ URL::base() }}/payment/credit-card/?order={{ $order->id }}'>{{ URL::base() }}/payment/credit-card/?order={{ $order->id }}</a>
   <br>
  @else
  	<b> Payment for order has been confirmed. </b>
  @endif

  <br>

<hr>
  <h4>Thank you, <br>
      Pinke Promise
  
  <h4>


