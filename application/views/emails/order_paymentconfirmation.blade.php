<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />



<style type="text/css">
.auto-style1 {
	font-size: small;
	font-family:"Comic Sans MS";
	padding:10px;
}
</style>



</head>

<body class="auto-style1">

  <strong>PinkePromise - Thank you for your payment<br />
  
</strong><br>
{{ $Customer->primary()->firstname }} {{ $Customer->primary()->lastname  }}, your payment for order #{{ $Order->id }} has been confirmed, 
  total payment amount ${{ $Order->totalCost() }}.<br />
  <br />
  <br>
  Ordered products will be shipped to address below.<br>
  <em>{{ $ShippingUser->firstname }} {{ $ShippingUser->lastname }} <br>
{{ $ShippingUser->address }} <br>
{{ $ShippingUser->city }} , {{ $ShippingUser->state }}  {{ $ShippingUser->zipcode }}  </em>  <br>
  

   @if ($DownloadableProducts)
   
   <br>
   Below products are delivered digitally (click on download link to get them).<br>
   <br>
   @foreach($DownloadableProducts as $product)
    Name: {{ $product->deal()->name }} <br>
    Download link: {{ $product->deal()->downloadable_link($product->id) }} [<a href='{{ $product->deal()->downloadable_link($product->id) }}'> download </a> ]<br>
    <br>
   @endforeach

  <br><br>
  Please note that the items are available for download directly from your dashboard. <br>

   @endif



  <p>You can login anytime in your dashboard to see the status of your orders.</p>
&nbsp;<br />
  <br />
  <hr />
  <p>Please do not reply to this email.</p>
  <p>--<br />
  best regards<br />
  PinkePromise Team<br />
  </p>
  <p><br />
  </p>



</body>

</html>
