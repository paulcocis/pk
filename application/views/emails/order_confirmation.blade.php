<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />



<style type="text/css">
.auto-style1 {
	font-size: small;
	font-family:"Comic Sans MS";
	padding:10px;
}
.auto-style2 {
	text-align: right;
}
</style>



</head>

<body class="auto-style1">

  <strong>Order Confirmation Email
  
</strong>

<br><br>
{{ $Customer->primary()->firstname }} {{ $Customer->primary()->lastname  }}, thank you for your order.&nbsp;
  <br />
  Your generated order number is: #{{ $Order->id }}, total order cost is ${{ $Order->totalCost() }}.<br />
  <br>
Your order will ship to:<br>
  <em>{{ $ShippingUser->firstname }} {{ $ShippingUser->lastname }} <br>
{{ $ShippingUser->address }} <br>
{{ $ShippingUser->city }} , {{ $ShippingUser->state }}  {{ $ShippingUser->zipcode }}  </em> <br>


  @if ($Order->status != 'paid')
  <br />
  At this moment payment for order is not confirmed.<br />
  If you wish to pay this order with credit card please use the link specified 
  below:<br />

<a href='{{ URL::base() }}/payment/credit-card/?order={{ $Order->id }}'>{{ URL::base() }}/payment/credit-card/?order={{ $Order->id }}</a>
&nbsp;&nbsp;&nbsp; <br />
@endif


  <br />
  <br />

  @foreach ($Products as $product)
  <table style="width: 37%">
	  <tr>
		  <td colspan="3" style="border-bottom:1px solid #e1e1e1;padding-bottom:10px;"><b>{{ $product->name }}</b></td>
	  </tr>
	  <tr>
		  <td colspan="3" ></td>
	  </tr>

	  <tr>
		  <td rowspan="6" style="width: 185px; padding-right:15px;" valign="top">
		  @if ($product->deal()->getPrimaryImage())
		  	<img src="{{ $product->deal()->getPrimaryImage()->thumb_url(200,200) }}">
		  @endif


		  </td>
		  <td class="auto-style2" style="width: 157px">Item Price:</td>
		  <td>${{ $product->amount }}</td>
	  </tr>
	  <tr>
		  <td class="auto-style2" style="height: 17px; width: 157px">Quantity:</td>
		  <td style="height: 17px">{{ $product->quantity }}</td>
	  </tr>
	  <tr>
		  <td class="auto-style2" style="width: 157px">Shipping cost:</td>
		  <td>${{ $product->shipping_amount }}</td>
	  </tr>
	  <tr>
		  <td class="auto-style2" style="width: 157px">
		  <span lang="EN-US" style="font-size:11.0pt;mso-bidi-font-size:
10.0pt;line-height:107%;font-family:Calibri;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:Calibri;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">Tax (if applicable):</span></td>
		  <td>${{ $product->tax }}</td>
	  </tr>
	  <tr>
		  <td class="auto-style2" style="width: 157px">Total Discount:</td>
		  <td>${{ $product->discount_amount }}</td>
	  </tr>
	  <tr>
		  <td class="auto-style2" style="width: 157px">Item Total:</td>
		  <td>${{ $product->cost() }}</td>
	  </tr>
	  </table>
	@endforeach

  <hr />
  <p>Please do not reply to this email.</p>
  <p>--<br />
  best regards<br />
  PinkePromise Team<br />
  </p>
  <p><br />
  </p>



</body>

</html>
