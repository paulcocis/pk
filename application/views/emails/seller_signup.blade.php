<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />



<style type="text/css">
.auto-style1 {
	font-size: small;
	font-family:"Comic Sans MS";
	padding:10px;
}
</style>



</head>

<body class="auto-style1">

  <strong>New Seller sign up<br />
  </strong><br />
  Thank you for registering to sell with pinkEpromise.com. Below is your login 
  information to access your seller account. Please keep this email in a safe 
  place for your reference.<br />
  <br />
  User Name: {{ $Seller->email }} <br />
  Password:  {{ $Seller->real_password }}<br />
  <br />
  We require the following for deals to be submitted:<br />
  <br />
  Product inventory must be on hand before the sale. We do not run sales for 
  inventory that is not in hand.<br />
  <br />
  It is our expectation that all merchandise must be shipped within 3 days of 
  sale. <br />
  <br />
  Each deal must have a minimum sale price of $7.99 <br />
  <br />
  Payments are made on Wednesdays <br />
  We will send 50% of the payment the Wednesday after your deal ends. You will 
  receive an email with directions to submit tracking numbers to us via email.<br />
  Once we receive the tracking numbers, we will send the final 50% payment.<br />
  <br />
  If you have opened a Dwolla Account, please send the account number to Carrie@pinkepromise.com<br />

  <br />
  pinkEpromise.com earns a 25% commission on sales. <br />
  Part of that percentage is donated to The Healing Consciousness Foundation, a 
  breast cancer foundation that provides needed modalities to patients 
  undergoing treatment.<br />
  <br />
  pinkEpromise.com prides itself in customer service. <br />
  Our customers are our number one priority, therefore if you are experiencing 
  any problems with order fulfillment contact us immediately. <br />
  <br />
  Looking forward to working with you!<br />
  Cheers!<br />
&nbsp;<br />
  <br />
  <hr />
  <p>Please do not reply to this email.</p>
  <p>--<br />
  best regards<br />
  PinkePromise Team<br />
  </p>
  <p><br />
  </p>



</body>

</html>
