<div style='color:#333'>
  <h1>Thank you for your payment!</h1>
  <hr>
  {{ $order->user()->primary()->firstname }} {{ $order->user()->primary()->lastname }},

  <br>
  Your order payment has been confirmed.<br>
  We will process shortly your order. Bellow you can view order summary.


  <br>
  ========================================================= <br>
  Order ID: {{ $order->id }} <br>
  Order Date: {{ $order->created_at }} <br>
  Order Cost: ${{ $order->totalCost(TRUE) }} (Items: {{ $order->products()->count() }})  <br>
  ========================================================= <br>

  <br>
  @if ($downloadableProducts)

    <?php $i = 1; ?>

    <div style='color:#333'>Below you can get informations about downloadable items.</div>
    <br>
    @foreach($downloadableProducts as $item)
    <b>{{ $i }}.</b> Name: {{ $item->deal()->name }} <br>
    Download link: {{ $item->deal()->downloadable_link($item->id) }} <br>
    -----------------------------------------------------------------------------------------------------------------------------------<br>

    <?php $i++; ?>

    @endforeach

  @endif

</div>

