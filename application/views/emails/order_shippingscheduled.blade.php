<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />



<style type="text/css">
.auto-style1 {
  font-size: small;
  font-family:"Comic Sans MS";
  padding:10px;
}
</style>



</head>

<body class="auto-style1">

  <strong>Your order has been shipped!<br />
</strong><br />
  {{ $Customer->primary()->firstname }} {{ $Customer->primary()->lastname }} your item has shipped!
  <br>
  You can expect to receive your item within 3-5 working days. <br />
  <br />

  @if (!empty($Order->shipping_message))
  
    {{ $Order->shipping_message }}

  <br><br>
  @endif
 
  <br />
  Order has been shipped to:<br />
  <em>
  {{ $ShippingUser->firstname }} {{ $ShippingUser->lastname }} <br>
  {{ $ShippingUser->address }} <br>
  {{ $ShippingUser->city }} , {{ $ShippingUser->state }}  {{ $ShippingUser->zipcode }}  </em> <br />
  <br />
  <br />
  <br />
  
  <hr />
  
  <p>Please do not reply to this email.</p>
  <p>--<br />
  best regards<br />
  PinkePromise Team<br />
  </p>
  <p><br />
  </p>

</body>

</html>
