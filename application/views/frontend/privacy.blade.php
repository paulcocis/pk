<div class="container fit-on-pc fit-on-tablet fit-on-mobile m-25px-pc-b">
<!-- Row -->
<div class="row ptf-container">

<!-- Page Title -->
<div class="ptf-title">Privacy Policy - <span class="txt-pink">pinkEpromise</span></div>
<!-- /Page Title -->

<!-- Pivacy Data Container -->
<div class="panel bg-white-pink">


<!-- Privacy Body -->
<div class="ptf-body">

<!-- Privacy Group -->
<div class="panel-group">

<!-- General Questions -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseOne">1.Privacy Policy</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseOne" class="panel-collapse collapse in">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; This Privacy Policy governs the manner in which pinkEpromise collects, uses, maintains and discloses information collected from users (each, a "User") of this website ("Site").
<br /><br />
&bull; This privacy policy applies to the Site and all products and services offered by pinkEpromise.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>             
<!-- /Privacy Policy -->

<!-- Personal identification information -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwo">2.Personal Identification Information</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseTwo" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, 
place an order, respond to a survey, fill out a form, and in connection with other activities, services, features or resources we make available on our Site.
<br /><br />
&bull; Users may be asked for, as appropriate, name, email address, mailing address, and credit card information. Users may, however, visit our Site anonymously.
<br /><br />
&bull; We will collect personal identification information from Users only if they voluntarily submit such information to us.
<br /><br />
&bull; Users can always refuse to supply personal identification information, but it may prevent them from engaging in certain Site activities.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /Personal identification information -->

<!-- Non-personal identification information -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThree">3.Non-personal identification information</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseThree" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may 
include the browser name, the Internet Service Provider used to visit the site, and other technical information. 
<br /><br />
&bull; We may also collect other anonymous data, in aggregate, to help us understand how Users interact with the site.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /Non-personal identification information -->

<!-- Web browser cookies -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFour">4.Web browser cookies</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFour" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; Our Site may use "cookies" to enhance the User's experience. 
<br /><br />
&bull; User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. 
<br /><br />
&bull; Users may choose to set their web browser to refuse cookies, or to be alerted when cookies are being sent. If cookies are disabled, some parts of the site may not function correctly.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>           
<!-- /Web browser cookies -->

<!-- How we use collected information -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFive">5.How we use collected information</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFive" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
pinkEpromise collects and uses Users personal information for the following purposes:<br />
&bull; To personalize user experience - We may use information in the aggregate to understand how our Users, as a group, use our services and resources.
<br /><br />
&bull; To improve our Site - We continually strive to improve our website offerings based on the information and feedback we receive from you.
<br /><br />
&bull; To improve customer service - Your information helps us to more effectively respond to your customer service requests and support needs.
<br /><br />
&bull; To process transactions - We may use the information Users provide about themselves when placing an order to provide service to that order. 
We do not share this information with outside parties except to the extent necessary to process the transactions (for example, we will share your name,
address, and sometimes your email address with the seller of a product from whom you have purchased an item so they can ship, or email, the item to you).
<br /><br />
&bull; To administer a content, promotion, survey or other Site feature - We may use information about our Users in order to provide them topics we think they will
find interesting on our website. We may also send Users information they have agreed to receive.
<br /><br />
&bull; To send periodic emails - The email address Users provide for order processing, will only be used to send them information
and updates pertaining to their order. It may also be used to respond to their inquiries, and/or other requests or questions.
If User decides to opt-in to our mailing list, they may receive emails that include company news, updates, related product or
service information, etc. If at any time the User would like to unsubscribe from receiving future emails, unsubscribe instructions
are provided at the bottom of each email. 
<br /><br />
&bull; Additionally, the User may <a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">contact us</a> via our Site.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /How we use collected information -->


<!-- How we protect your information -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSix">6.How we protect your information</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseSix" class="panel-collapse collapse">
<!-- Privacy Text  -->
<div class="panel-body faq-tou-prv-text">
&bull; We adopt appropriate data collection, storage and processing practices and security measures to protect 
against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, 
transaction information and data stored on our Site.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container  -->
</div>           
<!-- /How we protect your information -->

<!-- We do not share your personal information -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSeven">7.We do not share your personal information</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseSeven" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic
information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and
advertisers for the purposes outlined above. 
<br /><br />
&bull; We may use third party service providers to help us operate our business and the Site or administer
activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited
purposes.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container  -->
</div>           
<!-- /We do not share your personal information -->

<!-- Third party websites -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseEight">8.Third party websites</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseEight" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers,
advertisers, sponsors, licensors and other third parties. 
<br /><br />
&bull; We do not control the content or links that appear on other sites and are not responsible for the practices employed by 
websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. 
These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, 
including websites which have a link to our Site, is subject to that website's own terms and policies.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>           
<!-- /Third party websites -->

<!-- Changes to this privacy policy -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseNine">9.Changes to this privacy policy</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseNine" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; pinkEpromise has the discretion to update this privacy policy at any time. When we do, we will update the date at the bottom of this page. 
<br /><br />
&bull; We encourage Users to frequently check this page for any changes to stay informed of how we are helping to protect the personal information we collect.
<br /><br />
&bull; You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /Changes to this privacy policy -->

<!-- Your acceptance of these terms -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTen">10.Your acceptance of these terms</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseTen" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site.
Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of the changes.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>           
<!-- /Your acceptance of these terms -->

<!-- Contacting us -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseEleven">11.Contacting us</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseEleven" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
&bull; If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please 
<a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">contact us</a>.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>       
<!-- /Contacting us -->

</div> 
<!-- Privacy Group -->

<!-- Document Update  -->
<div class="text-center p-10px-b">This document was last updated on November 14, 2011.</div>  
<!-- /Document Update  --> 

</div>
<!-- Privacy Body -->
</div>
<!-- /Pivacy Data Container -->
</div>
<!-- /Row -->
</div>
