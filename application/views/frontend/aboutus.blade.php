<div class="container m-10px-pc-b" align="center">
<!-- Row -->
<div class="row">

<!-- About us container -->
<div class="about-fit-on-pc about-fit-on-tablet about-fit-on-mobile">

<!-- Text Container -->
<div class="about-text-container pull-left txt-left">
<div class="about-title">You won't find a better deal for a better cause anywhere ... we <span class="txt-pink">pinkEpromise</span></div>

<!-- Blair Mimi Container -->
<div class="about-blairmimi-container div-inline-block">

<!-- Pics and text -->
<div class="pull-left about-pink-pic-container"><img src="images/big/about-photo.jpg" class="about-pink-pic"></div>
<div class="about-pink-describe pull-right"><b>"The mission of pinkEpromise is to offer you, our customer and new fiends, boutique style and handmade goods AT A DISCOUNT PRICE while at the same time raising money for The Healing Consciousness Foundation which provides spiritual, mental, physical, and emotional support to courageous women like Blair in their fight agains breast cancer."</b></div>
<!-- /Pics and text -->
</div>
<!-- /Blair Mimi Container -->

<!-- About us text -->
<div >

<!-- First text title -->
<div class="about-body-title">We are positively thrilled that you have stopped by!</div>

<!-- First text -->
<div class="about-body-description">The glowing blonde is Blair. Actually, the blonde is Monika, Blair's wig, and a really good time. Blair is a survivor. She was diagnosed with breast cancer at the age of 33. As Blair, her husband John, and her young sons, Trent and JP struggled to cope with the news and the uncertain future they faced an amazing thing happened. Blair learned about the power of the human spirit as her family, friends, and people she had never met rallied to support her. She faced a long, arduous road that included surgery, chemotherapy and radiation treatments. Fortunately, Blair has endured and is now cancer free. Now, through pinkEpromise, she wants nothing more than to pass along that indelible spirit that comes from staring terrifying uncertainty in the face and persevering.
<br /><br />
The vivacious brunette is Mimi. Mimi is a wife, mother of three, and a true bargain hunter who is known among her friends and by local business owners as a shrewd negotiator. If you ask those who know her best, they will tell you that Mimi can recall precisely what she paid for just about anything she’s purchased, from her home to her produce. Nothing gives her a bigger thrill than a good deal.
<br /><br />
Here is where you benefit. With Blair's spirit and eye for art and Mimi’s eye for a great bargain, these two great friends are on a mission:
</div>

<!-- Second text title -->
<div class="about-body-title">Here's how it works</div>

<!-- First text -->
<div class="about-body-description">We offer you time sensitive deals on unique, artisan products ranging from handcrafted jewelry to decorative home products and everything in between. A portion of your purchase is then donated to The Healing Consciousness Foundation. Your donation helps breast cancer patients pursue holistic treatments offered by HCF including yoga, massage, therapeutic touch, guided imagery, acupuncture, and other therapies that promote overall health and wellness to help manage the physical side effects and great emotional strain that patients face during their battle. According to Blair, the services provided by HCF were incredibly important and comforting to her during treatment.
<br /><br />
So we invite you to browse the site and enjoy the savings we offer you, and know that your purchase will offer a brave woman who is battling this terrible disease an opportunity to receive therapies that will comfort and soothe her while undergoing treatment.
<br /><br />
</div>
</div>
<!-- /About us text -->
</div>
<!-- Text Container -->

<!-- Deal&Facebook -->
<div class="div-right-pc div-right-tablet">
<iframe class="about-fb-frame" src="http://www.facebook.com/plugins/like_box.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FdgdTycPTSRj.js%3Fversion%3D41%23cb%3Df36c0b8311eeedc%26domain%3Dwww.pinkepromise.com%26origin%3Dhttp%253A%252F%252Fwww.pinkepromise.com%252Ff2e2ae04a03179c%26relation%3Dparent.parent&amp;header=false&amp;href=http%3A%2F%2Fwww.facebook.com%2FPinkEpromise&amp;locale=en_US&amp;sdk=joey&amp;stream=false&amp;show_border=false&amp;width=265" title="fb:like_box Facebook Social Plugin"></iframe>
<!-- Deal -->
<div class="about-newsletter">
<div class="about-newsletter-text txt-pink">Sign up today to receive our daily deal notification.</div>
<div><input type="text" class="form-control about-newsletter-input" id="txtInput" placeholder="Enter E-mail"/></div>
<div class="about-newsletter-btn"><button class="btn btn-pEp" type="button">Sign up</button></div>
</div>
</div>
<!-- /Deal&Facebook -->
</div>
<!-- /About us container -->
</div>
<!-- /Row -->
</div>
