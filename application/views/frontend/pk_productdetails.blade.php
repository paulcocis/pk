@section('scripts')
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<script src="{{ $theme_data['asset_url'] }}js/jquery.easing.1.3.js"></script> 
<script src="{{ $theme_data['asset_url'] }}js/jquery.galleryview-3.0-dev.js"></script> 
<script src="{{ $theme_data['asset_url'] }}js/jquery.timers-1.2.js"></script> 

<script type="text/javascript">
	$(function(){
		$('#myGallery').galleryView({
		transition_speed: 0, 		//INT - duration of panel/frame transition (in milliseconds)
		transition_interval: 0, 		//INT - delay between panel/frame transitions (in milliseconds)
		easing: 'swing', 				//STRING - easing method to use for animations (jQuery provides 'swing' or 'linear', more available with jQuery UI or Easing plugin)
		show_panels: true, 				//BOOLEAN - flag to show or hide panel portion of gallery
		show_panel_nav: true, 			//BOOLEAN - flag to show or hide panel navigation buttons
		enable_overlays: false, 		//BOOLEAN - flag to show or hide panel overlays
		
		panel_width: 410, 				//INT - width of gallery panel (in pixels)
		panel_height: 368,		        //INT - height of gallery panel (in pixels)
		panel_animation: 'fade',        //STRING - animation method for panel transitions (crossfade,fade,slide,none)
		panel_scale: 'crop', 			//STRING - cropping option for panel images (crop = scale image and fit to aspect ratio determined by panel_width and panel_height, fit = scale image and preserve original aspect ratio)
		overlay_position: 'bottom', 	//STRING - position of panel overlay (bottom, top)
		pan_images: false,				//BOOLEAN - flag to allow user to grab/drag oversized images within gallery
		pan_style: 'drag',				//STRING - panning method (drag = user clicks and drags image to pan, track = image automatically pans based on mouse position
		pan_smoothness: 0,				//INT - determines smoothness of tracking pan animation (higher number = smoother)
		start_frame: 1, 				//INT - index of panel/frame to show first when gallery loads
		show_filmstrip: true, 			//BOOLEAN - flag to show or hide filmstrip portion of gallery
		show_filmstrip_nav: false, 		//BOOLEAN - flag indicating whether to display navigation buttons
		enable_slideshow: true,		    //BOOLEAN - flag indicating whether to display slideshow play/pause button
		autoplay: false,				//BOOLEAN - flag to start slideshow on gallery load
		show_captions: true, 			//BOOLEAN - flag to show or hide frame captions	
		filmstrip_size: 1, 				//INT - number of frames to show in filmstrip-only gallery
		filmstrip_style: 'scroll', 		//STRING - type of filmstrip to use (scroll = display one line of frames, scroll filmstrip if necessary, showall = display multiple rows of frames if necessary)
		filmstrip_position: 'bottom', 	//STRING - position of filmstrip within gallery (bottom, top, left, right)
		frame_width: 50, 				//INT - width of filmstrip frames (in pixels)
		frame_height: 50, 				//INT - width of filmstrip frames (in pixels)
		frame_opacity: 0.5, 			//FLOAT - transparency of non-active frames (1.0 = opaque, 0.0 = transparent)
		frame_scale: 'crop', 			//STRING - cropping option for filmstrip images (same as above)
		frame_gap: 5, 					//INT - spacing between frames within filmstrip (in pixels)
		show_infobar: true,			    //BOOLEAN - flag to show or hide infobar
		infobar_opacity: 1				//FLOAT - transparency for info bar
		});
	});
</script>
@endsection
@section('js')
@endsection

<div class="container" align="center">

<!-- Pinkestore Media Fit -->
<div class="pinkestore-fit-on-pc pinkestore-fit-on-tablet pinkestore-fit-on-mobile">

<!-- Pinkestore Side Menu -->
<div class="pinkestore-menu-container hidden-xs">
<!-- Pinkestore Logo -->
<div class="pinkestore-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/pEstore_logo.png"></div> 
<!-- Pinkestore Menu Tabs -->
<!-- Pinkestore Menu Blank You -->
<div class="pinkestore-menu-norablue-container">
<div class="pinkestore-menu-norablue-title">Our family of brands...</div>
<div class="pinkestore-menu-norablue-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_related_logo.png"></div>
<div class="pinkestore-menu-norablue-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor.</div>



<div class="pinkestore-menu-norablue-prod">
<div class="pinkestore-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_related.png"></div>
<div class="pinkestore-menu-norablue-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="pinkestore-menu-norablue-prod-price">$29.99</div>
<div class="pinkestore-menu-norablue-prod-button"><a href="#" class="menu-norablue-btn menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>

<div class="pinkestore-menu-norablue-prod">
<div class="pinkestore-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_related.png"></div>
<div class="pinkestore-menu-norablue-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="pinkestore-menu-norablue-prod-price">$29.99</div>
<div class="pinkestore-menu-norablue-prod-button"><a href="#" class="menu-norablue-btn menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>


</div>
</div>
<!-- /Pinkestore Side Menu -->



<!-- Pinkestore Side Products -->

<div class="pinkestore-banner"><img src="{{ $theme_data['asset_url'] }}images/big/pinkestore_banner.png"></div>

<div class="pinkestore-product-container">

<!-- Pinkestore Category -->
<div class="pinkestore-product-title">Featured Products</div>

<!-- Pinkestore Products -->
<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/3188_-Personalized-Canvas-Clutch.jpg"></a></div>
<div class="pinkestore-product-description">Mesh Buckle Bracelet ...... FREE SHIPPING</div>
<div class="pinkestore-product-price">$19.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/3188_-Personalized-Canvas-Clutch.jpg"></a></div>
<div class="pinkestore-product-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="pinkestore-product-price">$12.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/3188_-Personalized-Canvas-Clutch.jpg"></a></div>
<div class="pinkestore-product-description">5 Piece Teardrop Jewelry Set FREE SHIP</div>
<div class="pinkestore-product-price">$29.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/3188_-Personalized-Canvas-Clutch.jpg"></a></div>
<div class="pinkestore-product-description">Mesh Buckle Bracelet ...... FREE SHIPPING</div>
<div class="pinkestore-product-price">$19.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/3188_-Personalized-Canvas-Clutch.jpg"></a></div>
<div class="pinkestore-product-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="pinkestore-product-price">$12.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/3188_-Personalized-Canvas-Clutch.jpg"></a></div>
<div class="pinkestore-product-description">5 Piece Teardrop Jewelry Set FREE SHIP</div>
<div class="pinkestore-product-price">$29.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<!-- Old Code
@if ($deals == NULL) 
<div class='pinkestore-product-alert alert alert-warning'>No active deals found in.</div> 
@else 
<?php $i = 0; ?>
@foreach ($deals as $deal) 
@if ($i == 3)
<?php $i = 0; ?> 
@endif

<div class="pinkestore-product pull-left">
<div class="pinkestore-product-img">
@if ($deal->images())
<a href="{{ URL::base() }}/home/details/{{ $deal->id }}"><img src="{{ $deal->getDealPrimaryImageLocation() }}"></a>
@else
<a href="{{ URL::base() }}/home/details/{{ $deal->id }}"><img src="{{ $images_url }}no_image.jpg"></a>
@endif
</div>
<div class="pinkestore-product-description"> {{ $deal->name }}</div>
<div class="pinkestore-product-price"><b>${{ number_format($deal->sale_price,2); }}</b></div>
<div class="pinkestore-product-button"><a href="{{ URL::base() }}/home/details/{{ $deal->id }}" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<?php $i++; ?>
@endforeach
@endif
-->

</div>
<!-- /Pinkestore Side Products -->
</div>
<!-- Pinkestore Media Fit -->
</div>