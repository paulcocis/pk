<div class="container" align="center">

<!-- Pinkestore Media Fit -->
<div class="pinkestore-fit-on-pc pinkestore-fit-on-tablet pinkestore-fit-on-mobile">

<!-- Pinkestore Side Menu -->
<div class="pinkestore-menu-container hidden-xs">
<!-- Pinkestore Logo -->
<div class="pinkestore-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/pEstore_logo.png"></div> 
<!-- Pinkestore Menu Tabs -->
<!-- Pinkestore Menu Blank You -->
<div class="pinkestore-menu-norablue-container">
<div class="pinkestore-menu-norablue-title">Our family of brands...</div>
<div class="pinkestore-menu-norablue-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_related_logo.png"></div>
<div class="pinkestore-menu-norablue-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor.</div>



<div class="pinkestore-menu-norablue-prod">
<div class="pinkestore-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_1.png"></div>
<div class="pinkestore-menu-norablue-prod-description">Mesh Buckle Bracelet ...... FREE SHIPPING</div>
<div class="pinkestore-menu-norablue-prod-price">$29.99</div>
<div class="pinkestore-menu-norablue-prod-button"><a href="#" class="menu-norablue-btn menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>

<div class="pinkestore-menu-norablue-prod">
<div class="pinkestore-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_2.png"></div>
<div class="pinkestore-menu-norablue-prod-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="pinkestore-menu-norablue-prod-price">$29.99</div>
<div class="pinkestore-menu-norablue-prod-button"><a href="#" class="menu-norablue-btn menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>


</div>
</div>
<!-- /Pinkestore Side Menu -->



<!-- Pinkestore Side Products -->

<div class="pinkestore-banner"><img src="{{ $theme_data['asset_url'] }}images/big/pinkestore_banner.png"></div>

<div class="pinkestore-product-container">

<!-- Pinkestore Category -->
<div class="pinkestore-product-title">Featured Products</div>

<!-- Pinkestore Products -->
<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/4432_Crystal-Head-Wrap.jpg"></a></div>
<div class="pinkestore-product-description">Crystal Head Wrap ...... FREE SHIPPING</div>
<div class="pinkestore-product-price">$19.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/4118_The-Perfect-Black-Duffel-Bag-with-FREE-Monogramming.jpg"></a></div>
<div class="pinkestore-product-description">The Perfect Black Duffel Bag with FREE Monogramming</div>
<div class="pinkestore-product-price">$12.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/4434_Leather-Wallet-with-Credit-Card-Organizer.jpg"></a></div>
<div class="pinkestore-product-description">Leather Wallet with Credit Card Organizer</div>
<div class="pinkestore-product-price">$29.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/4402_Starfish-Teardrop-Earrings-in-Silver-and-Gold.jpg"></a></div>
<div class="pinkestore-product-description">Starfish Teardrop Earrings in Silver and Gold</div>
<div class="pinkestore-product-price">$19.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/4007_REDUCED-----Cable-Twist-Ring-in-Antique-Silver--Gold-and-Rose-Gold---FREE-SHIPPING.jpg"></a></div>
<div class="pinkestore-product-description">REDUCED.....Cable Twist Ring in Antique Silver, Gold and Rose Gold...FREE SHIPPING</div>
<div class="pinkestore-product-price">$12.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#"><img src="{{ $theme_data['asset_url'] }}images/products/4419_Statement-Crystal-Leaf-Necklace-and-Earring-Set.jpg"></a></div>
<div class="pinkestore-product-description">Statement Crystal Leaf Necklace and Earring Set</div>
<div class="pinkestore-product-price">$29.99</div>
<div class="pinkestore-product-button"><a href="#" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>

<!-- Old Code
@if ($deals == NULL) 
<div class='pinkestore-product-alert alert alert-warning'>No active deals found in.</div> 
@else 
<?php $i = 0; ?>
@foreach ($deals as $deal) 
@if ($i == 3)
<?php $i = 0; ?> 
@endif

<div class="pinkestore-product pull-left">
<div class="pinkestore-product-img">
@if ($deal->images())
<a href="{{ URL::base() }}/home/details/{{ $deal->id }}"><img src="{{ $deal->getDealPrimaryImageLocation() }}"></a>
@else
<a href="{{ URL::base() }}/home/details/{{ $deal->id }}"><img src="{{ $images_url }}no_image.jpg"></a>
@endif
</div>
<div class="pinkestore-product-description"> {{ $deal->name }}</div>
<div class="pinkestore-product-price"><b>${{ number_format($deal->sale_price,2); }}</b></div>
<div class="pinkestore-product-button"><a href="{{ URL::base() }}/home/details/{{ $deal->id }}" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<?php $i++; ?>
@endforeach
@endif
-->

</div>
<!-- /Pinkestore Side Products -->
</div>
<!-- Pinkestore Media Fit -->
</div>