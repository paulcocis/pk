<div class="container" align="center">

<div class="index-fit-on-pc index-fit-on-tablet index-fit-on-mobile">

<div class="index-product-title">Featured Items</div>
@if ($deals == NULL)

 <div class='alert alert-warning'>
 		No active deals found.
 </div>

@else


<?php $i = 0; ?>

@foreach ($deals as $deal)

@if ($i == 3)

<?php $i = 0; ?>

@endif


<!-- product div -->
<div class="index-product pull-left">
<div class="index-product-img">

@if ($deal->images())
<img src="{{ $deal->getDealPrimaryImageLocation() }}"><b class="bulmov"><small>

@else
<img src="{{ $images_url }}no_image.jpg"><b class="bulmov"><small>
@endif

{{ number_format($deal->discount_price,0) }}%


</small></b></div>
<div class="index-description">{{ $deal->name }}</div>
<div><span class="index-product-price">${{ number_format($deal->sale_price,2); }}</span></div>
<!-- product button -->
<div class="index-product-button" align="center"><a href="{{ URL::base() }}/home/details/{{ $deal->id }}" class="index-product-btn index-product-btn-xl"><b>Buy Now</b></a></div>
</div>



<?php $i++; ?>

@endforeach

@endif
</div>
</div>
