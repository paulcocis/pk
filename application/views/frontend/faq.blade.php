<!-- 5.$FREQUENTLY_ASKED_QUESTIONS -->
<div class="container fit-on-pc fit-on-tablet fit-on-mobile m-25px-pc-b">
<!-- Row -->
<div class="row ptf-container">

<!-- Page Title -->
<div class="ptf-title">Frequently Asked Questions - <span class="txt-pink">pinkEpromise</span></div>
<!-- /Page Title -->

<!-- Pivacy Data Container -->
<div class="panel bg-white-pink">

<!-- Privacy Body -->
<div class="ptf-body">

<!-- Privacy Group -->
<div class="panel-group">

<!-- General Questions -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseOne">1.General Questions</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseOne" class="panel-collapse collapse in">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
<b>What is pinkEpromise?</b><br />
&bull; pinkEpromise is an online discount boutique, working to connect great sellers, selling fabulous discounted handcrafted items, with buyers
looking for something sweetly unique. 
<br /><br />
<b>One of the featured deals says Expired. Can I still order one?</b><br />
&bull; Sorry, it looks like you found the deal too late to purchase it. Luckily, we link to the seller's store, if they have one, so you can still
go to their store to find another handcrafted item from the same seller. Then be sure to sign up for our daily emails to so that you won't miss another great deal.
<br /><br />
<b>Do you accept exchanges?</b><br />
&bull; There are no exchanges, as we don't keep any stock on hand.
<br /><br />
<b>Is it possible for me to be featured on pinkEpromise?</b><br>
&bull; If you have a unique handcrafted product you would like to sell and are committed to making each of your customer's shopping experience fabulous,
 <a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">send us a note</a> and we'll help you get started. 
<br /><br />
<b>I live outside the United States. Can I purchase from pinkEpromise?</b><br>
&bull; Please check the seller's shipping information to see where they ship.
<br /><br />
<b>I love the featured shop's items, where can I find more?</b><br>
&bull; With all of our sellers we do our best to include a link to their shop or store page, where you can find more of their unique items.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>             
<!-- /General Questions -->

<!-- Sharing Links to Deals -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-example" href="#collapseTwo">2.Sharing Links to Deals</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container  -->
<div id="collapseTwo" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
<b>What is the link sharing feature?</b><br />
&bull; Many of you have been sharing our links on sites such as Facebook, Twitter, Google Plus, and Pinterest. Thanks! Now we would like to reward you for spreading
 the word. When you share the bee.tl link and a friend clicks on it we note that you sent them to our site. We then give you an entry into a drawing for each friend you send our way. 
<br /><br />
<b>One of the featured deals says Expired. Can I still order one?</b><br />
&bull; Sorry, it looks like you found the deal too late to purchase it. Luckily, we link to the seller's store, if they have one, so you can still go to
 their store to find another handcrafted item from the same seller. Then be sure to sign up for our daily emails to so that you won't miss another great deal.
<br /><br />
<b>What is bee.tl?</b><br />
&bull; bee.tl is the service we use to tie the friends you send to pinkEpromise back to you. It generates a link unique to you for each deal you share.
 That way you can share each deal you feel your friends would enjoy.
<br /><br />
<b>I don't see a bee.tl link. Where do I find it?</b><br>
&bull; Because each bee.tl link is unique to you, you need to first sign in to pinkEpromise. Once signed in you will see a unique link on each deal page. 
<br /><br />
<b>How do I share a link?</b><br>
&bull; Sharing is easy! Simply click on the Facebook, Twitter, Google Plus, or Pinterest icons. Or you can copy and paste the bee.tl link into an email or any other social media site.
<br /><br />
<b>What happens when someone clicks on a link I share?</b><br>
&bull; When your friend clicks on your link, they will be taken to the same deal on pinkEpromise from where you shared the link. They can then browse the
 site normally, including creating their own pinkEpromise account, buying a deal, or even sharing their own bee.tl link.
<br /><br />
<b>What information do you share with bee.tl?</b><br>
&bull; We are careful to not share personal information of pinkEpromise users with bee.tl. We are required to share a unique user ID that allows us to tie
 a visit to pinkEpromise from your friends back to you so we can enter you into a drawing. However, the unique user ID is encrypted in a way as to be anonymous to bee.tl.
 Furthermore, bee.tl does not share this detailed information with other sites, although it may publish aggregate data about trends. Your encrypted unique user ID
 and other details will not be published beyond this anonymous aggregate form.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>           
<!-- /Sharing Links to Deals -->

<!-- Your responsibilities and registration obligations -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-example" href="#collapseThree">3.Ordering</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseThree" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
<b>I can't place an order. What can I do?</b><br />
&bull; We are sorry it's not working for you. Please <a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">contact us</a> and we'll get your
 order placed. (We'll also get working on a solution to the problem!) 
<br /><br />
<b>Can I change the size, color, or other options of a completed order?</b><br />
&bull; No, please check your order carefully, before completing it. Once placed, we cannot change the order.
<br /><br />
<b>I just placed an order and the address is wrong. What should I do?</b><br />
&bull; When placing your order on the Review Your Purchase page prior to paying you can verify your shipping address. This address is provided by your PayPal account.
 If you need to change the address, simply edit it on the Review Your Purchase page. If for some reason, you notice the address is incorrect after finishing your
 purchase, please <a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">contact us</a>, so we can notify the seller. If your
 order has not yet shipped they will try to make sure your purchase gets sent to the right address. 
<br /><br />
<b>How do I cancel an order?</b><br>
&bull; Sorry, right now you can't cancel an order. Due to the already discounted nature of the boutique items and the number of sellers we work with, we cannot
 cancel an order. We suggest using the item as a gift for a friend. 
<br /><br />
<b>How can I contact the seller before making a purchase?</b><br>
&bull; Leave a comment on the deal page. Most seller's also have a link to their store on their deal page. You should be able to contact them through their store.
 We encourage all sellers to be available for questions while their deal is posted. Please remember that most of our sellers also have little people to care for
 and may not be able to respond immediately.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>            
<!-- /Your responsibilities and registration obligations -->

<!-- Privacy policy -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-example" href="#collapseFour">4.Shipping</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFour" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
<b>When can I expect to receive my order?</b><br />
&bull; See the seller's notes on shipment details. Please <a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">contact us</a> if your order is taking much longer than expected.
<br /><br />
<b>Do you ship internationally?</b><br />
&bull; Please see each seller's details on whether or not they ship to your location. Please confirm this before placing your order, as we cannot cancel orders.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>                  
<!-- /Privacy policy -->

<!-- Registration and password -->
<div class="panel">
<!-- Privacy Title -->
<div class="p-5px"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-example" href="#collapseFive">5.Returns</a></div>
<!-- /Privacy Title -->
<!-- Privacy Text Container -->
<div id="collapseFive" class="panel-collapse collapse">
<!-- Privacy Text -->
<div class="panel-body faq-tou-prv-text">
<b>Keeping our pinkE's happy!</b><br />
&bull; If you are unhappy with your purchase within 14 days of receipt, you can return it for a refund. Please send the item to: 
<br /><br />
pinkEpromise<br>
318 S Norwood Ave<br>
Newtown, PA 19840<br>
<br /><br />
... and we will process your refund. If your item is damaged or missing, please email us at <a href="mailto:support@pinkEpromise" class="link-pink" target="_blank">support@pinkEpromise</a>
right away so we can make it right. Cheers! 
<br /><br >
More questions? Please <a href="{{ URL::base() }}/contact" class="link-pink" target="_blank">contact us</a>.
</div>
<!-- /Privacy Text -->
</div> 
<!-- /Privacy Text Container -->
</div>             
<!-- /Registration and password -->

</div> 
<!-- Privacy Group -->

<!-- Document Update -->
<div class="text-center p-10px-b">This document was last updated on January 30, 2013.</div>  
<!-- /Document Update --> 

</div>
</div>
</div>
</div>