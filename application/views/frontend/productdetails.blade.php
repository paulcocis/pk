@section('scripts')

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>


<script src="{{ $theme_data['asset_url'] }}js/jquery.easing.1.3.js"></script> 
<script src="{{ $theme_data['asset_url'] }}js/jquery.galleryview-3.0-dev.js"></script> 
<script src="{{ $theme_data['asset_url'] }}js/jquery.timers-1.2.js"></script> 


<script type="text/javascript">
	$(function(){
		$('#myGallery').galleryView({
		transition_speed: 0, 		//INT - duration of panel/frame transition (in milliseconds)
		transition_interval: 0, 		//INT - delay between panel/frame transitions (in milliseconds)
		easing: 'swing', 				//STRING - easing method to use for animations (jQuery provides 'swing' or 'linear', more available with jQuery UI or Easing plugin)
		show_panels: true, 				//BOOLEAN - flag to show or hide panel portion of gallery
		show_panel_nav: true, 			//BOOLEAN - flag to show or hide panel navigation buttons
		enable_overlays: false, 		//BOOLEAN - flag to show or hide panel overlays
		
		panel_width: 410, 				//INT - width of gallery panel (in pixels)
		panel_height: 368,		        //INT - height of gallery panel (in pixels)
		panel_animation: 'fade',        //STRING - animation method for panel transitions (crossfade,fade,slide,none)
		panel_scale: 'crop', 			//STRING - cropping option for panel images (crop = scale image and fit to aspect ratio determined by panel_width and panel_height, fit = scale image and preserve original aspect ratio)
		overlay_position: 'bottom', 	//STRING - position of panel overlay (bottom, top)
		pan_images: false,				//BOOLEAN - flag to allow user to grab/drag oversized images within gallery
		pan_style: 'drag',				//STRING - panning method (drag = user clicks and drags image to pan, track = image automatically pans based on mouse position
		pan_smoothness: 0,				//INT - determines smoothness of tracking pan animation (higher number = smoother)
		start_frame: 1, 				//INT - index of panel/frame to show first when gallery loads
		show_filmstrip: true, 			//BOOLEAN - flag to show or hide filmstrip portion of gallery
		show_filmstrip_nav: false, 		//BOOLEAN - flag indicating whether to display navigation buttons
		enable_slideshow: true,		    //BOOLEAN - flag indicating whether to display slideshow play/pause button
		autoplay: false,				//BOOLEAN - flag to start slideshow on gallery load
		show_captions: true, 			//BOOLEAN - flag to show or hide frame captions	
		filmstrip_size: 1, 				//INT - number of frames to show in filmstrip-only gallery
		filmstrip_style: 'scroll', 		//STRING - type of filmstrip to use (scroll = display one line of frames, scroll filmstrip if necessary, showall = display multiple rows of frames if necessary)
		filmstrip_position: 'bottom', 	//STRING - position of filmstrip within gallery (bottom, top, left, right)
		frame_width: 50, 				//INT - width of filmstrip frames (in pixels)
		frame_height: 50, 				//INT - width of filmstrip frames (in pixels)
		frame_opacity: 0.5, 			//FLOAT - transparency of non-active frames (1.0 = opaque, 0.0 = transparent)
		frame_scale: 'crop', 			//STRING - cropping option for filmstrip images (same as above)
		frame_gap: 5, 					//INT - spacing between frames within filmstrip (in pixels)
		show_infobar: true,			    //BOOLEAN - flag to show or hide infobar
		infobar_opacity: 1				//FLOAT - transparency for info bar
		});
	});
</script>

@endsection

@section('js')

@endsection

<div class="container" align="center">

<!-- Row -->
<div class="row">

<!-- Product Details Container -->
<div class="prod-details-fit-on-pc prod-details-fit-on-tablet prod-details-fit-on-mobile">

<!-- Product Details title -->
<div class="prod-details-title">Product Details - <span class="txt-pink">pinkEpromise</span></div>

<!-- Product Container -->
<div class="prod-details-buy-container">
<!-- Product Details Title -->
<div class="prod-details-title-by txt-pink txt-16px-pc txt-14px-tablet txt-13px-mobile"><b>{{ $deal->name }}</b></div>
<!-- Product Image Container -->

<!-- Jssor Slider Begin -->
<!-- You can move inline styles to css file or css block. -->
<div style="float:left" >
    @if ($deal->images() != NULL)    
        @foreach($deal->images() as $image)
            @if ($image)
            <ul id="myGallery">
            <li><img src="{{ $image->thumb_url() }}" /><li>
            @endif
                     
        @endforeach
        </ul> 
        @else
        <ul id="myGallery">
        <li><img src="{{ $images_url }}no_image.jpg"><li>
        </ul>
        @endif
        
</div>
    <!-- Jssor Slider End -->





                <!-- End Advanced Gallery Html Containers -->

	

<!-- Product Details By -->
<div class="prod-details-by txt-gray"><i><b>by pinkEpromise</b></i></div>

<!-- Product Details Price Hide div -->
<div id="product-price">
<div class="prod-details-our-price"><b>Our Price: ${{ number_format($deal->sale_price,2) }}</b></div>

<!-- Product Details Original Price -->
<div class="prod-details-original-price">Original Price: <b>${{ number_format($deal->original_price,2); }}</b></div>
<!-- Product Details Discount -->
<div class="prod-details-discount">Our Discount: <b>{{ $deal->discount_price }}% off</b></div>
<!-- Product Details Time Left -->
<div class="prod-details-time-left">Time Left for Deal: <b>{{ $deal->getTimeLeft() }}</b></div>
<!-- Product Details Our Price -->

<!-- Product Details Buy Now Button -->
<div class="prod-details-buy-button"><button type="button" class="btn btn-pEp" onclick="buy_product()">Buy Now</button>&nbsp; <a href="index.html" class="btn btn-pEp-default">Back to Store</a></div>

<!-- Product Details Save -->
<div class="prod-details-save-total"><b><i>$974,138.64</i></b></div>
<div class="prod-details-save-pink txt-15px-pc"><b><i>saved on pinkEpromise</i></b></div>
</div>
<!-- /Product Details Price Hide div -->

<!-- Product Details Container Hide Div -->
<div class="prod-details-container" id="product-details">
<div class="prod-details"><b>Details:</b>

@if (empty($deal->description))
 No description available.
@else
	{{ $deal->description }}
@endif



</div>
<div class="prod-details-shipping"><b>Shipping Details:</b> Ships within {{ $deal->ship_within }} days. Shipping is ${{ @number_format($deal->first_item_shipping_cost,2); }} for the first item and ${{ @number_format($deal->aditional_item_shipping_cost,2); }} for each additional item. </div>
</div>
<!-- /Product Details Container Hide Div -->

<!-- Product Details Cart Hide div -->
<form  action="{{ URL::base() }}/cart?action=add" method="post">

<input type='hidden' name='product_id' value='{{ $deal->id }}'>

<div id="product-add-cart" style="display:none">
<!-- Product Details Cart Our Price -->
<div class="prod-details-cart-our-price"><b>Our Price: ${{ number_format($deal->sale_price,2); }}</b></div>
<!-- Product Details Cart Color -->
<div>


@if ($deal->options()->count())


@foreach ($deal->options()->get() as $o)

<!-- Select -->
<select class="prod-details-cart-select" name='options[]'>
<option value="">- Select {{ $o->name }} -</option>

@foreach ($o->options()->get() as $option)
<option value="{{ $option->id }}">{{ $option->name }}</option>

@endforeach

</select>

@endforeach

@endif

<!-- Input -->
<input type="text" class="prod-details-cart-input" placeholder="Quantity" name='quantity'>
<!-- Input -->
<input type="text" class="prod-details-cart-input" name='promotion' placeholder="Promo Code">
</div>

<!-- Product Details Buy Now Button -->
<div class="prod-details-buy-button"><button type="submit" class="btn btn-pEp">Add to Cart</button></div>
</div>
<!-- /Product Details Cart Hide div -->


@if ($deal->require_personalization == 1)
<!-- Product Details Customize Cart Hide Div -->
<div class="prod-details-customize" id="product-customize" style="display:none;">
<div class="prod-details-customize-note">This deal requires that you provide customization or personalization instructions to the seller.<br />
     Please see customization options in the description or associated images.<br />
     If you add multiple items to your basket, make sure that you customize each item before you check out.
</div>

<textarea class="prod-details-textarea" name='comments' placeholder="Comments/Personalization"></textarea> 
</div>
@endif

</form>
<!-- /Product Details Customize Cart Hide Div -->

<!-- Product Details Social Buttons --> 	
<div class="social-buttons" style="width:100%;" >
<!-- Product Details Fb Button --> 		
<div class="fb-like" data-href="http://www.pinkepromise.com/deals/show-deal/id/3201" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
<!-- Product Details Pinterest Button -->  	
<div class="a2a_kit"><a class="a2a_button_pinterest_pin" data-url="http://www.pinterest.com/pin/create/button/?url=http://www.pinkepromise.com/deals/show-deal/id/3201/turquoise-drop-earrings-in-2-styles/" data-media="http://www.pinkepromise.com/img/deals/3201_Turquoise-Drop-Earrings-in-2-Styles.jpg" data-description="Kent Brewster's next stop: Pinterest!"></a> </div>
<!-- Product Details Google Button  -->	
<div class="google_plus"><iframe title="+1" data-gapiattached="true" src="https://apis.google.com/_/+1/fastbutton?usegapi=1&amp;size=medium&amp;origin=http%3A%2F%2Fwww.pinkepromise.com&amp;url=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.ro.Vfm7opacHNE.O%2Fm%3D__features__%2Fam%3DEQ%2Frt%3Dj%2Fd%3D1%2Fz%3Dzcms%2Frs%3DAItRSTPgREc9jyDOza_7Yft-DLG_x-2Brg#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1402653779122&amp;parent=http%3A%2F%2Fwww.pinkepromise.com&amp;pfname=&amp;rpctoken=48088411" name="I0_1402653779122" id="I0_1402653779122" vspace="0" tabindex="0" scrolling="no" marginwidth="0" marginheight="0" hspace="0" frameborder="0" width="100%"></iframe></div>
<!-- Product Details Twiter Button  -->
<div class="twitter_button"><iframe data-twttr-rendered="true" title="Twitter Tweet Button" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-horizontal" src="http://platform.twitter.com/widgets/tweet_button.1401325387.html#_=1402653779109&amp;count=horizontal&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles%2F&amp;size=m&amp;text=Turquoise%20Drop%20Earrings%20in%202%20Styles%20-%20Deals%20by%20pinkEpromise&amp;url=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles" allowtransparency="true" scrolling="no" id="twitter-widget-0" frameborder="0" target="_blank"></iframe></div>
</div>
<!-- /Product Details Social Buttons-->

<!-- Product Details Facebook Comments Buttons -->
<div class="prod-details-facebook">
<div id="fb-root"></div>
<div class="fb-comments" id="facebook-comments" data-href="http://www.pinkepromise.com/deals/show-deal/id/3201/turquoise-drop-earrings-in-2-styles" data-numposts="5" data-mobile="auto-detected"  data-colorscheme="light"></div>
</div>
<!-- /Product Details Facebook Comments Buttons -->
</div>
<!-- /Product Container -->

<!-- Product Related Deals Container -->
<div class="prod-details-related-container">
<!-- Product Related Deals Title -->
<div class="prod-details-related-title"><b>Related Deals</b></div>


@if ($related)
@foreach ($related as $relatedDeal)


	@if ($relatedDeal->id != $deal->id)

	<!-- Product Related Deals -->
	<div class="prod-details-related-prod-title"><b><a href='{{ URL::base() }}/home/details/{{ $relatedDeal->id }}'>{{ $relatedDeal->name }}</a></b></div>
	@if ($relatedDeal->getDealPrimaryImageLocation() != NULL)
	<div class="prod-details-related-prod-image"><a href='{{ URL::base() }}/home/details/{{ $relatedDeal->id }}'><img src="{{ $relatedDeal->getDealPrimaryImageLocation() }}"></a></div>
    @else
    <div class="prod-details-related-prod-image"><a href='{{ URL::base() }}/home/details/{{ $relatedDeal->id }}'><img src="{{ $images_url }}no_image.jpg"></a></div>
	@endif
@endif
@endforeach
@endif

</div>
<!-- /Product Related Deals Container -->


</div>
<!-- /Product Details Container -->
</div>
<!-- /Row -->
</div>
