<!-- 5.$HOW_IT_WORKS&FACEBOOK&DEAL -->
<div class="container m-10px-pc-b" align="center">
<!-- Row -->
<div class="row">
<!-- How It Works Step's -->
<div class="affiliate-fit-on-pc affiliate-fit-on-tablet affiliate-fit-on-mobile">

<!-- Steps -->
<div class="affiliate-text-container pull-left">

<!-- Step 1 -->
<div class="affiliate-steps m-30px-pc-t m-30px-tablet-t">
<div class="affiliate-title">Become A pinkEpromise Affiliate</div>
<div class="affiliate-describe">Make some money while helping us to help others!</div>
</div>

<!-- Sign up Button -->
<div class="affiliate-steps">
<div><a href="http://www.shareasale.com/shareasale.cfm?merchantID=46115" class="btn btn-pEp" target="_blank">Sign up here</a></div>
</div>

<!-- Step 2 -->
<div class="affiliate-steps">
<div class="affiliate-title">Join our affiliate program</div>
<div class="affiliate-describe">Join our affiliate program and we'll pay you a commission on orders placed by vistors you refer to us. <br /> We use a <b>trusted third party network</b> to automatically track visits and pay you commissions.</div>
</div>

<!-- Step 3 -->
<div class="affiliate-steps">
<div class="affiliate-title">Program Highlights</div>
<div class="affiliate-describe">10% Per Sale<br />90 Day Tracking Gap<br />Product Datafeed Updates<br />Auto Deposit Enabled<br />Affiliate Promotions<br />Excellent Conversion!</div>
</div>

<!-- Step 4 -->
<div class="affiliate-steps">
<div class="affiliate-title">The Company</div>
<div class="affiliate-describe">
We shop around so you don't have to! We find exclusive deal prices on the finest handmade and boutique style items for women, kids and the home. Saving and donating all at the same time! You will not find a better deal for a better cause anywhere - we pinkEpromise!
<br /><br />
We offer time sensitive deals on unique, artisan products ranging from handcrafted jewelry to decorative home products and everything in between. A portion of the purchase is then donated to <b>The Healing Consciousness Foundation.</b> Your donation helps breast cancer patients pursue holistic treatments that promote overall health and wellness to help manage the physical side effects and great emotional strain that patients face during their battle.</div>
</div>
</div>
<!-- /Steps -->


<!-- Deal&Facebook -->
<div class="div-right-pc div-right-tablet">
<iframe class="affiliate-fb-frame" src="http://www.facebook.com/plugins/like_box.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FdgdTycPTSRj.js%3Fversion%3D41%23cb%3Df36c0b8311eeedc%26domain%3Dwww.pinkepromise.com%26origin%3Dhttp%253A%252F%252Fwww.pinkepromise.com%252Ff2e2ae04a03179c%26relation%3Dparent.parent&amp;header=false&amp;href=http%3A%2F%2Fwww.facebook.com%2FPinkEpromise&amp;locale=en_US&amp;sdk=joey&amp;stream=false&amp;show_border=false&amp;width=265" title="fb:like_box Facebook Social Plugin"></iframe>
<!-- Deal -->
<div class="affiliate-newsletter">
<div class="affiliate-newsletter-text txt-pink">Sign up today to receive our daily deal notification.</div>
<div><input type="text" class="form-control affiliate-newsletter-input" id="txtInput" placeholder="Enter E-mail"/></div>
<div class="affiliate-newsletter-btn"><button class="btn btn-pEp" type="button">Sign up</button></div>
</div>
</div>
<!-- /Deal&Facebook -->
</div>
<!-- /Affiliate Container -->
</div>
<!-- /Row -->
</div>
<!-- /5.$HOW_IT_WORKS&FACEBOOK&DEAL -->