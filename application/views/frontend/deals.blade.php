<div class="container m-25px-pc-t m-40px-pc-b m-8px-mobile-t m-20px-mobile-b m-30px-tablet-t m-40px-tablet-b">
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2291_Unwritten-Open-Heart-Necklace-and-Earrings.jpg"><b class="bulmov"><small>-57%</small></b></div>
<div class="product-description">Unwritten Open Heart Necklace And Earrings.</div>
<div class="product-price"><b>OUR Price: $14.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>

<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2696_Anthropologie-Inspired-Patriotic--Red--White-and-Blue-Scarf.jpg"><b class="bulmov"><small>-50%</small></b></div>
<div class="product-description">Anthropologie Inspired Patriotic, Red, White And Blue Scarf.</div>
<div class="product-price"><b>OUR Price: $17.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2699_Set-of-2-Extra-Large-Water-Resistant-Bags-for-Summer--QUICK-SHIPPING-.jpg"><b class="bulmov"><small>-57%</small></b></div>
<div class="product-description">Set Of 2 Extra Large Water Resistant Bags For Summer - Quick Shipping.</div>
<div class="product-price"><b>OUR Price: $33.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2818_Cross-Charm-Bracelet.jpg"><b class="bulmov"><small>-40%</small></b></div>
<div class="product-description">Cross Charm Bracelet.</div>
<div class="product-price"><b>OUR Price: $11.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2839_Initial-Wax-Charm-Necklace.jpg"><b class="bulmov"><small>-30%</small></b></div>
<div class="product-description">Initial Wax Charm Necklace.</div>
<div class="product-price"><b>OUR Price: $10.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2841_Owl-Dangles.jpg"><b class="bulmov"><small>-55%</small></b></div>
<div class="product-description">Owl Dangles.</div>
<div class="product-price"><b>OUR Price: $48.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2844_Mother-s-Day-Necklace.jpg"><b class="bulmov"><small>-66%</small></b></div>
<div class="product-description">Mother's Day Necklace.</div>
<div class="product-price"><b>OUR Price: $35.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2845_Anchors-Away-Bangle.jpg"><b class="bulmov"><small>-88%</small></b></div>
<div class="product-description">Anchors Away Bangle.</div>
<div class="product-price"><b>OUR Price: $18.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2868_Round-CZ-Pendant-Necklace-Twinkle-Setting-in-Sterling-Silver---Valentines-Gift-for.jpg"><b class="bulmov"><small>-61%</small></b></div>
<div class="product-description">Round Cz Pendant Necklace Twinkle Setting In Sterling Silver.</div>
<div class="product-price"><b>OUR Price: $14.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2869_Solitaire-CZ-Antique-Boomerang-Pendant-Necklace-Twinkle-Setting-925-Sterling-Silver.jpg"><b class="bulmov"><small>-48%</small></b></div>
<div class="product-description">Solitaire Cz Antique Boomerang Pendant Necklace Twinkle Setting.</div>
<div class="product-price"><b>OUR Price: $39.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2875_Embroidered-Cooler-Bag--.jpg"><b class="bulmov"><small>-99%</small></b></div>
<div class="product-description">Embroidered Cooler Bag!</div>
<div class="product-price"><b>OUR Price: $29.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $theme_data['asset_url'] }}images/products/2878_5000mah-external-battery-for-Apple-iPhone-5S-5C-5-4S----CP525S--Apple-lightning-cable-is-not-include.jpg"><b class="bulmov"><small>-57%</small></b></div>
<div class="product-description">5000mah External Battery For Apple Iphone 5s,5c,5 4s, : Cp525s</div>
<div class="product-price"><b>OUR Price: $19.99</b></div>
<!-- product button -->
<div align="center"><a href="prod-details.html" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
</div>
