<div class="container" align="center">

<!-- Nora Media Fit -->
<div class="norableu-details-fit-on-pc norableu-details-fit-on-tablet norableu-details-fit-on-mobile">
<!-- Nora Side Menu -->
<div class="norableu-menu-container">
<!-- Nora Logo -->
<div class="norableu-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_logo.png"></div> 
<!-- Nora Menu Tabs -->
<div class="norableu-menu-tabs-bg">
<a href="{{ URL::base() }}/nora/thumbs" class="link-norableu-menu"><div class="norableu-menu-tabs">All Products</div></a>
@foreach (DealCategory::get() as $category)
<a href="{{ URL::base() }}/nora/thumbs" class="link-norableu-menu"><div class="norableu-menu-tabs">{{ $category->name }}</div></a> 
@endforeach
</div>
<!-- Nora Menu Blank You -->
<div class="norableu-menu-blank-container">
<div class="norableu-menu-blank-title">Our family of brands</div>
<div class="norableu-menu-blank-logo"><img src="{{ $theme_data['asset_url'] }}images/big/blankyou_logo.png"></div>
<div class="norableu-menu-blank-text">Let us see what loves or pursues customer pain, but because occasionally circumstances, product is pain, so blinded by desire, but in this season.</div>


<div class="norableu-menu-blank-prod">
<div class="norableu-menu-blank-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/blank_related.png"></div>
<div class="norableu-menu-blank-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="norableu-menu-blank-prod-price">$29.99</div>
<div class="norableu-menu-blank-prod-button"><a href="#" class="menu-blank-btn menu-blank-btn-md"><b>Order Now</b></a></div>
</div>

<div class="norableu-menu-blank-prod">
<div class="norableu-menu-blank-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/blank_related_2.png"></div>
<div class="norableu-menu-blank-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="norableu-menu-blank-prod-price">$29.99</div>
<div class="norableu-menu-blank-prod-button"><a href="#" class="menu-blank-btn menu-blank-btn-md"><b>Order Now</b></a></div>
</div>

</div>
</div>
<!-- /Nora Side Menu -->
<div class="hidden-md hidden-lg">
<select class="norableu-mobile-select" onchange="location = this.options[this.selectedIndex].value;">
<option>- Select Category -</option>
<option value="{{ URL::base() }}/nora/thumbs">All Products</option>

@foreach (DealCategory::get() as $category)

<option value="{{ URL::base() }}/nora/thumbs">{{ $category->name }}</option>
@endforeach
</select>
</div>

<!-- Norablue Details Container -->
<div class="norableu-details-container">

<!-- Norableu Category -->
<div class="norableu-details-title">{{ $deal->name }}</div>

<!-- Pinkestore Product gallery -->
<div class="norableu-details-mygallery">
<a id="Zoomer2"  href="{{ $deal->primaryImage()->thumb_url() }}" class="MagicZoomPlus" rel="selectors-effect: fade; selectors-class: Active" title="NoraBleu">
<img src="{{ $deal->primaryImage()->thumb_url() }}"/></a> <br/>
@if ($deal->images() != NULL)   

<div class="norableu-details-mygallery-thumb">

@foreach($deal->images() as $image)

<div class="norableu-details-thumb-pics">
<a href="{{ $image->thumb_url() }}" rel="zoom-id: Zoomer2" rev="{{ $image->thumb_url() }}" class="Selector">
<img src="{{ $image->thumb_url() }}" class="MagicZoomImg" /></a>
</div>
@endforeach

</div>
@endif


</div>
<!-- /Norablue Details Image Container -->

<!-- Norableu Details Order  -->
<div class="norableu-order-container">
<div class="norableu-details-mouseover-mobile"><img src="{{ $theme_data['asset_url'] }}images/icons/zoom_norableu.png">&nbsp; Tap over the image and slide to zoom.</div>

<div class="norableu-signature-details"><img src="{{ $theme_data['asset_url'] }}images/icons/norableu_sig.png"></div>


<!-- Norableu Details Product Container -->
<div class="norableu-details-prod-price-mobile">
<div class="norableu-details-our-price">Starting at: <span class="norableu-details-big-text">${{ $deal->sale_price }}</span></div>
</div>


<!-- Norableu Details Order Quantity -->
<div class="norableu-details-step-nr_1">1</div>
<div class="norableu-details-step-text">Select Quantity</div>
<div class="norableu-details-qunatity">
{{ $deal->quantityDropdown() }}
</div>
<!-- /Norableu Details Order Quantity -->


<!-- Norableu Details -->
<div class="norableu-details-step-nr_2">2</div>
<div class="norableu-details-step-text">Personalize (included)</div>
<div><a href="#" class="link-nora" data-toggle="modal" data-target="#modal-personalize">click here</a></div>

<!-- Modals Personalize -->
<div id="modal-personalize" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
<!-- Modal Container -->
<div class="norableu-modal-container">
<div class="norableu-modal-header">
<button type="button" class="btn-pEp-dark pull-right" data-dismiss="modal">X</button>
<h4 class="norableu-modal-title">Personalize</h4>
</div>
<!-- Modal Body -->
<div class="norableu-modal-body">
<div id="reg-msg"></div>

<div>



<!-- Personalize -->
<div class="form-group">
<input type="text" class="form-control norableu-modal-input" id="personalize1" name="personalize1" placeholder="Disc 1 - 8 characters max">
</div>
<!-- Personalize -->
<div class="form-group">
<input type="text" class="form-control norableu-modal-input" id="personalize2" name="personalize2" placeholder="Disc 2 - 8 characters max">
</div>
<!-- Personalize -->
<div class="form-group">
<input type="text" class="form-control norableu-modal-input" id="personalize3" name="personalize3" placeholder="Disc 3 - 8 characters max">
</div>

<!-- Line Spacer -->
<hr class="panel-wide">

<!-- Register button -->
<div class="norableu-modal-button">
<button type="submit" class="btn btn-pEp">Save</button>
</div>

</div>
<!-- /Modal body -->
<form  action="{{ URL::base() }}/cart?action=add" method="post">

<input type='hidden' name='product_id' value='{{ $deal->id }}'><!-- /Form -->
</div>
<!-- /Modal body -->
</div>
<!-- Modal Container -->
</div> 
<!-- Modals Personalize -->

<!-- /Norableu Details -->

<!-- Norableu Details Order Button -->
<div class="norableu-details-step-nr_3">3</div>
<div class="norableu-details-step-text">Order Product</div>
<div class="norableu-details-button"><button type="submit" class="norableu-btn norableu-btn-xl">Add to Cart</button></div>
<!-- /Norableu Details Order Button  -->
</div>
</form>
<!-- /Norableu Details Order  -->

<!-- Tablet & PC -->
<div class="norableu-details-mouseover"><img src="{{ $theme_data['asset_url'] }}images/icons/zoom_norableu.png">&nbsp; Mouse over image to zoom.</div>
<!-- Mobile -->

<!-- Norableu Details Product Container -->
<div class="norableu-details-prod-price">
<div class="norableu-details-our-price">Starting at: <span class="norableu-details-big-text">${{ $deal->sale_price }}</span></div>
</div>

<div class="norableu-details-description">
{{ $deal->description }}
<br><br>
Shipping Details<br> 
Ships within {{ $deal->ship_within }} days. Shipping is ${{ @number_format($deal->first_item_shipping_cost,2); }} for the first item and ${{ @number_format($deal->aditional_item_shipping_cost,2); }} for each additional item.
</div>


<!-- /Norableu Details Product Container -->
</div>
<!-- Norablue Details Container -->
</div>
<!-- /Norablue Side Products -->
</div>
