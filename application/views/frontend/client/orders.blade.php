
<div class="container" align="center">


@if (!$orderlist)
   <div class='alert alert-warning'>You have no order placed at this moment.</div>

@else

<div class="client-orders-fit-on-pc client-orders-fit-on-tablet client-orders-fit-on-mobile">

<div class="client-orders-title">Order history - <span class="txt-pink">pinkEpromise</span></div>


<!-- Client Profile Select Category-->
<div class="hidden-sm hidden-md hidden-lg">
<select class="client-panel-mobile-menu" onchange="location = this.options[this.selectedIndex].value;">
<option>- My Account -</option>
<option value="{{ URL::base() }}/account">Edit Profile</option>
<option value="{{ URL::base() }}/account/orders">My Orders</option>
<option value="{{ URL::base() }}/login/logout">Log Out</option>
</select>
</div>


<table id="example" class="display" cellspacing="0">
<thead class="client-order-thead">
 <tr>
  <th>Order ID</th>
  <th>Amount</th>
  <th class="hidden-items">Items</th>
  <th>Payment Status</th>
  <th class="hidden-shipped">Shipping</th>
 </tr>
</thead>
<tbody>
@foreach ($orderlist as $order)
 <tr>
  <td><a href='#'>#{{ $order->id }}</a></td>
  <td>${{ $order->totalCost() }}</td>
  <td class="hidden-items">{{ $order->products()->count() }}</td>
  <td>{{ strtoupper($order->status) }}</td>
  <td class="hidden-shipped">{{ strtoupper($order->shipping_status) }}</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<!-- Client Profile Main Container -->
</div>
@endif
