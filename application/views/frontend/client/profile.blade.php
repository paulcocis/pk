<div class="container" align="center">

<div class="client-profile-fit-on-pc client-profile-fit-on-tablet">

<!-- Client Profile Title -->
<div class="client-profile-title">Edit your profile - <span class="txt-pink">pinkEpromise</span></div>

<!-- Client Profile Main Container -->
<div align="center">


<!-- Client Profile Select Category-->
<div class="hidden-sm hidden-md hidden-lg">
<select class="client-panel-mobile-menu" onchange="location = this.options[this.selectedIndex].value;">
<option>- My Account -</option>
<option value="{{ URL::base() }}/account">Edit Profile</option>
<option value="{{ URL::base() }}/account/orders">My Orders</option>
<option value="{{ URL::base() }}/login/logout">Log Out</option>
</select>
</div>


<!-- Client Profile Data Container -->

<!-- Client Profile Data -->
<div class="client-profile-data-container">
<!-- Client Profile Header -->
<hr class="panel-wide">
<!-- Form -->
<form class="form-horizontal" id="registration" >
<!-- First Name -->
<div class="form-group"><input type="text" class="form-control client-profile-input" id="firstname" name="firstname" value="{{ $user->primary()->firstname }}"></div>
<!-- Last Name -->
<div class="form-group"><input type="text" class="form-control client-profile-input" id="lastname" name="lastname" value="{{ $user->primary()->lastname }}"></div>
<!-- Address -->
<div class="form-group"><input type="email" class="form-control client-profile-input" id="address" name="address" value="{{ $user->primary()->address }}"></div>
<!-- City -->
<div class="form-group"><input type="email" class="form-control client-profile-input" id="city" name="city" value="{{ $user->primary()->city }}"></div>
<!-- Zip -->
<div class="form-group"><input type="email" class="form-control client-profile-input" id="zip" name="zip" value="{{ $user->primary()->zipcode }}"></div>
<!-- State -->
<div class="form-group">
<select class="form-control client-profile-input" id="state" name="state">
<option value="AL" label="Alabama">Alabama</option>
<option value="AK" label="Alaska">Alaska</option>
<option value="AZ" label="Arizona">Arizona</option>
<option value="AR" label="Arkansas">Arkansas</option>
<option value="CA" label="California">California</option>
<option value="CO" label="Colorado">Colorado</option>
<option value="CT" label="Connecticut">Connecticut</option>
<option value="DC" label="District Of Columbia">District Of Columbia</option>
<option value="DE" label="Delaware">Delaware</option>
<option value="FL" label="Florida">Florida</option>
<option value="GA" label="Georgia">Georgia</option>
<option value="HI" label="Hawaii">Hawaii</option>
<option value="ID" label="Idaho">Idaho</option>
<option value="IL" label="Illinois">Illinois</option>
<option value="IN" label="Indiana">Indiana</option>
<option value="IA" label="Iowa">Iowa</option>
<option value="KS" label="Kansas">Kansas</option>
<option value="KY" label="Kentucky">Kentucky</option>
<option value="LA" label="Louisiana">Louisiana</option>
<option value="ME" label="Maine">Maine</option>
<option value="MD" label="Maryland">Maryland</option>
<option value="MA" label="Massachusetts">Massachusetts</option>
<option value="MI" label="Michigan">Michigan</option>
<option value="MN" label="Minnesota">Minnesota</option>
<option value="MS" label="Mississippi">Mississippi</option>
<option value="MO" label="Missouri">Missouri</option>
<option value="MT" label="Montana">Montana</option>
<option value="NE" label="Nebraska">Nebraska</option>
<option value="NV" label="Nevada">Nevada</option>
<option value="NH" label="New Hampshire">New Hampshire</option>
<option value="NJ" label="New Jersey" selected="selected">New Jersey</option>
<option value="NM" label="New Mexico">New Mexico</option>
<option value="NY" label="New York">New York</option>
<option value="NC" label="North Carolina">North Carolina</option>
<option value="ND" label="North Dakota">North Dakota</option>
<option value="OH" label="Ohio">Ohio</option>
<option value="OK" label="Oklahoma">Oklahoma</option>
<option value="OR" label="Oregon">Oregon</option>
<option value="PA" label="Pennsylvania">Pennsylvania</option>
<option value="RI" label="Rhode Island">Rhode Island</option>
<option value="SC" label="South Carolina">South Carolina</option>
<option value="SD" label="South Dakota">South Dakota</option>
<option value="TN" label="Tennessee">Tennessee</option>
<option value="TX" label="Texas">Texas</option>
<option value="UT" label="Utah">Utah</option>
<option value="VT" label="Vermont">Vermont</option>
<option value="VA" label="Virginia">Virginia</option>
<option value="WA" label="Washington">Washington</option>
<option value="WV" label="West Virginia">West Virginia</option>
<option value="WI" label="Wisconsin">Wisconsin</option>
<option value="WY" label="Wyoming">Wyoming</option>									
</select>
</div>	
<!-- Line Spacer -->
<hr class="panel-wide">
<!-- Email -->
<div class="form-group"><input type="email" class="form-control client-profile-input" id="email" name="email" value="{{ $user->email }}"></div>
<!-- Password -->
<div class="form-group"><input type="password" class="form-control client-profile-input" id="password" name="password" placeholder="Password"></div>
<!-- Confirm Password -->
<!-- Check Leave Password-->
<div class="client-profile-note"><label>*Leave the password fields blank if you do not want to change your password.</label></div>
<!-- Line Spacer -->
<hr class="panel-wide">
<!-- Register button -->
<div class="client-profile-button"><button type="submit" class="client-profile-btn client-profile-btn-xl">Update Profile</button></div>
<!-- /Client Form Body -->
</form>
<!-- /Form -->
<!-- /Client Panel Body -->
</div>
<!-- /Client Profile Data -->

<!-- Client My Account Navigation -->
<div class="client-profile-menu-container">
<div class="client-profile-menu-header">My Account</div>
<!-- Client My Account Body -->
<div class="client-profile-menu-body">
<!-- Edit Profile -->
<a href="{{ URL::base() }}/account" class="link-white"><div class="client-profile-menu-tabs">Edit Profile</div></a>
<!-- My Orders -->
<a href="{{ URL::base() }}/account/orders" class="link-white"><div class="client-profile-menu-tabs">My Orders</div></a>
<!-- Log Out -->
<a href="{{ URL::base() }}/logout" class="link-white"><div class="client-profile-menu-tabs">Log Out!</div></a>
</div>
<!-- /Client My Account Body -->
</div>
<!-- /Client My Account Navigation -->
</div>
<!-- /Client Profile Data Container -->
</div>
<!-- Client Profile Main Container -->
</div>