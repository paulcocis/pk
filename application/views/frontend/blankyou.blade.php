<div class="container" align="center">

<!-- blankyou Media Fit -->
<div class="blankyou-fit-on-pc blankyou-fit-on-tablet blankyou-fit-on-mobile">

<!-- blankyou Side Menu -->
<div class="blankyou-menu-container hidden-xs">
<!-- blankyou Logo -->
<div class="blankyou-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/blankyou_logo_big.png"></div> 
<!-- blankyou Menu Tabs -->
<div class="blankyou-menu-tabs-bg">

<a href="{{ URL::base() }}/blankyou" class="link-blankyou-menu"><div class="blankyou-menu-tabs">All Products</div></a>

@if (DealCategory::where('type','=','blankyou')->count())

@foreach (DealCategory::where('type','=','blankyou')->get() as $category)
<a href="{{ URL::base() }}/blankyou/index/{{ $category->id }}" class="link-blankyou-menu"><div class="blankyou-menu-tabs">{{ $category->name }}</div></a> 
@endforeach

@endif

</div>


<!-- blankyou Menu Blank You -->
<div class="blankyou-menu-norablue-container">
<div class="blankyou-menu-norablue-title">Our family of brands...</div>
<div class="blankyou-menu-norablue-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_related_logo.png"></div>
<div class="blankyou-menu-norablue-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor.</div>



<div class="blankyou-menu-norablue-prod">
<div class="blankyou-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_1.png"></div>
<div class="blankyou-menu-norablue-prod-description">Mesh Buckle Bracelet ...... FREE SHIPPING</div>
<div class="blankyou-menu-norablue-prod-price">$29.99</div>
<div class="blankyou-menu-norablue-prod-button"><a href="#" class="menu-norablue-btn menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>

<div class="blankyou-menu-norablue-prod">
<div class="blankyou-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_2.png"></div>
<div class="blankyou-menu-norablue-prod-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="blankyou-menu-norablue-prod-price">$29.99</div>
<div class="blankyou-menu-norablue-prod-button"><a href="#" class="menu-norablue-btn menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>


</div>
</div>
<!-- /blankyou Side Menu -->
<div class="hidden-md hidden-lg">
<select class="blankyou-mobile-select" onchange="location = this.options[this.selectedIndex].value;">
<option>- Select Category -</option>
<option value="{{ URL::base() }}/blankyou/thumbs">All Products</option>

@if (DealCategory::where('type','=','blankyou')->count())
@foreach (DealCategory::where('type','=','blankyou')->get() as $category)

<option value="{{ URL::base() }}/blankyou/thumbs">{{ $category->name }}</option>
@endforeach
@endif
</select>
</div>

<!-- blankyou Side Products -->
<div class="blankyou-banner"><img src="{{ $theme_data['asset_url'] }}images/big/blankyou_banner.png"></div>
<div class="blankyou-product-container">

<!-- blankyou Category -->
<div class="blankyou-product-favorite-top" align="right">
<div class="blankyou-product-favorite-title">Our Favorites</div>
</div>


@foreach ($deals as $deal) 
<!-- blankyou Products -->
<div class="blankyou-product-favorite">
<div class="blankyou-product-img"><a href="#"><img src="{{ $deal->getDealPrimaryImageLocation() }}"></a></div>
<div class="blankyou-product-description"><b>{{ $deal->name }}</b></div>
<div class="blankyou-product-price">${{ number_format($deal->sale_price,2); }}</div>
<div class="blankyou-product-button"><a href="{{ URL::base() }}/blankyou/details/{{ $deal->id }}" class="blankyou-btn blankyou-btn-xl">Order Now</a></div>
</div>
@endforeach



<!-- Old Code
@if ($deals == NULL) 
<div class='blankyou-product-alert alert alert-warning'>No active deals found in.</div> 
@else 
<?php $i = 0; ?>
@foreach ($deals as $deal) 
@if ($i == 3)
<?php $i = 0; ?> 
@endif

<div class="blankyou-product pull-left">
<div class="blankyou-product-img">
@if ($deal->images())
<a href="{{ URL::base() }}/home/details/{{ $deal->id }}"><img src="{{ $deal->getDealPrimaryImageLocation() }}"></a>
@else
<a href="{{ URL::base() }}/home/details/{{ $deal->id }}"><img src="{{ $images_url }}no_image.jpg"></a>
@endif
</div>
<div class="blankyou-product-description"> {{ $deal->name }}</div>
<div class="blankyou-product-price"><b>${{ number_format($deal->sale_price,2); }}</b></div>
<div class="blankyou-product-button"><a href="{{ URL::base() }}/home/details/{{ $deal->id }}" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>
<?php $i++; ?>
@endforeach
@endif
-->

</div>
<!-- /blankyou Side Products -->
</div>
<!-- blankyou Media Fit -->
</div>