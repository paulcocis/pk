<div class="container order-step3-fit-on-pc order-step3-fit-on-tablet order-step3-fit-on-mobile">
<!-- Order-step3 Title -->

<br><div class="order-step3-title"><span class="txt-pink">pinkEpromise</span> - Thank You for your order!</div>
<!-- Order-step3 Container -->
<div class="order-step3-container">
<!-- Order-step3 top -->
<div class="order-step3-top-goback" id="button-go-back" style="display:none"><button type="button" class="btn-pEp-default" onclick="back_step3_button()"><i class="fa fa-long-arrow-left"></i></button></div>
</div>

<!-- Order-step3 Credit Card Information -->
<div style="display: ;padding-top:5px;" id="card_payment_form">

     <div class="alert alert-success"> 

        Your payment for order #{{ $order->id }} has been confirmed. <br>
        An email with detailed informations about order and payment has been sent to your email.

        <br><br>
        If you dont receive any email within <b>1-5 minutes</b> please click on this link to re-send payment confirmation email.

     </div>

     <button class='btn btn-default' onclick="window.location='{{ URL::base() }}/home'">Continue Shopping</button> 

</div>
<!-- /Order-step3 Credit Card Hidden -->


<!-- /Order-Step1 Chose Button -->

</div>
<!-- /Order-Step1 Body -->
</div>
