<div class="container order-step1-fit-on-pc order-step1-fit-on-tablet order-step1-fit-on-mobile">
<!-- Order-Step1 Title -->
<div class="order-step1-title">Order Step 1 - <span class="txt-pink">pinkEpromise</span></div>
<!-- Order-Step1 Container -->
<div class="order-step1-container">

@if ($errors)
<div class='alert alert-danger'>
  @foreach ($errors as $error)
    {{ $error }} <br>
  @endforeach

</div>
@endif


<!-- Order-Step1 top -->
<div class="order-step1-top">Client Information
<div class="order-step1-top-goback" id="button-go-back" style="display:none"><button type="button" class="btn-pEp-default" onclick="back_step1_button()"><i class="fa fa-long-arrow-left"></i></button></div>
</div>

<!-- Order-Step1 Singn in Hidden -->
<div class="order-step1-login" style="display:none;" id="login_form">
<!-- Singn in Container -->
<div align="center">
<!-- Singn in Media Fit -->
<div class="order-step1-login-container">
<!-- Singn in Form -->
<form action="{{ URL::base() }}/checkout/index" method='POST'>
<input type='hidden' name='action' value='sign-in'>
<input type='hidden' name='checkout' value='TRUE'>

<!-- Singn in Title -->
<div class="order-step1-login-border-left"></div>
<div class="order-step1-login-title txt-14px-pc txt-12px-mobile">Sign In To Your Account</div>
<div class="order-step1-login-border-right"></div> 
<!-- Singn in Email -->
<div class="order-step1-login-standard-input"><input type="text" name="username" id="username_id" class="form-control txt-14px-pc txt-12px-mobile" placeholder="Enter email"></div>
<!-- Singn in Password -->		
<div class="order-step1-login-standard-input"><input type="password" name="password" id="password_id" class="form-control txt-14px-pc txt-12px-mobile" placeholder="Password"></div> 
<!-- Singn in Button -->	
<div><button type="submit" class="btn btn-pEp">Sing In</button></div>
<!-- Singn in Reset Password -->	
<div class="order-step1-login-reset"><a href="#" class="link-dark txt-12px">Forgot your password?</a></div>
</form>
<!-- /Singn in Form -->
</div>
<!-- /Singn in Media Fit -->
</div>
<!-- /Singn in Container -->
</div>
<!-- /Order-Step1 Singn in Hidden -->

<!-- Order-Step1 Register Account Hidden -->
<div style="display: none;" id="register_form">
<!-- Register Account Container -->
<div align="center">
<!-- Register Account Media Fit -->
<div class="order-step1-register-container">
<!-- Register Account Form -->
<form action="{{ URL::base() }}/checkout/index" method='POST'>
<input type='hidden' name='action' value='sign-up'>
<input type='hidden' name='checkout' value='TRUE'><!-- Register Accoun Title -->
<div class="order-step1-register-border-left"></div>
<div class="order-step1-register-title txt-12px-mobile">Create a New Account</div>
<div class="order-step1-register-border-right"></div> 
<!-- First Name -->
<div class="order-step1-register-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="firstname" name="contact[firstname]" placeholder="First Name"></div>
<!-- Last Name -->
<div class="order-step1-register-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="lastname" name="contact[lastname]" placeholder="Last Name"></div>
<!-- Address -->
<div class="order-step1-register-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="address" name="contact[address]" placeholder="Address"></div>
<!-- City -->
<div class="order-step1-register-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="city" name="contact[city]" placeholder="City"></div>
<!-- Zip -->
<div class="order-step1-register-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="zip" name="contact[zipcode]" placeholder="Zip"></div>
<!-- State -->
<div class="order-step1-register-standard-input">
<select class="form-control" id="state" name="contact[state]">
<option value=""  label="Select">- Select State -</option>
<option value="AL" label="Alabama">Alabama</option>
<option value="AK" label="Alaska">Alaska</option>
<option value="AZ" label="Arizona">Arizona</option>
<option value="AR" label="Arkansas">Arkansas</option>
<option value="CA" label="California">California</option>
<option value="CO" label="Colorado">Colorado</option>
<option value="CT" label="Connecticut">Connecticut</option>
<option value="DC" label="District Of Columbia">District Of Columbia</option>
<option value="DE" label="Delaware">Delaware</option>
<option value="FL" label="Florida">Florida</option>
<option value="GA" label="Georgia">Georgia</option>
<option value="HI" label="Hawaii">Hawaii</option>
<option value="ID" label="Idaho">Idaho</option>
<option value="IL" label="Illinois">Illinois</option>
<option value="IN" label="Indiana">Indiana</option>
<option value="IA" label="Iowa">Iowa</option>
<option value="KS" label="Kansas">Kansas</option>
<option value="KY" label="Kentucky">Kentucky</option>
<option value="LA" label="Louisiana">Louisiana</option>
<option value="ME" label="Maine">Maine</option>
<option value="MD" label="Maryland">Maryland</option>
<option value="MA" label="Massachusetts">Massachusetts</option>
<option value="MI" label="Michigan">Michigan</option>
<option value="MN" label="Minnesota">Minnesota</option>
<option value="MS" label="Mississippi">Mississippi</option>
<option value="MO" label="Missouri">Missouri</option>
<option value="MT" label="Montana">Montana</option>
<option value="NE" label="Nebraska">Nebraska</option>
<option value="NV" label="Nevada">Nevada</option>
<option value="NH" label="New Hampshire">New Hampshire</option>
<option value="NJ" label="New Jersey">New Jersey</option>
<option value="NM" label="New Mexico">New Mexico</option>
<option value="NY" label="New York">New York</option>
<option value="NC" label="North Carolina">North Carolina</option>
<option value="ND" label="North Dakota">North Dakota</option>
<option value="OH" label="Ohio">Ohio</option>
<option value="OK" label="Oklahoma">Oklahoma</option>
<option value="OR" label="Oregon">Oregon</option>
<option value="PA" label="Pennsylvania">Pennsylvania</option>
<option value="RI" label="Rhode Island">Rhode Island</option>
<option value="SC" label="South Carolina">South Carolina</option>
<option value="SD" label="South Dakota">South Dakota</option>
<option value="TN" label="Tennessee">Tennessee</option>
<option value="TX" label="Texas">Texas</option>
<option value="UT" label="Utah">Utah</option>
<option value="VT" label="Vermont">Vermont</option>
<option value="VA" label="Virginia">Virginia</option>
<option value="WA" label="Washington">Washington</option>
<option value="WV" label="West Virginia">West Virginia</option>
<option value="WI" label="Wisconsin">Wisconsin</option>
<option value="WY" label="Wyoming">Wyoming</option>									
</select>
</div>	

<input type='hidden' name='contact[country]' value='US'>

<!-- Line Spacer -->
<hr class="panel-wide">

<!-- user -->
<div class="order-step1-register-standard-input"><input type="username" class="form-control" id="username" name="username" placeholder="username"></div>

<!-- Email -->
<div class="order-step1-register-standard-input"><input type="email" class="form-control" id="email" name="email" placeholder="Email"></div>
<!-- Password -->
<div class="order-step1-register-standard-input"><input type="password" class="form-control" id="password" name="password" placeholder="Password"></div>
<!-- Confirm Password -->

<!-- Check boxses -->
<div class="order-step1-register-checkbox">
<label><input type="checkbox" name="contact[receive_deal_email]" value='1' class="px-2"><span class="txt-12px-mobile"> I would like to receive email alerts when there is a new deal.</span></label>
<label><input type="checkbox" name="terms" class="px-2"><span class="txt-12px-mobile"> Confirm policy &amp; Terms of Use.</span></label>
</div>

<!-- Register Button -->	
<div class="order-step1-register-button"><button type="submit" class="btn btn-pEp">Register</button></div>
</form>
<!-- /Register Account Form -->
</div>
<!-- /Register Account Media Fit -->
</div>
<!-- /Register Account Container -->
</div>
<!-- /Order-Step1 Register Account Hidden -->

<!-- Order-Step1 Input Body -->
<div id="input-form">
@if (Auth::logged_in())
<div style="padding-left:40px;padding-top:20px;"><a href='{{ URL::base() }}/checkout/step2'>Continue with account {{ Auth::user()->email }}</a></div>
@endif

<!-- Order-Step1 Exist Option -->
<div class="order-step1-exist" style="padding-top:10px;margin-top:15px;">
<label>
<div class="order-step1-exist-input"><input type="radio" name="cient-info" id="login-input" onclick="show_login_form();" /></div>
<div class="order-step1-exist-new">Existing Account.</div>
</label>
<div class="order-step1-exist-new-sub">Already client? Select this option to login in your account.</div>
</div>
<!-- Order-Step1 Exist Register -->
<div class="order-step1-register">
<label>
<div class="order-step1-register-input"><input type="radio" name="cient-info" id="register-input" onclick="show_register_form();" /></div>
<div class="order-step1-register-new">Create a New Account.</div>
</label>
<div class="order-step1-register-new-sub">Are you new? Select this option to create an account.</div>
</div>
</div>

<!-- Order-Step1 Bot Container -->
<div class="order-step1-bot-container"></div>

</div>
<!-- /Order-Step1 Body -->
</div>
