<div class="container order-step3-fit-on-pc order-step3-fit-on-tablet order-step3-fit-on-mobile">
<!-- Order-step3 Title -->

<br><br><br>
<div class="order-step3-title">Credit Card Payment - <span class="txt-pink">pinkEpromise</span></div>
<!-- Order-step3 Container -->
<div class="order-step3-container">
<!-- Order-step3 top -->
<div class="order-step3-top">Payment Information
<div class="order-step3-top-goback" id="button-go-back" style="display:none"><button type="button" class="btn-pEp-default" onclick="back_step3_button()"><i class="fa fa-long-arrow-left"></i></button></div>
</div>

 @if (isset($error))
    <div class="alert alert-danger"> {{ $error }} </div>
 @endif

<!-- Order-step3 Credit Card Information -->
<div style="display: ;" id="card_payment_form">
<!-- Credit Card Container -->
<div align="center">
<!-- Credit Card Media Fit -->
<div class="order-step3-card-container">
<!-- Credit Card Form -->
<form action="{{ URL::base() }}/payment/process" method="POST">
<input type='hidden' name='gateway' value='paypal-express'>
<input type='hidden' name='order' value='{{ Input::get("order", 0) }}'>

<!-- Credit Card Information -->
<div class="order-step3-card-border-left"></div>
<div class="order-step3-card-title txt-12px-mobile">Credit Card Information</div>
<div class="order-step3-card-border-right"></div> 
<!-- Card Name -->
<div class="order-step3-card-general-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="card-name" name="name" placeholder="Card Name"></div>
<!-- Card Number -->
<div class="order-step3-card-general-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="card-number" name="number" placeholder="Card Number"></div>
<!-- Security Code(CVV) -->
<div class="order-step3-card-cvv-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="security-code" name="cvv" placeholder="Security Code"></div>
<!-- State -->
<div class="order-step3-card-month-input">
<select class="form-control" id="state" name="month">
<option value=""  label="Select">- Month -</option>
<option value="01" label="January">1</option>
<option value="02" label="February">2</option>
<option value="03" label="March">3</option>
<option value="04" label="April">4</option>
<option value="05" label="May">5</option>
<option value="06" label="June">6</option>
<option value="07" label="July">7</option>
<option value="08" label="August">8</option>
<option value="09" label="September">9</option>	
<option value="10" label="October">10</option>		
<option value="11" label="November">11</option>		
<option value="12" label="December">12</option>								
</select>
</div>	

<div class="order-step3-card-year-input">
<select class="form-control" id="state" name="year">
<option value="" label="Select">- Year -</option>
<option value="14" label="2014">2014</option>
<option value="15" label="2015">2015</option>
<option value="16" label="2016">2016</option>
<option value="17" label="2017">2017</option>
<option value="18" label="2018">2018</option>
<option value="19" label="2019">2019</option>
<option value="20" label="2020">2020</option>							
</select>
</div>	

<!-- Check boxses -->
<div class="order-step3-card-checkbox">
<label><input type="checkbox" name="terms" class="px-2"><span class="txt-12px-mobile"> Confirm policy &amp; Terms of Use.</span></label>
</div>

<!-- Checkout Card -->	
<div class="order-step3-shipping-button"><button type="submit" class="btn btn-pEp">Confirm Payment</button></div>
</form>
<!-- /Credit Card Form -->
</div>
<!--  /Credit Card Media Fit  -->
</div>
<!-- /Credit Card Container -->
</div>
<!-- /Order-step3 Credit Card Hidden -->


<!-- /Order-Step1 Chose Button -->

</div>
<!-- /Order-Step1 Body -->
</div>
