
@section('js')
		
	<script>

		function modifyQty(item, id)
		{
			var quantity = $(id).val();

			$.ajax({
		        url: '{{ URL::base() }}/cart/changeItemQuantity',
		        type: "POST",
		        data: { item: item, quantity: quantity },
		        success: function(data){
		            window.location.reload();
		        }
		    })
		}

		function countItems()
		{	

			$.ajax({
		        url: '{{ URL::base() }}/cart/countItems',
		        type: "POST",
		        success: function(data)
		        {
			        if (data < 1)	
			        {
				        $('#notice').show().html('Your shopping cart is empty. Please buy minimum 1 item before checkout.');
		  				return false;
			        }
		        }
		    })


		}

		$( "#checkout" ).submit(function( event ) {

  			var termsChecked = $('#terms').is(':checked');

  			if (!termsChecked)
  			{
  				$('#notice').show().html('You need to agree with our terms before checkout.');
  				return false;
  			}

		});


		function sendCoupon()
		{
				// get the coupon
				var coupon = $('#promotion').val();
				var url    = '{{ URL::base() }}/cart/applyPromotion?promotion='+coupon;

				window.location = url;		
		}


		$(document).ready(function() {

			$('#applyPromotion').click(function() {

				alert('a');
				// get the coupon
				var coupon = $('#promotion').val();
				var url    = '{{ URL::base() }}/cart/applyPromotion?promotion='+coupon;

				window.location = url;
			});


		});


	</script>	


@endsection


<div class="container m-10px-pc-b" align="center">

<!-- Row -->
<div class="row">

<!-- cart container -->
<div class="cart-fit-on-pc cart-fit-on-tablet cart-fit-on-mobile">


<form action="{{ URL::base() }}/checkout" id='checkout'>

<!-- cart title -->
<div class="cart-title">Shopping Cart - <span class="txt-pink">pinkEpromise</span></div>


@if (!Auth::logged_in())
<div class="alert alert-danger" style="text-align:left;">
 <h3 style="font-size:16px;margin-top:5px;">You are not logged in.</h3>
 To login in your account please click on <b>'Member Login'</b> link on the top right of the page. If you dont have an account created with us, you will be asked to create one once you proceed with checkout button.

</div>

@endif

@if (Session::has('ActionMessage'))
<div class="alert alert-warning" id='notice1'>
{{ Session::get('ActionMessage') }}
</div>
@endif


<div class="alert alert-danger" id='notice' style="display:none;">

</div>

<!-- cart top -->
<div class="cart-top">
<div class="cart-top-product txt-12px-sm txt-12px-mobile">Product</div>
<div class="cart-top-quantity txt-12px-sm hidden-xs">Quantity</div>
<div class="cart-top-price txt-12px-sm hidden-xs">Unit Price</div> 
<div class="cart-top-item txt-12px-sm hidden-xs">Item Total</div>
<div class="cart-top-options txt-12px-sm hidden-xs">Options</div>
</div>
<!-- /cart top -->

<!-- cart mid -->

@if (!Cart::instance()->items())

<br>
<div class="alert alert-warning">
 You have no products in your shopping cart.
</div>

@else


@foreach(Cart::instance()->items() as $row)
<div class="cart-mid">
<!-- nora blue icon -->

@if ($row->deal()->nora_bleu == 1)
<div class="cart-mid-icon"><img src="{{ $images_url }}icons/pep.png"></div>

@elseif ($row->deal()->blank_you == 1)
<div class="cart-mid-icon"><img src="{{ $images_url }}icons/pep.png"></div>

@else
<div class="cart-mid-icon"><img src="{{ $images_url }}icons/pep.png"></div>

@endif

<!-- product image -->
<div class="cart-mid-product">
@if ( $row->deal()->getPrimaryImage() )
<img src="{{ $row->deal()->getPrimaryImage()->thumb_url(100,70) }}" class="cart-mid-product-image">

@else

<img src='{{ $images_url }}noimg.png'>

@endif
</div>
<!-- product details -->
<div class="cart-mid-product-desc txt-12px-sm txt-12px-mobile"><b>{{ $row->name }}</b><br /><br />

@if ($row->options())
		
		@foreach ($row->options() as $o)

			{{ $o->name() }}: {{ $o->option_value_name() }} <br>

		@endforeach

@endif


@if ($row->deal()->signature_gift)
<div class="cart-mid-product-gift txt-12px-sm txt-12px-mobile"><img src="{{ $images_url }}icons/gift.png" class="cart-mid-product-gift-image">&nbsp; complimentary signature gift wrapping</div>
@endif

</div>

<div class="cart-mid-spacer div-hidden-pc-tablet"></div>

<!-- product quantity -->
<div class="cart-mid-quantity txt-12px-mobile div-hidden-pc-tablet">Quantity:</div>
<div class="cart-mid-product-quantity">
<select class="cart-mid-quantity-select" id='p-{{ $row->id }}' onchange="modifyQty({{ $row->id }}, '#p-{{ $row->id }}')">

@if (!$row->deal()->quantity)
<option>1</option>
@else

<?php for($i = 1; $i <= $row->deal()->quantity; $i++): ?>
<option @if ($i == $row->quantity) selected="selected" @endif> {{ $i }}</option>
<?php endfor; ?>

@endif

</select>
</div>

<!-- product unit price -->
<div class="cart-mid-price txt-12px-mobile div-hidden-pc-tablet">Unit Price:</div>
<div class="cart-mid-unit-price txt-12px-sm txt-12px-mobile"><b>${{ $row->sale_price }}</b></div>
<!-- product unit price -->
<div class="cart-mid-price txt-12px-mobile div-hidden-pc-tablet">Item Total:</div> 
<div class="cart-mid-item-total txt-12px-sm txt-12px-mobile"><b>${{ $row->total() }}</b></div>
<!-- product unit price -->
<div class="cart-mid-delete txt-12px-mobile div-hidden-pc-tablet">Options:</div>
<div class="cart-mid-item-delete"><a href="{{ URL::base() }}/cart?action=delete&itemId={{ $row->id }}" class="link-pink"><i class="fa fa-times-circle fa-lg"></i></a></div>
</div>
@endforeach
@endif


<!-- /cart mid -->

<!-- cart bot -->
<div class="cart-bot" >

<!-- promo cod -->
<div class="cart-bot-promo txt-12px-sm txt-12px-mobile" >Coupon Code<input class="form-control cart-bot-input-promo" name='promotion' id='promotion'></div>
<!-- promo button -->
<div class="cart-bot-promo-button txt-12px-sm txt-12px-mobile"><button type="button" onclick="sendCoupon();" class="btn-pEp btn-pEp-md" id='applyPromotion'>Apply</button></div>
<!-- cart total -->
<div class="cart-bot-item-total pull-right">
<div class="txt-12px-sm txt-14px-pc hidden-xs">Subtotal: <b class="pull-right">${{ Cart::instance()->cart_subtotal() }}</b></div>
<div class="txt-12px-sm txt-14px-pc hidden-xs">Discount: <b class="pull-right">${{ Cart::instance()->cart_discount_total() }}</b></div>
<div class="txt-12px-sm txt-14px-pc hidden-xs">Shipping: <b class="pull-right">${{ Cart::instance()->cart_shipping_total() }}</b></div>
<div class="txt-12px-sm txt-14px-pc hidden-xs">Total: <b class="pull-right">${{ Cart::instance()->cart_total() }}</b></div>
</div>

</div>
<!-- /cart bot -->

<!-- mobile cart total -->
<div class="cart-mobile">
<div class="cart-mobile-align">
<div class="cart-mobile-fild div-hidden-pc-tablet txt-12px-mobile">Subtotal: <b class="pull-right">${{ Cart::instance()->cart_subtotal() }}</b></div>
<div class="cart-mobile-fild div-hidden-pc-tablet txt-12px-mobile">Discount: <b class="pull-right">-$0</b></div>
<div class="cart-mobile-fild div-hidden-pc-tablet txt-12px-mobile">Shipping: <b class="pull-right">${{ Cart::instance()->cart_shipping_total() }}</b></div>
<div class="cart-mobile-fild div-hidden-pc-tablet txt-12px-mobile">Total: <b class="pull-right">${{ Cart::instance()->cart_total() }}</b></div>
</div>
</div>


<!-- cart finish -->
<div class="cart-finish txt-12px-sm txt-12px-mobile">
<!-- tax & shipping -->
<div class="cart-text txt-12px-sm txt-12px-mobile">
You can review the final total before submitting your order. <br />
Total include tax, and <u class="txt-pink">shipping and handling rates</u>. <br /><br />
<label>
<input type="checkbox" name="terms" class="px-2" id='terms' value='1'> <span class="lbl">I understand and agree to the pinkEpromise Terms of Use.</span>
</label>
</div>
<!-- cart & Shopping -->
<div class="txt-12px-sm"><button type="button" class="btn btn-pEp-default" onclick="window.location='{{ URL::base() }}/deals'">Continue Shopping</button> <button type="submit" class="btn btn-pEp">Checkout</button></div>
</div>
<!-- /cart finish -->
</form>
<!-- /form finish -->
</div>
<!-- /cart container -->
</div>
<!-- /Row -->
</div>
