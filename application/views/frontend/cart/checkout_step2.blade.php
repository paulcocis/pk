<div class="container order-step2-fit-on-pc order-step2-fit-on-tablet order-step2-fit-on-mobile">
<!-- Order-Step2 Title -->
<div class="order-step2-title">Order Step 2 - <span class="txt-pink">pinkEpromise</span></div>
<!-- Order-Step2 Container -->
<div class="order-step2-container">
<!-- Order-Step2 top -->
<div class="order-step2-top"><b>Hello {{ Auth::user()->primary()->lastname }}</b> - Please set Shipping Information
<div class="order-step2-top-goback" id="button-go-back" style="display:none"><button type="button" class="btn-pEp-default" onclick="back_step2_button()"><i class="fa fa-long-arrow-left"></i></button></div>
</div>

<!-- Order-Step2 Shipping Information -->
<div style="display: none;" id="add_shipping">
<!-- Shipping Information Container -->
<div align="center">
<!-- Shipping Information Media Fit -->
<div class="order-step2-shipping-container">
<!-- Shipping Information Form -->

<form action="{{ URL::base() }}/checkout/step2" method='POST'>
<input type='hidden' name='shipping' value='new'>
<input type='hidden' name='checkout' value='TRUE'>

<!-- New Shipping Information -->
<div class="order-step2-shipping-border-left"></div>
<div class="order-step2-shipping-title txt-12px-mobile">Add New Shipping Information</div>
<div class="order-step2-shipping-border-right"></div> 
<!-- First Name -->
<div class="order-step2-shipping-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="firstname" name="firstname" placeholder="First Name"></div>
<!-- Last Name -->
<div class="order-step2-shipping-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="lastname" name="lastname" placeholder="Last Name"></div>
<!-- Address -->
<div class="order-step2-shipping-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="address" name="address" placeholder="Address"></div>
<!-- City -->
<div class="order-step2-shipping-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="city" name="city" placeholder="City"></div>
<!-- Zip -->
<div class="order-step2-shipping-standard-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="zip" name="zipcode" placeholder="Zip"></div>
<!-- State -->
<div class="order-step2-shipping-standard-input"><select class="form-control" id="state" name="state">
<option value=""   label="Select">- Select State -</option>
<option value="AL" label="Alabama">Alabama</option>
<option value="AK" label="Alaska">Alaska</option>
<option value="AZ" label="Arizona">Arizona</option>
<option value="AR" label="Arkansas">Arkansas</option>
<option value="CA" label="California">California</option>
<option value="CO" label="Colorado">Colorado</option>
<option value="CT" label="Connecticut">Connecticut</option>
<option value="DC" label="District Of Columbia">District Of Columbia</option>
<option value="DE" label="Delaware">Delaware</option>
<option value="FL" label="Florida">Florida</option>
<option value="GA" label="Georgia">Georgia</option>
<option value="HI" label="Hawaii">Hawaii</option>
<option value="ID" label="Idaho">Idaho</option>
<option value="IL" label="Illinois">Illinois</option>
<option value="IN" label="Indiana">Indiana</option>
<option value="IA" label="Iowa">Iowa</option>
<option value="KS" label="Kansas">Kansas</option>
<option value="KY" label="Kentucky">Kentucky</option>
<option value="LA" label="Louisiana">Louisiana</option>
<option value="ME" label="Maine">Maine</option>
<option value="MD" label="Maryland">Maryland</option>
<option value="MA" label="Massachusetts">Massachusetts</option>
<option value="MI" label="Michigan">Michigan</option>
<option value="MN" label="Minnesota">Minnesota</option>
<option value="MS" label="Mississippi">Mississippi</option>
<option value="MO" label="Missouri">Missouri</option>
<option value="MT" label="Montana">Montana</option>
<option value="NE" label="Nebraska">Nebraska</option>
<option value="NV" label="Nevada">Nevada</option>
<option value="NH" label="New Hampshire">New Hampshire</option>
<option value="NJ" label="New Jersey">New Jersey</option>
<option value="NM" label="New Mexico">New Mexico</option>
<option value="NY" label="New York">New York</option>
<option value="NC" label="North Carolina">North Carolina</option>
<option value="ND" label="North Dakota">North Dakota</option>
<option value="OH" label="Ohio">Ohio</option>
<option value="OK" label="Oklahoma">Oklahoma</option>
<option value="OR" label="Oregon">Oregon</option>
<option value="PA" label="Pennsylvania">Pennsylvania</option>
<option value="RI" label="Rhode Island">Rhode Island</option>
<option value="SC" label="South Carolina">South Carolina</option>
<option value="SD" label="South Dakota">South Dakota</option>
<option value="TN" label="Tennessee">Tennessee</option>
<option value="TX" label="Texas">Texas</option>
<option value="UT" label="Utah">Utah</option>
<option value="VT" label="Vermont">Vermont</option>
<option value="VA" label="Virginia">Virginia</option>
<option value="WA" label="Washington">Washington</option>
<option value="WV" label="West Virginia">West Virginia</option>
<option value="WI" label="Wisconsin">Wisconsin</option>
<option value="WY" label="Wyoming">Wyoming</option>									
</select>
</div>	
<input type='hidden' name='country' value='US'>

<!-- Line Spacer -->
<hr class="panel-wide">

<!-- Email -->
<div class="order-step2-shipping-standard-input"><input type="email" class="form-control" id="email" name="email" placeholder="Email"></div>

<!-- Add Shipping Information -->	
<div class="order-step2-shipping-button"><button type="submit" class="btn btn-pEp">Add Information</button></div>
</form>
<!-- /Shipping Information Form -->
</div>
<!-- /Shipping Information Media Fit -->
</div>
<!-- /Shipping Information Container -->
</div>
<!-- /Order-Step2 Shipping Information Hidden -->

<!-- Order-Step2 Input Body -->
<div id="input-form">
<!-- Order-Step2 Exist Option -->
<div class="order-step2-exist-shipping">
<label>
<div class="order-step2-exist-shipping-input"><input type="radio" name="shipping-info" onclick="show_shipping_next();" /></div>
<div class="order-step2-exist-shipping-new">Existing Shipping Address.</div>
</label>
<div class="order-step2-exist-shipping-new-sub">Use my client information for shipping for goodies deliver.</div>
</div>
<!-- Order-Step2 Exist Register -->
<div class="order-step2-shipping">
<label>
<div class="order-step2-shipping-input"><input type="radio" name="shipping-info" onclick="show_add_shipping_form();" /></div>
<div class="order-step2-shipping-new">New Shipping Address.</div>
</label>
<div class="order-step2-shipping-new-sub">Add new shipping information for goodies deliver.</div>
</div>
</div>

<!-- Order-Step1 Chose Button -->
<div class="order-step2-bot-container">
<form action="{{ URL::base() }}/checkout/step2" method='POST'>
<input type='hidden' name='shipping' value='existing'>
<input type='hidden' name='checkout' value='TRUE'>
<button type="submit" name="shipping-next" class="btn btn-pEp" style="display: none" id="shipping-next" onclick="">Next</button>
</form>
</div>
<!-- /Order-Step1 Chose Button -->

</div>
<!-- /Order-Step1 Body -->
</div>
