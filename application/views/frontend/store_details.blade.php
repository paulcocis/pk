@section('scripts')
@endsection
@section('js')
@endsection


<div class="container" align="center">

<!-- Pinkestore Media Fit -->
<div class="pinkestore-details-fit-on-pc pinkestore-details-fit-on-tablet pinkestore-details-fit-on-mobile">

<!-- Pinkestore Side Menu -->
<div class="pinkestore-details-menu-container hidden-xs">
<!-- Pinkestore Logo -->
<div class="pinkestore-details-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/pEstore_logo.png"></div> 
<!-- Pinkestore Menu Tabs -->
<!-- Pinkestore Menu Blank You -->
<div class="pinkestore-details-menu-blank-container">
<div class="pinkestore-details-menu-blank-title">Our family of brands...</div>
<div class="pinkestore-details-menu-blank-logo"><img src="{{ $theme_data['asset_url'] }}images/big/blankyou_logo.png"></div>
<div class="pinkestore-details-menu-blank-text">Let us see what loves or pursues customer pain, but because occasionally circumstances, product is pain, so blinded by desire, but in this season.</div>


<div class="pinkestore-details-menu-blank-prod">
<div class="pinkestore-details-menu-blank-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/blank_related_4.png"></div>
<div class="pinkestore-details-menu-blank-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="pinkestore-details-menu-blank-prod-price">$29.99</div>
<div class="pinkestore-details-menu-blank-prod-button"><a href="#" class="menu-pinkestore-blank-btn menu-pinkestore-blank-btn-md"><b>Order Now</b></a></div>
</div>

<div class="pinkestore-details-menu-despline"></div>


<div class="pinkestore-related-deals-title">Related Deals</div>
<div class="pinkestore-details-menu-related-prod">
<div class="pinkestore-details-menu-related-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/poinkestore_related_pics.png"></div>
<div class="pinkestore-details-menu-related-prod-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="pinkestore-details-menu-related-prod-price">$29.99</div>
<div class="pinkestore-details-menu-related-prod-button"><a href="#" class="menu-pinkestore-related-btn menu-pinkestore-related-btn-md"><b>Order Now</b></a></div>
</div>


<div class="pinkestore-details-menu-despline"></div>

<iframe class="menu-pinkestore-fb-frame" src="http://www.facebook.com/plugins/like_box.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FdgdTycPTSRj.js%3Fversion%3D41%23cb%3Df36c0b8311eeedc%26domain%3Dwww.pinkepromise.com%26origin%3Dhttp%253A%252F%252Fwww.pinkepromise.com%252Ff2e2ae04a03179c%26relation%3Dparent.parent&amp;header=false&amp;href=http%3A%2F%2Fwww.facebook.com%2FPinkEpromise&amp;locale=en_US&amp;sdk=joey&amp;stream=false&amp;show_border=false&amp;width=265" title="fb:like_box Facebook Social Plugin"></iframe>


</div>
</div>
<!-- /Pinkestore Side Menu -->




<!-- Pinkestore Side Products -->
<div class="pinkestore-details-container">

<!-- Pinkestore Category -->
<div class="pinkestore-details-title">{{ $deal->name }}</div>
<div class="pinkestore-details-product-by">{{ $deal->user()->primary()->business_name }}</div>
<!-- Pinkestore Products -->


<div class="pinkestore-details-right-side">
<div class="pinkestore-details-timeleft">Time Left for Deal: <b>{{ $deal->getTimeLeft() }}.</b></div>
<div class="pinkestore-details-timeleft-order"><form  action="{{ URL::base() }}/cart?action=add" method="post" id="addToCart">
<input type='hidden' name='product_id' value='{{ $deal->id }}'>

@if (isset($pk_item))
<input type='hidden' name='pk_item' value='1'>
@endif


<a href='javascript:void(0)' onclick="$('#addToCart').submit();" class="menu-pinkestore-related-btn menu-pinkestore-related-btn-md">

<b>Order Now</b></a>
</form>
</div>
<div class="pinkestore-details-timeleft-back-store"><a href="#" class="menu-pinkestore-related-btn-back-store menu-pinkestore-related-btn-md"><b>Back to Store</b></a></div>
<div class="pinkestore-details-timeleft-save">$974,138.4</div>
<div class="pinkestore-details-timeleft-save-pinke">saved on pinkEpromise</div>
</div>

<!-- Pinkestore Product gallery -->
<div class="pinkestore-details-mygallery">
<a id="Zoomer2"  href="{{ $deal->primaryImage()->thumb_url() }}" class="MagicZoomPlus" rel="selectors-effect: fade; selectors-class: Active" title="PinkeStore">
<img src="{{ $deal->primaryImage()->thumb_url() }}"/></a> <br/>

@if ($deal->images() != NULL)   

<div class="pinkestore-details-mygallery-thumb">
@foreach($deal->images() as $image)
<div class="pinkestore-details-thumb-pics">
<a href="{{ $image->thumb_url() }}" rel="zoom-id: Zoomer2" rev="{{ $image->thumb_url() }}" class="Selector">
<img src="{{ $image->thumb_url() }}" class="MagicZoomImg" /></a>
</div>

@endforeach

</div>
@endif

</div>
<!-- End Pinkestore Product gallery -->
<!-- Tablet & PC -->
<div class="pinkestore-details-mouseover"><img src="{{ $theme_data['asset_url'] }}images/icons/zoom_pinkestore.png">&nbsp; Mouse over image to zoom.</div>
<!-- Mobile -->
<div class="pinkestore-details-mouseover-mobile"><img src="{{ $theme_data['asset_url'] }}images/icons/zoom_pinkestore.png">&nbsp; Tap over the image and slide to zoom.</div>

<!-- Pinkestore Product Price -->
<div class="pinkestore-details-prod-price">
<div class="pinkestore-details-our-price">Our Price: <span class="pinkestore-details-big-text">${{ number_format($deal->sale_price,2) }}</span></div>
<div class="pinkestore-details-price-desp"></div>
<div class="pinkestore-details-original-price">Original Price: <span class="pinkestore-details-big-text">${{ number_format($deal->original_price,2); }}</span></div>
<div class="pinkestore-details-price-desp"></div>
<div class="pinkestore-details-our-discount">Our Discount: <span class="pinkestore-details-big-text">{{ $deal->discount_price }}%</span></div>
</div>

<div class="pinkestore-details-right-side-mobile">
<div class="pinkestore-details-timeleft-mobile" align="center">Time Left for Deal: <b>17 days</b> and <b>6 hours</b>.</div>
<div class="pinkestore-details-timeleft-order-mobile">

<form  action="{{ URL::base() }}/cart?action=add" method="post" id="addToCart">
<input type='hidden' name='product_id' value='{{ $deal->id }}'>

@if (isset($pk_item))
<input type='hidden' name='pk_item' value='1'>
@endif


<a href='javascript:void(0)' onclick="$('#addToCart').submit();" class="menu-pinkestore-related-btn menu-pinkestore-related-btn-md">

<b>Order Now</b></a>
</form>


</div>
<div class="pinkestore-details-timeleft-back-store-mobile"><a href="#" class="menu-pinkestore-related-btn-back-store menu-pinkestore-related-btn-md"><b>Back to Store</b></a></div>
<div class="pinkestore-details-timeleft-save-mobile">$974,138.4</div>
<div class="pinkestore-details-timeleft-save-pinke-mobile">saved on pinkEpromise</div>
</div>

<!-- Pinkestore Product Description -->
<div class="pinkestore-details-description">
@if (empty($deal->description))
 No description available.
@else
	{{ $deal->description }}
@endif

<br><br>
<b>Shipping Details</b><br> 
Ships within {{ $deal->ship_within }} days. Shipping is ${{ @number_format($deal->first_item_shipping_cost,2); }} for the first item and ${{ @number_format($deal->aditional_item_shipping_cost,2); }} for each additional item.
</div>


<!-- Product Details Social Buttons --> 	
<div class="social-buttons">
<!-- Product Details Fb Button --> 		
<div class="fb-like" data-href="#" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
<!-- Product Details Pinterest Button -->  	
<div class="a2a_kit"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles%2F&media=http%3A%2F%2Fwww.pinkepromise.com%2Fimg%2Fdeals%2F3201_Turquoise-Drop-Earrings-in-2-Styles.jpg&description=Next%20stop%3A%20Pinterest" data-pin-do="buttonPin" data-pin-config="beside" target="_blank"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a></div>
<!-- Product Details Google Button  -->	
<div class="google_plus"><iframe title="+1" data-gapiattached="true" src="https://apis.google.com/_/+1/fastbutton?usegapi=1&amp;size=medium&amp;origin=http%3A%2F%2Fwww.pinkepromise.com&amp;url=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.ro.Vfm7opacHNE.O%2Fm%3D__features__%2Fam%3DEQ%2Frt%3Dj%2Fd%3D1%2Fz%3Dzcms%2Frs%3DAItRSTPgREc9jyDOza_7Yft-DLG_x-2Brg#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1402653779122&amp;parent=http%3A%2F%2Fwww.pinkepromise.com&amp;pfname=&amp;rpctoken=48088411" name="I0_1402653779122" id="I0_1402653779122" vspace="0" tabindex="0" scrolling="no" marginwidth="0" marginheight="0" hspace="0" frameborder="0" width="100%"></iframe></div>
<!-- Product Details Twiter Button  -->
<div class="twitter_button"><iframe data-twttr-rendered="true" title="Twitter Tweet Button" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-horizontal" src="http://platform.twitter.com/widgets/tweet_button.1401325387.html#_=1402653779109&amp;count=horizontal&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles%2F&amp;size=m&amp;text=Turquoise%20Drop%20Earrings%20in%202%20Styles%20-%20Deals%20by%20pinkEpromise&amp;url=http%3A%2F%2Fwww.pinkepromise.com%2Fdeals%2Fshow-deal%2Fid%2F3201%2Fturquoise-drop-earrings-in-2-styles" allowtransparency="true" scrolling="no" id="twitter-widget-0" frameborder="0" target="_blank"></iframe></div>
</div>
<!-- /Product Details Social Buttons-->

<div>
<div id="fb-root"></div>
<div class="fb-comments" data-href="http://www.pinkepromise.com/deals/show-deal/id/3201/turquoise-drop-earrings-in-2-styles" data-width="100%"  data-numposts="5" data-colorscheme="light"></div>
</div>


<!-- /Product Details Facebook Comments Buttons -->
</div>
<!-- /Pinkestore Side Products -->
</div>
<!-- Pinkestore Media Fit -->
</div>