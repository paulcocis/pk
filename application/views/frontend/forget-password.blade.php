<div class="container order-step3-fit-on-pc order-step3-fit-on-tablet order-step3-fit-on-mobile">
<!-- Order-step3 Title -->

<div class="order-step3-title">Password Recovery - <span class="txt-pink">pinkEpromise</span></div>
<!-- Order-step3 Container -->
<div class="order-step3-container">
<!-- Order-step3 top -->
<div class="order-step3-top">Recovery lost password
<div class="order-step3-top-goback" id="button-go-back" style="display:none"><button type="button" class="btn-pEp-default" onclick="back_step3_button()"><i class="fa fa-long-arrow-left"></i></button></div>
</div>

 @if ($errors)
    <div class="alert alert-danger"> {{ $errors }} </div>
 @endif

  @if ($info)
    <div class="alert alert-info"> {{ $info }} </div>
 @endif


<!-- Order-step3 Credit Card Information -->
<div style="display: ;" id="card_payment_form">
<!-- Credit Card Container -->
<div align="center">
<!-- Credit Card Media Fit -->
<div class="order-step3-card-container">
<!-- Credit Card Form -->
<form action="{{ URL::base() }}/login/forget-password?recovery=true" method="POST">
<input type="hidden" class="form-control login-input" name="login" value='TRUE'>

<!-- Credit Card Information -->
<div class="order-step3-card-border-left"></div>
<div class="order-step3-card-title txt-12px-mobile">Forget Password</div>
<div class="order-step3-card-border-right"></div> 
<!-- Card Name -->
<div class="order-step3-card-general-input"><input type="text" class="form-control txt-14px-pc txt-12px-mobile" id="card-name" name="email" placeholder="Enter your account email address"></div>
<!-- Card Number -->


<!-- Checkout Card -->	
<div class="order-step3-shipping-button"><button type="submit" class="btn btn-pEp">Send Password to Email</button></div>
</form>
<!-- /Credit Card Form -->
</div>
<!--  /Credit Card Media Fit  -->
</div>
<!-- /Credit Card Container -->
</div>
<!-- /Order-step3 Credit Card Hidden -->


<!-- /Order-Step1 Chose Button -->

</div>
<!-- /Order-Step1 Body -->
</div>
