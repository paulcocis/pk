<div class="container m-10px-pc-b" align="center">
<!-- Row -->
<div class="row ">

<!-- How It Works Step's -->
<div class="how-fit-on-pc how-fit-on-tablet how-fit-on-mobile">
<!-- How It Works Container-->
<div class="how-container pull-left txt-left">
<!-- How It Works Title -->
<div class="how-title">How it Works - <span class="txt-pink">pinkEpromise</span></div>
<!-- How It Works Title Step 1 -->
<div class="how-step-main">
<div class="how-step-icon"><img src="images/icons/how_step1.png"></div>
<div class="how-step-icon-txt"><img src="images/icons/how-step-txt-1.png"></div>
<div class="how-step-txt-title"> - Shop</div>
<div class="how-step-txt">Find amazing flash deals on hand-crafted and popular boutique items. An added bonus, a portion of every sale goes to a breast cancer foundation!</div>
</div>
<!-- How It Works Title Step 2 -->
<div class="how-step-main">
<div class="how-step-icon"><img src="images/icons/how_step2.png"></div>
<div class="how-step-icon-txt"><img src="images/icons/how-step-txt-2.png"></div>
<div class="how-step-txt-title"> - Wait...</div>
<div class="how-step-txt">The artisan or vendor will receive your order and start preparing your special package.</div>
</div>
<!-- How It Works Title Step 3 -->
<div class="how-step-main">
<div class="how-step-icon"><img src="images/icons/how_step3.png"></div>
<div class="how-step-icon-txt"><img src="images/icons/how-step-txt-3.png"></div>
<div class="how-step-txt-title"> - Celebrate!</div>
<div class="how-step-txt">Celebrate when your goodies arrive at your door and feel great about helping others at the same time!</div>
</div>
</div>

<!-- Deal&Facebook -->
<div class="div-right-pc div-right-tablet">
<iframe class="how-fb-frame" src="http://www.facebook.com/plugins/like_box.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FdgdTycPTSRj.js%3Fversion%3D41%23cb%3Df36c0b8311eeedc%26domain%3Dwww.pinkepromise.com%26origin%3Dhttp%253A%252F%252Fwww.pinkepromise.com%252Ff2e2ae04a03179c%26relation%3Dparent.parent&amp;header=false&amp;href=http%3A%2F%2Fwww.facebook.com%2FPinkEpromise&amp;locale=en_US&amp;sdk=joey&amp;stream=false&amp;show_border=false&amp;width=265" title="fb:like_box Facebook Social Plugin"></iframe>
<!-- Deal -->
<div class="how-newsletter">
<div class="how-newsletter-text txt-pink">Sign up today to receive our daily deal notification.</div>
<div><input type="text" class="form-control how-newsletter-input" id="txtInput" placeholder="Enter E-mail"/></div>
<div class="how-newsletter-btn"><button class="btn btn-pEp" type="button">Sign up</button></div>
</div>
</div>
<!-- /Deal&Facebook -->
</div>
<!-- -->
</div>
<!-- /Row -->
</div>
