<div class="container" align="center">

<!-- Pinkestore Media Fit -->
<div class="pinkestore-fit-on-pc pinkestore-fit-on-tablet pinkestore-fit-on-mobile">

<!-- Pinkestore Side Menu -->
<div class="pinkestore-menu-container hidden-xs">
<!-- Pinkestore Logo -->
<div class="pinkestore-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/pEstore_logo.png"></div> 
<!-- Pinkestore Menu Tabs -->
<!-- Pinkestore Menu Blank You -->
<div class="pinkestore-menu-norablue-container">
<div class="pinkestore-menu-norablue-title">Our family of brands...</div>
<div class="pinkestore-menu-norablue-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_related_logo.png"></div>
<div class="pinkestore-menu-norablue-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor.</div>



<div class="pinkestore-menu-norablue-prod">
<div class="pinkestore-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_1.png"></div>
<div class="pinkestore-menu-norablue-prod-description">Mesh Buckle Bracelet ...... FREE SHIPPING</div>
<div class="pinkestore-menu-norablue-prod-price">$29.99</div>
<div class="pinkestore-menu-norablue-prod-button"><a href="#" class="pinkestore-menu-norablue-btn pinkestore-menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>

<div class="pinkestore-menu-norablue-prod">
<div class="pinkestore-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_2.png"></div>
<div class="pinkestore-menu-norablue-prod-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="pinkestore-menu-norablue-prod-price">$29.99</div>
<div class="pinkestore-menu-norablue-prod-button"><a href="#" class="pinkestore-menu-norablue-btn pinkestore-menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>


</div>
</div>
<!-- /Pinkestore Side Menu -->



<!-- Pinkestore Side Products -->

<div class="pinkestore-banner"><img src="{{ $theme_data['asset_url'] }}images/big/pinkestore_banner.png" class="pinkestore-banner-size"></div>

<div class="pinkestore-product-container">

<!-- Pinkestore Category -->
<div class="pinkestore-product-title">Featured Products</div>

@foreach ($deals as $deal)

<!-- Pinkestore Products -->
<div class="pinkestore-product">
<div class="pinkestore-product-img"><a href="#">
	

@if ($deal->images())
<img src="{{ $deal->getDealPrimaryImageLocation() }}">

@else
<img src="{{ $images_url }}no_image.jpg">
@endif

</a></div>
<div class="pinkestore-product-description">{{ $deal->name }}</div>
<div class="pinkestore-product-price">${{ number_format($deal->sale_price,2); }}</div>
<div class="pinkestore-product-button"><a href="{{ URL::base() }}/store/details/{{ $deal->id }}" class="pinkestore-btn pinkestore-btn-xl">Order Now</a></div>
</div>


@endforeach

</div>
<!-- /Pinkestore Side Products -->
</div>
<!-- Pinkestore Media Fit -->
</div>