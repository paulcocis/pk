<div class="container" align="center">

<!-- Blankyou Media Fit -->
<div class="blankyou-details-fit-on-pc blankyou-details-fit-on-tablet blankyou-details-fit-on-mobile">
<!-- Blankyou Side Menu -->
<div class="blankyou-menu-container">
<!-- Blankyou Logo -->
<div class="blankyou-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/blankyou_logo_big.png"></div>
<!-- blankyou Menu Tabs -->
<div class="blankyou-menu-tabs-bg">
<a href="{{ URL::base() }}/blankyou" class="link-blankyou-menu"><div class="blankyou-menu-tabs">All Products</div></a>

@if (DealCategory::where('type','=','blankyou')->count())

@foreach (DealCategory::where('type','=','blankyou')->get() as $category)
<a href="{{ URL::base() }}/blankyou/index/{{ $category->id }}" class="link-blankyou-menu"><div class="blankyou-menu-tabs">{{ $category->name }}</div></a> 
@endforeach

@endif
</div>
<!-- blankyou Menu Blank You -->
<div class="blankyou-menu-norablue-container">
<div class="blankyou-menu-norablue-title">Our family of brands...</div>
<div class="blankyou-menu-norablue-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_related_logo.png"></div>
<div class="blankyou-menu-norablue-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor.</div>

<div class="blankyou-menu-norablue-prod">
<div class="blankyou-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_1.png"></div>
<div class="blankyou-menu-norablue-prod-description">Mesh Buckle Bracelet ...... FREE SHIPPING</div>
<div class="blankyou-menu-norablue-prod-price">$29.99</div>
<div class="blankyou-menu-norablue-prod-button"><a href="#" class="blankyou-menu-norablue-btn blankyou-menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>

<div class="blankyou-menu-norablue-prod">
<div class="blankyou-menu-norablue-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/norableu_product_2.png"></div>
<div class="blankyou-menu-norablue-prod-description">Australian Crystal Drop Earrings FREE SHIPPING</div>
<div class="blankyou-menu-norablue-prod-price">$29.99</div>
<div class="blankyou-menu-norablue-prod-button"><a href="#" class="blankyou-menu-norablue-btn blankyou-menu-norablue-btn-md"><b>Order Now</b></a></div>
</div>

</div>
</div>
<!-- /blankyou Side Menu -->
<!-- /Nora Side Menu -->
<div class="hidden-md hidden-lg">
<select class="blankyou-mobile-select" onchange="location = this.options[this.selectedIndex].value;">
<option>- Select Category -</option>
<option value="{{ URL::base() }}/blankyou">All Products</option>

@if (DealCategory::where('type','=','blankyou')->count())
@foreach (DealCategory::where('type','=','blankyou')->get() as $category)

<option value="{{ URL::base() }}/blankyou/index/{{ $category->id }}">{{ $category->name }}</option>
@endforeach
@endif
</select>
</div>

<!-- Norablue Details Container -->
<div class="blankyou-details-container">

<!-- Norableu Category -->
<div class="blankyou-details-title">{{ $deal->name }}</div>

<!-- Pinkestore Product gallery -->
<div class="blankyou-details-mygallery">
<a id="Zoomer2"  href="{{ $deal->primaryImage()->thumb_url() }}" class="MagicZoomPlus" rel="selectors-effect: fade; selectors-class: Active" title="BlankYou">
<img src="{{ $deal->primaryImage()->thumb_url() }}"/></a> <br/>

@if ($deal->images() != NULL)   

<div class="pinkestore-details-mygallery-thumb">
@foreach($deal->images() as $image)
<div class="pinkestore-details-thumb-pics">
<a href="{{ $image->thumb_url() }}" rel="zoom-id: Zoomer2" rev="{{ $image->thumb_url() }}" class="Selector">
<img src="{{ $image->thumb_url() }}" class="MagicZoomImg" /></a>
</div>

@endforeach

</div>
@endif
</div>
<!-- /Norablue Details Image Container -->

<!-- Norableu Details Order  -->
<div class="blankyou-order-container">
<div class="blankyou-details-mouseover-mobile"><img src="{{ $theme_data['asset_url'] }}images/icons/zoom_blankyou.png">&nbsp; Tap over the image and slide to zoom.</div>

<!-- Norableu Details Product Container -->
<div class="blankyou-details-prod-price-mobile">
<div class="blankyou-details-our-price">Starting at: <span class="blankyou-details-big-text">${{ $deal->sale_price }}</span></div>
</div>
<form  action="{{ URL::base() }}/cart?action=add" method="post">

<input type='hidden' name='product_id' value='{{ $deal->id }}'>
<!-- Norableu Details Order Quantity -->
<div class="blankyou-details-text">Embroidery selections</div>
<div class="blankyou-details-qunatity">
<select>
<option>Select a Font</option>
<option onclick="blankyou_font1()">1 - $39.99</option>
<option onclick="blankyou_font2()">2 - $79,98</option>
<option onclick="blankyou_font3()">3 - $119,97</option>
</select>
</div>
<!-- /Norableu Details Order Quantity -->

<!-- Norableu Details Order Button -->
<div class="blankyou-details-qunatity">
<input type="text"  id="entername" name="entername" placeholder="Enter Name">
</div>
<!-- /Norableu Details Order Button  -->
<div class="blankyou-details-button"><button type="submit" class="blankyou-btn blankyou-btn-xl">Add to Cart</button></div>
</div>
</form>
<!-- /Norableu Details Order  -->

<!-- Tablet & PC -->
<div class="blankyou-details-mouseover"><img src="{{ $theme_data['asset_url'] }}images/icons/zoom_blankyou.png">&nbsp; Mouse over image to zoom.</div>
<!-- Mobile -->

<!-- Blankyou Details Product Container -->
<div class="blankyou-details-prod-price">
<div class="blankyou-details-our-price">Starting at: <span class="blankyou-details-big-text">${{ $deal->sale_price }}</span></div>
</div>

<div class="blankyou-details-description">
{{ $deal->description }}
</div>


<!-- /Norableu Details Product Container -->
</div>
<!-- Norablue Details Container -->
</div>
<!-- /Norablue Side Products -->
</div>
