<div class="container m-25px-pc-t m-40px-pc-b m-8px-mobile-t m-20px-mobile-b m-30px-tablet-t m-40px-tablet-b">

@if (!isset($deals) OR $deals == NULL)

 <div class='alert alert-warning'>
 		No deals found.
 </div>

@else


<?php $i = 0; ?>

@foreach ($deals as $deal)

@if ($i == 3)
<!-- @Susu Disable -> tablet & mobile incompatibility 
<div class='clearfix'></div>
-->


<?php $i = 0; ?>

@endif


<!-- product div -->
<div class="product pull-left">
<div class="product-img"><img src="{{ $deal->getDealPrimaryImageLocation() }}"><b class="bulmov"><small>


{{ number_format($deal->discount_price,2) }}%

</small></b></div>
<div class="product-description">{{ $deal->name }}</div>
<div class="product-price"><b>OUR Price: ${{ number_format($deal->sale_price,2); }}</b></div>
<!-- product button -->
<div align="center"><a href="{{ URL::base() }}/home/details/{{ $deal->id }}" class="btn-sm btn-pEp"><b>Buy Now</b></a></div>
</div>



<?php $i++; ?>

@endforeach

@endif
</div>
