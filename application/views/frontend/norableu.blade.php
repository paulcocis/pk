<div class="container" align="center">

<!-- Nora Media Fit -->
<div class="norableu-fit-on-pc norableu-fit-on-tablet norableu-fit-on-mobile">
<!-- Nora Side Menu -->
<div class="norableu-menu-container">
<!-- Nora Logo -->
<div class="norableu-menu-logo"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_logo.png"></div> 
<!-- Nora Menu Tabs -->

<div class="norableu-menu-tabs-bg">
<a href="{{ URL::base() }}/nora/index/all" class="link-norableu-menu"><div class="norableu-menu-tabs">All Products</div></a>
@foreach (DealCategory::get() as $category)
<a href="{{ URL::base() }}/nora/index/{{ $category->id }}" class="link-norableu-menu"><div class="norableu-menu-tabs">{{ $category->name }}</div></a> 
@endforeach
</div>

<!-- Nora Menu Blank You -->
<div class="norableu-menu-blank-container">
<div class="norableu-menu-blank-title">Our family of brands</div>
<div class="norableu-menu-blank-logo"><img src="{{ $theme_data['asset_url'] }}images/big/blankyou_logo.png"></div>
<div class="norableu-menu-blank-text">Let us see what loves or pursues customer pain, but because occasionally circumstances, product is pain, so blinded by desire, but in this season.</div>


<div class="norableu-menu-blank-prod">
<div class="norableu-menu-blank-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/blank_related.png"></div>
<div class="norableu-menu-blank-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="norableu-menu-blank-prod-price">$29.99</div>
<div class="norableu-menu-blank-prod-button"><a href="#" class="menu-blank-btn menu-blank-btn-md"><b>Order Now</b></a></div>
</div>

<div class="norableu-menu-blank-prod">
<div class="norableu-menu-blank-prod-image"><img src="{{ $theme_data['asset_url'] }}images/products/blank_related_2.png"></div>
<div class="norableu-menu-blank-prod-description">Glamorous Freshwater Pearl Necklace and Earring</div>
<div class="norableu-menu-blank-prod-price">$29.99</div>
<div class="norableu-menu-blank-prod-button"><a href="#" class="menu-blank-btn menu-blank-btn-md"><b>Order Now</b></a></div>
</div>

</div>
</div>
<!-- /Nora Side Menu -->
<div class="hidden-md hidden-lg">
<select class="norableu-mobile-select" onchange="location = this.options[this.selectedIndex].value;">
<option>- Select Category -</option>
<option value="{{ URL::base() }}/blankyou/thumbs">All Products</option>

@foreach (DealCategory::get() as $category)

<option value="{{ URL::base() }}/blankyou/thumbs">{{ $category->name }}</option>
@endforeach
</select>
</div>

<!-- Nora Side Products -->
@if ($showHeader)
<div class="norableu-banner"><img src="{{ $theme_data['asset_url'] }}images/big/norableu_banner.png" class="norableu-banner-size"></div>
@endif
<div class="norableu-product-container">

<!-- Norableu Category -->
<div class="norableu-product-favorite-top" align="right">
<div class="norableu-product-favorite-title">

@if (!$cat OR $cat == 'all')
All Products
@else
	{{ App\Deal\Category::find($cat)->name }}
@endif



</div>
<div class="norableu-signature-favorite"><img src="{{ $theme_data['asset_url'] }}images/icons/norableu_sig.png"></div>
</div>
<!-- /Category Title -->


@foreach($deals as $deal)

<div class="norableu-product-favorite">
<div class="norableu-product-img"><a href="#"><img src="{{ $deal->getDealPrimaryImageLocation() }}"></a><b class="bulsig"><small></small></b></div>
<div class="norableu-product-description">{{ $deal->name }}</div>
<div class="norableu-product-price">${{ number_format($deal->sale_price,2); }}</div>
<div class="norableu-product-button"><a href="{{ URL::base() }}/nora/view/{{ $deal->id }}" class="norableu-btn norableu-btn-xl">Order Now</a></div>
</div>
@endforeach





<!-- Old Code



@if ($deals == NULL)

 <div class='alert alert-warning'>
 		No active deals found in.
 </div>

@else


<?php $i = 0; ?>


@foreach ($deals as $deal)

@if ($i == 3)

<?php $i = 0; ?>

@endif



<div class="norableu-product pull-left">

<div class="norableu-product-gift">
@if ($deal->options()->count())
<div class="norableu-product-gift-icon"><img src="{{ $theme_data['asset_url'] }}images/icons/gift.png"></div>
<div class="norableu-product-gift-text">signature gift wrapping</div>
@else
<div class="norableu-product-gift-text-notavail">signature - not available</div>
@endif
</div>

<div class="norableu-product-img">
@if ($deal->images())
<a href="{{ URL::base() }}/nora/view/{{ $deal->id }}"><img src="{{ $deal->getDealPrimaryImageLocation() }}"></a>
@else
<a href="{{ URL::base() }}/nora/view/{{ $deal->id }}"><img src="{{ $images_url }}no_image.jpg"></a>
@endif
</div>


<div class="norableu-product-description"> {{ $deal->name }}</div>
<div class="norableu-product-price"><b>${{ number_format($deal->sale_price,2); }}</b></div>

<div class="norableu-product-button"><a href="{{ URL::base() }}/nora/view/{{ $deal->id }}" class="btn-sm btn-nora"><b>Order Now</b></a></div>
</div>



<?php $i++; ?>

@endforeach


@endif
-->

</div>
<!-- /Nora Side Products -->
</div>
<!-- Nora Media Fit -->
</div>