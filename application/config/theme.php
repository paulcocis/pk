<?php

 return array(

 		/**
 		 * Themes root directory.
 		 */
		'theme_root_path' => path('public'),

		/**
		 * Theme path inside root path.
		 */
		'theme_path'      => 'themes/frontend/',

		// the default theme name.
		'theme_name'      => 'default',
 );