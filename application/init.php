<?php

  // Application helpers path.
  $application_helpers_path = path('app').'helpers/';

  // Add new path to list array.
  set_path('helpers', $application_helpers_path);

  // the bootstrap path.
  set_path('bootstrap', path('public').'boostrap/');

    // deal downloadable location
  set_path('deal_downloadable_path', path('public').'deals/downloadable_files/');
  
  set_path('deal_images', path('public').'deals/images/');

  if (!file_exists(path('public').'deals/'))
  {
    mkdir(path('public').'deals/');
  }

  if (!file_exists(path('deal_downloadable_path')))
  {
     mkdir(path('deal_downloadable_path', 0777, TRUE));
  }

  if (!file_exists(path('deal_images')))
  {
     mkdir(path('deal_images', 0777, TRUE));
  }


  Autoloader::map(array(
     'Base_Controller'       => path('app').'controllers/base.php',
     'Frontend_Controller'   => path('app').'controllers/frontend.php',
     'Admin_Base_Controller' => path('app').'controllers/admin/base.php',
     'Seller_Base_Controller' => path('app').'controllers/seller/base.php',
  ));

  Autoloader::namespaces(array(
     'App'     => path('app').'libraries/app',
     'Cart'    => path('app').'libraries/cart',
     'Gateway' => path('app').'libraries/gateway'

  ));

  import('helperengine', 'helpers');
  import('general', 'helpers');


  // initiate site setup.
  Setup::instance()->init();

  // Captcha settings.
  $captcha_location = path('helpers').'captcha/';

  // get the file.
  include($captcha_location.'simple-php-captcha.php');

  


function pluralize( $count, $text ) 
{ 
    return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
}


/**
 * A simple function to include files.
 *
 * @param  string $name filename
 * @param  string $from location path variable.
 * @return boolean
 */
function import($name = '', $from = '')
{
    static $loaded = array();

    // get the root path.
    $from = (empty($from)) ? path('base') : path($from);

    foreach((array) $name as $file)
    {
        if (!in_array($file, $loaded))
        {
            $filename = str_replace('.', '/', $file).EXT;

            $path = rtrim($from . $filename);

            if (file_exists($path))
            {
                $loaded[$file] = $path;
                require($path);
                return TRUE;
            }

            throw new Exception('Cannot load '.$path.' file not found.');
        }
    }
}

function public_image_dir()
{
    return URL::base().'/img/';
}

/**
 * Generate a link.
 *
 * @param  string $to
 * @param  string $link
 * @return string $path
 */
function url_to($to, $link = '')
{
    // get link.
    $link = Config::get("system.{$link}", FALSE);

    if (!$link)
    {
      $link = URL::base();
    }

    $path = rtrim($link,'/').'/'.$to;
    echo $path;
}


/**
 * Simple pagination function.
 */
function paginate($query, $per_page = 10, $field_selector = array())
{

    $total    = $query->count();
    $paginate = $query->paginate($per_page);

    // copy and unset.
    $results = $paginate->results;
    unset($paginate->results);

    // Set the offset pagew.
    $page = ($paginate->page == 1) ? 0 : ($paginate->page - 1);
    $start = 1;

    $paginate->count = $total;

    foreach($results as $row)
    {
      $row->increment      = $start;
      $row->rows_count     = $start + ($page * $per_page);

      $paginate->results[] = $row;

      $start++;
    }

    $first = current($paginate->results);
    $last  = end($paginate->results);

    $paginate->first_row = $first->rows_count;
    $paginate->last_row  = $last->rows_count;

    return $paginate;
}

/**
 * Manual pagination.
 * @return object array
 */
function paginate_manual($data, $per_page = 10)
{
    // count total
    $total = count((array) $data);

    // create the paginator instance
    $paginator = Paginator::make($data, $total, $per_page);

    $paginator->count  = $total;
    $paginator->offset = ($paginator->page -1) * $per_page;

    $paginator->results = array_slice($paginator->results, $paginator->offset, $per_page, true);

    return $paginator;
}

function get_controller()
{
    // get the current action
    $controller = empty(Request::$route->controller) ? 'home' : Request::$route->controller;

    $controller = substr($controller,strrpos($controller, '.')+1);

    // return lowe case action.
    return strtolower($controller);
}

function get_action()
{
    // get the current action
    $action = empty(Request::$route->controller_action) ? 'index' : Request::$route->controller_action;

    $action = str_replace('-', '_', $action);
    // return lowe case action.
    return strtolower($action);
}

function timeleft($from, $current = 'now', $print = 'dh') {

    $diff = \Date::diff($from, $current);

    if ($print)
    {
        if ($print == 'dh')
        {
          return $diff->d.' days, '. $diff->h.' hours';
        }
        else
        {
          return $diff->y.' years, '. $diff->m.' months '.$diff->d.' days';
        }
    }

    return $diff;
}


function getTimezoneList()
{
    $identifiers = DateTimeZone::listIdentifiers();

    for ($i=0; $i < count($identifiers); $i++)
    {
        $timezones[] = $identifiers[$i];
    }

    return $timezones;
}

function table_fields($table)
{
   $columns = DB::query("SHOW COLUMNS FROM ".$table." ");

    // primary columns should be excluded.
    foreach($columns as $column)
    {
        if ($column->key == 'PRI')
        {
            $primary[] = $column->field;
        }
        else
        {
            $fields[] = $column->field;
        }

        $all_columns[] = $column->field;
    }

    return $fields;
}

function table_columns($table = '', $data = array())
{
    if (!empty($table))
    {
        $columns = DB::query("SHOW COLUMNS FROM ".$table." ");

        $primary = array();
        $fields  = array();

        // primary columns should be excluded.
        foreach($columns as $column)
        {
            if ($column->key == 'PRI')
            {
                $primary[] = $column->field;
            }
            else
            {
                $fields[] = $column->field;
            }

            $all_columns[] = $column->field;
        }

        if (empty($data))
        {
            return $all_columns;
        }

        if (!empty($primary))
        {
            $data = array_remove_keys($data, $primary);
        }

        $valid_data = array();

        foreach($fields as $field)
        {
            if (isset($data[$field]))
            {
                $valid_data[$field] = $data[$field];
            }
        }

        return $valid_data;
    }
}

/**
 * Get mySQL enum values.
 * @return array
 */
function enum_mysql($table = '', $field = '')
{
    if (!empty($table))
    {
        $query = DB::query("SHOW COLUMNS FROM `$table` LIKE '".$field."' ");
        $field = current($query);

        $values = preg_replace('/(?:^enum|set)|\(|\)/', '', $field->type);

        $array_values = explode(',', $values);

        // now format them.
        foreach($array_values as $key => $value)
        {
            $array_values[$key] = preg_replace('/^\'|\'$/', '', $array_values[$key]);
        }

        return $array_values;
    }
}

function dropdown_enum($table, $select_name = 'status', $selected = null)
{
    // get options.
    $options = enum_mysql($table, $select_name);

    $html = '<select name='.$select_name.'>';

    foreach($options as $option)
    {
        $s = '';

        if ($option == $selected)
        {
            $s = 'selected="selected"';
        }

        $html .= '<option value='.$option.' '.$s.'>'.ucfirst($option).'</option>';
    }

    $html .= '</select>';
    return $html;
}


/**
 * Get country list.
 * @return array
 */
function countries($code = '', $search_by_name = false)
{
    static $countries = null;

    if ($countries === null)
    {
        $countries = Config::get('countries');
    }

    if (isset($code) AND !empty($code))
    {
        if ($search_by_name)
        {
            $code = strtolower($code);
            $countries_lower = array_map('strtolower', $countries);

            return $found = array_search($code, $countries_lower);
        }

        $code = strtoupper($code);

        if (isset($countries[$code]))
        {
            return $countries[$code];
        }

        return FALSE;
    }

    return $countries;
}


/**
 * Remove some keys from an array.
 * @return array
 */
function array_remove_keys($array, $keys = array()) {

    // If array is empty or not an array at all, don't bother
    // doing anything else.
    if(empty($array) || (! is_array($array))) {
        return $array;
    }

    // If $keys is a comma-separated list, convert to an array.
    if(is_string($keys)) {
        $keys = explode(',', $keys);
    }

    // At this point if $keys is not an array, we can't do anything with it.
    if(! is_array($keys)) {
        return $array;
    }

    // array_diff_key() expected an associative array.
    $assocKeys = array();
    foreach($keys as $key) {
        $assocKeys[$key] = true;
    }

    return array_diff_key($array, $assocKeys);
}

/**
 * Calculate the timestamp interval.
 * @return timestamp
 */
function interval($interval = 1, $type = 'day', $compute = '-', $timestamp = NULL)
{
    // get the current timestamp.
    if ($timestamp == NULL)
    {
        $timestamp = \Date::forge()->time();
    }

    // get the interval timestamp
    $interval_timestamp = strtotime ( ' '.$compute.' '.$interval.' '.$type.'' , $timestamp ) ;
    return $interval_timestamp;
}


