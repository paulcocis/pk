<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

Route::get('/', function()
{
	//return View::make('home.index');
});

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});


Route::get('how-it-works', array('as' => 'how-it-works', 'uses' => 'home@info'));
Route::get('about-us', array('as' => 'about-us', 'uses' => 'home@about'));
Route::get('privacy', array('as' => 'privacy', 'uses' => 'home@privacy'));
Route::get('terms', array('as' => 'terms', 'uses' => 'home@terms'));
Route::get('affiliate-program', array('as' => 'terms', 'uses' => 'home@affiliate'));
Route::get('faq', array('as' => 'faq', 'uses' => 'home@faq'));
Route::get('deals', array('as' => 'deals', 'uses' => 'home@index'));



/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Route::get('/', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});

// login..
Route::filter('admin_login', function()
{
	if (!Auth::logged_in(array('admin', 'staff')))
	{
		Session::put('redirect_back_to', URI::current());
		return Redirect::to('admin/login/index/');
	}
});

Route::filter('seller_login', function()
{
	if (!Auth::logged_in(array('admin', 'seller')))
	{
		Session::put('redirect_back_to', URI::current());
		return Redirect::to('seller/login/index/');
	}
});


Route::get('admin/logout', function()
{
	// logout.
	Auth::logout();
	return Redirect::to('admin/login/index');
});


Route::get('seller/logout', function()
{
	// logout.
	Auth::logout();
	return Redirect::to('seller/login/index');
});


Route::get('logout', function()
{
	// logout.
	Auth::logout();
	return Redirect::to('home/');
});



Route::get('download-file/(:any)', function($secretId)
{
	$deal = new Deal;

	// execute download.
	$response = $deal->download($secretId);

	if ($response == -1)
	{
		return 'You have reached maximum download times.';
	}

	// get path / name.
	list($path, $name) = $response;

	return Response::download($path, $name);
});




// Auto detect controllers.
Route::controller(Controller::detect());
//Route::controller(Controller::detect('queue'));