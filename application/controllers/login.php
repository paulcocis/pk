<?php

class Login_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{
		if (Input::has('login'))
 			{
	 			$credentials = array(
	 				'username' => Input::get('username'),
	 				'password' => Input::get('password'),
	 				'remember' => Input::get('remember'),
	 				'roles'	   => array('customer', 'admin', 'seller')
	 			);

 			try
 			{
 				$result = Auth::attempt($credentials);

 				$redirect_to = 'account/'; 	

 				return Redirect::to($redirect_to);
 			}

 			catch (Exception $e)
 			{
 				$error = $e->getMessage();
 				$this->theme->set('error', $error);
 			}
 		}

 		$this->theme->display('frontend.login');
	}

	public function action_logout()
	{
		// logout.
		Auth::logout();
		return Redirect::to('login/');		
	}

	public function action_forget_password()
	{
		$errors = FALSE;
		$info   = FALSE;

		if (Input::has('recovery'))
		{
			$account = User::where('email', '=', Input::get('email'))->first();

			if (!$account)
			{
				$errors = 'Invalid account/email address.';
			}
			else
			{
				$info = 'Instructions has been sent to '.$account->email;
				$account->send_password_recovery_email();				
			}
		}

		$this->theme->set('errors', $errors);
		$this->theme->set('info', $info);

		$this->theme->display('frontend.forget-password');
	}

}

