<?php

class GetMemberList_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{
		$conn = DB::connection('old_db');

		$data = $conn->table('users')->where('role', '=', 'member')->get();

		$Content = NULL;

		foreach ($data as $record)
		{
		    // If you want 1 email per line
		    $Content .= '"'.$record->email.'"'.PHP_EOL;
		}

		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename=MemberList.csv');
		header('Pragma: no-cache');
		header('Expires: 0');

		echo $Content;

	}

}

