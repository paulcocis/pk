<?php

class Contact_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');

		@session_start();

	}

	public function action_index()
	{	

		$errors = FALSE;
		if (Input::has('contact'))
		{		
			if (!Input::get('email'))
			{
				$errors[] = 'Please enter an e-mail address.';
			}

			if (Input::get('code') != $_SESSION['captcha']['code'])
			{
				$errors[] = 'Invalid captcha code.';
			}


			if (!$errors)
			{
				Message::send(function($message)
				{
					$input = Input::get();

				    $message->to('paul.cocis87@gmail.com');
				    $message->bcc(array('Blair@pinkepromise.com', 'paul@ipv.ro'));
				    $message->from(Input::get('email'), 'New Message From Contact Page');

				    $message->subject('PINKE PROMISE - New Message Contact');

				    $message->body->data = $input;
				    $message->body('view: emails.contact');

				    // You can add View data by simply setting the value
				    // to the message.

				    $message->html(true);
			    });	

			    return Redirect::back()->with('ActionMessage', 'Thank you, we will get back with your shortly!')	;
			}
		}

		$this->theme->set('errors', $errors);
		$this->theme->display('frontend.contact');
	}

	public function action_captcha()
	{	

		$_SESSION['captcha'] = simple_php_captcha();
	 	
	 	return '<img src="'.$_SESSION['captcha']['image_src'].'">';

	}


}