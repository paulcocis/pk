<?php

class Payment_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_credit_card()
	{
		// get the order.
		$order = App\Order::find(Input::get('order', 0));

		if (!$order OR $order->status == 'paid')
		{
			return Redirect::to('home');
		}

		if (Input::has('failed') AND Input::get('failed') == 'YES')
		{	
			$this->theme->set('error', Session::get('error'));
		}

		return $this->theme->display('frontend.cart.credit_card');
	}

	public function action_process()
	{
		// get the order.
		$order = App\Order::find(Input::get('order'));

		if (!$order)
		{
			return FALSE;
		}

		$payment = new Cart\Payment;

		// get the payment form.
		$paymentForm = $payment->paypal_express($order->id, Input::get());

		if ($paymentForm['success'] == 1)
		{
			// Send payment fonfirmation.
			App\Message\Message::forge('order')->paymentConfirmation($order);

			// return.
			return Redirect::to('checkout/complete?order='.$paymentForm['order']);
		}
		else
		{
			return Redirect::to('payment/credit-card/?failed=YES&order='.$paymentForm['order'])->with('error', $paymentForm['message']);
		}			

	}



}

