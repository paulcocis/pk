<?php

class Store_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{

		$deal = new Deal;

		$list = $deal->retrieve(FALSE, TRUE);

		$this->theme->set('deals', $list);
		$this->theme->display('frontend.store');
	}
	
	
	public function action_details($id)
	{

		$deal = new Deal;

		$deal = $deal->find($id);

		$this->theme->set('deal', $deal);
		$this->theme->set('pk_item', 1);
		$this->theme->display('frontend.store_details');
	}


}