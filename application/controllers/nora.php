<?php

class Nora_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{
		$showHeader = TRUE;
		$category = URI::segment(3, 0);

		if ($category)
		{
			if ($category == 'all')
			{
				$list = Deal::instance()->nora();
			}
			else
			{
				$list = Deal::instance()->nora($category);
			}

			$showHeader = FALSE;
		}
		else
		{
			$list = Deal::instance()->nora();
		}

		$this->theme->set('showHeader', $showHeader);
		$this->theme->set('deals', $list);
		$this->theme->set('cat', $category);
		return $this->theme->display('frontend.norableu');
	}
	

	public function action_view($id) 
	{
		// Original Code
		$deal = Deal::find($id);
		$this->theme->set('deal', $deal);

		return $this->theme->display('frontend.norableu_details');
	}

}