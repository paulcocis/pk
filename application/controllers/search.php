<?php

class Search_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{
		// get the search keyword.
		$keyword = Input::get('search', NULL);

		if ($keyword)
		{
			$deal = new Deal;
			// retrieve the deal list.
			$deals = $deal->where('name', 'LIKE', "%$keyword%")->get();

			$this->theme->set('deals', $deals);
		}

		$this->theme->display('frontend.search');
	
	}


}