<?php

class Blankyou_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{
		$category = URI::segment(3, 0);

		echo $category;
		$list = Deal::instance()->blankyou($category);

		$this->theme->set('deals', $list);
		return $this->theme->display('frontend.blankyou');
	}

	public function action_details($id)
	{

		$deal = Deal::find($id);

		$this->theme->set('deal', $deal);
		return $this->theme->display('frontend.blankyou_details');	
	}

	
	public function action_thumbs()
	{
		// Old Code
		// $deal = Deal::find($id);
	    // $this->theme->set('deal', $deal);
      
	  	$category = URI::segment(4, 0);
		$list = Deal::instance()->blankyou($category);

		$this->theme->set('deals', $list);
		return $this->theme->display('frontend.blankyou_thumbs');
	}


	public function action_view()
 	{
		//$deal = Deal::find();

		$deal = new Deal;
		$list = $deal->retrieve(FALSE, TRUE);
		$this->theme->set('deals', $deal);
		return $this->theme->display('frontend.blankyou_details');
	}

}