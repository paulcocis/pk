<?php

class Cart_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');
	}

	public function action_index()
	{	
		$cart = Cart::instance();

		// get the cart action.
		$action = Input::get('action', NULL);

		if ($action)
		{
			 // add new option.
			 if ($action == 'add')
			 {	
			 	$pid = 0;
		
		 		$promotion = App\Promotion::where('code', '=', Input::get('promotion'));

		 		if ($promotion->count())
		 		{
		 			$pid = $promotion->first()->id;
		 		}

			 	// get the product id.
			 	$product_id = Input::get('product_id', 0);


			 	$pk_item = Input::get('pk_item', 0);

			 	$settings = array(
					'pk_item'  => $pk_item,
					'quantity' => Input::get('quantity', 1),
					'comments' => Input::get('comments', NULL)
			 	);
			 	// add product in cart.
			 	$cart->add($product_id, Input::get('options'), $settings, $pid);

			 	return Redirect::to('cart');
			 }

			 // delete an product.
			if ($action == 'delete')
			{
			 	$cart->delete(Input::get('itemId'));
			 	return Redirect::to('cart');
			}
		}

		$this->theme->display('frontend.cart.basket');				
	}


	public function action_changeItemQuantity()
	{
		$cart = Cart::instance();

		if ($cart->item(Input::get('item', 0)))
		{
			$cart->change(Input::get('item'), array('quantity' => Input::get('quantity')));
		}
	}

	public function action_countItems()
	{
		return Cart::instance()->countItems();
	}

	public function action_applyPromotion()
	{	
		$e = NULL;

		$cart   = Cart::instance();
		$coupon = Input::get('promotion');

		$message = 'Coupon has been applied.';

		try
		{
			$cart->applyCoupon($coupon);	
		} catch (\Exception $e) {}

		if ($e and $e instanceof Exception)
		{
			$message = $e->getMessage();
		}

		return Redirect::back()->with('ActionMessage', $message);		
	}

}