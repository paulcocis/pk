<?php

class Cc_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{

// Single Transaction
$PayFlow = new PaypalFlow('pinkEpromise', 'Paypal', 'pinkEpromise', 'Marytrent2014', 'single');

$PayFlow->setEnvironment('test');                           // test or live
$PayFlow->setTransactionType('S');                          // S = Sale transaction, R = Recurring, C = Credit, A = Authorization, D = Delayed Capture, V = Void, F = Voice Authorization, I = Inquiry, N = Duplicate transaction
$PayFlow->setPaymentMethod('C');                            // A = Automated clearinghouse, C = Credit card, D = Pinless debit, K = Telecheck, P = PayPal.
$PayFlow->setPaymentCurrency('USD');                        // 'USD', 'EUR', 'GBP', 'CAD', 'JPY', 'AUD'.

$PayFlow->setAmount('15.00', FALSE);
$PayFlow->setCCNumber('5383258113147409');
$PayFlow->setCVV('333');
$PayFlow->setExpiration('1116');
$PayFlow->setCreditCardName('Richard Castera');



if($PayFlow->processTransaction()):
  echo('Transaction Processed Successfully!');
else:
  echo('Transaction could not be processed at this time.');
endif;
 
echo('<h2>Name Value Pair String:</h2>');
echo('<pre>');
print_r($PayFlow->debugNVP('array'));
echo('</pre>');
 
echo('<h2>Response From Paypal:</h2>');
echo('<pre>');
print_r($PayFlow->getResponse());
echo('</pre>');
 
unset($PayFlow);

	}


}

