<?php

 use App\Order as Order;

class Account_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');

		if (!$this->logged())
		{
			//die();
		}
	}

	public function action_index()
	{
		$user = Auth::user();

		$this->theme->set('user', $user);
		return $this->theme->display('frontend.client.profile');
	}

	public function action_orders()
	{
		$order = new Order;

		// retrieve order list.
		$orders = $order->where('customer_id', '=', Auth::user()->id)->get();

		$this->theme->set('orderlist', $orders);
		return $this->theme->display('frontend.client.orders');
	}


 	public function logged()
 	{
 		return Auth::logged_in(array('customer'));
 	}	



}