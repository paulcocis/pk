<?php

class Admin_Seller_Home_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{
		if (Input::has('addNewSeller'))
		{
			$userManager = new UserManager;

			$data = array_merge(Input::get(), array('roles' => array(2)));

			// mark seller as pending.
			$data['status'] = 'pending';

			$result =  $userManager->create($data);

			if (!$result['status'])
			{
				return implode('<br>', $result['errors']);
			}

			$user = \User::find($result['id']);

			$username = $user->email;
			$password = $user->real_password;
			$email    = $user->email;

			// Send signup informations email.
			App\Message\Message::forge('seller')->signup($user);
			return 'OK';
		}


		$this->theme->display('admin.seller.list');
	}

	public function action_edit($id = 0)
	{	

		// check for availability
		$user = User::find($id);

		// negative
		if (!$user)
		{
			return FALSE;
		}

		$userManager = new UserManager;

		if (Input::has('updateProfile'))
		{
			$data = Input::get();
			$result = $userManager->modify($id, $data);
		}

		if (Input::has('updateBilling'))
		{
			$data = Input::get();
			$userManager->modifyBilling($id, $data);
		}

		$this->theme->set('u', $user);
		$this->theme->set('DataCollection', App\Seller::DataCollection($id));
		return $this->theme->display('admin.seller.edit');
	}

	public function action_updateAdvancedSettings($id = 0)
	{	
		$userManager = new UserManager;
		// check for id.
		$user = User::find($id);

		if ($user)
		{
			$data = Input::get();

			$update = array();

			if (isset($data['password']) AND !empty($data['password']))
			{
				$update['password'] = $data['password'];
			}

			if (isset($data['status']) AND !empty($data['status']))
			{
				$update['status'] = $data['status'];
			}


			$result = $userManager->modify($id, $update, FALSE);

			print_r($result);

			return TRUE;
		}

		return FALSE;
	}


	public function action_setAvatar($id = 0)
	{	
		// create user avatar instance.
		$UserAvatar = new UserAvatar($id);

		// initiate upload
		$UserAvatar->upload();

		 return Redirect::back();
	}


	public function action_do($action = NULL, $uid = NULL)
	{
		if (!$action)
		{
			return;
		}

		if ($action == 'export')
		{
			return var_dump(Input::get('ids'));
		}


		if ($action == 'remove')
		{
			User::find(Input::get('id'))->delete();
			return Redirect::to('admin/seller/home');
		}

		if ($action == 'sendSignupEmail')
		{
			App\Message\Message::forge('seller')->signup(Input::get('id'));
			return;
		}

		if ($action == 'testApproveEmail')
		{
			App\Message\Message::forge('seller')->approved($uid);
		}

		if ($action == 'approve')
		{
			if ($user = User::find($uid))
			{
				// approve the account.
				$user->approve();
				return Redirect::back()->with('ActionMessage', 'Account '.$user->bill()->business_name.' has been succesfully approved.');
			}

		}

	}


}

