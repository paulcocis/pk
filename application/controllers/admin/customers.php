<?php

class Admin_Customers_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{
		$customers =$this->getCustomerList();


		$this->theme->set('customers', $customers);


		return $this->theme->display('admin.customers');
	}

	public function action_info($id = 0)
	{
		$user = User::find($id);

		if (!$user)
		{
			return Redirect::to('admin/customers/index')->with('ActionMessage', 'You are attempting to view an unavailable account.');
		}

		$this->theme->set('User', $user);
		return $this->theme->display('admin.customer_edit');
	}


	protected function getCustomerList($status = NULL)
	{
		$result =  Role::find(3)->users()->order_by('id','DESC')->paginate(10);
		return $result;

	}



}

