<?php

class Admin_Deals_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{

		if (Input::has('delete'))
		{
			$r = Deal::forge()->find(Input::get('delete'));

			if ($r)
			{
				$r->delete();
			}
		}

		return $this->theme->display('admin.deals.list');
	}

	public function action_add()
	{
		$added = NULL;

		$action = Input::get('action', FALSE);

		if ($action)
		{
			if ($action == 'add')
			{
				$added = Deal::forge()->add(Input::get());

				return Redirect::to('admin/deals/post-setup/')->with('deal_postbacksetup_id', $added->id);

			}	
		}

		$this->theme->set('added', $added);


		return $this->theme->display('admin.deals.add');
	}

	public function action_post_setup($deal_id = 0)
	{	
		// keep the session.
		Session::keep('deal_postbacksetup_id');

		if (!$deal_id)
		{
			$deal_id = Session::get('deal_postbacksetup_id');	
		}
		
		$deal = Deal::find($deal_id);

		$action = Input::get('action', NULL);

		if ($action)
		{

			if ($action == 'setDealPrimaryImage')
			{
				$image = new App\Deal\Image;

				// upload deal images.
				$result = $image->upload(1);				
			}

			if ($action == 'addMoreImages')
			{
				$image = new App\Deal\Image;

				// upload deal images.
				$result = $image->upload(0);
				return Redirect::back();				
			}

			if ($action == 'deleteImage')
			{
				$image = new App\Deal\Image;
				$image->find(Input::get('imageId'))->delete();

				return Redirect::back();
			}


			if ($action == 'makeImagePrimary')
			{
				$image = new App\Deal\Image;
				$image->find(Input::get('imageId'))->primary();

				return Redirect::back();
			}


			if ($action == 'addNewOption')
			{
				DealOptions::forge()->add();
				return Redirect::back();
			}

			if ($action == 'deleteOption')
			{
				DealOptions::find(Input::get('optionId'))->delete();
				return Redirect::back();
			}



		}



		$this->theme->set('deal', $deal);		
		return $this->theme->display('admin.deals.add_postsetup');
	}

	public function action_edit($id = 0)
	{	

		// attempt to retrieve deal informations.
		$deal = Deal::find($id);

		if (!$deal)
		{
			return Redirect::to('admin/deals');
		}

		$action = Input::get('action', NULL);
		$object = Input::get('object', NULL);

		if ($action)
		{
			// manage object deal.
			if ($object AND $object == 'deal')
			{

				if ($action == 'modify')
				{

					$data = Input::get();

					if (!isset($data['require_personalization']))
					{
						$data['require_personalization'] = 0;
					}

					if (isset($data['name']))
					{

						if (!isset($data['feautured']))
						{
							$data['feautured'] = 0;
						}	

						if (!isset($data['nora_bleu']))
						{
							$data['nora_bleu'] = 0;
						}	

						if (!isset($data['blank_you']))
						{
							$data['blank_you'] = 0;
						}						

						if (!isset($data['favorite']))
						{
							$data['favorite'] = 0;
						}	

						if (!isset($data['pkstore_item']))
						{
							$data['pkstore_item'] = 0;
						}	
					}
					
					$deal->modify($id, $data);
					return Redirect::to('admin/deals/edit/'.$id);
				}
			}

			// Manage deal images.
			if ($object AND $object == 'image')
			{
				if ($action == 'setPrimaryImage')
				{			
					DealImages::forge()->setPrimaryImage(Input::get('image_id'), $id);
					
				}

				if ($action == 'delete')
				{
					$deleteImage = DealImages::forge()->find(Input::get('image_id'));
					$deleteImage->delete();

					return Redirect::to('admin/deals/edit/'.$id);
				}
			}

			if ($object AND $object == 'options')
			{
				if ($action == 'add')
				{
					DealOptions::forge()->add();

					if (Request::ajax())
					{
						return TRUE;
					}
					else
					{
						return Redirect::to("admin/deals/edit/".$id);
					}
				}
			}
		}

		$this->theme->set('o', $deal);
		return $this->theme->display('admin.deals.edit');
	}

	public function action_editoption($id = 0)
	{
		$this->theme->setLayout('ajax');

		if (!DealOptions::find($id))
		{
			return FALSE;
		}

		$action = Input::get('action', NULL);

		$optionlist = new App\Deal\OptionList;


		if ($action == 'updateOptionName')
		{
			$record = DealOptions::find(Input::get('option_id'));
			$record->name = Input::get('new_name');

			$record->save();
		}

		// fetch data record.
		$record = DealOptions::find($id);
		
		if ($action == 'add')
		{
			$data = Input::get();
			$optionlist->add($data);

			$this->theme->set('o', $record);
			return $this->theme->display('admin.ajax.deal_optionvalues');
		}

		if ($action == 'delete')
		{
			$deletedId = Input::get('optionValueId', 0);

			// delete the option.
			$dR = $optionlist->find($deletedId);

			if ($dR)
			{
				$dR->delete();
			}

			$this->theme->set('o', $record);
			return $this->theme->display('admin.ajax.deal_optionvalues');
		}

		$this->theme->set('o', $record);
		return $this->theme->display('admin.deals.editoption');
	}


	public function action_upload()
	{
		$image = new App\Deal\Image;

		// upload deal images.
		$result = $image->upload();

		if ($result)
		{
			$deal_id = $result->deal_id;
			return Redirect::to('admin/deals/edit/'.$deal_id.'?tab=deal-images');
		}

		return 'FAILED';
	}


	public function action_categories()
	{

		$action = Input::get('action', NULL);

		if ($action == 'add')
		{
			DealCategory::instance()->set_data(Input::get())->save();
		}

		if ($action == 'delete')
		{
			$deleteCategory = DealCategory::find(Input::get('id'));

			// remove if found.
			if ($deleteCategory)
			{
				$deleteCategory->delete();
				return Redirect::to('admin/deals/categories');
			}

		}		

		return $this->theme->display('admin.deals.categories');
	}

}

