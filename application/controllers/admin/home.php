<?php

class Admin_Home_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{
		return $this->theme->display('admin.dashboard');
	}


	public function action_testSellerInvoice($order)
	{
		$invoice = new App\SellerInvoice;
		$invoice->generate($order);

	}

	public function action_report()
	{
		$r = new App\Report\Transaction;

		print_r($r->salesSummaryByPeriod('2014-08-01', '2014-09-02'));
	}

	public function action_message()
	{
		$order = App\Message\Message::forge('order');

		$order->confirmation(109);

	}

}

