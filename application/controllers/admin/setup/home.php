<?php

class Admin_Setup_Home_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{
		$settings = new App\Setup\Settings;
		$signup = new App\Setup\Signup;

		if (Request::ajax())
		{
			$settings->modify(Input::get());
			return;
		}


		$this->theme->set('row', $settings->retrieve());
		$this->theme->set('rowSignup', $signup->retrieve());
		$this->theme->display('admin.administration.generalsettings');
	}

	public function action_store()
	{

		$this->theme->display('admin.administration.storesettings');

	}


	public function action_signup()
	{
		$signup = new App\Setup\Signup;

		if (Request::ajax())
		{
			$data = Input::get();
			$data['user_allow_update_profile'] = Input::get('user_allow_update_profile', 0);
			$data['user_allow_update_email'] = Input::get('user_allow_update_email', 0);


			$signup->modify($data);
			return;
		}

		return FALSE;
	}



}

