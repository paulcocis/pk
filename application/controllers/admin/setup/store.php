<?php

class Admin_Setup_Store_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{
		$store = new App\Setup\Store;

		if (Request::ajax())
		{
			$data = Input::get();
			$data['allow_new_order']      = Input::get('allow_new_order', 0);
			$data['allow_checkout_notes'] = Input::get('allow_checkout_notes', 0);
			$data['allow_guest_checkout'] = Input::get('allow_guest_checkout', 0);
			$data['allow_coupon_usage']   = Input::get('allow_coupon_usage', 0);
			$data['enable_terms']         = Input::get('enable_terms', 0);

			$store->modify($data);
			return;
		}

		$this->theme->set('row', $store->retrieve());
		$this->theme->display('admin.administration.storesettings');

	}

}

