<?php

class Admin_Setup_PaymentGateways_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{	
		$gw = new App\Setup\PaymentGateway;

		if (Request::ajax())
		{
			// get action.
			$action = Input::get('action');

			if ($action == 'create')
			{

				// try to create new gateway.
				$result = $gw->add(Input::get());

				if ($result === TRUE)
				{
					return 'Gateway has been succesfully created.';
				}

				return implode('<br>',$result);
			}
		}

		$this->theme->display('admin.administration.paymentgateways');
	}

	public function action_edit($id = 0)
	{

		$gw = App\Setup\PaymentGateway::forge();

		if (!$gw->find($id))
		{
			return Redirect::to('admin/setup/paymentgateways');
		}


		$action = Input::get('action', NULL);

		if ($action == 'delete')
		{
			$gw->find($id)->delete();
			return Redirect::to('admin/setup/paymentgateways');
		}


		if (Request::ajax())
		{
			$r = $gw->find(Input::get('gwid'));
			$r->set_data(Input::get());
			$r->save();

			return 'Gateway settings has been succesfully updated.';
		}


		$this->theme->set('row', $gw->find($id));
		return $this->theme->display('admin.administration.paymentgatewayedit');
	}

}

