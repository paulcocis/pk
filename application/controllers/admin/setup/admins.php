<?php

class Admin_Setup_Admins_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{

		if (Input::has('delete'))
		{
			$id = Input::get('delete', 0);

			if (Auth::user()->id != $id)
			{
				$found = User::find($id);
				
				if ($found)
				{
					$found->delete();
				}
			}
		}

		$admin = new App\Admin;
		$this->theme->display('admin.administration.adminlist');
	}

	public function action_edit($id = 0)
	{	
		if (!User::find($id))
		{
			return FALSE;
		}

		if (Request::ajax())
		{
			$result = UserManager::forge()->modify($id, Input::get());

			if (!$result['status'])
			{
				return implode('<br>', $result['errors']);
			}
			else
			{
				return 'Admin has been modified.';
			}

		}


		$this->theme->set('user', User::find($id));
		return $this->theme->display('admin.administration.adminedit');
	}

	public function action_create()
	{
		$userManager = new UserManager;

		// attepmt to create the admin.
		$result = $userManager->create(Input::get());

		if (!$result['status'])
		{
			return implode('<br>', $result['errors']);
		}

		return 'Admin has been succesfully created.';

	}

}

