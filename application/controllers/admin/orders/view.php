<?php

class Admin_Orders_View_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_manager()
	{

		$order = new Order;

		if (Input::has('search'))
		{
			$order_id        = Input::get('order_id', NULL);
			$payment_status  = Input::get('payment_status', NULL);
			$shipping_status = Input::get('shipping_status', NULL);
			
			$interval_start = Input::get('start', NULL);
			$interval_end   = Input::get('end', date('Y-m-d'));

			if ($order_id)
			{
				$order = $order->where('id', '=', $order_id);	
			}
			
			if ($payment_status)
			{
				$order = $order->where('status', '=', $payment_status);	
			}			

			if ($shipping_status)
			{
				$order = $order->where('shipping_status', '=', $shipping_status);	
			}

			if ($interval_start)
			{
				$order = $order->where(DB::raw('DATE(created_at)'), '>=', date('Y-m-d', strtotime($interval_start)))
							   ->where(DB::raw('DATE(created_at)'), '<=', date('Y-m-d', strtotime($interval_start)));
			}

		}

		$list = $order->get();

		$this->theme->set('orderlist', $list);

		$this->theme->display('admin.order.order_managment');
	}


	public function action_index()
	{	
		$pk = Input::get('pk', FALSE);

		// check for any actions passed.
		$action = Input::get('action', NULL);

		if ($action)
		{

			if ($action == 'delete')
			{
				$order = Order::find(Input::get('oid', 0));

				if ($order)
				{
					$order->delete();
				}
			}
		}
		
		$this->theme->set('pk_order', $pk);
		$this->theme->display('admin.order.list');
	}

	public function action_order($id = 0)
	{
		$order = Order::find($id);

		$this->theme->set('Order', $order);
		return $this->theme->display('admin.order.edit_v2');
	}


	/**
	  * **************************************************************************************************
	  *
	  * Order actions and events.
	  * @todo Create an event class for managment.
	  * @todo Create an observable for events.
	  *
 	  * **************************************************************************************************
	  */	

	/**
	 * Manage Order Actions.
	 */
	public function action_do($action = NULL)
	{
		$order = Input::get('order', NULL);

		// ops no action passed.
		if (!$action OR !$order)
		{
			return Redirect::back();
		}

		// get the action method
		$actionMethod = 'action_'.$action;


		// process the action.
		$result = $this->$actionMethod(Input::get());

		// result with informative notification.
		return Redirect::back()->with('ActionMessage', $result);
	}


	/**
	 * CHange Order Status.
	 */
	protected function action_ChangeOrderStatus($data)
	{
		// lets extract the data now.
		extract($data);

		// Attempt to find order.
		$order = Order::find($order);

		// execute update query.
		$query = $order->set_data(array('status' => $status))->save();

		// set the action message.
		$message = 'Order status has been modified.';

		return $message;
	}

	/**
	 * Add Order Comment
	 */
	protected function action_addOrderComment($data)
	{	
		// save in new var.
		$newCommentData = $data;

		// create db var.
		$newCommentData['order_id'] = $data['order'];

		// remove unnecessary field.
		unset($newCommentData['order']);

		extract($data);

		// Attempt to find order.
		$order = Order::find($order);	


		// add the comment
		$order->comment()->add($newCommentData);

		$message = 'Comment has been succesfully added to order.';

		return $message;
	}

	/**
	 * Delete Order Comment.
	 */
	protected function action_deleteOrderComment($data)
	{
		// delete the order.
		App\Order\Comment::find($data['commentId'])->delete();

		return $message = 'Comment has been deleted.';
	}

	/**
	 * Schedule order shipping.
	 */
	protected function action_scheduleOrderShipping($data)
	{	
		// attempt to find the order.
		$order = Order::find($data['order']);

		// attempt to schedule.
		$order->schedule('shipped', $data['schedule_shipping_at'], $data['shipping_message'], FALSE);

		// return message.
		return $message = 'Your order has been marked as shipped. Expect delivery at '.$data['schedule_shipping_at'];
	}

	/**
	 * Cancel order shipping.
	 */
	protected function action_cancelOrderShipping($data)
	{	
		// attempt to find the order.
		$order = Order::find($data['order']);

		// attempt to schedule.
		$order->cancelShipping();

		// return message.
		return $message = 'Your order shipping process has been canceled.';
	}

	/**
	 * Change Order Gateway.
	 */
	protected function action_changeOrderGateway($data)
	{	
		// attempt to find the order.
		$order = Order::find($data['order']);

		// attempt to schedule.
		$order->changeGateway($data['gateway_id']);

		// return message.
		return $message = 'Your order Tender has been succesfully modified.';
	}

	protected function action_createRefundTransaction($data)
	{
		$e = NULL;
		// attempt to find the order.
		$order = Order::find($data['order']);	

		try {
			$order->create_refund($data);

		} catch (Exception $e) {}

		if ($e instanceof Exception)
		{
			return $message = $e->getMessage();
		}

		return 'Refund transaction has ben generated for order.';
	}

	protected function action_deleteTransaction($data)
	{
		$e = NULL;
		// attempt to find the order.
		$order = Order::find($data['order']);	

		App\Billing\Transaction::find($data['transactionId'])->delete();

		// regenerate seller invoices.
		$order->regenerateSellerInvoices();

		return 'Transaction has been removed.';
	}

	protected function action_regenerateSellerTransactions($data)
	{

		// attempt to find the order.
		$order = Order::find($data['order']);	

		$order->regenerateSellerInvoices();

		return 'Seller invoices has been regenerated for this order.';		
	}

	protected function action_removeAllRefundTransactions($data)
	{

		// attempt to find the order.
		$order = Order::find($data['order']);	

		// get refund transactions
		$refundTransactions = $order->getRefundTransactions();		

		if ($refundTransactions)
		{
			foreach($refundTransactions as $transaction)
			{
				$transaction->delete();
			}
		}

		// regenerate seller invoices.
		$order->regenerateSellerInvoices();

		return 'Refund transactions has been removed for this order.';
	}

	protected function action_changeTransactionComment($data)
	{
		$transaction = App\Billing\Transaction::find($data['transactionId']);

		// update transaction comments
		$transaction->set_data(array('custom_invoice_notice' => $data['comment']))->save();

		// return message
		return 'Transaction notice has been modified.';

	}


}

