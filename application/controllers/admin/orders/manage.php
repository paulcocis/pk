<?php

class Admin_Orders_Manage_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{	
		// create order instance.
		$order = new Order;

		$start = Input::get('start', NULL);

		
		$end   = Input::get('end', NULL);

		// get status.
		$status = Input::get('status', NULL);

		// get shipping status.
		$shipping_status = Input::get('shipping_status', NULL);

		$UniqueOrderId = Input::get('UniqueOrderId', NULL);

		if ($UniqueOrderId AND !empty($UniqueOrderId))
		{
			$order = $order->where('id', '=', $UniqueOrderId);
		}

		// apply status filter.
		if ($status)
		{
			$order = $order->where_in('status', $status);
		}

		if ($shipping_status)
		{
			$order = $order->where_in('shipping_status', $shipping_status);
		}

		if ($start)
		{

			if (!$end)
			{
				$end = date('Y-m-d');
			}

			$start = date('Y-m-d', strtotime($start));
			$end   = date('Y-m-d', strtotime($end));


			$order = $order->where(DB::raw('DATE(created_at)'), '>=', $start)
						   ->where(DB::raw('DATE(created_at)'), '<=', $end);
		}


		$data = paginate_manual($order->order_by('id', 'DESC')->get());

		$this->theme->set('list', $data);
		$this->theme->display('admin.order.manage');
	}

	public function action_do($action = NULL, $order = 0)
	{
		if (!$action)
		{
			return FALSE;
		}

		// Resend Email Confirmation.
		if ($action == 'sendConfirmationEmail')
		{	
			// fire the event.
			App\Message\Message::forge('order')->confirmation($order);

			// set the returning back message.
			$ActionMessage = 'Confirmation email for order '.$order.' has been succesfully sent.';
		}

		// Regenerate seller invoices for order.
		if ($action == 'regenerateSellerInvoice')
		{
			App\SellerInvoice::forge()->regenerate($order);

			// set the returning back message.
			$ActionMessage = 'Seller payout invoices has been generated for order '.$order;			
		}

		return Redirect::back()->with('ActionMessage', $ActionMessage);

	}

	public function action_details($id)
	{
		$this->theme->setLayout('ajax');

		$order = Order::find($id);

		$this->theme->set('order', $order);
		return $this->theme->display('admin.order.ajax.details');
	}


	public function action_comments($id)
	{
		$this->theme->setLayout('ajax');

		$order = Order::find($id);

		$action = Input::get('action', NULL);

		if ($action)
		{
			if ($action == 'add')
			{
				$order->comment()->add(Input::get());

				return 'OK';
			}

			if ($action == 'delete')
			{
				$order->comment()->find(Input::get('comment_id'))->delete();

				return 'OK';
			}

		}

		$this->theme->set('order', $order);
		return $this->theme->display('admin.order.ajax.comments');
	}


	public function action_refund($id)
	{
		$this->theme->setLayout('ajax');

		$order = Order::find($id);

		$action = Input::get('action', NULL);

		if ($action)
		{
			if ($action == 'refund')
			{
				$order->create_refund(Input::get());

				return 'OK';
			}
		}

		$this->theme->set('order', $order);
		return $this->theme->display('admin.order.ajax.refund');
	}



	public function action_schedule_shipping($id)
	{
		$this->theme->setLayout('ajax');

		$order = Order::find($id);

		$action = Input::get('action', NULL);

		if ($action)
		{
			if ($action == 'schedule')
			{
				$order->schedule(Input::get('status'), Input::get('date'), Input::get('shipping_message'));

				return 'OK';
			}
		}


		$this->theme->set('order', $order);
		return $this->theme->display('admin.order.ajax.schedule_shipping');
	}


	public function action_delete($id = 0)
	{
		if ($order = Order::find($id))
		{
			$order->delete();
		}

		return Redirect::back();
	}


	public function action_delete_until()
	{
		$date = Input::get('date');

		$orders = Order::where(DB::raw('DATE(created_at)'), '<', $date)->get();

		foreach($orders as $order)
		{
			$order->delete();
		}

		return 'OK';
	}




}

