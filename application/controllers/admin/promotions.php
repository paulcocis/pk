<?php

class Admin_Promotions_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{

		if (Input::has('delete'))
		{
			$promotion = new Promotion;
			$deletePromotion = $promotion->find(Input::get('id'))->delete();
		}


		return $this->theme->display('admin.promotions.list');
	}

	public function action_add()
	{
		
		$this->theme->setLayout('ajax');

		if (Input::has('add'))
		{
			$promotion = new Promotion;
			$newPromotion = $promotion->add(Input::get());
		}

		return $this->theme->display('admin.promotions.add');
	}

	public function action_edit($id = 0)
	{
		$this->theme->setLayout('ajax');

		// fetch the promotion data.
		$promotion = Promotion::find($id);

		// invalid promotion passed.
		if (!$promotion)
		{
			return FALSE;
		}

		if (Input::has('save'))
		{
			$promotion = Promotion::find($id);
			$promotion->set_data(Input::get())->save();		
		}

		$promotion = Promotion::find($id);
		
		$this->theme->set('o', $promotion);
		return $this->theme->display('admin.promotions.edit');

	}

	public function action_test($id)
	{
		print_r(Promotion::find($id)->applyAmountDiscount(85));
	}



}

