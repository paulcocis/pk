<?php

 use App\Billing\Transaction as Transaction;

class Admin_Billing_Transactions_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{
		$status = Session::get('status', FALSE);

		$this->theme->set('transactionStatus', $status);

		$this->theme->display('admin.billing.transactions');
	}

	public function action_view($id = 0)
	{
		$this->theme->setLayout('ajax');


		$Transaction = new App\Billing\Transaction;

		if (!$Transaction->find($id))
		{
			return 'Invalid Transaction.';
		}	

		$this->theme->set('o', $Transaction->find($id));
		
		return $this->theme->display('admin.billing.transactionedit');
	}

	public function action_delete($id = 0)
	{
		if (!Transaction::find($id))
		{
			return FALSE;
		}

		// remove the transaction.
		$deleted = Transaction::find($id)->delete();

		return Redirect::to('admin/billing/transactions')->with('status', 'deleted');

	}


}

