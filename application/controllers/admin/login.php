<?php

class Admin_Login_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('ajax');
	}

	public function action_index()
	{
		if (Input::has('login'))
 			{
	 			$credentials = array(
	 				'username' => Input::get('username'),
	 				'password' => Input::get('password'),
	 				'remember' => Input::get('remember'),
	 				'roles'	   => array('admin', 'staff')
	 			);

 			try
 			{
 				$result = Auth::attempt($credentials);

 			
 				$redirect_to = 'admin/home';
 				

 				return Redirect::to($redirect_to);
 			}

 			catch (Exception $e)
 			{
 				$error = $e->getMessage();
 				$this->theme->set('error', $error);
 			}

 		}

 		$this->theme->display('admin.login');
	}

}

