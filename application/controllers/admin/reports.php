<?php

class Admin_Reports_Controller extends Admin_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_invoices()
	{	
		$do = Input::get('do', NULL);

		if ($do)
		{	
			$id = Input::get('invoice');

			if ($do == 'delete')
			{
				SellerInvoice::find($id)->delete();
				return Redirect::back()->with('ActionMessage', 'Invoice '.$id.' has been succesfully deleted.');
			}

			if ($do == 'markAsPaid')
			{
				SellerInvoice::find($id)->setPaidStatus();
				return Redirect::back()->with('ActionMessage', 'Invoice '.$id.' has been marked as paid.');
			}

			if ($do == 'markAsUnpaid')
			{
				SellerInvoice::find($id)->setUnpaidStatus();
				return Redirect::back()->with('ActionMessage', 'Invoice '.$id.' has been marked as unpaid.');
			}
		}

		$invoice = new App\SellerInvoice;

		if (Input::has('orderId'))
		{
			$invoice = $invoice->where('order_id', '=', Input::get('orderId'));
		}

		if (Input::has('sellerId'))
		{
			$invoice = $invoice->where('seller_id', '=', Input::get('sellerId'));
		}
		
		if (Input::has('status'))
		{
			$invoice = $invoice->where('status', '=', Input::get('status'));
		}

		// order descendending.
		$invoice = $invoice->order_by('id', 'DESC');

		// fetching the data.
		$result = paginate_manual($invoice->get(), 10);

		$this->theme->set('records', $result);
		return $this->theme->display('admin.sellerinvoices');
	}

	public function action_viewInvoiceData($id)
	{
		$invoice = SellerInvoice::find($id);

		if ($invoice)
		{		
			$this->theme->setLayout('ajax');

			$this->theme->set('invoice', $invoice);
			echo $this->theme->display('admin.invoice_details');

		}
	}

	public function action_salesSummaryByTransactions()
	{
		$this->theme->display('admin.report.sales_summary_by_transaction');
	}

	public function action_seller()
	{
		return $this->theme->display('admin.seller_report');
	}

	public function action_FetchSellerReport()
	{	
		// get the seller id.
		$seller_id = Input::get('sellerId', NULL);

		$user = NULL;
		// check.
		$checkForEmail = User::where('email', '=', $seller_id)->first();

		if ($checkForEmail)
		{
			$user = $checkForEmail;
		}
		else
		{
			$checkBussiness = Billing::where(DB::raw('LOWER(business_name)'), '=', strtolower($seller_id))->first();

			if ($checkBussiness)
			{
				$user = $checkBussiness;
			}
		}

		// Ops invalid seller.
		if (!$user)
		{
			return json_encode(array('status' => 0, 'error' => 'Seller was not found.'));
		}

		// get the seller data collection.
		$seller = App\Seller::forge()->DataCollection($user->id);

		$theme = $this->theme;
		// set ajax layout.
		$theme->setLayout('ajax');

		$theme->set('seller', $seller);

		// fetch the html.
		$html = $theme->fetch('admin.ajax.seller_report');

		return Response::json(array(
			'status' => 1,
			'html'   => $html->render()
		));
		
		

	}


}

