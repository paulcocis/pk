<?php

class Base_Controller extends Controller {


	public $theme;

	public $init_settings = TRUE;


 	public function __construct()
 	{
 		// parent class constructorr.
 		parent::__construct();

 		// initiate the session shopping cart.
 		Cart\Cart::forge();

 	}

	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */
	public function __call($method, $parameters)
	{
		return Response::error('404');
	}


 	protected function init_theme_config()
 	{
		IoC::singleton('Theme', function($theme_name, $theme_path)
		{
  			return $theme = Theme::forge($theme_name, array('theme_path' => $theme_path));
		});
 	}


}