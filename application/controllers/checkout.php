<?php
	
 use Cart\Cart     as Cart;
 use Cart\Order    as CartOrder;	
 use Cart\Checkout as Checkout;
 use Cart\Payment  as Payment;

class Checkout_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home2');

		if (!Checkout::instance()->is_checkout_allowed())
		{
			return Redirect::to('cart');
		}

	}

	public function action_index()
	{	
		$error = FALSE;

		if (!Cart::instance()->countItems())
		{
			return Redirect::to('cart')->with('checkout_empty_cart', TRUE);
		}

		// process the step 1 checkout.
		if (Input::has('checkout'))
		{
			$data   = Input::get();
			$result = Checkout::instance()->step1($data);

			if ($result['status'] == TRUE)
			{
				return Redirect::to('checkout/step2');
			}
			else
			{
				$error = $result['error'];
			}
		}

		$this->theme->set('errors', $error);
		return $this->theme->display('frontend.cart.checkout_step1');
	}

	public function action_step2()
	{
		// --------------------------------------------------------------------------------------

		if (!$this->is_logged())
		{
			//return Redirect::to('checkout/index');
		}

		// --------------------------------------------------------------------------------------

		if (Input::has('checkout'))
		{
			$result = Checkout::instance()->step2(Input::get());

			if ($result)
			{
				return Redirect::to('checkout/step3');
			}
		}

		return $this->theme->display('frontend.cart.checkout_step2');
	}

	public function action_step3()
	{
		// --------------------------------------------------------------------------------------

		if (!$this->is_logged())
		{
			return Redirect::to('checkout/index');
		}

		// --------------------------------------------------------------------------------------

		return $this->theme->display('frontend.cart.checkout_step3');
	}

	public function action_payment()
	{
		$e = NULL;
		// create new cart order instance.
		$order = new CartOrder;

		try
		{
			// process the new order.
			$newOrder = $order->process(Cart::forge()->id);	
		} 

		// catch any errors.
		catch(Exception $e) {}

		// failed to complete the order.
		if ($e instanceof Exception)
		{
			return $e->getMessage();
		}

		// get the gateway type.
		$GatewayType = Input::get('gateway', FALSE);

		if ($GatewayType)
		{	
			// get the payment cart class.
			$payment = new Payment;
	
			// paypal standard payment.		
			if ($GatewayType == 'paypal')
			{
				// get the payment form.
				$paymentForm = $payment->paypal_standard($newOrder->id);
				return $paymentForm;
			}

			// paypal standard payment.		
			if ($GatewayType == 'paypal-express')
			{
				// get the payment form.
				$paymentForm = $payment->paypal_express($newOrder->id, Input::get());

				if ($paymentForm['success'] == 1)
				{	
					// Send payment fonfirmation.
					App\Message\Message::forge('order')->paymentConfirmation($newOrder->id);

					// return to form.
					return Redirect::to('checkout/complete?order='.$paymentForm['order']);
				}
				else
				{
					return Redirect::to('payment/credit-card/?failed=YES&order='.$paymentForm['order'])->with('error', $paymentForm['message']);
				}	
			}
		}
	}

	public function action_complete()
	{
		// get the order.
		$order = App\Order::find(Input::get('order', 0));

		if (!$order)
		{
			return FALSE;
		}

		$this->theme->set('order', $order);
		return $this->theme->display('frontend.cart.complete');
	}


	protected function is_logged()
	{
		return Auth::logged_in();
	}


}