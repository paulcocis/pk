<?php

class Mecanic_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_create_customer_account()
	{
		$data = Input::get();

		$userManager = new UserManager;

		// set the roles.
		$data['roles'] = array(3);

		$data['username'] = str_replace('@', '', $data['email']);

		// attepmt to create the admin.
		$result = $userManager->create($data);

		if ($result['status'])
		{	
			// get user
			$user = User::find($result['id']);

			$user->set_data(array('verified' => 1))->save();

			$credentials = array('username' => $user->email, 'password' => $user->real_password);
			
			try
			{
				$done = Auth::attempt($credentials);	
			}
			
			catch(Exception $e) {}		
		    
		    return json_encode(array('status' => 1, 'error' => NULL));
		}
		
		else
		{
			return json_encode(array('status' => 0, 'error' =>  implode('<br>',$result['errors'])));
		}
	}

	public function action_login()
	{
		$notify  = NULL;
		$success = TRUE;
		$message = 'You have been succesfully logged in.';
		$login   = NULL;

		$user  = Input::get('username');
		$pass  = Input::get('password');

		$credentials = array('username' => $user, 'password' => $pass);
		try
		{
			$login = Auth::attempt($credentials);	
		}
		catch (Exception $notify) {}

		// ops.
		if ($notify instanceof Exception)	
		{	
			$success = FALSE;
			$message = $notify->getMessage();
		}

		$response = array('success' => (int) $success, 'response' => $message, 'authId' => @Auth::user()->id);
		return json_encode($response);
	}
}