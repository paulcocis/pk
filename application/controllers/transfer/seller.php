<?php

class Transfer_Seller_Controller extends Frontend_Controller {

	protected $db = NULL;

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('ajax');

		$this->db = \DB::connection('pk');
	}

	public function action_index()
	{
		$this->_getActiveSellers();
	}

	protected function _getActiveSellers()
	{

		$activeSellers = $this->db->table('users')
							      ->where('role', '=', 'seller')
							      ->where('status', '=', 'active')
							      ->get();
		if ($activeSellers)
		{

			$error_users = array();

			foreach($activeSellers as $seller)
			{	
				// get the seller profile information.
				$sellerProfile = $this->db->table('advertisers')->where('users_id', '=', $seller->id)->first();

				if ($sellerProfile)
				{
					$newSellerData = array(
						'email'      => $seller->email,
						'username'   => str_replace('@','', $seller->email),
						'password'   => str_replace('@','', $seller->email),
						'created_at' => @$sellerProfile->start_date,
						'contact' => array(
								'firstname'     => $seller->first_name,
								'lastname'      => $seller->last_name,
								'address'       => @$sellerProfile->address,
								'address2'      => @$sellerProfile->address2,
								'city'          => @$sellerProfile->city,
								'state'         => @$sellerProfile->state,
								'country'		=> 'US',
								'zipcode'       => @$sellerProfile->zip,							
								'business_name' => @$sellerProfile->name,
								'website_url'   => @$sellerProfile->website,
								'bio_url'       => @$sellerProfile->bio_page,
								'created_at'    => @$sellerProfile->start_date
						),
						'bill' => array(
							'business_name'  => @$sellerProfile->name,
							'comission_rate' => @$sellerProfile->commission_rate,
							'paypal_email'   => @$sellerProfile->paypal_email,
							'created_at'     => @$sellerProfile->start_date
						),

						'roles' => array(2)
					);

					// get user manager instance.
					$userManager = new UserManager;

					$result =  $userManager->create($newSellerData);

					if (!$result['status'])
					{
						$error_users[$seller->id] =  implode('<br>', $result['errors']);
					}					
				}
			}
		}
	}




}