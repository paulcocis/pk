<?php

class Transfer_Customer_Controller extends Frontend_Controller {

	protected $db = NULL;

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('ajax');

		$this->db = \DB::connection('pk');
	}

	public function action_index()
	{
		$this->_getCustomers();
	}

	protected function _getCustomers()
	{

		$customers = $this->db->table('users')
							      ->where('role', '=', 'member')
							      ->where('status', '=', 'active')
							      ->get();
		if ($customers)
		{

			$error_users = array();

			foreach($customers as $customer)
			{	

				if (!User::where('email', '=', $customer->email)->count())
				{
					$newCustomerData = array(
						'email'      => $customer->email,
						'password'   => str_replace('@','', $customer->email),
						'created_at' => @$customer->created_date,
						'contact' => array(
								'firstname'     => '',
								'lastname'      => '',

						),
		
						'roles' => array(3)
					);

					// get user manager instance.
					$userManager = new UserManager;

					$result =  $userManager->create($newCustomerData);

					if (!$result['status'])
					{
						$error_users[$customer->id] =  implode('<br>', $result['errors']);
					}	
				}				
				
			}
		}
	}




}