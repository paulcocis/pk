<?php

class Seller_Orders_Controller extends Seller_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}


	public function action_index()
	{	
		// get seller orders.
		$orders = $this->_getSellerOrders();

		$this->theme->set('orders', $orders);
		$this->theme->display('seller.order.list');
	}

	public function action_view($id = 0)
	{
		// Invalid order ID passed.
		if (!Order::find($id))
		{
			return Redirect::to('admin/orders/view');
		}

		$action = Input::get('action', NULL);

		if ($action)
		{
			if ($action == 'delete')
			{
				Order::find($id)->delete();
				return FALSE;
			}

			if ($action == 'resendConfirmationEmail')
			{
				$cartOrder = new Cart\Order;
				$cartOrder->sendConfirmationEmail($id);
			}
		}

		// retrieve order informations.
		$order = Order::find($id);
		$this->theme->set('o', $order);
		return $this->theme->display('seller.order.edit');
	}


	protected function _getSellerOrders()
	{
		// main query.
		$result = DB::table('orders_products')->join('deals', 'orders_products.parent_product_id', '=', 'deals.id')
											  ->where('deals.seller_id', '=', Auth::user()->id)
											  ->group_by('orders_products.order_id', 'DESC')
											  ->get();

		if (!$result)
		{
			return FALSE;
		}

		$orders = array();

		foreach($result as $row)
		{
			$orders[] = Order::find($row->order_id);
		}

		return $orders;
	}


}

