<?php

class Seller_Home_Controller extends Seller_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{

		// get seller orders.
		$orders = $this->_getSellerOrders();

		$this->theme->set('orders', $orders);

		return $this->theme->display('seller.dashboard');
	}


	protected function _getSellerOrders()
	{
		// main query.
		$result = DB::table('orders_products')->join('deals', 'orders_products.parent_product_id', '=', 'deals.id')
											  ->where('deals.seller_id', '=', Auth::user()->id)
											  ->group_by('orders_products.order_id', 'DESC')
											  ->take(5)
											  ->get();

		if (!$result)
		{
			return FALSE;
		}

		$orders = array();

		foreach($result as $row)
		{
			$orders[] = Order::find($row->order_id);
		}

		return $orders;
	}


}

