<?php

class Seller_Base_Controller extends Base_Controller {

 	public function __construct()
 	{
 		parent::__construct();

 		// initiate the theme.
 		$this->init_theme_config();

 		// initiate the admin theme.
 		$this->theme = Ioc::resolve('Theme', array('default','themes/seller/'));

 		$config = array(
			'base_url'      => URL::base(),
			'timezones'     => getTimezoneList(),	

 		);

 		$this->theme->set('app', Config::get('system'));

 		// set the config.
 		$this->theme->set('cfg', $config);
 		$this->theme->set('assets', $this->theme->getAssetUrl());

 		// sync the helpers with admin theme.
 		HelperEngine::sync($this->theme);

 		if (!URI::is('seller/login/*'))
		{
			$this->filter('before', 'seller_login')->except(array());
		}

 	}

 	public function logged()
 	{
 		return Auth::logged_in(array('admin', 'seller'));
 	}


	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */
	public function __call($method, $parameters)
	{
		return Response::error('404');
	}


 	protected function init_theme_config()
 	{
		IoC::singleton('Theme', function($theme_name, $theme_path)
		{
  			return $theme = Theme::forge($theme_name, array('theme_path' => $theme_path));
		});
 	}


}

