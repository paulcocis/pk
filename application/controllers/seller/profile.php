<?php

class Seller_Profile_Controller extends Seller_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{
		return $this->theme->display('seller.profile');
	}
}

