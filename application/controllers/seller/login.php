<?php

class Seller_Login_Controller extends Seller_Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('ajax');
	}

	public function action_index()
	{
		if (Input::has('login'))
 			{
	 			$credentials = array(
	 				'username' => Input::get('username'),
	 				'password' => Input::get('password'),
	 				'remember' => Input::get('remember'),
	 				'roles'	   => array('admin', 'seller')
	 			);

 			try
 			{
 				$result = Auth::attempt($credentials);

 				$redirect_to = 'seller/home'; 	

 				return Redirect::to($redirect_to);
 			}

 			catch (Exception $e)
 			{
 				$error = $e->getMessage();
 				$this->theme->set('error', $error);
 			}

 		}

 		$this->theme->display('seller.login');
	}

	public function action_signup()
	{
		$notification = FALSE;

		if (Input::has('add'))
		{

			$userManager = new UserManager;

			$data = array_merge(Input::get(), array('roles' => array(2)));

			// mark seller as pending.
			$data['status'] = 'pending';
			$data['username'] = str_replace('@', '',$data['email']);

			// set default country
			$data['contact']['country'] = 'US';

			$result =  $userManager->create($data);

			if (!$result['status'])
			{
				$notification =  implode('<br>', $result['errors']);
			}

			if (!$notification)
			{
				$user = \User::find($result['id']);

				$username = $user->email;
				$password = $user->real_password;
				$email    = $user->email;

				// Send signup informations email.
				App\Message\Message::forge('seller')->signup($user);

				$notification = 'Your account has been succesfully created. Please check your email address.';			
			}


		}

		$this->theme->set('notification', $notification);
		$this->theme->display('seller.signup');
	}

}

