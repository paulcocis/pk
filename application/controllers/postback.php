<?php

class Postback_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_paypal_ipn()
	{
		// get the order.
		$payment = new Cart\Payment;

		// process IPN request.
		$payment->paypal_standard_ipn();
	}




}

