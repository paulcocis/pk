<?php

class Home_Controller extends Frontend_Controller {

	function __construct()
	{
		parent::__construct();
		$this->theme->setLayout('home');
	}

	public function action_index()
	{
		$deal = new Deal;

		$list = $deal->retrieve();

		$this->theme->set('deals', $list);
		$this->theme->display('frontend.index');
	}


	public function action_feautured()
	{
		$this->theme->setLayout('home2');

		$deal = new Deal;

		$list = $deal->retrieveFeautured(TRUE, FALSE);

		$this->theme->set('deals', $list);
		$this->theme->display('frontend.index');		
	}

	public function action_details($id = 0)
	{
		$this->theme->setLayout('home2');

		if (!Deal::find($id))
		{
			return Redirect::to('home/');
		}

		$deal = Deal::find($id);

		// related deals
		$list  = $deal->retrieve();

		$related = NULL;

		if ($list)
		{
			$input = array_rand($list, count($list));

			if ($input)
			{
				foreach($input as $data)
				{
					$related[] = $list[$data];
				}

				$this->theme->set('related', $related);
				// end related deals.		
			}
		
		}

		$this->theme->set('related', $related);


		$this->theme->set('deal', $deal);
		$this->theme->display('frontend.store_details');
	}

	public function action_view($id = 0)
	{
		$this->theme->setLayout('home2');

		if (!Deal::find($id))
		{
			return Redirect::to('home/');
		}

		$deal = Deal::find($id);


		// related deals
		$list  = $deal->retrieve();
		$related = NULL;

		if ($list)
		{
			$input = array_rand($list, count($list));

			if ($input)
			{
				foreach($input as $data)
				{
					$related[] = $list[$data];
				}

				$this->theme->set('related', $related);
				// end related deals.		
			}
		
		}

		$this->theme->set('related', $related);


		$this->theme->set('deal', $deal);
		$this->theme->display('frontend.pk_productdetails');
	}	



	public function action_info()
	{
		$this->theme->setLayout('home2');
		$this->theme->display('frontend.howitworks');
	}


	public function action_about()
	{
		$this->theme->setLayout('home2');
		$this->theme->display('frontend.aboutus');
	}

	public function action_privacy()
	{
		$this->theme->setLayout('home2');
		$this->theme->display('frontend.privacy');		
	}


	public function action_terms()
	{
		$this->theme->setLayout('home2');
		$this->theme->display('frontend.terms');		
	}

	public function action_affiliate()
	{
		$this->theme->setLayout('home2');
		$this->theme->display('frontend.affiliate');		
	}


	public function action_faq()
	{
		$this->theme->setLayout('home2');
		$this->theme->display('frontend.faq');		
	}



}